<?php

/* mod_servicesolusvm_settings.phtml */
class __TwigTemplate_39f18e36b22a18ef39d1cfa09f6b5431042daf0bbd1027780905f11471485f79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'breadcrumbs' => array($this, 'block_breadcrumbs'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ajax")) ? ("layout_blank.phtml") : ("layout_default.phtml")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["active_menu"] = "system";
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_meta_title($context, array $blocks = array())
    {
        echo gettext("SolusVM");
    }

    // line 4
    public function block_breadcrumbs($context, array $blocks = array())
    {
        // line 5
        echo "    <ul>
        <li class=\"firstB\"><a href=\"";
        // line 6
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("/");
        echo "\">";
        echo gettext("Home");
        echo "</a></li>
        <li><a href=\"";
        // line 7
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("system");
        echo "\">";
        echo gettext("Settings");
        echo "</a></li>
        <li class=\"lastB\">";
        // line 8
        echo gettext("SolusVM");
        echo "</li>
    </ul>
";
    }

    // line 12
    public function block_content($context, array $blocks = array())
    {
        // line 13
        $context["params"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "servicesolusvm_cluster_config");
        // line 14
        echo "
";
        // line 15
        if ($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "ipaddress")) {
            // line 16
            echo "    ";
            $context["master_url"] = (((("http://" . $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "ipaddress")) . ":") . (($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "port", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "port"), 5353)) : (5353))) . "/admincp");
        }
        // line 18
        echo "
<div class=\"widget simpleTabs\">

    <ul class=\"tabs\">
        <li><a href=\"#tab-start\">";
        // line 22
        echo gettext("SolusVM");
        echo "</a></li>
        <li><a href=\"#tab-index\" id=\"open-index-tab\">";
        // line 23
        echo gettext("API configuration");
        echo "</a></li>
    </ul>

    <div class=\"tabs_container\">
        <div class=\"fix\"></div>
        
        <div class=\"tab_content nopadding\" id=\"tab-start\">
            <div class=\"help\">
                <img src=\"";
        // line 31
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("bb-modules/servicesolusvm/icon.png");
        echo "\" alt=\"solusvm\" class=\"\" style=\"width: 150px; float: right; margin: 0 5px 0 10px;\"/>
                <h5>";
        // line 32
        echo gettext("Getting started with SolusVM");
        echo "</h5>
                <p>";
        // line 33
        echo gettext("This guide will help you to get started selling SolusVM virtual servers with BoxBilling.");
        echo "</p>
            </div>
            
            <div class=\"body\">    
                <h2>";
        // line 37
        echo gettext("1. Configure your SolusVm server");
        echo "</h2>
                <p>Make sure that your SolusVM master server is properly configured - has defined nodes, plans, free ip addresses, operating system templates.</p>
                ";
        // line 39
        if ((isset($context["master_url"]) ? $context["master_url"] : null)) {
            // line 40
            echo "                <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["master_url"]) ? $context["master_url"] : null), "html", null, true);
            echo "\" class=\"btnIconLeft mr10 mt10\" target=\"_blank\"><span>Open SolusVm admin panel</span></a></p>
                ";
        }
        // line 42
        echo "            </div>
            
            <div class=\"body\">
                <h2>";
        // line 45
        echo gettext("2. Configure API");
        echo "</h2>
                <p><a href=\"\" onclick=\"\$('#open-index-tab').click(); return false;\">Configure SolusVM API</a> and test if BoxBilling can connect successfully. To create an API user login to SolusVM admin area and click Configuration > API Access from the top menu then select Add API User.</p>
            </div>
            
            <div class=\"body\">    
                <h2>";
        // line 50
        echo gettext("3. Create new product category");
        echo "</h2>
                <p>Create <a href=\"";
        // line 51
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("product", array("cat_title" => "VPS", "cat_description" => "Virtual private servers"));
        echo "#tab-new-category\" target=\"_blank\">new product category</a> for VPS products. Category must contain only solusvm products otherwise order form might not work as expected.</p>
            </div>
            
            <div class=\"body list arrowBlue\">
                <h2>";
        // line 55
        echo gettext("4. Create SolusVM products");
        echo "</h2>
                <p>Imagine that product on BoxBilling is SolusVM plan.</p>
                <p><a href=\"";
        // line 57
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("product", array("type" => "solusvm", "title" => "VPS-1"));
        echo "#tab-new\" target=\"_blank\">Create products</a> in newly created category by selecting product type <strong>Solusvm</strong>.</p>
                <p>Define product prices and switch to \"Configuration\" tab:</p>
                <ul>
                    <li>Select virtualization type</li>
                    <li>Select node on which VPS will be created</li>
                    <li>Select plan</li>
                    <li>Define IPs amount for VPS server</li>
                    <li>Provide information about plan to BoxBilling so it can show these values to your clients. (SolusVM API does not provide this information)</li>
                    <li>Provide slider information to be displayed on order form</li>
                </ul>
                <p><a href=\"";
        // line 67
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("order");
        echo "\" target=\"_blank\">Check order page</a> while creating products to see slider changes</p>
            </div>
            
            <div class=\"body\">
                <h2>";
        // line 71
        echo gettext("5. Import data from SolusVM server");
        echo "</h2>
                <p>If you already have successfully connected to master server and have existing clients and server then you can import them using these tools:</p>
                ";
        // line 73
        if ((isset($context["master_url"]) ? $context["master_url"] : null)) {
            // line 74
            echo "                    <a href=\"";
            echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("servicesolusvm/import/clients");
            echo "\" title=\"\" class=\"btnIconLeft mr10 mt10\"><span>5.1 Import clients</span></a>
                    <a href=\"";
            // line 75
            echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("servicesolusvm/import/servers", array("node_id" => 1));
            echo "\" title=\"\" class=\"btnIconLeft mr10 mt10\"><span>5.2 Import servers</span></a>
                ";
        } else {
            // line 77
            echo "                    <p>Configure API before importing data. Refresh this page if you already confugured API.</p>
                ";
        }
        // line 79
        echo "            </div>
        </div>
        
        <div class=\"tab_content nopadding\" id=\"tab-index\">
            <div class=\"help\">
                <h5>";
        // line 84
        echo gettext("Connect to SolusVM master server via API");
        echo "</h5>
                <p>";
        // line 85
        echo gettext("To create an API user login to SolusVM admin area and click Configuration > API Access from the top menu then select Add API User.");
        echo "</p>
            </div>
            
            <div class=\"body\">
                <div class=\"nNote nInformation first\">
                    <p><strong>";
        // line 90
        echo gettext("INFORMATION");
        echo ": </strong>You need to specify IP <strong>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "system_env"), "ip"), "html", null, true);
        echo "</strong> in the SolusVM API user.</p>
                </div>
            </div>
            
            <form method=\"post\" action=\"";
        // line 94
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/servicesolusvm/cluster_config_update");
        echo "\" class=\"mainForm api-form\" data-api-msg=\"";
        echo gettext("Configuration updated");
        echo "\">
                <fieldset>
                    <div class=\"rowElem noborder\">
                        <label>";
        // line 97
        echo gettext("ID");
        echo ":</label>
                        <div class=\"formRight\">
                            <input type=\"text\" name=\"id\" value=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "id"), "html", null, true);
        echo "\" required=\"required\" placeholder=\"Example: t5kGu4EZqAyRqh0ZlN2O\"/>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    
                    <div class=\"rowElem\">
                        <label>";
        // line 105
        echo gettext("Key");
        echo ":</label>
                        <div class=\"formRight\">
                            <input type=\"password\" name=\"key\" value=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "key"), "html", null, true);
        echo "\" required=\"required\" placeholder=\"Example: XzTftPQTxEwRxHmTARIS\"/>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    
                    <div class=\"rowElem\">
                        <label>";
        // line 113
        echo gettext("Master server IP");
        echo ":</label>
                        <div class=\"formRight\">
                            <input type=\"text\" name=\"ipaddress\" value=\"";
        // line 115
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "ipaddress"), "html", null, true);
        echo "\" required=\"required\" placeholder=\"Example: 123.123.123.123\"/>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    
                    <div class=\"rowElem\">
                        <label>";
        // line 121
        echo gettext("Secure connection");
        echo ":</label>
                        <div class=\"formRight\">
                            <input type=\"radio\" name=\"secure\" value=\"1\" ";
        // line 123
        if (($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "secure") == "1")) {
            echo "checked=\"checked\"";
        }
        echo "/><label>";
        echo gettext("Yes");
        echo "</label>
                            <input type=\"radio\" name=\"secure\" value=\"0\" ";
        // line 124
        if ((!$this->getAttribute((isset($context["params"]) ? $context["params"] : null), "secure"))) {
            echo "checked=\"checked\"";
        }
        echo "/><label>";
        echo gettext("No");
        echo "</label>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    
                    <div class=\"rowElem\">
                        <label>";
        // line 130
        echo gettext("Custom port");
        echo ":</label>
                        <div class=\"formRight\">
                            <input type=\"text\" name=\"port\" value=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "port"), "html", null, true);
        echo "\" placeholder=\"";
        echo gettext("Leave blank to use default port 5353 and 5656 for secure connection");
        echo "\"/>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    
                    <input type=\"hidden\" name=\"cluster_id\" value=\"1\" />
                    <input type=\"button\" value=\"2. ";
        // line 138
        echo gettext("Test connection");
        echo "\" class=\"greyishBtn submitForm\" id=\"testconnection\"/>
                    <input type=\"submit\" value=\"1. ";
        // line 139
        echo gettext("Update");
        echo "\" class=\"greyishBtn submitForm\" />
                    
                </fieldset>
            </form>

            <div class=\"fix\"></div>

        </div>

    </div>
</div>
";
    }

    // line 152
    public function block_js($context, array $blocks = array())
    {
        // line 154
        echo "<script type=\"text/javascript\">
\$(function() {
    \$('#testconnection').click(function(){
        bb.post('admin/servicesolusvm/test_connection', null, function(result){
            bb.msg('Successfully connected to SolusVM server');
        });
        return false;
    });
});
</script>
";
    }

    public function getTemplateName()
    {
        return "mod_servicesolusvm_settings.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  338 => 154,  335 => 152,  319 => 139,  315 => 138,  304 => 132,  299 => 130,  286 => 124,  278 => 123,  273 => 121,  264 => 115,  259 => 113,  250 => 107,  245 => 105,  236 => 99,  231 => 97,  223 => 94,  214 => 90,  206 => 85,  202 => 84,  195 => 79,  191 => 77,  186 => 75,  181 => 74,  179 => 73,  174 => 71,  167 => 67,  154 => 57,  149 => 55,  142 => 51,  138 => 50,  130 => 45,  125 => 42,  119 => 40,  117 => 39,  112 => 37,  105 => 33,  101 => 32,  97 => 31,  86 => 23,  82 => 22,  76 => 18,  72 => 16,  70 => 15,  67 => 14,  65 => 13,  62 => 12,  55 => 8,  49 => 7,  43 => 6,  40 => 5,  37 => 4,  31 => 3,  26 => 2,);
    }
}
