<?php

/* mod_servicehosting_config.phtml */
class __TwigTemplate_500fd16acb88264f7a3ae390cbd2bcacd3f33bd8873cff826ef28f8abf5d56db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"help\">
    <h5>";
        // line 2
        echo gettext("Hosting settings");
        echo "</h5>
</div>

<form method=\"post\" action=\"";
        // line 5
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/product/update_config");
        echo "\" class=\"mainForm api-form save\" data-api-msg=\"Hosting settings updated\">
<fieldset>
    <div class=\"rowElem\">
        <label>";
        // line 8
        echo gettext("Server");
        echo ":</label>
        <div class=\"formRight noborder\">
            ";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mf"]) ? $context["mf"] : null), "selectbox", array(0 => "config[server_id]", 1 => $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "servicehosting_server_get_pairs"), 2 => $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "server_id"), 3 => 0, 4 => "Select server"), "method"), "html", null, true);
        echo "
        </div>
        <div class=\"fix\"></div>
    </div>
    <div class=\"rowElem\">
        <label>";
        // line 15
        echo gettext("Hosting plan");
        echo ":</label>
        <div class=\"formRight\">
            ";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mf"]) ? $context["mf"] : null), "selectbox", array(0 => "config[hosting_plan_id]", 1 => $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "servicehosting_hp_get_pairs"), 2 => $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "hosting_plan_id"), 3 => 0, 4 => "Select hosting plan"), "method"), "html", null, true);
        echo "
        </div>
        <div class=\"fix\"></div>
    </div>
    <div class=\"rowElem\">
        <label>";
        // line 22
        echo gettext("Reseller hosting");
        echo ":</label>
        <div class=\"formRight\">
            <input type=\"radio\" name=\"config[reseller]\" value=\"1\"";
        // line 24
        if ($this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "reseller")) {
            echo " checked=\"checked\"";
        }
        echo "/><label>Yes</label>
            <input type=\"radio\" name=\"config[reseller]\" value=\"0\"";
        // line 25
        if ((!$this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "reseller"))) {
            echo " checked=\"checked\"";
        }
        echo " /><label>No</label>
        </div>
        <div class=\"fix\"></div>
    </div>
    <div class=\"rowElem\">
        <label>";
        // line 30
        echo gettext("Free domain registration");
        echo ":</label>
        <div class=\"formRight\">
            <input type=\"radio\" name=\"config[free_domain]\" value=\"1\"";
        // line 32
        if ($this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "free_domain")) {
            echo " checked=\"checked\"";
        }
        echo "/><label>Yes</label>
            <input type=\"radio\" name=\"config[free_domain]\" value=\"0\"";
        // line 33
        if ((!$this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "free_domain"))) {
            echo " checked=\"checked\"";
        }
        echo " /><label>No</label>
        </div>
        <div class=\"fix\"></div>
    </div>
    <div class=\"rowElem free-tlds-row\">
        <label>";
        // line 38
        echo gettext("Select free tlds");
        echo "</label>
        <div class=\"formRight\">
            ";
        // line 40
        $context["tlds"] = $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "serviceDomain_tlds", array(0 => array("allow_register" => 1)), "method");
        // line 41
        echo "            <select name=\"config[free_tlds][]\" multiple=\"multiple\" class=\"multiple\" size=\"";
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["tlds"]) ? $context["tlds"] : null)), "html", null, true);
        echo "\">
                ";
        // line 42
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tlds"]) ? $context["tlds"] : null));
        foreach ($context['_seq'] as $context["id"] => $context["tld"]) {
            // line 43
            echo "                <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "tld"), "html", null, true);
            echo "\" ";
            if (twig_in_filter($this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "tld"), $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "free_tlds"))) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "tld"), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['tld'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "            </select>
        </div>
        <div class=\"fix\"></div>
    </div>
    <div class=\"rowElem\">
        <label>";
        // line 50
        echo gettext("Free domain transfer");
        echo ":</label>
        <div class=\"formRight\">
            <input type=\"radio\" name=\"config[free_transfer]\" value=\"1\"";
        // line 52
        if ($this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "free_transfer")) {
            echo " checked=\"checked\"";
        }
        echo "/><label>Yes</label>
            <input type=\"radio\" name=\"config[free_transfer]\" value=\"0\"";
        // line 53
        if ((!$this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "free_transfer"))) {
            echo " checked=\"checked\"";
        }
        echo " /><label>No</label>
        </div>
        <div class=\"fix\"></div>
    </div>

    <input type=\"submit\" value=\"";
        // line 58
        echo gettext("Update");
        echo "\" class=\"greyishBtn submitForm\" />
</fieldset>
        
<input type=\"hidden\" name=\"id\" value=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id"), "html", null, true);
        echo "\" />
</form>

<div class=\"help\">
    <h5>";
        // line 65
        echo gettext("Hosting plans");
        echo "</h5>
</div>

<table class=\"tableStatic wide\">
    <thead>
        <tr>
            <td>Title</td>
            <td style=\"width:5%\">&nbsp;</td>
        </tr>
    </thead>
    <tbody>
        ";
        // line 76
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "servicehosting_hp_get_pairs"));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["id"] => $context["plan"]) {
            // line 77
            echo "        <tr>
            <td>";
            // line 78
            echo twig_escape_filter($this->env, (isset($context["plan"]) ? $context["plan"] : null), "html", null, true);
            echo "</td>
            <td class=\"actions\"><a class=\"bb-button btn14\" href=\"";
            // line 79
            echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("/servicehosting/plan");
            echo "/";
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
            echo "\"><img src=\"images/icons/dark/pencil.png\" alt=\"\"></a></td>
        </tr>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 82
            echo "        <tr>
            <td colspan=\"2\">";
            // line 83
            echo gettext("The list is empty");
            echo "</td>
        </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['plan'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 86
        echo "    </tbody>
    <tfoot>
        <tr>
            <td colspan=\"2\">
                <a href=\"";
        // line 90
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("servicehosting#tab-new-plan");
        echo "\" title=\"\" class=\"btnIconLeft mr10 mt5\"><img src=\"images/icons/dark/settings2.png\" alt=\"\" class=\"icon\"><span>New hosting plan</span></a>
            </td>
        </tr>
    </tfoot>
</table>

<div class=\"help\">
    <h5>";
        // line 97
        echo gettext("Servers");
        echo "</h5>
</div>

<table class=\"tableStatic wide\">
    <thead>
        <tr>
            <td>Title</td>
            <td style=\"width:5%\">&nbsp;</td>
        </tr>
    </thead>
    <tbody>
        ";
        // line 108
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "servicehosting_server_get_pairs"));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["id"] => $context["server"]) {
            // line 109
            echo "        <tr>
            <td>";
            // line 110
            echo twig_escape_filter($this->env, (isset($context["server"]) ? $context["server"] : null), "html", null, true);
            echo "</td>
            <td class=\"actions\"><a class=\"bb-button btn14\" href=\"";
            // line 111
            echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("/servicehosting/server");
            echo "/";
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
            echo "\"><img src=\"images/icons/dark/pencil.png\" alt=\"\"></a></td>
        </tr>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 114
            echo "        <tr>
            <td colspan=\"7\">";
            // line 115
            echo gettext("The list is empty");
            echo "</td>
        </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['server'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 118
        echo "    </tbody>
    <tfoot>
        <tr>
            <td colspan=\"2\">
                <a href=\"";
        // line 122
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("servicehosting#tab-new-server");
        echo "\" title=\"\" class=\"btnIconLeft mr10 mt5\"><img src=\"images/icons/dark/computer.png\" alt=\"\" class=\"icon\"><span>New server</span></a>
            </td>
        </tr>
    </tfoot>
</table>

<script>
    var free_domain_radios = \$('input:radio[name=\"config[free_domain]\"]');
    var freeTldsRow = \$('.free-tlds-row');

    free_domain_radios.on('click', function(){
        if (\$(this).val() == 1){
            freeTldsRow.fadeIn('slow');
        }
        if (\$(this).val() == 0){
            freeTldsRow.fadeOut('slow');
        }
    });

    if (free_domain_radios.filter('[value=0]:checked').length > 0){
        freeTldsRow.hide();
    }

</script>
";
    }

    public function getTemplateName()
    {
        return "mod_servicehosting_config.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  292 => 122,  286 => 118,  277 => 115,  274 => 114,  264 => 111,  260 => 110,  257 => 109,  252 => 108,  238 => 97,  228 => 90,  222 => 86,  213 => 83,  210 => 82,  200 => 79,  196 => 78,  193 => 77,  188 => 76,  174 => 65,  167 => 61,  161 => 58,  151 => 53,  145 => 52,  140 => 50,  133 => 45,  118 => 43,  114 => 42,  109 => 41,  107 => 40,  102 => 38,  92 => 33,  86 => 32,  81 => 30,  71 => 25,  65 => 24,  60 => 22,  52 => 17,  47 => 15,  39 => 10,  34 => 8,  28 => 5,  22 => 2,  19 => 1,);
    }
}
