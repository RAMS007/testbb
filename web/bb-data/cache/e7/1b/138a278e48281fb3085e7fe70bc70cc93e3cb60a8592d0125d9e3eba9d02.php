<?php

/* mod_orderbutton_addons.phtml */
class __TwigTemplate_e71b138a278e48281fb3085e7fe70bc70cc93e3cb60a8592d0125d9e3eba9d02 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "addons")) > 0)) {
            // line 2
            echo "<hr/>
<section>
    <header>
        <h2>";
            // line 5
            echo gettext("Addons & extras");
            echo "</h2>
    </header>
</section>
<div class=\"row-fluid\">
        <table class=\"table table-condensed\">
            <tbody>
            ";
            // line 11
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "addons"));
            foreach ($context['_seq'] as $context["_key"] => $context["addon"]) {
                // line 12
                echo "            <label for=\"addon_";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                echo "\">
            <tr>
                <td>
                    <input type=\"hidden\" name=\"addons[";
                // line 15
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                echo "][selected]\" value=\"0\">
                    <input type=\"checkbox\" name=\"addons[";
                // line 16
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                echo "][selected]\" value=\"1\" id=\"addon_";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                echo "\">
                </td>

                <td ";
                // line 19
                if ((!$this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "icon_url"))) {
                    echo "style=\"display: none\"";
                }
                echo ">
                    <label for=\"addon_";
                // line 20
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "icon_url"), "html", null, true);
                echo "\" alt=\"\" width=\"36\"/></label>
                </td>

                <td>
                    <label for=\"addon_";
                // line 24
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                echo "\"><h3>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "title"), "html", null, true);
                echo "</h3></label>
                    ";
                // line 25
                echo twig_bbmd_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "description"));
                echo "
                </td>

                <td class=\"currency\">
                    <label for=\"addon_";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                echo "\">
                    ";
                // line 30
                if (($this->getAttribute($this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "pricing"), "type") === "recurrent")) {
                    // line 31
                    echo "                        ";
                    $context["periods"] = $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "system_periods");
                    // line 32
                    echo "                        <select name=\"addons[";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                    echo "][period]\" rel=\"addon-periods-";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "id"), "html", null, true);
                    echo "\">
                        ";
                    // line 33
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "pricing"), "recurrent"));
                    foreach ($context['_seq'] as $context["code"] => $context["prices"]) {
                        // line 34
                        echo "                        ";
                        if ($this->getAttribute((isset($context["prices"]) ? $context["prices"] : null), "enabled")) {
                            // line 35
                            echo "                        <option value=\"";
                            echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
                            echo "\">";
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["periods"]) ? $context["periods"] : null), (isset($context["code"]) ? $context["code"] : null), array(), "array"), "html", null, true);
                            echo " ";
                            echo twig_money_convert($this->env, $this->getAttribute((isset($context["prices"]) ? $context["prices"] : null), "price"));
                            echo " ";
                            if (($this->getAttribute((isset($context["prices"]) ? $context["prices"] : null), "setup") != "0.00")) {
                                echo gettext("Setup:");
                                echo " ";
                                echo twig_money_convert($this->env, $this->getAttribute((isset($context["prices"]) ? $context["prices"] : null), "setup"));
                            }
                            echo "</option>
                        ";
                        }
                        // line 37
                        echo "                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['code'], $context['prices'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 38
                    echo "                    </select>
                    ";
                }
                // line 40
                echo "
                    ";
                // line 41
                if (($this->getAttribute($this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "pricing"), "type") === "once")) {
                    // line 42
                    echo "                    ";
                    echo twig_money_convert($this->env, ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "pricing"), "once"), "price") + $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "pricing"), "once"), "setup")));
                    echo "
                    ";
                }
                // line 44
                echo "
                    ";
                // line 45
                if (($this->getAttribute($this->getAttribute((isset($context["addon"]) ? $context["addon"] : null), "pricing"), "type") === "free")) {
                    // line 46
                    echo "                    ";
                    echo twig_money_convert($this->env, 0);
                    echo "
                    ";
                }
                // line 48
                echo "                    </label>
                </td>
            </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['addon'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 52
            echo "            </tbody>
        </table>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "mod_orderbutton_addons.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  157 => 48,  151 => 46,  149 => 45,  146 => 44,  140 => 42,  138 => 41,  135 => 40,  131 => 38,  125 => 37,  106 => 34,  102 => 33,  95 => 32,  92 => 31,  90 => 30,  86 => 29,  79 => 25,  73 => 24,  64 => 20,  58 => 19,  50 => 16,  46 => 15,  39 => 12,  35 => 11,  292 => 75,  288 => 74,  285 => 73,  278 => 71,  273 => 68,  271 => 67,  266 => 66,  260 => 64,  255 => 63,  251 => 62,  240 => 61,  236 => 59,  227 => 56,  224 => 55,  219 => 54,  214 => 53,  210 => 52,  204 => 51,  201 => 50,  196 => 49,  192 => 47,  182 => 44,  177 => 43,  173 => 42,  166 => 52,  161 => 40,  156 => 37,  141 => 35,  137 => 34,  132 => 33,  128 => 31,  123 => 30,  117 => 29,  114 => 28,  109 => 35,  105 => 26,  98 => 25,  91 => 24,  78 => 23,  76 => 22,  72 => 20,  68 => 18,  65 => 17,  52 => 16,  48 => 14,  45 => 13,  41 => 12,  37 => 10,  30 => 6,  26 => 5,  23 => 3,  21 => 2,  19 => 1,);
    }
}
