<?php

/* mod_orderbutton_client.phtml */
class __TwigTemplate_b6b2bf763a9a4fa70fbdae18cd669e73fd849f93744301a91f1a5a79e01b655a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["mf"] = $this->env->loadTemplate("macro_functions.phtml");
        // line 2
        echo "<div class=\"accordion-group\" id=\"register-or-login\">
<div class=\"accordion-heading\">
    <a class=\"accordion-toggle\" href=\"#register\" data-parent=\"#accordion1\" data-toggle=\"collapse\"><span class=\"awe-user\"></span> ";
        // line 4
        echo gettext("Login or Register");
        echo " </a>
</div>
<div id=\"register\" class=\"accordion-body collapse ";
        // line 6
        if (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "checkout") && (!(isset($context["client"]) ? $context["client"] : null)))) {
            echo "in";
        }
        echo "\">


<div class=\"accordion-inner\">
<div class=\"tab-content\">
    <div class=\"tab-pane active\" id=\"login-tab\">
        <form method=\"post\" action=\"\" id=\"client-login\">
            <fieldset>
                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"email\">";
        // line 15
        echo gettext("Email Address");
        echo "</label>
                    <div class=\"controls\">
                        <input id=\"icon\" type=\"email\" class=\"span6\" placeholder=\"";
        // line 17
        echo gettext("Your email address");
        echo "\" name=\"email\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "email"), "html", null, true);
        echo "\" required=\"required\" data-validation-required-message=\"";
        echo gettext("You must fill in your email.");
        echo "\">
                        <div class=\"help-block\"></div>
                    </div>
                </div>
                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"password\">";
        // line 22
        echo gettext("Password");
        echo "</label>
                    <div class=\"controls\">
                        <input id=\"password\" type=\"password\" class=\"span6\" placeholder=\"";
        // line 24
        echo gettext("Password");
        echo "\" name=\"password\" required=\"required\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "password"), "html", null, true);
        echo "\" data-validation-required-message=\"";
        echo gettext("You must fill in your password.");
        echo "\">
                        <label class=\"checkbox\">
                            <input id=\"optionsCheckbox\" type=\"checkbox\" name=\"remember\" value=\"1\"> ";
        // line 26
        echo gettext("Remember me");
        // line 27
        echo "                        </label>
                        <div class=\"help-block\"></div>
                    </div>
                </div>
                <div class=\"form-actions\">
                    <div class=\"controls\">
                        <button class=\"btn btn-large btn-primary btn-alt span5\" type=\"submit\"><span class=\"awe-signin\"></span> ";
        // line 33
        echo gettext("Log in");
        echo "</button>
                        <div class=\"divider\"></div>
                        <button class=\"btn btn-alt btn-inverse offset1\" type=\"button\" style=\"position:relative; top:10px\" onclick=\"\$('#register-tab').show();\$('#login-tab').hide();\"><span class=\"awe-user\"> </span> ";
        // line 35
        echo gettext("Or register");
        echo "</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
    <div class=\"tab-pane\" id=\"register-tab\">
        <form action=\"\" method=\"post\" id=\"create-profile\">
            ";
        // line 43
        $context["r"] = $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "client_required");
        // line 44
        echo "            <fieldset>
                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"reg-email\">";
        // line 46
        echo gettext("Email Address");
        echo "</label>
                    <div class=\"controls\">
                        <input type=\"email\" name=\"email\" class=\"span6\" value=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "email"), "html", null, true);
        echo "\" required=\"required\" id=\"reg-email\">
                        <p class=\"help-block\"></p>
                    </div>
                </div>

                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"first-name\">";
        // line 54
        echo gettext("First Name");
        echo "</label>
                    <div class=\"controls\">
                        <input type=\"text\" name=\"first_name\" class=\"span6\" id=\"first-name\" value=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "first_name"), "html", null, true);
        echo "\" required=\"required\">
                        <p class=\"help-block\"></p>
                    </div>
                </div>

                ";
        // line 61
        if (twig_in_filter("last_name", (isset($context["r"]) ? $context["r"] : null))) {
            // line 62
            echo "                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"last_name\">";
            // line 63
            echo gettext("Last Name");
            echo "</label>
                    <div class=\"controls\">
                        <input type=\"text\" name=\"last_name\" class=\"span6\" id=\"last_name\" value=\"";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "last_name"), "html", null, true);
            echo "\" required=\"required\">
                        <p class=\"help-block\"></p>
                    </div>
                </div>
                ";
        }
        // line 70
        echo "

                ";
        // line 72
        if (twig_in_filter("company", (isset($context["r"]) ? $context["r"] : null))) {
            // line 73
            echo "                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"company\">";
            // line 74
            echo gettext("Company");
            echo "</label>
                    <div class=\"controls\">
                        <input type=\"text\" name=\"company\" class=\"span6\" id=\"company\" value=\"";
            // line 76
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "company"), "html", null, true);
            echo "\" required=\"required\">
                        <p class=\"help-block\"></p>
                    </div>
                </div>
                ";
        }
        // line 81
        echo "
                ";
        // line 82
        if (twig_in_filter("birthday", (isset($context["r"]) ? $context["r"] : null))) {
            // line 83
            echo "                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"birthday\">";
            // line 84
            echo gettext("Birthday");
            echo "</label>
                    <div class=\"controls\">
                        <input type=\"text\"  class=\"span6\" name=\"birthday\" id=\"birthday\" value=\"\">
                        <p class=\"help-block\"></p>
                    </div>
                </div>
                ";
        }
        // line 91
        echo "
                ";
        // line 92
        if (twig_in_filter("gender", (isset($context["r"]) ? $context["r"] : null))) {
            // line 93
            echo "                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"gender\">";
            // line 94
            echo gettext("You are");
            echo "</label>
                    <div class=\"controls\">
                        <select name=\"gender\"  class=\"span6\" id=\"gender\">
                            <option value=\"male\">Male</option>
                            <option value=\"female\">Female</option>
                        </select>
                        <p class=\"help-block\"></p>
                    </div>
                </div>
                ";
        }
        // line 104
        echo "
                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"reg-password\">";
        // line 106
        echo gettext("Password");
        echo "</label>
                    <div class=\"controls\">
                        <input type=\"password\" name=\"password\" class=\"span6\" value=\"\" required=\"required\" id=\"reg-password\">
                        <p class=\"help-block\"></p>
                    </div>
                </div>

                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"password-confirm\">";
        // line 114
        echo gettext("Password confirm");
        echo "</label>
                    <div class=\"controls\">
                        <input type=\"password\" name=\"password_confirm\" class=\"span6\" name=\"password-confirm\" value=\"\" required=\"required\">
                        <p class=\"help-block\"></p>
                    </div>
                </div>



                ";
        // line 123
        if (twig_in_filter("address_1", (isset($context["r"]) ? $context["r"] : null))) {
            // line 124
            echo "                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"address_1\">";
            // line 125
            echo gettext("Address");
            echo "</label>
                    <div class=\"controls\">
                        <input type=\"text\" name=\"address_1\" class=\"span6\" id=\"address_1\" value=\"";
            // line 127
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "address_1"), "html", null, true);
            echo "\">
                        <p class=\"help-block\"></p>
                    </div>
                </div>
                ";
        }
        // line 132
        echo "
                ";
        // line 133
        if (twig_in_filter("address_2", (isset($context["r"]) ? $context["r"] : null))) {
            // line 134
            echo "                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"address_2\">";
            // line 135
            echo gettext("Address 2");
            echo "</label>
                    <div class=\"controls\">
                        <input type=\"text\" name=\"address_2\" class=\"span6\" id=\"address_2\" value=\"";
            // line 137
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "address_2"), "html", null, true);
            echo "\">
                        <p class=\"help-block\"></p>
                    </div>
                </div>
                ";
        }
        // line 142
        echo "
                ";
        // line 143
        if (twig_in_filter("city", (isset($context["r"]) ? $context["r"] : null))) {
            // line 144
            echo "                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"city\">";
            // line 145
            echo gettext("City");
            echo "</label>
                    <div class=\"controls\">
                        <input type=\"text\" name=\"city\" class=\"span6\" id=\"city\" value=\"";
            // line 147
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "city"), "html", null, true);
            echo "\">
                        <p class=\"help-block\"></p>
                    </div>
                </div>
                ";
        }
        // line 152
        echo "
                ";
        // line 153
        if (twig_in_filter("country", (isset($context["r"]) ? $context["r"] : null))) {
            // line 154
            echo "                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"country\">";
            // line 155
            echo gettext("Country");
            echo "</label>
                    <div class=\"controls\">
                        <select name=\"country\" class=\"span6\" required=\"required\">
                            <option value=\"\">";
            // line 158
            echo gettext("-- Select country --");
            echo "</option>
                            ";
            // line 159
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "system_countries"));
            foreach ($context['_seq'] as $context["val"] => $context["label"]) {
                // line 160
                echo "                            <option value=\"";
                echo twig_escape_filter($this->env, (isset($context["val"]) ? $context["val"] : null), "html", null, true);
                echo "\" label=\"";
                echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : null));
                echo "\">";
                echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : null));
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['val'], $context['label'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 162
            echo "                        </select>
                        <p class=\"help-block\"></p>
                    </div>
                </div>
                ";
        }
        // line 167
        echo "
                ";
        // line 168
        if (twig_in_filter("state", (isset($context["r"]) ? $context["r"] : null))) {
            // line 169
            echo "                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"state\">";
            // line 170
            echo gettext("State");
            echo "</label>
                    <div class=\"controls\">
                        ";
            // line 173
            echo "                        <input type=\"text\" name=\"state\" class=\"span6\" id=\"state\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "state"), "html", null, true);
            echo "\" />
                        <p class=\"help-block\"></p>
                    </div>
                </div>
                ";
        }
        // line 178
        echo "
                ";
        // line 179
        if (twig_in_filter("postcode", (isset($context["r"]) ? $context["r"] : null))) {
            // line 180
            echo "                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"postcode\">";
            // line 181
            echo gettext("Zip/Postal Code");
            echo "</label>
                    <div class=\"controls\">
                        <input type=\"text\" name=\"postcode\" class=\"span6\" id=\"postcode\" value=\"";
            // line 183
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "postcode"), "html", null, true);
            echo "\">
                        <p class=\"help-block\"></p>
                    </div>
                </div>
                ";
        }
        // line 188
        echo "
                ";
        // line 189
        if (twig_in_filter("phone", (isset($context["r"]) ? $context["r"] : null))) {
            // line 190
            echo "                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"phone_cc\">";
            // line 191
            echo gettext("Phone Number");
            echo "</label>
                    <div class=\"controls controls-row\">
                        <input type=\"text\" name=\"phone_cc\" value=\"";
            // line 193
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "phone_cc"), "html", null, true);
            echo "\" style=\"width:20%\">
                        <input type=\"text\" name=\"phone\" value=\"";
            // line 194
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "phone"), "html", null, true);
            echo "\" style=\"width: 70%\">
                        <p class=\"help-block\"></p>
                    </div>
                </div>
                ";
        }
        // line 199
        echo "
                ";
        // line 200
        $context["custom_fields"] = $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "client_custom_fields");
        // line 201
        echo "                ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["custom_fields"]) ? $context["custom_fields"] : null));
        foreach ($context['_seq'] as $context["field_name"] => $context["field"]) {
            // line 202
            echo "                ";
            if ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "active")) {
                // line 203
                echo "                <div class=\"control-group\">
                    <label class=\"control-label\" for=\"";
                // line 204
                echo twig_escape_filter($this->env, (isset($context["field_name"]) ? $context["field_name"] : null), "html", null, true);
                echo "\">";
                if ((!twig_test_empty($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "title")))) {
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "title"), "html", null, true);
                } else {
                    echo " ";
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, (isset($context["field_name"]) ? $context["field_name"] : null)), "html", null, true);
                    echo " ";
                }
                echo "</label>
                    <div class=\"controls\">
                        <input type=\"text\" name=\"";
                // line 206
                echo twig_escape_filter($this->env, (isset($context["field_name"]) ? $context["field_name"] : null), "html", null, true);
                echo "\" id=\"";
                echo twig_escape_filter($this->env, (isset($context["field_name"]) ? $context["field_name"] : null), "html", null, true);
                echo "\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), (isset($context["field_name"]) ? $context["field_name"] : null)), "html", null, true);
                echo "\" ";
                if ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "required")) {
                    echo "required=\"required\"";
                }
                echo ">
                        <p class=\"help-block\"></p>
                    </div>
                </div>
                ";
            }
            // line 211
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['field_name'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 212
        echo "
                ";
        // line 213
        echo $context["mf"]->getrecaptcha();
        echo "

                <div class=\"form-actions\">

                    <div class=\"controls\">
                        <button class=\"btn btn-large btn-primary btn-alt span5\" type=\"submit\"><span class=\"awe-user\"></span> ";
        // line 218
        echo gettext("Register");
        echo "</button>
                        <div class=\"divider\"></div>
                        <button class=\"btn btn-alt btn-inverse offset1\" type=\"button\" style=\"position:relative; top:10px\" onclick=\"\$('#register-tab').hide();\$('#login-tab').show();\">
                            <span class=\"awe-signin\"> </span> ";
        // line 221
        echo gettext("Or Login");
        // line 222
        echo "                        </button>
                    </div>
                </div>
            </fieldset>

        </form>
    </div>
</div>
</div>
</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "mod_orderbutton_client.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  480 => 222,  478 => 221,  472 => 218,  464 => 213,  461 => 212,  455 => 211,  439 => 206,  426 => 204,  423 => 203,  420 => 202,  415 => 201,  413 => 200,  410 => 199,  402 => 194,  398 => 193,  393 => 191,  390 => 190,  388 => 189,  385 => 188,  377 => 183,  372 => 181,  369 => 180,  367 => 179,  364 => 178,  355 => 173,  350 => 170,  347 => 169,  345 => 168,  342 => 167,  335 => 162,  322 => 160,  318 => 159,  314 => 158,  308 => 155,  305 => 154,  303 => 153,  300 => 152,  292 => 147,  287 => 145,  284 => 144,  282 => 143,  279 => 142,  271 => 137,  266 => 135,  263 => 134,  261 => 133,  258 => 132,  250 => 127,  245 => 125,  242 => 124,  240 => 123,  228 => 114,  217 => 106,  200 => 94,  197 => 93,  195 => 92,  192 => 91,  182 => 84,  179 => 83,  177 => 82,  174 => 81,  166 => 76,  161 => 74,  156 => 72,  139 => 63,  134 => 61,  126 => 56,  107 => 46,  103 => 44,  101 => 43,  85 => 33,  66 => 24,  171 => 53,  163 => 48,  159 => 47,  153 => 44,  144 => 65,  141 => 40,  136 => 62,  131 => 37,  128 => 36,  112 => 48,  106 => 28,  100 => 26,  96 => 24,  90 => 35,  78 => 21,  75 => 26,  71 => 19,  68 => 18,  63 => 16,  58 => 14,  54 => 13,  40 => 9,  32 => 7,  30 => 6,  21 => 2,  62 => 14,  51 => 12,  48 => 10,  44 => 15,  36 => 6,  24 => 3,  22 => 2,  290 => 179,  278 => 170,  253 => 148,  236 => 138,  232 => 137,  213 => 104,  186 => 97,  170 => 84,  164 => 80,  158 => 73,  155 => 45,  152 => 70,  150 => 43,  138 => 39,  132 => 61,  130 => 60,  127 => 59,  124 => 35,  121 => 54,  119 => 33,  116 => 32,  114 => 54,  111 => 53,  109 => 52,  104 => 49,  77 => 27,  73 => 19,  69 => 18,  65 => 17,  61 => 22,  57 => 15,  53 => 14,  49 => 17,  45 => 12,  38 => 8,  34 => 7,  29 => 4,  27 => 4,  25 => 4,  19 => 1,);
    }
}
