<?php

/* mod_serviceopenvpn_order_form.phtml */
class __TwigTemplate_b60be9e6a236e80451778cc6797263a55f9ecf882642ae4ebd999d2f5a4ee062 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["periods"] = $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "system_periods");
        // line 2
        $context["pricing"] = $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "pricing");
        // line 3
        echo "
<section>
    <div class=\"well\">
        <strong>";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "title"), "html", null, true);
        echo "</strong>
        ";
        // line 7
        echo twig_bbmd_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "description"));
        echo "

        ";
        // line 9
        if (($this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "pricing"), "type") == "recurrent")) {
            // line 10
            echo "        <select name=\"period\" id=\"period-selector\">
            ";
            // line 11
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "pricing"), "recurrent"));
            foreach ($context['_seq'] as $context["code"] => $context["prices"]) {
                // line 12
                echo "            ";
                if ($this->getAttribute((isset($context["prices"]) ? $context["prices"] : null), "enabled")) {
                    // line 13
                    echo "            <option value=\"";
                    echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
                    echo "\" data-bb-price=\"";
                    echo twig_money_convert($this->env, $this->getAttribute((isset($context["prices"]) ? $context["prices"] : null), "price"));
                    echo "\" name=\"period\">";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["periods"]) ? $context["periods"] : null), (isset($context["code"]) ? $context["code"] : null), array(), "array"), "html", null, true);
                    echo " - ";
                    echo twig_money_convert($this->env, $this->getAttribute((isset($context["prices"]) ? $context["prices"] : null), "price"));
                    echo "</option>
            ";
                }
                // line 15
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['code'], $context['prices'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 16
            echo "        </select>
        ";
        } elseif (($this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "pricing"), "type") == "free")) {
            // line 18
            echo "        <span class=\"label label-info\">";
            echo gettext("FREE");
            echo "</span>
        ";
        } else {
            // line 20
            echo "        <span class=\"label label-info\">";
            echo twig_money_convert($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "pricing"), "once"), "price"));
            echo "</span>
        ";
        }
        // line 22
        echo "        <div class=\"row-fluid\">
  <!--      <label class=\"control-label\">";
        // line 23
        echo gettext("Vps hostname");
        echo " </label>
            <div class=\"controls\">
                <input type=\"text\" name=\"hostname\" value=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "hostname"), "html", null, true);
        echo "\" placeholder=\"";
        echo gettext("mydomain.com");
        echo "\">
            </div>
        </div> -->
        bb-modules/Serviceopenvpn/html_client/mod_serviceopenvpn_order_form.phtml:28
        bb-modules/Serviceopenvpn/html_admin/mod_serviceopenvpn_order.phtml:8
        \\Box\\Mod\\Serviceopenvpn\\Service::validateOrderData
 <!--       <div class=\"row-fluid\">
        <label>";
        // line 32
        echo gettext("OS template");
        echo " </label>
            <div class=\"controls\">
            <select name=\"template\">
                ";
        // line 35
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "serviceopenvpn_get_templates", array(0 => array("type" => $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "vtype"))), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["t"]) {
            // line 36
            echo "                <option value=\"";
            echo twig_escape_filter($this->env, (isset($context["t"]) ? $context["t"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, (isset($context["t"]) ? $context["t"] : null)), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['t'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "            </select>
            </div>  -->
        </div>
    </div>

    <input type=\"hidden\" name=\"single\" value=\"1\" />
    <input type=\"hidden\" name=\"id\" value=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id"), "html", null, true);
        echo "\" />
</section>


<script type=\"text/javascript\">
\$(function() {
    \$('#period-selector').change(function(){
        \$('.period').hide();
        \$('.period.' + \$(this).val()).show();
    }).trigger('change');

    \$('.addon-period-selector').change(function(){
        var r = \$(this).attr('rel');
        \$('#' + r + ' span').hide();
        \$('#' + r + ' span.' + \$(this).val()).fadeIn();
    }).trigger('change');
});
</script>
";
    }

    public function getTemplateName()
    {
        return "mod_serviceopenvpn_order_form.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 44,  124 => 38,  113 => 36,  109 => 35,  103 => 32,  91 => 25,  86 => 23,  83 => 22,  77 => 20,  71 => 18,  67 => 16,  61 => 15,  49 => 13,  46 => 12,  42 => 11,  39 => 10,  37 => 9,  32 => 7,  28 => 6,  23 => 3,  21 => 2,  19 => 1,);
    }
}
