<?php

/* mod_servicecustom_config.phtml */
class __TwigTemplate_a6a2086588b759d896e58da26e0d23128d468bd134e3aa28de39b1d8728a0fc5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"help\">
    <h5>";
        // line 2
        echo gettext("Stock control settings");
        echo "</h5>
</div>

<form method=\"post\" action=\"";
        // line 5
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/product/update");
        echo "\" class=\"mainForm api-form save\" data-api-msg=\"Settings updated\">
<fieldset>
    <div class=\"rowElem noborder\">
        <label>";
        // line 8
        echo gettext("Enable stock control");
        echo ":</label>
        <div class=\"formRight\">
            <input type=\"radio\" name=\"stock_control\" value=\"1\"";
        // line 10
        if ($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "stock_control")) {
            echo " checked=\"checked\"";
        }
        echo "/><label>Yes</label>
            <input type=\"radio\" name=\"stock_control\" value=\"0\"";
        // line 11
        if ((!$this->getAttribute((isset($context["product"]) ? $context["product"] : null), "stock_control"))) {
            echo " checked=\"checked\"";
        }
        echo " /><label>No</label>
        </div>
        <div class=\"fix\"></div>
    </div>
    
    <div class=\"rowElem\">
        <label>";
        // line 17
        echo gettext("Allow quantity select on order form");
        echo ":</label>
        <div class=\"formRight\">
            <input type=\"radio\" name=\"allow_quantity_select\" value=\"1\"";
        // line 19
        if ($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "allow_quantity_select")) {
            echo " checked=\"checked\"";
        }
        echo "/><label>Yes</label>
            <input type=\"radio\" name=\"allow_quantity_select\" value=\"0\"";
        // line 20
        if ((!$this->getAttribute((isset($context["product"]) ? $context["product"] : null), "allow_quantity_select"))) {
            echo " checked=\"checked\"";
        }
        echo " /><label>No</label>
        </div>
        <div class=\"fix\"></div>
    </div>

    <div class=\"rowElem\">
        <label>";
        // line 26
        echo gettext("Quantity in stock");
        echo ":</label>
        <div class=\"formRight\">
            <input type=\"text\" name=\"quantity_in_stock\" value=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "quantity_in_stock"), "html", null, true);
        echo "\">
        </div>
        <div class=\"fix\"></div>
    </div>
    
    <input type=\"submit\" value=\"";
        // line 33
        echo gettext("Update");
        echo "\" class=\"greyishBtn submitForm\" />
</fieldset>
    
<input type=\"hidden\" name=\"id\" value=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id"), "html", null, true);
        echo "\" />
</form>

<div class=\"help\">
    <h5>";
        // line 40
        echo gettext("Plugin");
        echo "</h5>
</div>

<form method=\"post\" action=\"";
        // line 43
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/product/update");
        echo "\" class=\"mainForm api-form save\" data-api-msg=\"Settings updated\">
<fieldset>
    <div class=\"rowElem noborder\">
        <label>";
        // line 46
        echo gettext("Plugin name");
        echo ":</label>
        <div class=\"formRight\">
            <input type=\"text\" name=\"plugin\" value=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "plugin"), "html", null, true);
        echo "\"/>
        </div>
        <div class=\"fix\"></div>
    </div>
    
    <input type=\"submit\" value=\"";
        // line 53
        echo gettext("Update");
        echo "\" class=\"greyishBtn submitForm\" />
</fieldset>
    
<input type=\"hidden\" name=\"id\" value=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id"), "html", null, true);
        echo "\" />
</form>

<form method=\"post\" action=\"";
        // line 59
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/product/update_config");
        echo "\" class=\"mainForm save api-form\" data-api-reload=\"1\">
    <fieldset>
        <legend>Custom parameters</legend>
        ";
        // line 62
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"));
        foreach ($context['_seq'] as $context["k"] => $context["v"]) {
            // line 63
            echo "        <div class=\"rowElem\">
            <label>";
            // line 64
            echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
            echo ":</label>
            <div class=\"formRight\">
                <input type=\"text\" name=\"config[";
            // line 66
            echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
            echo "]\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
            echo "\">
            </div>
            <div class=\"fix\"></div>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "    </fieldset>
    
    <fieldset>
        <legend>Add custom parameter</legend>
        <div class=\"rowElem\">
            <label>Parameter name:</label>
            <div class=\"formRight\">
                <input type=\"text\" name=\"new_config_name\">
            </div>
            <div class=\"fix\"></div>
        </div>

        <div class=\"rowElem\">
            <label>Parameter value:</label>
            <div class=\"formRight noborder\">
                <textarea rows=\"7\" cols=\"\" name=\"new_config_value\"></textarea>
            </div>
            <div class=\"fix\"></div>
        </div>
        <input type=\"submit\" value=\"";
        // line 90
        echo gettext("Update configuration fields");
        echo "\" class=\"greyishBtn submitForm\" />
        <div class=\"fix\"></div>
        <input type=\"hidden\" name=\"id\" value=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id"), "html", null, true);
        echo "\"/>
    </fieldset>
</form>";
    }

    public function getTemplateName()
    {
        return "mod_servicecustom_config.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 92,  193 => 90,  172 => 71,  159 => 66,  154 => 64,  151 => 63,  147 => 62,  141 => 59,  135 => 56,  129 => 53,  121 => 48,  116 => 46,  110 => 43,  104 => 40,  97 => 36,  91 => 33,  83 => 28,  78 => 26,  67 => 20,  61 => 19,  56 => 17,  45 => 11,  39 => 10,  34 => 8,  28 => 5,  22 => 2,  19 => 1,);
    }
}
