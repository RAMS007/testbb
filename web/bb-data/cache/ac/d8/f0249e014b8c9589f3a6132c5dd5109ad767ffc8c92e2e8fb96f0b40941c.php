<?php

/* mod_email_settings.phtml */
class __TwigTemplate_acd8f0249e014b8c9589f3a6132c5dd5109ad767ffc8c92e2e8fb96f0b40941c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'breadcrumbs' => array($this, 'block_breadcrumbs'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ajax")) ? ("layout_blank.phtml") : ("layout_default.phtml")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["mf"] = $this->env->loadTemplate("macro_functions.phtml");
        // line 14
        $context["active_menu"] = "system";
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_meta_title($context, array $blocks = array())
    {
        echo gettext("Email");
    }

    // line 5
    public function block_breadcrumbs($context, array $blocks = array())
    {
        // line 6
        echo "    <ul>
        <li class=\"firstB\"><a href=\"";
        // line 7
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("/");
        echo "\">";
        echo gettext("Home");
        echo "</a></li>
        <li><a href=\"";
        // line 8
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("system");
        echo "\">";
        echo gettext("Settings");
        echo "</a></li>
        <li class=\"lastB\">";
        // line 9
        echo gettext("Email");
        echo "</li>
    </ul>
";
    }

    // line 15
    public function block_content($context, array $blocks = array())
    {
        // line 16
        echo "<div class=\"widget simpleTabs\">

    <ul class=\"tabs\">
        <li><a href=\"#tab-index\">";
        // line 19
        echo gettext("Email templates");
        echo "</a></li>
        <li><a href=\"#tab-new\">";
        // line 20
        echo gettext("New template");
        echo "</a></li>
        <li><a href=\"#tab-email\">";
        // line 21
        echo gettext("Email settings");
        echo "</a></li>
    </ul>

    <div class=\"tabs_container\">
        <div class=\"fix\"></div>
        <div class=\"tab_content nopadding\" id=\"tab-index\">

            ";
        // line 28
        echo $context["mf"]->gettable_search();
        echo "
            <table class=\"tableStatic wide\">
                <thead>
                    <tr>
                        <td style=\"width: 50%\">";
        // line 32
        echo gettext("Title");
        echo "</td>
                        <td>";
        // line 33
        echo gettext("Extension");
        echo "</td>
                        <td>";
        // line 34
        echo gettext("Code");
        echo "</td>
                        <td>";
        // line 35
        echo gettext("Enabled");
        echo "</td>
                        <td width=\"13%\">&nbsp;</td>
                    </tr>
                </thead>

                <tbody>
                ";
        // line 41
        $context["templates"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "email_template_get_list", array(0 => twig_array_merge(array("per_page" => 100, "page" => $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "page")), (isset($context["request"]) ? $context["request"] : null))), "method");
        // line 42
        echo "                ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["templates"]) ? $context["templates"] : null), "list"));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["i"] => $context["template"]) {
            // line 43
            echo "                <tr>
                    <td>";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "subject"), "html", null, true);
            echo "</td>
                    <td>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "category"), "html", null, true);
            echo "</td>
                    <td>";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "action_code"), "html", null, true);
            echo "</td>
                    <td>";
            // line 47
            echo $context["mf"]->getq($this->getAttribute((isset($context["template"]) ? $context["template"] : null), "enabled"));
            echo "</td>
                    <td class=\"actions\">
                        <a class=\"bb-button btn14\" href=\"";
            // line 49
            echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("/email/template");
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "id"), "html", null, true);
            echo "\"><img src=\"images/icons/dark/pencil.png\" alt=\"\"></a>
                        <a class=\"btn14 bb-rm-tr api-link\" href=\"";
            // line 50
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/email/template_delete", array("id" => $this->getAttribute((isset($context["template"]) ? $context["template"] : null), "id")));
            echo "\" data-api-confirm=\"Are you sure?\" data-api-reload=\"1\"><img src=\"images/icons/dark/trash.png\" alt=\"\"></a>
                    </td>
                    
                </tr>

                ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 56
            echo "                <tr>
                    <td colspan=\"5\">
                        ";
            // line 58
            echo gettext("The list is empty. Depending on modules enabled email templates will be inserted after first event occurrence.");
            // line 59
            echo "                    </td>
                </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['template'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=\"5\">
                            <div class=\"aligncenter\">
                                <a href=\"";
        // line 67
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/email/batch_template_generate");
        echo "\" title=\"\" class=\"btn55 mr10 api-link\" data-api-reload=\"1\"><img src=\"images/icons/middlenav/power.png\" alt=\"\"><span>Generate templates</span></a>
                                <a href=\"";
        // line 68
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/email/batch_template_enable");
        echo "\" title=\"\" class=\"btn55 mr10 api-link\" data-api-msg=\"All email templates enabled\"><img src=\"images/icons/middlenav/play2.png\" alt=\"\"><span>Enable all</span></a>
                                <a href=\"";
        // line 69
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/email/batch_template_disable");
        echo "\" title=\"\" class=\"btn55 mr10 api-link\" data-api-msg=\"All email templates disabled\"><img src=\"images/icons/middlenav/stop.png\" alt=\"\"><span>Disable all</span></a>
                            </div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        
        ";
        // line 77
        $context["params"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "extension_config_get", array(0 => array("ext" => "mod_email")), "method");
        // line 78
        echo "        <div id=\"tab-email\" class=\"tab_content nopadding\">
            <div class=\"help\">
                <h3>";
        // line 80
        echo gettext("Configure email options");
        echo "</h3>
                <p>";
        // line 81
        echo gettext("BoxBilling sends emails using <em>sendmail</em> by default or you can configure your own SMTP server");
        echo "</p>
            </div>

            <form method=\"post\" action=\"";
        // line 84
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/extension/config_save");
        echo "\" class=\"mainForm api-form\" data-api-msg=\"Email settings updated\">
                <fieldset>
                
                <div class=\"rowElem noborder\">
                    <label class=\"topLabel\">Queue options</label>
                    <div class=\"formBottom\" id=\"mailer\">
                        <label>";
        // line 90
        echo gettext("Send emails per cron run (0 = unlimited)");
        echo "</label><input type=\"text\" name=\"queue_once\" placeholder=\"0\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "queue_once"), "html", null, true);
        echo "\"/>
                        <label>";
        // line 91
        echo gettext("Max email sending time in minutes (0 = unlimited, default 5 min.)");
        echo "</label><input type=\"text\" name=\"time_limit\" placeholder=\"5\" value=\"";
        if ($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "time_limit")) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "time_limit"), "html", null, true);
        } else {
            echo "5";
        }
        echo "\"/>
                        <label>";
        // line 92
        echo gettext("Cancel sending email after unsuccessful tries (0 = do not cancel)");
        echo "</label><input type=\"text\" name=\"cancel_after\" placeholder=\"0\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "cancel_after"), "html", null, true);
        echo "\"/>
                    </div>
                    <div class=\"fix\"></div>
                </div>

                <div class=\"rowElem noborder\">
                    <label class=\"\">";
        // line 98
        echo gettext("Log sent emails to database");
        echo "</label>
                    <div class=\"formRight\" id=\"mailer\">
                        <input type=\"radio\" name=\"log_enabled\" value=\"1\" ";
        // line 100
        if ($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "log_enabled")) {
            echo "checked=\"checked\"";
        }
        echo "/><label>";
        echo gettext("Yes");
        echo "</label>
                        <input type=\"radio\" name=\"log_enabled\" value=\"0\" ";
        // line 101
        if ((!$this->getAttribute((isset($context["params"]) ? $context["params"] : null), "log_enabled"))) {
            echo "checked=\"checked\"";
        }
        echo " /><label>";
        echo gettext("No");
        echo "</label>
                    </div>
                    <div class=\"fix\"></div>
                </div>

                <div class=\"rowElem\">
                    <label class=\"\">";
        // line 107
        echo gettext("Email transport");
        echo "</label>
                    <div class=\"formRight\" id=\"mailer\">
                        <input type=\"radio\" name=\"mailer\" value=\"sendmail\" ";
        // line 109
        if ((($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "mailer") == "sendmail") || (!$this->getAttribute((isset($context["params"]) ? $context["params"] : null), "mailer")))) {
            echo "checked=\"checked\"";
        }
        echo "/><label>";
        echo gettext("SendMail");
        echo "</label>
                        <input type=\"radio\" name=\"mailer\" value=\"smtp\" ";
        // line 110
        if (($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "mailer") == "smtp")) {
            echo "checked=\"checked\"";
        }
        echo " /><label>";
        echo gettext("SMTP");
        echo "</label>
                    </div>
                    <div class=\"fix\"></div>
                </div>
                </fieldset>

                <fieldset class=\"smtp\" ";
        // line 116
        if (($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "mailer") != "smtp")) {
            echo "style=\"display:none;\"";
        }
        echo " >
                <div class=\"rowElem\">
                    <label class=\"topLabel\">";
        // line 118
        echo gettext("SMTP Hostname");
        echo "</label>
                    <div class=\"formBottom\">
                        <input type=\"text\" name=\"smtp_host\" value=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "smtp_host"), "html", null, true);
        echo "\" placeholder=\"smtp.gmail.com\"/>
                    </div>
                    <div class=\"fix\"></div>
                </div>
                    
                <div class=\"rowElem\">
                    <label class=\"topLabel\">";
        // line 126
        echo gettext("SMTP Port");
        echo "</label>
                    <div class=\"formBottom\">
                        <input type=\"text\" name=\"smtp_port\" value=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "smtp_port"), "html", null, true);
        echo "\" placeholder=\"587\"/>
                    </div>
                    <div class=\"fix\"></div>
                </div>
                    
                <div class=\"rowElem\">
                    <label class=\"topLabel\">";
        // line 134
        echo gettext("SMTP Username");
        echo "</label>
                    <div class=\"formBottom\">
                        <input type=\"text\" name=\"smtp_username\" value=\"";
        // line 136
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "smtp_username"), "html", null, true);
        echo "\" placeholder=\"my.email@gmail.com\"/>
                    </div>
                    <div class=\"fix\"></div>
                </div>
                    
                <div class=\"rowElem\">
                    <label class=\"topLabel\">";
        // line 142
        echo gettext("SMTP Password");
        echo "</label>
                    <div class=\"formBottom\">
                        <input type=\"password\" name=\"smtp_password\" value=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "smtp_password"), "html", null, true);
        echo "\" placeholder=\"******\"/>
                    </div>
                    <div class=\"fix\"></div>
                </div>
                    
                <div class=\"rowElem\">
                    <label class=\"\">";
        // line 150
        echo gettext("SMTP Security");
        echo "</label>
                    <div class=\"formRight\">
                        <input type=\"radio\" name=\"smtp_security\" value=\"\"";
        // line 152
        if ((!$this->getAttribute((isset($context["params"]) ? $context["params"] : null), "smtp_security"))) {
            echo "checked=\"checked\"";
        }
        echo "/><label>None</label>
                        <input type=\"radio\" name=\"smtp_security\" value=\"ssl\"";
        // line 153
        if (($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "smtp_security") == "ssl")) {
            echo "checked=\"checked\"";
        }
        echo "/><label>SSL</label>
                        <input type=\"radio\" name=\"smtp_security\" value=\"tls\"";
        // line 154
        if (($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "smtp_security") == "tls")) {
            echo "checked=\"checked\"";
        }
        echo "/><label>TLS</label>
                    </div>
                    <div class=\"fix\"></div>
                </div>
                </fieldset>
                    
                <fieldset>
                    <input type=\"submit\" value=\"";
        // line 161
        echo gettext("Update email settings");
        echo "\" class=\"greyishBtn submitForm\" />
                    <input type=\"button\" value=\"";
        // line 162
        echo gettext("Send test email to staff members");
        echo "\" class=\"blackBtn leftBtn\" id=\"emailTest\"/>
                    <input type=\"hidden\" name=\"ext\" value=\"mod_email\"/>
                </fieldset>
            </form>
        </div>

        <div class=\"tab_content nopadding\" id=\"tab-new\">
            <div class=\"help\">
                <h3>";
        // line 170
        echo gettext("Where I can use new email template?");
        echo "</h3>
                <p>";
        // line 171
        echo gettext("Newly created email templates can be used in custom event hooks.");
        echo "</p>
            </div>

            <form method=\"post\" action=\"";
        // line 174
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/email/template_create");
        echo "\" class=\"mainForm save api-form\" data-api-reload=\"1\">
                <fieldset>
                    <div class=\"rowElem noborder\">
                        <label>";
        // line 177
        echo gettext("Enabled");
        echo ":</label>
                        <div class=\"formRight noborder\">
                            <input type=\"radio\" name=\"enabled\" value=\"1\"/><label>";
        // line 179
        echo gettext("Yes");
        echo "</label>
                            <input type=\"radio\" name=\"enabled\" value=\"0\" checked=\"checked\"/><label>";
        // line 180
        echo gettext("No");
        echo "</label>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    <div class=\"rowElem\">
                        <label>";
        // line 185
        echo gettext("Category");
        echo "</label>
                        <div class=\"formRight\">
                            <input type=\"text\" name=\"category\" value=\"";
        // line 187
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["requests"]) ? $context["requests"] : null), "category"), "html", null, true);
        echo "\" required=\"required\" placeholder=\"General\"/>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    <div class=\"rowElem\">
                        <label>";
        // line 192
        echo gettext("Action code");
        echo "</label>
                        <div class=\"formRight\">
                            <input type=\"text\" name=\"action_code\" value=\"";
        // line 194
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["requests"]) ? $context["requests"] : null), "action_code"), "html", null, true);
        echo "\" required=\"required\"/>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    <div class=\"rowElem\">
                        <label>";
        // line 199
        echo gettext("Subject");
        echo "</label>
                        <div class=\"formRight\">
                            <input type=\"text\" name=\"subject\" value=\"";
        // line 201
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["requests"]) ? $context["requests"] : null), "subject"), "html", null, true);
        echo "\" required=\"required\"/>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    <div class=\"rowElem\">
                        <label>";
        // line 206
        echo gettext("Content");
        echo "</label>
                        <div class=\"formRight\">
                            <textarea name=\"content\" cols=\"5\" rows=\"10\">";
        // line 208
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["requests"]) ? $context["requests"] : null), "content"), "html", null, true);
        echo "</textarea>
                        </div>
                        <div class=\"fix\"></div>
                    </div>

                    <input type=\"submit\" value=\"";
        // line 213
        echo gettext("Create");
        echo "\" class=\"greyishBtn submitForm\" />
                </fieldset>
            </form>
        </div>
    </div>
</div>
";
    }

    // line 221
    public function block_js($context, array $blocks = array())
    {
        // line 222
        echo "<script type=\"text/javascript\">
\$(function() {
    \$('#emailTest').click(function(){
        bb.post('admin/email/send_test', null, function(result){
            bb.msg('Email was successfully sent');
        });
        return false;
    });
\t\$(\"#mailer input\").click( function() {
        if(\$(this).val() == 'smtp') {
            \$('fieldset.smtp').slideDown();
        } else {
            \$('fieldset.smtp').slideUp();
        }
\t});
});
</script>
";
    }

    public function getTemplateName()
    {
        return "mod_email_settings.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  513 => 222,  510 => 221,  499 => 213,  491 => 208,  486 => 206,  478 => 201,  473 => 199,  465 => 194,  460 => 192,  452 => 187,  447 => 185,  439 => 180,  435 => 179,  430 => 177,  424 => 174,  418 => 171,  414 => 170,  403 => 162,  399 => 161,  387 => 154,  381 => 153,  375 => 152,  370 => 150,  361 => 144,  356 => 142,  347 => 136,  342 => 134,  333 => 128,  328 => 126,  319 => 120,  314 => 118,  307 => 116,  294 => 110,  286 => 109,  281 => 107,  268 => 101,  260 => 100,  255 => 98,  244 => 92,  234 => 91,  228 => 90,  219 => 84,  213 => 81,  209 => 80,  205 => 78,  203 => 77,  192 => 69,  188 => 68,  184 => 67,  177 => 62,  169 => 59,  167 => 58,  163 => 56,  152 => 50,  146 => 49,  141 => 47,  137 => 46,  133 => 45,  129 => 44,  126 => 43,  120 => 42,  118 => 41,  109 => 35,  105 => 34,  101 => 33,  97 => 32,  90 => 28,  80 => 21,  76 => 20,  72 => 19,  67 => 16,  64 => 15,  57 => 9,  51 => 8,  45 => 7,  42 => 6,  39 => 5,  33 => 3,  28 => 14,  26 => 2,);
    }
}
