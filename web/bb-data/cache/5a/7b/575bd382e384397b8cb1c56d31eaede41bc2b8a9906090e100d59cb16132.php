<?php

/* mod_news_post.phtml */
class __TwigTemplate_5a7b575bd382e384397b8cb1c56d31eaede41bc2b8a9906090e100d59cb16132 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout_default.phtml");

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'breadcrumbs' => array($this, 'block_breadcrumbs'),
            'content' => array($this, 'block_content'),
            'head' => array($this, 'block_head'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout_default.phtml";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["active_menu"] = "support";
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_meta_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "title"), "html", null, true);
    }

    // line 5
    public function block_breadcrumbs($context, array $blocks = array())
    {
        // line 6
        echo "<ul>
    <li class=\"firstB\"><a href=\"";
        // line 7
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("/");
        echo "\">";
        echo gettext("Home");
        echo "</a></li>
    <li><a href=\"";
        // line 8
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("news");
        echo "\">";
        echo gettext("News");
        echo "</a></li>
    <li class=\"lastB\">";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "title"), "html", null, true);
        echo "</li>
</ul>
";
    }

    // line 13
    public function block_content($context, array $blocks = array())
    {
        // line 14
        echo "<div class=\"widget\">
    
    <div class=\"head\">
        <h5>";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "title"), "html", null, true);
        echo "</h5>
    </div>
    
    <form method=\"post\" action=\"";
        // line 20
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/news/update");
        echo "\" class=\"mainForm save api-form\" data-api-msg=\"";
        echo gettext("Post updated");
        echo "\">
        <fieldset>
            <p>If the text is very long you can use <strong>&lt;!--more--&gt;</strong> tag. Inserting this tag within the post will create and excerpt of text (before the tag) to be displayed in posts list. Users will be able to see whole content when they click on \"Read more\" button.</p>
            <br/>
            <textarea name=\"content\" cols=\"5\" rows=\"20\" class=\"bb-textarea\">";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "content"), "html", null, true);
        echo "</textarea>
            <input type=\"submit\" value=\"";
        // line 25
        echo gettext("Update");
        echo "\" class=\"greyishBtn submitForm\" />
            <input type=\"hidden\" name=\"id\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id"), "html", null, true);
        echo "\"/>
        </fieldset>
    </form>
</div>

<div class=\"widget\">
    
    <div class=\"head\">
        <h5>";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "title"), "html", null, true);
        echo "</h5>
    </div>
    
    <form method=\"post\" action=\"";
        // line 37
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/news/update");
        echo "\" class=\"mainForm save api-form\" data-api-msg=\"";
        echo gettext("Post updated");
        echo "\">
        <fieldset>
            <div class=\"rowElem noborder\">
                <label>";
        // line 40
        echo gettext("Title");
        echo "</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"title\" value=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "title"), "html", null, true);
        echo "\" required=\"required\"/>
                </div>
                <div class=\"fix\"></div>
            </div>
            
            <div class=\"rowElem\">
                <label>";
        // line 48
        echo gettext("Slug");
        echo "</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"slug\" value=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "slug"), "html", null, true);
        echo "\" required=\"required\"/>
                </div>
                <div class=\"fix\"></div>
            </div>
            <div class=\"rowElem\">
                <label>";
        // line 55
        echo gettext("Image");
        echo "</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"image\" value=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "image"), "html", null, true);
        echo "\" id=\"post_image\" placeholder=\"";
        echo gettext("http://www.yourdomain.com/image.jpg");
        echo "\"/>
                </div>
                <div class=\"fix\"></div>
            </div>
            <div class=\"rowElem\">
                <label>";
        // line 62
        echo gettext("Status");
        echo "</label>
                <div class=\"formRight\">
                    <input type=\"radio\" name=\"status\" value=\"draft\"";
        // line 64
        if (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "status") == "draft")) {
            echo " checked=\"checked\"";
        }
        echo "/><label>Draft</label>
                    <input type=\"radio\" name=\"status\" value=\"active\"";
        // line 65
        if (($this->getAttribute((isset($context["post"]) ? $context["post"] : null), "status") == "active")) {
            echo " checked=\"checked\"";
        }
        echo " /><label>Active</label>
                </div>
                <div class=\"fix\"></div>
            </div>
            <div class=\"rowElem\">
                <label>";
        // line 70
        echo gettext("Post created");
        echo "</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"created_at\" value=\"";
        // line 72
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "created_at"), "Y-m-d"), "html", null, true);
        echo "\" required=\"required\" class=\"datepicker\"/>
                </div>
                <div class=\"fix\"></div>
            </div>
            <div class=\"rowElem\">
                <label>";
        // line 77
        echo gettext("Last update");
        echo "</label>
                <div class=\"formRight\">
                    <input type=\"text\" name=\"updated_at\" value=\"";
        // line 79
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "updated_at"), "Y-m-d"), "html", null, true);
        echo "\" required=\"required\" class=\"datepicker\"/>
                </div>
                <div class=\"fix\"></div>
            </div>
            <input type=\"submit\" value=\"";
        // line 83
        echo gettext("Update");
        echo "\" class=\"greyishBtn submitForm\" />
            <input type=\"hidden\" name=\"id\" value=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "id"), "html", null, true);
        echo "\"/>
        </fieldset>
    </form>
    <div class=\"body\">
        <a href=\"";
        // line 88
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("news");
        echo "\"  title=\"\" class=\"btnIconLeft mr10\"><img src=\"images/icons/dark/arrowLeft.png\" alt=\"\" class=\"icon\"><span>";
        echo gettext("Go back");
        echo "</span></a>
    </div>
</div>
";
    }

    // line 93
    public function block_head($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mf"]) ? $context["mf"] : null), "bb_editor", array(0 => ".bb-textarea"), "method"), "html", null, true);
    }

    public function getTemplateName()
    {
        return "mod_news_post.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  229 => 93,  219 => 88,  212 => 84,  208 => 83,  201 => 79,  196 => 77,  188 => 72,  183 => 70,  173 => 65,  167 => 64,  162 => 62,  152 => 57,  147 => 55,  139 => 50,  134 => 48,  125 => 42,  120 => 40,  112 => 37,  106 => 34,  95 => 26,  91 => 25,  87 => 24,  78 => 20,  72 => 17,  67 => 14,  64 => 13,  57 => 9,  51 => 8,  45 => 7,  42 => 6,  39 => 5,  33 => 2,  28 => 3,);
    }
}
