<?php

/* mod_servicesolusvm_config.phtml */
class __TwigTemplate_3e6d43eeefff4d17d36b3c9d572532126aa0b05c656e9eff0f756fa9d3d3a12e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"help\">
    <h5>";
        // line 2
        echo gettext("SolusVM configuration");
        echo "</h5>
    <p>More information what does each configuration parameter means at ";
        // line 3
        echo twig_autolink_filter("http://wiki.solusvm.com/index.php/API:Admin");
        echo "</p>
</div>

<form method=\"post\" action=\"";
        // line 6
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/product/update_config");
        echo "\" class=\"mainForm api-form save\" data-api-reload=\"1\">
<fieldset>
    <div class=\"rowElem noborder\">
        <label>";
        // line 9
        echo gettext("Virtualization type");
        echo ":</label>
        <div class=\"formRight\">
            ";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mf"]) ? $context["mf"] : null), "selectbox", array(0 => "config[vtype]", 1 => $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "servicesolusvm_get_virtualization_types"), 2 => $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "vtype"), 3 => 0, 4 => "Select type"), "method"), "html", null, true);
        echo "
        </div>
        <div class=\"fix\"></div>
    </div>
    
    ";
        // line 16
        if ($this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "vtype")) {
            // line 17
            echo "    
    ";
            // line 28
            echo "    
    <div class=\"rowElem\">
        <label>";
            // line 30
            echo gettext("Node");
            echo ":</label>
        <div class=\"formRight\">
            ";
            // line 32
            $context["nodes"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "servicesolusvm_get_nodes", array(0 => array("type" => $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "vtype"))), "method");
            // line 33
            echo "            ";
            if ((isset($context["nodes"]) ? $context["nodes"] : null)) {
                // line 34
                echo "            ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mf"]) ? $context["mf"] : null), "selectbox", array(0 => "config[node]", 1 => (isset($context["nodes"]) ? $context["nodes"] : null), 2 => $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "node"), 3 => 0, 4 => "Select node"), "method"), "html", null, true);
                echo "
            ";
            } else {
                // line 36
                echo "            ";
                echo gettext("Make sure BoxBilling can connect to SolusVM master server and check if nodes are configured for selected virtualizator type. Refresh page if you have changed virtualization type to see new nodes.");
                // line 37
                echo "            ";
            }
            // line 38
            echo "        </div>
        <div class=\"fix\"></div>
    </div>
    
    <div class=\"rowElem\">
        <label>";
            // line 43
            echo gettext("Plan");
            echo ":</label>
        <div class=\"formRight\">
            ";
            // line 45
            $context["plans"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "servicesolusvm_get_plans", array(0 => array("type" => $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "vtype"))), "method");
            // line 46
            echo "            ";
            if ((isset($context["plans"]) ? $context["plans"] : null)) {
                // line 47
                echo "            ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mf"]) ? $context["mf"] : null), "selectbox", array(0 => "config[plan]", 1 => (isset($context["plans"]) ? $context["plans"] : null), 2 => $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "plan"), 3 => 0, 4 => "Select plan"), "method"), "html", null, true);
                echo "
            ";
            } else {
                // line 49
                echo "            ";
                echo gettext("Make sure BoxBilling can connect to SolusVM master server and check if plans are configured for selected virtualizator type.");
                // line 50
                echo "            ";
            }
            // line 51
            echo "        </div>
        <div class=\"fix\"></div>
    </div>
    <input type=\"submit\" value=\"";
            // line 54
            echo gettext("Update");
            echo "\" class=\"greyishBtn submitForm\" />
</fieldset>
    
<input type=\"hidden\" name=\"config[cluster_id]\" value=\"1\" />
<input type=\"hidden\" name=\"id\" value=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id"), "html", null, true);
            echo "\" />
</form>

<form method=\"post\" action=\"";
            // line 61
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/product/update_config");
            echo "\" class=\"mainForm api-form save\" data-api-msg=\"Plan confuguration updated\">
<fieldset>
    <legend>";
            // line 63
            echo gettext("Plan parameters");
            echo "</legend>
    <div class=\"rowElem\">
        <label>";
            // line 65
            echo gettext("IPs amount");
            echo ":</label>
        <div class=\"formRight\">
            <input type=\"text\" name=\"config[ips]\" value=\"";
            // line 67
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "ips"), "html", null, true);
            echo "\" required=\"required\" placeholder=\"Define how many ips should be assigned for new VPS\"/>
        </div>
        <div class=\"fix\"></div>
    </div>
    
    <div class=\"rowElem\">
        <label>";
            // line 73
            echo gettext("Guaranteed RAM");
            echo ":</label>
        <div class=\"formRight\">
            <input type=\"text\" name=\"config[custommemory]\" value=\"";
            // line 75
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "custommemory"), "html", null, true);
            echo "\" placeholder=\"Overide plan memory with this amount. Number of MB\"/>
        </div>
        <div class=\"fix\"></div>
    </div>
    
    <div class=\"rowElem\">
        <label>";
            // line 81
            echo gettext("Hdd Space");
            echo ":</label>
        <div class=\"formRight\">
            <input type=\"text\" name=\"config[customdiskspace]\" value=\"";
            // line 83
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "customdiskspace"), "html", null, true);
            echo "\" placeholder=\"Overide plan diskspace with this amount. Number of GB\"/>
        </div>
        <div class=\"fix\"></div>
    </div>
    
    <div class=\"rowElem\">
        <label>";
            // line 89
            echo gettext("Bandwidth");
            echo ":</label>
        <div class=\"formRight\">
            <input type=\"text\" name=\"config[custombandwidth]\" value=\"";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "custombandwidth"), "html", null, true);
            echo "\" placeholder=\"Overide plan bandwidth with this amount. Number of GB per month\"/>
        </div>
        <div class=\"fix\"></div>
    </div>
    
    <div class=\"rowElem\">
        <label>";
            // line 97
            echo gettext("CPU cores");
            echo ":</label>
        <div class=\"formRight\">
            <input type=\"text\" name=\"config[customcpu]\" value=\"";
            // line 99
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "customcpu"), "html", null, true);
            echo "\" placeholder=\"Overide plan cpu cores with this amount\"/>
        </div>
        <div class=\"fix\"></div>
    </div>
    
    <div class=\"rowElem\">
        <label>";
            // line 105
            echo gettext("Custom extraip");
            echo ":</label>
        <div class=\"formRight\">
            <input type=\"text\" name=\"config[customextraip]\" value=\"";
            // line 107
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "customextraip"), "html", null, true);
            echo "\" placeholder=\"Add this amount of extra ips\"/>
        </div>
        <div class=\"fix\"></div>
    </div>
    
    <div class=\"rowElem\">
        <label>";
            // line 113
            echo gettext("HVMT");
            echo ":</label>
        <div class=\"formRight\">
            <input type=\"text\" name=\"config[hvmt]\" value=\"";
            // line 115
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "hvmt"), "html", null, true);
            echo "\" placeholder=\"[0|1] This allows to to define templates & isos for Xen HVM\"/>
        </div>
        <div class=\"fix\"></div>
    </div>
    
    <div class=\"rowElem\">
        <label>";
            // line 121
            echo gettext("Issue cPanel License");
            echo ":</label>
        <div class=\"formRight\">
            <input type=\"radio\" name=\"config[issuelicense]\" value=\"1\" ";
            // line 123
            if (($this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "issuelicense") == "1")) {
                echo "checked=\"checked\"";
            }
            echo "/><label>";
            echo gettext("Yes");
            echo "</label>
            <input type=\"radio\" name=\"config[issuelicense]\" value=\"0\" ";
            // line 124
            if ((!$this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "issuelicense"))) {
                echo "checked=\"checked\"";
            }
            echo "/><label>";
            echo gettext("No");
            echo "</label>
        </div>
        <div class=\"fix\"></div>
    </div>
    
    ";
        }
        // line 130
        echo "    <input type=\"submit\" value=\"";
        echo gettext("Update");
        echo "\" class=\"greyishBtn submitForm\" />
</fieldset>
<input type=\"hidden\" name=\"id\" value=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id"), "html", null, true);
        echo "\" />
</form>

<form method=\"post\" action=\"";
        // line 135
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/product/update_config");
        echo "\" class=\"mainForm api-form save\" data-api-msg=\"Slider information updated\">
<fieldset>
    <legend>Slider information. Set values to be displayed for client on order page</legend>
    
    <div class=\"rowElem\">
        <label>";
        // line 140
        echo gettext("Dedicated CPU");
        echo ":</label>
        <div class=\"formRight\">
            <input type=\"text\" name=\"config[slider_cpu]\" value=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "slider_cpu"), "html", null, true);
        echo "\" placeholder=\"4.2GHz\"/>
        </div>
        <div class=\"fix\"></div>
    </div>
    
    <div class=\"rowElem\">
        <label>";
        // line 148
        echo gettext("Dedicated RAM");
        echo ":</label>
        <div class=\"formRight\">
            <input type=\"text\" name=\"config[slider_ram]\" value=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "slider_ram"), "html", null, true);
        echo "\" placeholder=\"1504MB\"/>
        </div>
        <div class=\"fix\"></div>
    </div>
    
    <div class=\"rowElem\">
        <label>";
        // line 156
        echo gettext("Disk space");
        echo ":</label>
        <div class=\"formRight\">
            <input type=\"text\" name=\"config[slider_hdd]\" value=\"";
        // line 158
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "slider_hdd"), "html", null, true);
        echo "\" placeholder=\"40GB\"/>
        </div>
        <div class=\"fix\"></div>
    </div>
    
    <div class=\"rowElem\">
        <label>";
        // line 164
        echo gettext("Network transfer");
        echo ":</label>
        <div class=\"formRight\">
            <input type=\"text\" name=\"config[slider_bandwidth]\" value=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "slider_bandwidth"), "html", null, true);
        echo "\" placeholder=\"4TB\"/>
        </div>
        <div class=\"fix\"></div>
    </div>
    
    <div class=\"rowElem\">
        <label>";
        // line 172
        echo gettext("Description");
        echo ":</label>
        <div class=\"formRight\">
            <textarea name=\"config[slider_desc]\" placeholder=\"Very simple description, like: Perfect for WordPress hosting\">";
        // line 174
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "slider_bandwidth"), "html", null, true);
        echo "</textarea>
        </div>
        <div class=\"fix\"></div>
    </div>
    
    <input type=\"submit\" value=\"";
        // line 179
        echo gettext("Update");
        echo "\" class=\"greyishBtn submitForm\" />
</fieldset>
    
<input type=\"hidden\" name=\"config[cluster_id]\" value=\"1\" />
<input type=\"hidden\" name=\"id\" value=\"";
        // line 183
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id"), "html", null, true);
        echo "\" />
</form>";
    }

    public function getTemplateName()
    {
        return "mod_servicesolusvm_config.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  359 => 183,  352 => 179,  344 => 174,  339 => 172,  330 => 166,  325 => 164,  316 => 158,  311 => 156,  302 => 150,  297 => 148,  288 => 142,  283 => 140,  275 => 135,  269 => 132,  263 => 130,  250 => 124,  242 => 123,  237 => 121,  228 => 115,  223 => 113,  214 => 107,  209 => 105,  200 => 99,  195 => 97,  186 => 91,  181 => 89,  172 => 83,  167 => 81,  158 => 75,  153 => 73,  144 => 67,  139 => 65,  134 => 63,  129 => 61,  123 => 58,  116 => 54,  111 => 51,  108 => 50,  105 => 49,  99 => 47,  96 => 46,  94 => 45,  89 => 43,  82 => 38,  79 => 37,  76 => 36,  70 => 34,  67 => 33,  65 => 32,  60 => 30,  56 => 28,  53 => 17,  51 => 16,  43 => 11,  38 => 9,  32 => 6,  26 => 3,  22 => 2,  19 => 1,);
    }
}
