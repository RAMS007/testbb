<?php

/* mod_formbuilder_build.phtml */
class __TwigTemplate_ea41583f682a68140c766819c08b604b108c9e060cae4ceef363a80d83137712 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "extension_is_on", array(0 => array("mod" => "formbuilder")), "method")) {
            // line 2
            $context["form"] = $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "formbuilder_get", array(0 => array("id" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "form_id"))), "method");
            // line 3
            echo "        ";
            if (($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "style"), "show_title") != "0")) {
                // line 4
                echo "        <div class=\"control-group\">
            <div class=\"controls\">
                <legend>";
                // line 6
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "name"), "html", null, true);
                echo "</legend>
            </div>
        </div>
        ";
            }
            // line 10
            echo "
    <fieldset>
        ";
            // line 12
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fields"));
            foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
                // line 13
                echo "    ";
                if (($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "style"), "type") == "horizontal")) {
                    // line 14
                    echo "       <div class=\"control-group\">
    ";
                }
                // line 16
                echo "            ";
                if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "hide_label") != 1)) {
                    echo "<label ";
                    if (($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "style"), "type") == "horizontal")) {
                        echo "class=\"control-label\"";
                    }
                    echo " for=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "label"), "html", null, true);
                    echo "</label>";
                }
                // line 17
                echo "       ";
                if (($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "style"), "type") == "horizontal")) {
                    // line 18
                    echo "           <div class=\"controls\">
       ";
                }
                // line 20
                echo "

                ";
                // line 22
                if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "text")) {
                    // line 23
                    echo "                    ";
                    if (((!twig_test_empty($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "prefix"))) || (!twig_test_empty($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "suffix"))))) {
                        echo "<div class=\"";
                        if ((!twig_test_empty($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "prefix")))) {
                            echo "input-prepend";
                        }
                        echo " ";
                        if ((!twig_test_empty($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "suffix")))) {
                            echo "input-append";
                        }
                        echo " \">";
                    }
                    // line 24
                    echo "                    ";
                    if ((!twig_test_empty($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "prefix")))) {
                        echo "<span class=\"add-on\">";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "prefix"), "html", null, true);
                        echo "</span>";
                    }
                    // line 25
                    echo "                    <input type=\"text\" name=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name"), "html", null, true);
                    echo "\" value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "default_value"), "html", null, true);
                    echo "\"
                    ";
                    // line 26
                    if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "required") == 1)) {
                        echo "required=\"required\"";
                    }
                    // line 27
                    echo "                    ";
                    if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "readonly") == 1)) {
                        echo "readonly=\"readonly\"";
                    }
                    // line 28
                    echo "                    />
                    ";
                    // line 29
                    if ((!twig_test_empty($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "suffix")))) {
                        echo "<span class=\"add-on\">";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "suffix"), "html", null, true);
                        echo "</span>";
                    }
                    // line 30
                    echo "                    ";
                    if (((!twig_test_empty($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "prefix"))) || (!twig_test_empty($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "suffix"))))) {
                        echo " </div>";
                    }
                    // line 31
                    echo "
                ";
                } elseif (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "select")) {
                    // line 33
                    echo "                <select name=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name"), "html", null, true);
                    echo "\" required=\"required\">
                    ";
                    // line 34
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"));
                    foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                        // line 35
                        echo "                        <option value=\"";
                        echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                        echo "\" ";
                        if (((isset($context["v"]) ? $context["v"] : null) == $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "default_value"))) {
                            echo "selected=\"selected\"";
                        }
                        echo ">";
                        echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                        echo "</option>
                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 37
                    echo "                </select>
    
                ";
                } elseif (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "checkbox")) {
                    // line 40
                    echo "                    ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"));
                    foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                        // line 41
                        echo "                    <label class=\"checkbox\"><input type=\"checkbox\" name=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name"), "html", null, true);
                        echo "[]\" value=\"";
                        echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                        echo "\"
                    ";
                        // line 42
                        if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "readonly") == 1)) {
                            echo "readonly=\"readonly\"";
                        }
                        // line 43
                        echo "                    ";
                        if (twig_in_filter((isset($context["v"]) ? $context["v"] : null), $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "default_value"))) {
                            echo "checked=\"checked\"";
                        }
                        // line 44
                        echo "                        />";
                        echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                        echo "
                    </label>
                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 47
                    echo "
                ";
                } elseif (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "radio")) {
                    // line 49
                    echo "                ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"));
                    foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                        // line 50
                        echo "                <label class=\"radio\">
                    <input type=\"radio\" name=\"";
                        // line 51
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name"), "html", null, true);
                        echo "\" value=\"";
                        echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                        echo "\"
                    ";
                        // line 52
                        if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "default_value") == (isset($context["v"]) ? $context["v"] : null))) {
                            echo "checked";
                        }
                        // line 53
                        echo "                    ";
                        if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "required") == 1)) {
                            echo "required=\"required\"";
                        }
                        // line 54
                        echo "                    ";
                        if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "readonly") == 1)) {
                            echo "readonly=\"readonly\"";
                        }
                        // line 55
                        echo "                    />
                    ";
                        // line 56
                        echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                        echo "
                </label>
                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 59
                    echo "
                ";
                } elseif (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "textarea")) {
                    // line 61
                    echo "                <textarea id=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name"), "html", null, true);
                    echo "\" name=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name"), "html", null, true);
                    echo "\" cols=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"), "cols"), "html", null, true);
                    echo "\" rows=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"), "rows"), "html", null, true);
                    echo "\"
                    ";
                    // line 62
                    if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "required") == 1)) {
                        echo "required=\"required\"";
                    }
                    // line 63
                    echo "                    ";
                    if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "readonly") == 1)) {
                        echo "readonly=\"readonly\"";
                    }
                    // line 64
                    echo "                >";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "default_value"), "html", null, true);
                    echo "</textarea>
                ";
                }
                // line 66
                echo "                <span class=\"help-block\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "description"), "html", null, true);
                echo "</span>
    ";
                // line 67
                if (($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "style"), "type") == "horizontal")) {
                    // line 68
                    echo "        </div>
        </div>
    ";
                }
                // line 71
                echo "
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 73
            echo "    </fieldset>
    <input type=\"hidden\" name=\"form_id\" value=\"";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "id"), "html", null, true);
            echo "\" />
    <input type=\"hidden\" name=\"id\" value=\"";
            // line 75
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id"), "html", null, true);
            echo "\" />
";
        }
    }

    public function getTemplateName()
    {
        return "mod_formbuilder_build.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  292 => 75,  288 => 74,  285 => 73,  278 => 71,  273 => 68,  271 => 67,  266 => 66,  260 => 64,  255 => 63,  251 => 62,  240 => 61,  236 => 59,  227 => 56,  224 => 55,  219 => 54,  214 => 53,  210 => 52,  204 => 51,  201 => 50,  196 => 49,  192 => 47,  182 => 44,  177 => 43,  173 => 42,  166 => 41,  161 => 40,  156 => 37,  141 => 35,  137 => 34,  132 => 33,  128 => 31,  123 => 30,  117 => 29,  114 => 28,  109 => 27,  105 => 26,  98 => 25,  91 => 24,  78 => 23,  76 => 22,  72 => 20,  68 => 18,  65 => 17,  52 => 16,  48 => 14,  45 => 13,  41 => 12,  37 => 10,  30 => 6,  26 => 4,  23 => 3,  21 => 2,  19 => 1,);
    }
}
