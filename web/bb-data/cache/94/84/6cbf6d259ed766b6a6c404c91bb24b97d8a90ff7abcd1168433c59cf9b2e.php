<?php

/* partial_extensions.phtml */
class __TwigTemplate_94846cbf6d259ed766b6a6c404c91bb24b97d8a90ff7abcd1168433c59cf9b2e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["header"]) ? $context["header"] : null)) {
            // line 2
            echo "<div class=\"help\">
    <h5>";
            // line 3
            echo twig_escape_filter($this->env, (isset($context["header"]) ? $context["header"] : null), "html", null, true);
            echo "</h5>
</div>
";
        }
        // line 6
        echo "<table class=\"tableStatic wide\">
    <thead>
        <tr>
            <td>&nbsp;</td>
            <td style=\"width: 30%\">Extension</td>
            <td>Description</td>
            <td width=\"18%\">&nbsp;</td>
        </tr>
    </thead>

    <tbody>
        ";
        // line 17
        $context["extensions"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "extension_get_latest", array(0 => array("type" => (isset($context["type"]) ? $context["type"] : null))), "method");
        // line 18
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["extensions"]) ? $context["extensions"] : null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["extension"]) {
            // line 19
            echo "        <tr>
            <td><img src=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["extension"]) ? $context["extension"] : null), "icon_url"), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["extension"]) ? $context["extension"] : null), "name"), "html", null, true);
            echo "\" style=\"width: 32px; height: 32px;\"/></td>
            <td>
                <a class=\"bb-button\" href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["extension"]) ? $context["extension"] : null), "project_url"), "html", null, true);
            echo "\" target=\"_blank\" title=\"View extension details\">";
            echo twig_escape_filter($this->env, twig_truncate_filter($this->env, $this->getAttribute((isset($context["extension"]) ? $context["extension"] : null), "name"), 40), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["extension"]) ? $context["extension"] : null), "version"), "html", null, true);
            echo "</a>
                <br/>by <a href=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["extension"]) ? $context["extension"] : null), "author_url"), "html", null, true);
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["extension"]) ? $context["extension"] : null), "author"), "html", null, true);
            echo "</a>
            </td>
            <td>";
            // line 25
            echo twig_escape_filter($this->env, twig_truncate_filter($this->env, $this->getAttribute((isset($context["extension"]) ? $context["extension"] : null), "description"), 150), "html", null, true);
            echo "</td>
            <td class=\"actions\">
                <a class=\"bb-button btn14 api-link\" data-api-confirm=\"By installing '";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["extension"]) ? $context["extension"] : null), "name"), "html", null, true);
            echo "' you agree with terms and conditions. Install only extensions you trust. Continue?\" data-api-reload=\"1\" href=\"";
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/extension/install", array("type" => $this->getAttribute((isset($context["extension"]) ? $context["extension"] : null), "type"), "id" => $this->getAttribute((isset($context["extension"]) ? $context["extension"] : null), "id")));
            echo "\" title=\"Install extension\"><img src=\"images/icons/dark/cog.png\" alt=\"\"></a>
                <a class=\"bb-button btn14\" href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["extension"]) ? $context["extension"] : null), "project_url"), "html", null, true);
            echo "\" target=\"_blank\" title=\"Details\"><img src=\"images/icons/dark/globe.png\" alt=\"\"></a>
                <a class=\"bb-button btn14\" href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["extension"]) ? $context["extension"] : null), "download_url"), "html", null, true);
            echo "\" title=\"Download ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["extension"]) ? $context["extension"] : null), "name"), "html", null, true);
            echo "\"><img src=\"images/icons/dark/download.png\" alt=\"\"></a>
            </td>
        </tr>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 33
            echo "        <tr>
            <td colspan=\"4\" class=\"aligncenter\"><a href=\"http://extensions.boxbilling.com/\" target=\"_blank\">Explore BoxBilling extensions</a></td>
        </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['extension'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "    </tbody>
    <tfoot>
        <tr>
            <td colspan=\"4\"></td>
        </tr>
    </tfoot>

</table>";
    }

    public function getTemplateName()
    {
        return "partial_extensions.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 37,  91 => 29,  87 => 28,  81 => 27,  76 => 25,  69 => 23,  61 => 22,  51 => 19,  45 => 18,  43 => 17,  30 => 6,  24 => 3,  21 => 2,  19 => 1,  620 => 278,  616 => 276,  607 => 272,  605 => 271,  600 => 268,  589 => 262,  584 => 260,  581 => 259,  576 => 258,  568 => 253,  564 => 252,  559 => 249,  557 => 248,  544 => 242,  540 => 241,  536 => 240,  530 => 239,  526 => 238,  519 => 234,  515 => 233,  512 => 232,  510 => 231,  501 => 225,  494 => 221,  489 => 219,  480 => 213,  475 => 211,  467 => 206,  462 => 204,  453 => 198,  448 => 196,  442 => 193,  436 => 190,  432 => 189,  422 => 181,  412 => 177,  408 => 176,  402 => 175,  397 => 173,  394 => 172,  390 => 171,  382 => 166,  373 => 160,  369 => 159,  357 => 150,  349 => 145,  345 => 144,  340 => 142,  331 => 136,  327 => 135,  322 => 133,  313 => 127,  309 => 126,  304 => 124,  293 => 116,  282 => 108,  271 => 100,  260 => 92,  252 => 86,  241 => 84,  237 => 83,  231 => 80,  221 => 73,  215 => 70,  209 => 67,  205 => 66,  195 => 58,  185 => 54,  179 => 53,  170 => 51,  167 => 50,  159 => 49,  149 => 48,  139 => 47,  134 => 45,  130 => 44,  126 => 43,  122 => 42,  119 => 41,  115 => 40,  106 => 34,  102 => 33,  98 => 32,  94 => 31,  90 => 30,  86 => 29,  77 => 23,  73 => 22,  62 => 14,  58 => 13,  54 => 20,  50 => 11,  46 => 10,  40 => 6,  37 => 5,  31 => 2,  26 => 3,);
    }
}
