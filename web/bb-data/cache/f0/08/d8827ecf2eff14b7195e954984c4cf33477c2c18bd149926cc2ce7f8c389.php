<?php

/* mod_formbuilder_preview.phtml */
class __TwigTemplate_f008d8827ecf2eff14b7195e954984c4cf33477c2c18bd149926cc2ce7f8c389 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"rowElem preview\">
    <label for=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name"), "html", null, true);
        echo "\">";
        if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "hide_label") != 1)) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "label"), "html", null, true);
        }
        echo "</label>

    <div class=\"formRight\">
        ";
        // line 5
        if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "text")) {
            // line 6
            echo "        <div class=\"moreFields allUpper\">
            <ul>
                <li class=\"sep\">";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "prefix"), "html", null, true);
            echo "</li>
                <li style=\"width: 50%\">
                    <input type=\"text\" name=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name"), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "default_value"), "html", null, true);
            echo "\"  id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name"), "html", null, true);
            echo "\"
                        ";
            // line 11
            echo ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "readonly") == 1)) ? ("readonly=\"readonly\"") : (""));
            echo "
                    />
                </li>
                <li class=\"sep\">";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "suffix"), "html", null, true);
            echo "</li>
            </ul>
        </div>
        ";
        } elseif (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "select")) {
            // line 18
            echo "            ";
            if (twig_test_empty($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"))) {
                // line 19
                echo "            <ul>
                <li style=\"width: 80%\"><blockquote>";
                // line 20
                echo gettext("Please click on \"Edit\" in order to add new select box");
                echo "</blockquote></li>
            </ul>
            ";
            } else {
                // line 23
                echo "                <select name=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name"), "html", null, true);
                echo "\" required=\"required\"   id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name"), "html", null, true);
                echo "\"
                    ";
                // line 24
                echo ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "readonly") == 1)) ? ("disabled=\"disabled\"") : (""));
                echo "
                    />
                    ";
                // line 26
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"));
                foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                    // line 27
                    echo "                    <option value=\"";
                    echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                    echo "\"
                    ";
                    // line 28
                    echo ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "default_value") == (isset($context["v"]) ? $context["v"] : null))) ? ("selected") : (""));
                    echo ">";
                    echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                    echo "</option>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 30
                echo "
                </select>
            ";
            }
            // line 33
            echo "        ";
        } elseif (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "checkbox")) {
            // line 34
            echo "            ";
            if (twig_test_empty($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"))) {
                // line 35
                echo "            <ul>
                <li style=\"width: 80%\"><blockquote>";
                // line 36
                echo gettext("Please click on \"Edit\" in order to add new checkbox");
                echo "</blockquote></li>
            </ul>
            ";
            } else {
                // line 39
                echo "                ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"));
                foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                    // line 40
                    echo "                <input type=\"checkbox\" name=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name"), "html", null, true);
                    echo "\" value=\"";
                    echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                    echo "\" id=\"";
                    echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                    echo "_";
                    echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                    echo "\" ";
                    if (twig_in_filter((isset($context["v"]) ? $context["v"] : null), $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "default_value"))) {
                        echo "checked=\"checked\"";
                    }
                    // line 41
                    echo "                    ";
                    echo ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "readonly") == 1)) ? ("disabled=\"disabled\"") : (""));
                    echo "
            />
                <label for=\"";
                    // line 43
                    echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                    echo "_";
                    echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                    echo "</label>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 45
                echo "            ";
            }
            // line 46
            echo "        ";
        } elseif (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "radio")) {
            // line 47
            echo "            ";
            if (twig_test_empty($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"))) {
                // line 48
                echo "                <ul>
                    <li style=\"width: 80%\"><blockquote>";
                // line 49
                echo gettext("Please click on \"Edit\" in order to add new radio box");
                echo "</blockquote></li>
                </ul>
            ";
            } else {
                // line 52
                echo "                ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"));
                foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                    // line 53
                    echo "                <input type=\"radio\" name=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name"), "html", null, true);
                    echo "\" value=\"";
                    echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                    echo "\" id=\"";
                    echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                    echo "_";
                    echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                    echo "\" ";
                    echo ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "default_value") == (isset($context["v"]) ? $context["v"] : null))) ? ("checked") : (""));
                    echo "
                    ";
                    // line 54
                    echo ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "readonly") == 1)) ? ("disabled=\"disabled\"") : (""));
                    echo "
            />
                <label for=\"";
                    // line 56
                    echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                    echo "_";
                    echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                    echo "</label>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 58
                echo "            ";
            }
            // line 59
            echo "        ";
        } elseif (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "textarea")) {
            // line 60
            echo "            <div style=\"width: auto; max-width:50%;\">
                <textarea name=\"";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name"), "html", null, true);
            echo "\" style=\"width: ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"), "width"), "html", null, true);
            echo "px; height: ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"), "height"), "html", null, true);
            echo "px; overflow: scroll;\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "default_value"), "html", null, true);
            echo "</textarea>
            </div>
        ";
        }
        // line 64
        echo "        </div>
        <div class=\"fix\"></div>

    <div class=\"actions\" style=\"right: 0; bottom: 0; padding: 5px; position: absolute;\">
<!--        <a class=\"btn14 pr btnIconLeft\" href=\"#\" style=\"display: none;\"><img src=\"images/icons/dark/preview.png\" alt=\"Preview\" class=\"icon\">Preview</a>-->
        <a class=\"btn14 ed\" href=\"#\"><img src=\"images/icons/dark/pencil.png\" alt=\"Edit\" class=\"icon\"></a>
        <a class=\"btn14 rm\" href=\"#\" data-field-id=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "id"), "html", null, true);
        echo "\"><img src=\"images/icons/dark/trash.png\" alt=\"\" class=\"icon\"></a>
    </div>
    <div class=\"fix\"></div>

</div>";
    }

    public function getTemplateName()
    {
        return "mod_formbuilder_preview.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 70,  225 => 61,  222 => 60,  219 => 59,  216 => 58,  199 => 54,  181 => 52,  172 => 48,  169 => 47,  166 => 46,  163 => 45,  151 => 43,  145 => 41,  132 => 40,  121 => 36,  118 => 35,  112 => 33,  107 => 30,  92 => 27,  88 => 26,  76 => 23,  70 => 20,  64 => 18,  57 => 14,  51 => 11,  43 => 10,  38 => 8,  32 => 5,  22 => 2,  481 => 224,  477 => 223,  473 => 222,  464 => 216,  453 => 208,  445 => 203,  437 => 197,  427 => 194,  424 => 193,  420 => 191,  410 => 189,  405 => 188,  401 => 187,  397 => 185,  394 => 184,  382 => 182,  369 => 181,  364 => 180,  361 => 179,  349 => 177,  336 => 176,  331 => 175,  326 => 173,  322 => 172,  317 => 171,  315 => 170,  308 => 166,  304 => 164,  294 => 157,  288 => 154,  283 => 152,  279 => 151,  273 => 148,  268 => 146,  260 => 141,  257 => 140,  255 => 139,  242 => 129,  237 => 64,  231 => 124,  226 => 122,  218 => 117,  215 => 116,  213 => 115,  204 => 56,  200 => 108,  195 => 106,  191 => 105,  186 => 53,  182 => 102,  175 => 49,  171 => 96,  155 => 85,  138 => 70,  127 => 39,  120 => 61,  115 => 34,  111 => 57,  104 => 53,  97 => 28,  86 => 43,  83 => 24,  81 => 41,  67 => 19,  58 => 24,  50 => 18,  48 => 17,  42 => 14,  34 => 6,  24 => 2,  19 => 1,);
    }
}
