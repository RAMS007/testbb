<?php

/* mod_serviceopenvpn_settings.phtml */
class __TwigTemplate_89de6fea33968c7717564d25482056f4a54be99f13af8aa9f368bc883aa3c9dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'breadcrumbs' => array($this, 'block_breadcrumbs'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ajax")) ? ("layout_blank.phtml") : ("layout_default.phtml")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["active_menu"] = "system";
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_meta_title($context, array $blocks = array())
    {
        echo gettext("OpenVPN");
    }

    // line 4
    public function block_breadcrumbs($context, array $blocks = array())
    {
        // line 5
        echo "    <ul>
        <li class=\"firstB\"><a href=\"";
        // line 6
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("/");
        echo "\">";
        echo gettext("Home");
        echo "</a></li>
        <li><a href=\"";
        // line 7
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("system");
        echo "\">";
        echo gettext("Settings");
        echo "</a></li>
        <li class=\"lastB\">";
        // line 8
        echo gettext("OpenVPN");
        echo "</li>
    </ul>
";
    }

    // line 12
    public function block_content($context, array $blocks = array())
    {
        // line 13
        $context["params"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "serviceopenvpn_cluster_config");
        // line 14
        echo "
";
        // line 15
        if ($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "ipaddress")) {
            // line 16
            echo "    ";
            $context["master_url"] = (((("http://" . $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "ipaddress")) . ":") . (($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "port", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "port"), 5353)) : (5353))) . "/admincp");
        }
        // line 18
        echo "
<div class=\"widget simpleTabs\">

    <ul class=\"tabs\">
        <li><a href=\"#tab-start\">";
        // line 22
        echo gettext("OpenVPN");
        echo "</a></li>
        <li><a href=\"#tab-index\" id=\"open-index-tab\">";
        // line 23
        echo gettext("API configuration");
        echo "</a></li>
    </ul>

    <div class=\"tabs_container\">
        <div class=\"fix\"></div>
        
        <div class=\"tab_content nopadding\" id=\"tab-start\">
            <div class=\"help\">
                <img src=\"";
        // line 31
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("bb-modules/serviceopenvpn/icon.png");
        echo "\" alt=\"openvpn\" class=\"\" style=\"width: 150px; float: right; margin: 0 5px 0 10px;\"/>
                <h5> ";
        // line 32
        echo gettext("Getting started with OpenVPN");
        echo "</h5>
                <p><b> bb-modules/Serviceopenvpn/html_admin/mod_serviceopenvpn_settings.phtml:33 </b>  </p>
            </div>

            <div class=\"body\">
                <h2>";
        // line 37
        echo gettext("1. Configure API");
        echo "</h2>
                <p><a href=\"\" onclick=\"\$('#open-index-tab').click(); return false;\">Configure OpenVPN API</a> and test if BoxBilling can connect successfully.</p>
            </div>
            
            <div class=\"body\">    
                <h2>";
        // line 42
        echo gettext("2. Create new product category");
        echo "</h2>
                <p>Create <a href=\"";
        // line 43
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("product", array("cat_title" => "VPS", "cat_description" => "Virtual private servers"));
        echo "#tab-new-category\" target=\"_blank\">new product category</a> for VPS products. Category must contain only openvpn products otherwise order form might not work as expected.</p>
            </div>
            
            <div class=\"body list arrowBlue\">
                <h2>";
        // line 47
        echo gettext("3. Create OpenVPN products");
        echo "</h2>
                <p>Imagine that product on BoxBilling is OpenVPN plan.</p>
                <p><a href=\"";
        // line 49
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("product", array("type" => "openvpn", "title" => "VPS-1"));
        echo "#tab-new\" target=\"_blank\">Create products</a> in newly created category by selecting product type <strong>OpenVpn</strong>.</p>
                <p>Define product prices and switch to \"Configuration\" tab:</p>
                <ul>
                    <li>Select virtualization type</li>
                    <li>Select node on which VPS will be created</li>
                    <li>Select plan</li>
                    <li>Define IPs amount for VPS server</li>
                    <li>Provide information about plan to BoxBilling so it can show these values to your clients. (OpenVPN API does not provide this information)</li>
                    <li>Provide slider information to be displayed on order form</li>
                </ul>
                <p><a href=\"";
        // line 59
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("order");
        echo "\" target=\"_blank\">Check order page</a> while creating products to see slider changes</p>
            </div>
          <!--
            <div class=\"body\">
                <h2>";
        // line 63
        echo gettext("4. Import data from OpenVPN server");
        echo "</h2>
                <p>If you already have successfully connected to master server and have existing clients and server then you can import them using these tools:</p>
                ";
        // line 65
        if ((isset($context["master_url"]) ? $context["master_url"] : null)) {
            // line 66
            echo "                    <a href=\"";
            echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("serviceopenvpn/import/clients");
            echo "\" title=\"\" class=\"btnIconLeft mr10 mt10\"><span>5.1 Import clients</span></a>
                    <a href=\"";
            // line 67
            echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("serviceopenvpn/import/servers", array("node_id" => 1));
            echo "\" title=\"\" class=\"btnIconLeft mr10 mt10\"><span>5.2 Import servers</span></a>
                ";
        } else {
            // line 69
            echo "                    <p>Configure API before importing data. Refresh this page if you already confugured API.</p>
                ";
        }
        // line 71
        echo "            </div> -->
        </div>
        
        <div class=\"tab_content nopadding\" id=\"tab-index\">
            <div class=\"help\">
                <h5>";
        // line 76
        echo gettext("Connect to OpenVPN master server via API");
        echo "</h5>
            </div>
            
            <div class=\"body\">
                <div class=\"nNote nInformation first\">
                    <p><strong>";
        // line 81
        echo gettext("INFORMATION");
        echo ": </strong>You need to specify IP <strong>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "system_env"), "ip"), "html", null, true);
        echo "</strong> in the OpenVPN API user.</p>
                </div>
            </div>
            
            <form method=\"post\" action=\"";
        // line 85
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/cluster_config_update");
        echo "\" class=\"mainForm api-form\" data-api-msg=\"";
        echo gettext("Configuration updated");
        echo "\">
                <fieldset>
                    <div class=\"rowElem noborder\">
                        <label>";
        // line 88
        echo gettext("ID");
        echo ":</label>
                        <div class=\"formRight\">
                            <input type=\"text\" name=\"id\" value=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "id"), "html", null, true);
        echo "\" required=\"required\" placeholder=\"Example: t5kGu4EZqAyRqh0ZlN2O\"/>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    
                    <div class=\"rowElem\">
                        <label>";
        // line 96
        echo gettext("Key");
        echo ":</label>
                        <div class=\"formRight\">
                            <input type=\"password\" name=\"key\" value=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "key"), "html", null, true);
        echo "\" required=\"required\" placeholder=\"Example: XzTftPQTxEwRxHmTARIS\"/>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    
                    <div class=\"rowElem\">
                        <label>";
        // line 104
        echo gettext("Master server IP");
        echo ":</label>
                        <div class=\"formRight\">
                            <input type=\"text\" name=\"ipaddress\" value=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "ipaddress"), "html", null, true);
        echo "\" required=\"required\" placeholder=\"Example: 123.123.123.123\"/>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    
                    <div class=\"rowElem\">
                        <label>";
        // line 112
        echo gettext("Secure connection");
        echo ":</label>
                        <div class=\"formRight\">
                            <input type=\"radio\" name=\"secure\" value=\"1\" ";
        // line 114
        if (($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "secure") == "1")) {
            echo "checked=\"checked\"";
        }
        echo "/><label>";
        echo gettext("Yes");
        echo "</label>
                            <input type=\"radio\" name=\"secure\" value=\"0\" ";
        // line 115
        if ((!$this->getAttribute((isset($context["params"]) ? $context["params"] : null), "secure"))) {
            echo "checked=\"checked\"";
        }
        echo "/><label>";
        echo gettext("No");
        echo "</label>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    
                    <div class=\"rowElem\">
                        <label>";
        // line 121
        echo gettext("Custom port");
        echo ":</label>
                        <div class=\"formRight\">
                            <input type=\"text\" name=\"port\" value=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "port"), "html", null, true);
        echo "\" placeholder=\"";
        echo gettext("Leave blank to use default port 5353 and 5656 for secure connection");
        echo "\"/>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    
                    <input type=\"hidden\" name=\"cluster_id\" value=\"1\" />
                    <input type=\"button\" value=\"2. ";
        // line 129
        echo gettext("Test connection");
        echo "\" class=\"greyishBtn submitForm\" id=\"testconnection\"/>
                    <input type=\"submit\" value=\"1. ";
        // line 130
        echo gettext("Update");
        echo "\" class=\"greyishBtn submitForm\" />
                    
                </fieldset>
            </form>

            <div class=\"fix\"></div>

        </div>

    </div>
</div>
";
    }

    // line 143
    public function block_js($context, array $blocks = array())
    {
        // line 145
        echo "<script type=\"text/javascript\">
\$(function() {
    \$('#testconnection').click(function(){
        bb.post('admin/serviceopenvpn/test_connection', null, function(result){
            bb.msg('Successfully connected to OpenVPN server');
        });
        return false;
    });
});
</script>
";
    }

    public function getTemplateName()
    {
        return "mod_serviceopenvpn_settings.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  313 => 145,  310 => 143,  294 => 130,  290 => 129,  279 => 123,  274 => 121,  261 => 115,  253 => 114,  248 => 112,  239 => 106,  234 => 104,  225 => 98,  220 => 96,  211 => 90,  206 => 88,  198 => 85,  189 => 81,  181 => 76,  174 => 71,  170 => 69,  165 => 67,  160 => 66,  158 => 65,  153 => 63,  146 => 59,  133 => 49,  128 => 47,  121 => 43,  117 => 42,  109 => 37,  101 => 32,  97 => 31,  86 => 23,  82 => 22,  76 => 18,  72 => 16,  70 => 15,  67 => 14,  65 => 13,  62 => 12,  55 => 8,  49 => 7,  43 => 6,  40 => 5,  37 => 4,  31 => 3,  26 => 2,);
    }
}
