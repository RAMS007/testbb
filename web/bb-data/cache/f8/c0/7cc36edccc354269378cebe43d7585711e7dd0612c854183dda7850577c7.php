<?php

/* mod_serviceopenvpn_manage.phtml */
class __TwigTemplate_f8c07cc36edccc354269378cebe43d7585711e7dd0612c854183dda7850577c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["node_online"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "serviceopenvpn_test_connection", array(0 => array("return" => "bool")), "method");
        // line 2
        echo "
";
        // line 3
        if (((isset($context["node_online"]) ? $context["node_online"] : null) && $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "vserverid"))) {
            // line 4
            $context["info"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "serviceopenvpn_info", array(0 => array("order_id" => $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id"))), "method");
            // line 5
            echo "
<div class=\"help\">
    <h2>";
            // line 7
            echo gettext("Details");
            echo "</h2>
    bb-modules/Serviceopenvpn/html_admin/mod_serviceopenvpn_manage.phtml:9
</div>

<table class=\"tableStatic wide\">
    <tbody>
        <tr class=\"noborder\">
            <td style=\"width: 30%\">";
            // line 14
            echo gettext("State");
            echo ":</td>
            <td>";
            // line 15
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "state")), "html", null, true);
            echo "</td>
        </tr>

        <tr>
            <td>";
            // line 19
            echo gettext("Hostname");
            echo ":</td>
            <td>
                <a target=\"_blank\" href=\"http://";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "hostname"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "hostname"), "html", null, true);
            echo "</a>
            </td>
        </tr>

        <tr>
            <td>";
            // line 26
            echo gettext("Type");
            echo ":</td>
            <td>";
            // line 27
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "type")), "html", null, true);
            echo "</td>
        </tr>
        
        <tr>
            <td>";
            // line 31
            echo gettext("Node / ID / VMID");
            echo ":</td>
            <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "node"), "html", null, true);
            echo " / ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "vserverid"), "html", null, true);
            echo " / ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "virtid"), "html", null, true);
            echo " </td>
        </tr>
        
        <tr>
            <td>";
            // line 36
            echo gettext("Operating System");
            echo ":</td>
            <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "template"), "html", null, true);
            echo "</td>
        </tr>
        
        <tr>
            <td>";
            // line 41
            echo gettext("IP address");
            echo ":</td>
            <td><strong>";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "mainipaddress"), "html", null, true);
            echo "</strong></td>
        </tr>
        
        <tr>
            <td>";
            // line 46
            echo gettext("Additional IP addresses");
            echo ":</td>
            <td>";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "ipaddresses"), "html", null, true);
            echo "</td>
        </tr>
        
        <tr>
            <td>";
            // line 51
            echo gettext("Root password");
            echo ":</td>
            <td>";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "rootpassword"), "html", null, true);
            echo "</td>
        </tr>
        
        <tr>
            <td>";
            // line 56
            echo gettext("Client area username / password");
            echo ":</td>
            <td>";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "username"), "html", null, true);
            echo " / ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "password"), "html", null, true);
            echo "</td>
        </tr>
        
        <tr>
            <td>";
            // line 61
            echo gettext("Console user / password");
            echo ":</td>
            <td>";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "consoleuser"), "html", null, true);
            echo " / ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "consolepassword"), "html", null, true);
            echo "</td>
        </tr>

    </tbody>
    <tfoot>
        <tr>
            <td colspan=\"2\">
                <div class=\"aligncenter\">
                    <a href=\"";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "master_url"), "html", null, true);
            echo "\" target=\"_blank\" class=\"btn55 mr10\"><img src=\"images/icons/middlenav/user2.png\" alt=\"\"><span>";
            echo gettext("Client area");
            echo "</span></a>
                    <a href=\"";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "master_url"), "html", null, true);
            echo "/admincp\" target=\"_blank\" class=\"btn55 mr10\"><img src=\"images/icons/middlenav/adminUser2.png\" alt=\"\"><span>";
            echo gettext("Admin area");
            echo "</span></a>
                    <a href=\"";
            // line 72
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/status", array("order_id" => $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id")));
            echo "\" data-api-jsonp=\"onAfterStatusCheck\" class=\"btn55 mr10 api-link\"><img src=\"images/icons/middlenav/transfer.png\" alt=\"\"><span>Check Status</span></a>
                    <a href=\"";
            // line 73
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/reboot", array("order_id" => $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id")));
            echo "\" data-api-confirm=\"Are you sure?\" data-api-msg=\"VPS rebooted\" class=\"btn55 mr10 api-link\"><img src=\"images/icons/middlenav/refresh4.png\" alt=\"\"><span>Reboot</span></a>
                    <a href=\"";
            // line 74
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/boot", array("order_id" => $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id")));
            echo "\" data-api-confirm=\"Are you sure?\" data-api-msg=\"VPS booted\" class=\"btn55 mr10 api-link\"><img src=\"images/icons/middlenav/electroPlug.png\" alt=\"\"><span>Boot</span></a>
                    <a href=\"";
            // line 75
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/shutdown", array("order_id" => $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id")));
            echo "\" data-api-confirm=\"Are you sure?\" data-api-msg=\"VPS shutdown\" class=\"btn55 mr10 api-link\"><img src=\"images/icons/middlenav/logoff.png\" alt=\"\"><span>Shutdown</span></a>
                    <a href=\"";
            // line 76
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/addip", array("order_id" => $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id")));
            echo "\" data-api-confirm=\"Are you sure?\" data-api-msg=\"IP added\" class=\"btn55 mr10 api-link\"><img src=\"images/icons/middlenav/add.png\" alt=\"\"><span>Add IP</span></a>
                    <hr class=\"sep\">
                    <a href=\"";
            // line 78
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/pae_disable", array("order_id" => $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id")));
            echo "\" data-api-confirm=\"Are you sure?\" data-api-msg=\"Disabled PAE\" class=\"btn55 mr10 api-link\"><img src=\"images/icons/middlenav/switcher.png\" alt=\"\"><span>Disable PAE</span></a>
                    <a href=\"";
            // line 79
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/pae_enable", array("order_id" => $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id")));
            echo "\" data-api-confirm=\"Are you sure?\" data-api-msg=\"Enabled PAE\" class=\"btn55 mr10 api-link\"><img src=\"images/icons/middlenav/switcher.png\" alt=\"\"><span>Enable PAE</span></a>
                    <a href=\"";
            // line 80
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/network_disable", array("order_id" => $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id")));
            echo "\" data-api-confirm=\"Are you sure?\" data-api-msg=\"Network disabled\" class=\"btn55 mr10 api-link\"><img src=\"images/icons/middlenav/switcher.png\" alt=\"\"><span>Disable network</span></a>
                    <a href=\"";
            // line 81
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/network_enable", array("order_id" => $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id")));
            echo "\" data-api-confirm=\"Are you sure?\" data-api-msg=\"Network enabled\" class=\"btn55 mr10 api-link\"><img src=\"images/icons/middlenav/switcher.png\" alt=\"\"><span>Enable network</span></a>
                    <a href=\"";
            // line 82
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/tun_disable", array("order_id" => $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id")));
            echo "\" data-api-confirm=\"Are you sure?\" data-api-msg=\"TUN disabled\" class=\"btn55 mr10 api-link\"><img src=\"images/icons/middlenav/switcher.png\" alt=\"\"><span>Disable TUN</span></a>
                    <a href=\"";
            // line 83
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/tun_enable", array("order_id" => $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id")));
            echo "\" data-api-confirm=\"Are you sure?\" data-api-msg=\"TUN enabled\" class=\"btn55 mr10 api-link\"><img src=\"images/icons/middlenav/switcher.png\" alt=\"\"><span>Enable TUN</span></a>
                </div>
            </td>
        </tr>
    </tfoot>
</table>

<div class=\"help\">
    <h5>";
            // line 91
            echo gettext("Change root password");
            echo "</h5>
</div>

<form method=\"post\" action=\"";
            // line 94
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/set_root_password");
            echo "\" class=\"mainForm api-form save\" data-api-msg=\"Root password changed\">
    <fieldset>
        <div class=\"rowElem noborder\">
            <label>";
            // line 97
            echo gettext("New password");
            echo ":</label>
            <div class=\"formRight\">
                <input type=\"text\" name=\"password\" value=\"\" placeholder=\"Enter new password\"/>
            </div>
            <div class=\"fix\"></div>
        </div>
        <input type=\"hidden\" name=\"order_id\" value=\"";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id"), "html", null, true);
            echo "\" />
        <input type=\"submit\" value=\"";
            // line 104
            echo gettext("Change");
            echo "\" class=\"greyishBtn submitForm\" />
    </fieldset>
</form>

<div class=\"help\">
    <h5>";
            // line 109
            echo gettext("Change hostname");
            echo "</h5>
</div>

<form method=\"post\" action=\"";
            // line 112
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/set_hostname");
            echo "\" class=\"mainForm api-form save\" data-api-msg=\"Hostname changed\">
    <fieldset>
        <div class=\"rowElem noborder\">
            <label>";
            // line 115
            echo gettext("New hostname");
            echo ":</label>
            <div class=\"formRight\">
                <input type=\"text\" name=\"hostname\" value=\"\" placeholder=\"Enter new hostname\"/>
            </div>
            <div class=\"fix\"></div>
        </div>
        <input type=\"hidden\" name=\"order_id\" value=\"";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id"), "html", null, true);
            echo "\" />
        <input type=\"submit\" value=\"";
            // line 122
            echo gettext("Change");
            echo "\" class=\"greyishBtn submitForm\" />
    </fieldset>
</form>

<div class=\"help\">
    <h5>";
            // line 127
            echo gettext("Change plan");
            echo "</h5>
</div>

<form method=\"post\" action=\"";
            // line 130
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/set_plan");
            echo "\" class=\"mainForm api-form save\" data-api-msg=\"Plan changed\">
    <fieldset>
        <div class=\"rowElem noborder\">
            <label>";
            // line 133
            echo gettext("New plan");
            echo ":</label>
            <div class=\"formRight\">
                ";
            // line 135
            $context["plans"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "serviceopenvpn_get_plans", array(0 => array("type" => $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "type"))), "method");
            // line 136
            echo "                ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mf"]) ? $context["mf"] : null), "selectbox", array(0 => "plan", 1 => (isset($context["plans"]) ? $context["plans"] : null), 2 => $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "plan"), 3 => 1, 4 => "Select plan"), "method"), "html", null, true);
            echo "
            </div>
            <div class=\"fix\"></div>
        </div>
        <input type=\"hidden\" name=\"order_id\" value=\"";
            // line 140
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id"), "html", null, true);
            echo "\" />
        <input type=\"submit\" value=\"";
            // line 141
            echo gettext("Change");
            echo "\" class=\"greyishBtn submitForm\" />
    </fieldset>
</form>

<div class=\"help\">
    <h5>";
            // line 146
            echo gettext("Rebuild");
            echo "</h5>
</div>

<form method=\"post\" action=\"";
            // line 149
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/rebuild");
            echo "\" class=\"mainForm api-form save\" data-api-msg=\"VPS rebuilt\">
    <fieldset>
        <div class=\"rowElem noborder\">
            <label>";
            // line 152
            echo gettext("New template");
            echo ":</label>
            <div class=\"formRight\">
                ";
            // line 154
            $context["plans"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "serviceopenvpn_get_templates", array(0 => array("type" => $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "type"))), "method");
            // line 155
            echo "                ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mf"]) ? $context["mf"] : null), "selectbox", array(0 => "template", 1 => (isset($context["plans"]) ? $context["plans"] : null), 2 => $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "template"), 3 => 1, 4 => "Select template"), "method"), "html", null, true);
            echo "
            </div>
            <div class=\"fix\"></div>
        </div>
        <input type=\"hidden\" name=\"order_id\" value=\"";
            // line 159
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id"), "html", null, true);
            echo "\" />
        <input type=\"submit\" value=\"";
            // line 160
            echo gettext("Rebuild");
            echo "\" class=\"greyishBtn submitForm\" />
    </fieldset>
</form>

";
            // line 164
            if (($this->getAttribute((isset($context["info"]) ? $context["info"] : null), "state") != "disabled")) {
                // line 165
                echo "<div class=\"help\">
    <h5>";
                // line 166
                echo gettext("Usage statistics");
                echo "</h5>
</div>

<div class=\"body\" style=\"width: 210px; float: left;\">
    ";
                // line 170
                $context["bw"] = twig_split_filter($this->getAttribute((isset($context["info"]) ? $context["info"] : null), "bandwidth"), ",");
                // line 171
                echo "    <table class=\"tableStatic wide with-border\">
        <caption>";
                // line 172
                echo gettext("Bandwidth");
                echo "</caption>
        <tbody>
            <tr>
                <td>";
                // line 175
                echo gettext("Total");
                echo ":</td>
                <td>";
                // line 176
                echo twig_escape_filter($this->env, twig_size_filter($this->getAttribute((isset($context["bw"]) ? $context["bw"] : null), 0, array(), "array")), "html", null, true);
                echo "</td>
            </tr>
            <tr>
                <td>";
                // line 179
                echo gettext("Used");
                echo ":</td>
                <td>";
                // line 180
                echo twig_escape_filter($this->env, twig_size_filter($this->getAttribute((isset($context["bw"]) ? $context["bw"] : null), 1, array(), "array")), "html", null, true);
                echo "</td>
            </tr>
            <tr>
                <td>";
                // line 183
                echo gettext("Free");
                echo ":</td>
                <td>";
                // line 184
                echo twig_escape_filter($this->env, twig_size_filter($this->getAttribute((isset($context["bw"]) ? $context["bw"] : null), 2, array(), "array")), "html", null, true);
                echo "</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan=\"2\">
                    <div class=\"progressbar\" style=\"border: 1px solid #DDD; padding: 2px;\">
                        <div style=\"background-color: #82d453; width: ";
                // line 191
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bw"]) ? $context["bw"] : null), 3, array(), "array"), "html", null, true);
                echo "%; height: 20px;\"></div>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
        
<div class=\"body\" style=\"width: 215px; float: left;\">
    ";
                // line 200
                $context["ram"] = twig_split_filter($this->getAttribute((isset($context["info"]) ? $context["info"] : null), "memory"), ",");
                // line 201
                echo "    <table class=\"tableStatic wide with-border\">
        <caption>";
                // line 202
                echo gettext("Memory");
                echo "</caption>
        <tbody>
            <tr>
                <td>";
                // line 205
                echo gettext("Total");
                echo ":</td>
                <td>";
                // line 206
                echo twig_escape_filter($this->env, twig_size_filter($this->getAttribute((isset($context["ram"]) ? $context["ram"] : null), 0, array(), "array")), "html", null, true);
                echo "</td>
            </tr>
            <tr>
                <td>";
                // line 209
                echo gettext("Used");
                echo ":</td>
                <td>";
                // line 210
                echo twig_escape_filter($this->env, twig_size_filter($this->getAttribute((isset($context["ram"]) ? $context["ram"] : null), 1, array(), "array")), "html", null, true);
                echo "</td>
            </tr>
            <tr>
                <td>";
                // line 213
                echo gettext("Free");
                echo ":</td>
                <td>";
                // line 214
                echo twig_escape_filter($this->env, twig_size_filter($this->getAttribute((isset($context["ram"]) ? $context["ram"] : null), 2, array(), "array")), "html", null, true);
                echo "</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan=\"2\">
                    <div class=\"progressbar\" style=\"border: 1px solid #DDD; padding: 2px;\">
                        <div style=\"background-color: #82d453; width: ";
                // line 221
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ram"]) ? $context["ram"] : null), 3, array(), "array"), "html", null, true);
                echo "%; height: 20px;\"></div>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
</div>

<div class=\"body\" style=\"width: 215px; float: left;\">
    ";
                // line 230
                $context["hdd"] = twig_split_filter($this->getAttribute((isset($context["info"]) ? $context["info"] : null), "hdd"), ",");
                // line 231
                echo "    <table class=\"tableStatic wide with-border\">
        <caption>";
                // line 232
                echo gettext("Disk usage");
                echo "</caption>
        <tbody>
            <tr>
                <td>";
                // line 235
                echo gettext("Total");
                echo ":</td>
                <td>";
                // line 236
                echo twig_escape_filter($this->env, twig_size_filter($this->getAttribute((isset($context["hdd"]) ? $context["hdd"] : null), 0, array(), "array")), "html", null, true);
                echo "</td>
            </tr>
            <tr>
                <td>";
                // line 239
                echo gettext("Used");
                echo ":</td>
                <td>";
                // line 240
                echo twig_escape_filter($this->env, twig_size_filter($this->getAttribute((isset($context["hdd"]) ? $context["hdd"] : null), 1, array(), "array")), "html", null, true);
                echo "</td>
            </tr>
            <tr>
                <td>";
                // line 243
                echo gettext("Free");
                echo ":</td>
                <td>";
                // line 244
                echo twig_escape_filter($this->env, twig_size_filter($this->getAttribute((isset($context["hdd"]) ? $context["hdd"] : null), 2, array(), "array")), "html", null, true);
                echo "</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan=\"2\">
                    <div class=\"progressbar\" style=\"border: 1px solid #DDD; padding: 2px;\">
                        <div style=\"background-color: #82d453; width: ";
                // line 251
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["hdd"]) ? $context["hdd"] : null), 3, array(), "array"), "html", null, true);
                echo "%; height: 20px;\"></div>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
</div>

<div class=\"clear\"></div>

<div class=\"help\">
    <h5>";
                // line 262
                echo gettext("Traffic");
                echo "</h5>
</div>

<div class=\"body aligncenter\">
    <img src=\"";
                // line 266
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "master_url"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "trafficgraph"), "html", null, true);
                echo "\" alt=\"\">
</div>

<div class=\"help\">
    <h5>";
                // line 270
                echo gettext("Load");
                echo "</h5>
</div>
<div class=\"body aligncenter\">
    <img src=\"";
                // line 273
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "master_url"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "loadgraph"), "html", null, true);
                echo "\" alt=\"\">
</div>
<div class=\"help\">
    <h5>";
                // line 276
                echo gettext("Memory");
                echo "</h5>
</div>
<div class=\"body aligncenter\">
    <img src=\"";
                // line 279
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "master_url"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "memorygraph"), "html", null, true);
                echo "\" alt=\"\">
</div>

";
            }
            // line 283
            echo "
";
        } else {
            // line 285
            echo "<div class=\"body\">
    <div class=\"nNote nWarning first\">
        <p><strong>";
            // line 287
            echo gettext("INFORMATION");
            echo ": </strong>";
            echo gettext("Master node is offline, order is not yet activated or API is not configured properly.");
            echo "</p>
    </div>
</div>
";
        }
        // line 291
        echo "
<div class=\"help\">
    <h5>";
        // line 293
        echo gettext("Service management");
        echo "</h5>
    <p>";
        // line 294
        echo gettext("Changing settings does not send actions to OpenVPN server.");
        echo "</p>
</div>

<form method=\"post\" action=\"";
        // line 297
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/serviceopenvpn/update");
        echo "\" class=\"mainForm api-form save\" data-api-msg=\"Configuration updated\">
    <fieldset>
        <div class=\"rowElem noborder\">
            <label>";
        // line 300
        echo gettext("Template");
        echo ":</label>
            <div class=\"formRight\">
                <input type=\"text\" name=\"template\" value=\"";
        // line 302
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "template"), "html", null, true);
        echo "\" placeholder=\"\"/>
            </div>
            <div class=\"fix\"></div>
        </div>

        <div class=\"rowElem\">
            <label>";
        // line 308
        echo gettext("Hostname");
        echo ":</label>
            <div class=\"formRight\">
                <input type=\"text\" name=\"hostname\" value=\"";
        // line 310
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "hostname"), "html", null, true);
        echo "\" placeholder=\"\"/>
            </div>
            <div class=\"fix\"></div>
        </div>

        <div class=\"rowElem\">
            <label>";
        // line 316
        echo gettext("Plan");
        echo ":</label>
            <div class=\"formRight\">
                <input type=\"text\" name=\"plan\" value=\"";
        // line 318
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "plan"), "html", null, true);
        echo "\" placeholder=\"\"/>
            </div>
            <div class=\"fix\"></div>
        </div>
        
        <div class=\"rowElem\">
            <label>";
        // line 324
        echo gettext("Main IP address");
        echo ":</label>
            <div class=\"formRight\">
                <input type=\"text\" name=\"mainipaddress\" value=\"";
        // line 326
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "mainipaddress"), "html", null, true);
        echo "\" placeholder=\"\"/>
            </div>
            <div class=\"fix\"></div>
        </div>

        ";
        // line 364
        echo "
        <input type=\"hidden\" name=\"order_id\" value=\"";
        // line 365
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id"), "html", null, true);
        echo "\" />
        <input type=\"submit\" value=\"";
        // line 366
        echo gettext("Update");
        echo "\" class=\"greyishBtn submitForm\" />
    </fieldset>
</form>

<script type=\"text/javascript\">
    function onAfterStatusCheck(result)
    {
        bb.msg(result);
    }
</script>";
    }

    public function getTemplateName()
    {
        return "mod_serviceopenvpn_manage.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  701 => 366,  697 => 365,  694 => 364,  686 => 326,  681 => 324,  672 => 318,  667 => 316,  658 => 310,  653 => 308,  644 => 302,  639 => 300,  633 => 297,  627 => 294,  623 => 293,  619 => 291,  610 => 287,  606 => 285,  602 => 283,  594 => 279,  588 => 276,  581 => 273,  575 => 270,  567 => 266,  560 => 262,  546 => 251,  536 => 244,  532 => 243,  526 => 240,  522 => 239,  516 => 236,  512 => 235,  506 => 232,  503 => 231,  501 => 230,  489 => 221,  479 => 214,  475 => 213,  469 => 210,  465 => 209,  459 => 206,  455 => 205,  449 => 202,  446 => 201,  444 => 200,  432 => 191,  422 => 184,  418 => 183,  412 => 180,  408 => 179,  402 => 176,  398 => 175,  392 => 172,  389 => 171,  387 => 170,  380 => 166,  377 => 165,  375 => 164,  368 => 160,  364 => 159,  356 => 155,  354 => 154,  349 => 152,  343 => 149,  337 => 146,  329 => 141,  325 => 140,  317 => 136,  315 => 135,  310 => 133,  304 => 130,  298 => 127,  290 => 122,  286 => 121,  277 => 115,  271 => 112,  265 => 109,  257 => 104,  253 => 103,  244 => 97,  238 => 94,  232 => 91,  221 => 83,  217 => 82,  213 => 81,  209 => 80,  205 => 79,  201 => 78,  196 => 76,  192 => 75,  188 => 74,  184 => 73,  180 => 72,  174 => 71,  168 => 70,  155 => 62,  151 => 61,  142 => 57,  138 => 56,  131 => 52,  127 => 51,  120 => 47,  116 => 46,  109 => 42,  105 => 41,  98 => 37,  94 => 36,  83 => 32,  79 => 31,  72 => 27,  68 => 26,  58 => 21,  53 => 19,  46 => 15,  42 => 14,  32 => 7,  28 => 5,  26 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
