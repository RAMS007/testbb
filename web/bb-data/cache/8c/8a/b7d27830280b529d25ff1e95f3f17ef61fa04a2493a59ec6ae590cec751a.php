<?php

/* mod_formbuilder_settings.phtml */
class __TwigTemplate_8c8ab7d27830280b529d25ff1e95f3f17ef61fa04a2493a59ec6ae590cec751a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ajax")) ? ("layout_blank.phtml") : ("layout_default.phtml")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "extension_is_on", array(0 => array("mod" => "formbuilder")), "method")) {
            // line 2
            $context["mf"] = $this->env->loadTemplate("macro_functions.phtml");
            $context["active_menu"] = "system";
        }
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    public function block_meta_title($context, array $blocks = array())
    {
        echo "Custom form builder";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "<style type=\"text/css\">
#background_overlay
    {
    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
    }
.update-field
{
    display: none;
    position: absolute;
    top: 20%;
    left: 20%;
    margin-left: 0px;
    margin-top: 0px;
    padding: 10px;
    background: url(images/alertOpacityOverlay.png) repeat;
    border-radius: 10px;
    --webkit-border-radius: 10px;
    z-index:1002;
    overflow:visible;
}
#background-loader
{
    display: none;
    position: absolute;
    left: 50%;
    top: 50%;
    margin-left: -110px;
    margin-top: -10px;
}
.manage
{
    position: relative;
    background: #ffffff;
}
</style>
";
    }

    // line 52
    public function block_content($context, array $blocks = array())
    {
        // line 53
        echo "<div class=\"widget simpleTabs\">

    <ul class=\"tabs\">
        <li><a href=\"#tab-index\" id=\"open-index-tab\">";
        // line 56
        echo gettext("Custom forms");
        echo "</a></li>
    </ul>

    <div class=\"tabs_container\">
        <div class=\"fix\"></div>

        <div class=\"tab_content nopadding\" id=\"tab-index\">
            <table class=\"tableStatic wide\">
                <thead>
                <tr>
                    <th>";
        // line 66
        echo gettext("Form");
        echo "</th>
                    <th>";
        // line 67
        echo gettext("Orders using this form");
        echo "</th>
                    <th>";
        // line 68
        echo gettext("Products using this form");
        echo "</th>
                    <td width=\"23%\">";
        // line 69
        echo gettext("Actions");
        echo "</td>
                </tr>
                </thead>

                <tbody>
                ";
        // line 74
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "formbuilder_get_forms"));
        foreach ($context['_seq'] as $context["_key"] => $context["form"]) {
            // line 75
            echo "
                <tr>
                    <td>";
            // line 77
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "name"), "html", null, true);
            echo "</td>
                    <td>";
            // line 78
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "order_count"), "html", null, true);
            echo "</td>
                    <td>";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "product_count"), "html", null, true);
            echo "</td>
                    <td class=\"actions\">
                        <a class=\"bb-button btn14\" href=\"";
            // line 81
            echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("extension/settings/formbuilder", array("id" => $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "id")));
            echo "\"><img src=\"images/icons/dark/pencil.png\" alt=\"\"></a>
                        <a class=\"bb-button btn14 copy_form\" href=\"#\" data-api-reload=\"1\"  title=\"Copy\" data-form-id=\"";
            // line 82
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "id"), "html", null, true);
            echo "\"><img src=\"images/icons/dark/baloons.png\" alt=\"\"></a>
                        <a class=\"btn14 bb-rm-tr api-link\" href=\"";
            // line 83
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/formbuilder/delete_form", array("id" => $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "id")));
            echo "\" data-api-confirm=\"Are you sure?\" ><img src=\"images/icons/dark/trash.png\" alt=\"\"></a>
                    </td>
                </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['form'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "                </tbody>
                <tfoot>
                <tr>
                    <td colspan=\"4\">
                        <input type=\"button\" value=\"";
        // line 91
        echo gettext("Create new form");
        echo "\" class=\"greyishBtn\" id=\"new-form\" style=\"width: 150px; float: right;\"/>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>

        <div class=\"tab_content\" id=\"tab-import\">
            <form method=\"post\" action=\"";
        // line 99
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/formbuilder/import");
        echo "\" class=\"mainForm api-form save\" data-api-reload=\"1\">
                <fieldset>
                    <div class=\"formBottom\">
                        <textarea name=\"form\" cols=\"5\" rows=\"5\" placeholder=\"Paste new form configuration text.\"></textarea>
                    </div>
                    <div class=\"fix\"></div>
                    <input type=\"submit\" value=\"";
        // line 105
        echo gettext("Import");
        echo "\" class=\"greyishBtn submitForm\" style=\"margin: 0\"/>
                </fieldset>
            </form>
        </div>

    </div>
</div>

";
        // line 113
        if ($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "id")) {
            $context["form"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "formbuilder_get_form", array(0 => array("id" => $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "id"))), "method");
            // line 114
            echo "
<div class=\"widget\" id=\"form-options-";
            // line 115
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "id"), "html", null, true);
            echo "\">
    <div class=\"head\">
        <h5 class=\"iCog\">";
            // line 117
            echo gettext("Form options");
            echo "</h5>
    </div>

    <div class=\"mainForm\">
                <form  id=\"update-form-options\">
                    <div class=\"rowElem\">
                        <label>
                            <strong>";
            // line 124
            echo gettext("New form name and options");
            echo "</strong>
                        </label>
                        <div class=\"formRight\">
                            <input type=\"text\" name=\"form_name\"  id=\"form_name\" value=\"";
            // line 127
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "name"), "html", null, true);
            echo "\" style=\"width:300px;\"/>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    <div class=\"rowElem\">
                        <div class=\"formRight\">
                            <select name=\"type\" id=\"form_type\" style=\"width:300px;\">
                                <option value=\"default\" ";
            // line 134
            if (($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "style"), "type") == "default")) {
                echo " selected ";
            }
            echo ">";
            echo gettext("Labels on top");
            echo "</option>
                                <option value=\"horizontal\" ";
            // line 135
            if (($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "style"), "type") == "horizontal")) {
                echo " selected ";
            }
            echo ">";
            echo gettext("Labels on the left");
            echo "</option>
                            </select>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    <div class=\"rowElem\">
                        <div class=\"formRight\">
                            <select name=\"show_title\" id=\"show_title\" style=\"width:300px;\">
                                <option value=\"1\" ";
            // line 143
            if (($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "style"), "show_title") == "1")) {
                echo " selected ";
            }
            echo ">";
            echo gettext("Show form title");
            echo "</option>
                                <option value=\"0\" ";
            // line 144
            if (($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "style"), "show_title") == "0")) {
                echo " selected ";
            }
            echo ">";
            echo gettext("Do not show form title");
            echo "</option>
                            </select>
                        </div>
                        <div class=\"fix\"></div>
                    </div>

                    <input type=\"hidden\" name=\"form_id\" value=\"";
            // line 150
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "id"), "html", null, true);
            echo "\">

                    <div class=\"rowElem\">
                        <div class=\"formRight\">
                            <button type=\"submit\" href=\"#\" class=\"button blueBtn\">";
            // line 154
            echo gettext("Update");
            echo "</button>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                </form>
            <div class=\"fix\"></div>
    </div>
</div>

<div class=\"widget\" id=\"form-";
            // line 163
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "id"), "html", null, true);
            echo "\">
<div class=\"head\">
    <h5 class=\"iCog\">";
            // line 165
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "name"), "html", null, true);
            echo "</h5>
</div>

<div class=\"mainForm\">

<div class=\"body\" id=\"fields\">
    <a href=\"";
            // line 171
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/formbuilder/add_field", array("form_id" => $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "id"), "type" => "text"));
            echo "\" title=\"\" class=\"btnIconLeft mr10 api-link\" data-api-reload=\"1\" ><img src=\"images/icons/dark/add.png\" alt=\"\" class=\"icon\"><span>";
            echo gettext("Text");
            echo "</span></a>
    <a href=\"";
            // line 172
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/formbuilder/add_field", array("form_id" => $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "id"), "type" => "select"));
            echo "\" title=\"\" class=\"btnIconLeft mr10 api-link\" data-api-reload=\"1\"><img src=\"images/icons/dark/add.png\" alt=\"\" class=\"icon\"><span>";
            echo gettext("Dropdown");
            echo "</span></a>
    <a href=\"";
            // line 173
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/formbuilder/add_field", array("form_id" => $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "id"), "type" => "radio"));
            echo "\" title=\"\" class=\"btnIconLeft mr10 api-link\" data-api-reload=\"1\"><img src=\"images/icons/dark/add.png\" alt=\"\" class=\"icon\"><span>";
            echo gettext("Radio");
            echo "</span></a>
    <a href=\"";
            // line 174
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/formbuilder/add_field", array("form_id" => $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "id"), "type" => "checkbox"));
            echo "\" title=\"\" class=\"btnIconLeft mr10 api-link\" data-api-reload=\"1\"><img src=\"images/icons/dark/add.png\" alt=\"\" class=\"icon\"><span>";
            echo gettext("Checkbox");
            echo "</span></a>
    <a href=\"";
            // line 175
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/formbuilder/add_field", array("form_id" => $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "id"), "type" => "textarea"));
            echo "\" title=\"\" class=\"btnIconLeft mr10 api-link\" data-api-reload=\"1\"><img src=\"images/icons/dark/add.png\" alt=\"\" class=\"icon\"><span>";
            echo gettext("Textarea");
            echo "</span></a>
</div>


<fieldset>
";
            // line 180
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fields"));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["i"] => $context["field"]) {
                // line 181
                echo "    <div class=\"wrap-field\">
    ";
                // line 182
                $this->env->loadTemplate("mod_formbuilder_field.phtml")->display(array_merge($context, (isset($context["field"]) ? $context["field"] : null)));
                // line 183
                echo "    ";
                $this->env->loadTemplate("mod_formbuilder_preview.phtml")->display(array_merge($context, (isset($context["field"]) ? $context["field"] : null)));
                // line 184
                echo "    </div>
";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['i'], $context['field'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 186
            echo "</fieldset>



</div>
<div class=\"fix\"></div>
</div>";
        }
        // line 193
        echo "<div id=\"background_overlay\">
    <img src=\"images/loaders/loader7.gif\" alt=\"\" id=\"background-loader\">
</div>

";
    }

    // line 199
    public function block_js($context, array $blocks = array())
    {
        // line 200
        echo "<script type=\"text/javascript\">

    \$(function () {

        \$(\"form\").submit( function () {
            \$(':checkbox:not(:checked)').removeAttr('checked');
        });

        \$('.new-field').click(function () {
            var p = \$(this).closest('.rowElem').prev();
            p.clone().insertAfter(p);
            p.next().find('input').val(\"\");
            return false;
        });

        \$('#textarea-width, #textarea-height').change(function () {
            var p = \$(this).closest('.rowElem');
            var width = \$('#textarea-width').val();
            var height = \$('#textarea-height').val();
            p.next().find('textarea').css({\"width\":width, \"height\":height});
            return false;
        });

        \$('#prefix_text, #suffix_text').change(function () {
            var prefix = \$('#prefix_text').val();
            var suffix = \$('#suffix_text').val();
           \$('#prepended_text').text(prefix);
           \$('#appended_text').text(suffix);
            return false;
        });


        \$('.rm').click(function () {
            var fid = \$(this).attr('data-field-id');
            var rm = \$(this);
            jConfirm('Are you sure?', 'Confirm', function(r) {
                if(r) {
                    bb.post('admin/formbuilder/delete_field', {id: fid}, function () {
                        \$(rm).parents('.wrap-field').slideUp(\"normal\", function () {
                            \$(rm).remove();
                        });
                    });
                }
            });
            return false;
        });

        \$('.ed').click(function () {
            var edit_button = \$(this);

            var center = function(edit_button){
                var leftPosition = (\$(window).width() / 2) - ((  \$(edit_button).parents('.wrap-field').find('.update-field').width() / 2) + 10);
                var topPosition = (\$(document).height() / 2) - ((  \$(edit_button).parents('.wrap-field').find('.update-field').height() / 2) + 50);

                \$(edit_button).parents('.wrap-field').find('.update-field').css({
                    'top': topPosition,
                    'left': leftPosition
                });
            };

            center(edit_button);

            \$('#background_overlay').fadeIn(function(){


                \$(window).resize(function() {
                    center(edit_button);
                });

                \$(edit_button).parents('.wrap-field').find('.update-field').fadeIn(function(){
                    \$('html, body').animate({
                        scrollTop: \$(this).offset().top
                    }, 500);
                });

                \$('#background_overlay').click(function(){
                    hide_edit_form();
                });

                \$('body').delegate('.close-field-form','click',function(){
                    hide_edit_form();
                    return false;
                });

                \$(document).keyup(function(e) {
                    if (e.keyCode == 27) {
                       hide_edit_form();
                    }
                });

                var hide_edit_form  = function (){
                    \$('#background_overlay').fadeOut();
                    \$(edit_button).parents('.wrap-field').find('.update-field').fadeOut();
                };
            });
            return false;
        });

        \$('.pr').click(function () {
            \$(this).hide().siblings('.ed').show();
            \$(this).parents('.wrap-field').find('.preview').show();
            \$(this).parents('.wrap-field').find('.manage').hide();
            return false;
        });

        \$('#update-form-options').bind('submit',function (event) {
            bb.post('admin/formbuilder/update_form_settings',  \$(this).serialize(), function(){
                bb.msg(\"";
        // line 307
        echo gettext("Form options were updated");
        echo "\")
            })
            return false;
        });

        \$('.update-field').bind('submit',function (event) {
            var field_form = \$(this);
            bb.post('admin/formbuilder/update_field',  \$(this).serialize(), function(){
                \$(field_form).fadeOut();
                \$('#background-loader').show();
                bb.reload();
            })
            return false;
        });

       \$('#new-form').click(function () {
            jPrompt('Give your new form a title', 'My new form', 'Form title', function (title) {
                if (title) {
                    bb.post('admin/formbuilder/create_form', {name: title}, function (id) {
                        bb.redirect(\"";
        // line 326
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("extension/settings/formbuilder", array("id" => ""));
        echo "\" + id);
                    });
                }
            });
            return false;
        });

        \$('.copy_form').click(function () {
            var fid = \$(this).attr('data-form-id');
            jPrompt('Give your new form a title', 'My new form', 'Form title', function (title) {
                if (title) {
                    bb.post('admin/formbuilder/copy_form', {name: title, form_id: fid}, function (id) {
                        bb.redirect(\"";
        // line 338
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("extension/settings/formbuilder", array("id" => ""));
        echo "\" + id);
                    });
                }
            });
            return false;
        });
    });

</script>";
    }

    public function getTemplateName()
    {
        return "mod_formbuilder_settings.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  563 => 338,  548 => 326,  526 => 307,  417 => 200,  414 => 199,  406 => 193,  397 => 186,  382 => 184,  379 => 183,  377 => 182,  374 => 181,  357 => 180,  347 => 175,  341 => 174,  335 => 173,  329 => 172,  323 => 171,  314 => 165,  309 => 163,  297 => 154,  290 => 150,  277 => 144,  269 => 143,  254 => 135,  246 => 134,  236 => 127,  230 => 124,  220 => 117,  215 => 115,  212 => 114,  209 => 113,  198 => 105,  189 => 99,  178 => 91,  172 => 87,  162 => 83,  158 => 82,  154 => 81,  149 => 79,  145 => 78,  141 => 77,  137 => 75,  133 => 74,  125 => 69,  121 => 68,  117 => 67,  113 => 66,  100 => 56,  95 => 53,  92 => 52,  43 => 5,  40 => 4,  28 => 2,  26 => 1,);
    }
}
