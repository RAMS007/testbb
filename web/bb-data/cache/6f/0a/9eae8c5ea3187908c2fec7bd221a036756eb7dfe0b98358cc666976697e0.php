<?php

/* mod_example_index.phtml */
class __TwigTemplate_6f0a9eae8c5ea3187908c2fec7bd221a036756eb7dfe0b98358cc666976697e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'page_header' => array($this, 'block_page_header'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ajax")) ? ("layout_blank.phtml") : ("layout_default.phtml")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_meta_title($context, array $blocks = array())
    {
        echo gettext("Example module");
    }

    // line 4
    public function block_breadcrumb($context, array $blocks = array())
    {
        echo " <li class=\"active\">";
        echo gettext("Example module");
        echo "</li>";
    }

    // line 5
    public function block_page_header($context, array $blocks = array())
    {
        // line 6
        echo "<article class=\"page-header\">
    <h1>";
        // line 7
        echo gettext("Example module title");
        echo "</h1>
</article>
";
    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        // line 11
        echo "<div class=\"row\">
    <article class=\"span12 data-block\">
        <div class=\"data-container\">
            <header>
                <h1>";
        // line 15
        echo gettext("Example module title");
        echo "</h1>
                <p>";
        // line 16
        echo gettext("Example module description");
        echo "</p>
            </header>
            <section>
                ";
        // line 19
        echo twig_markdown_filter($this->env, $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "example_readme"));
        echo "
                ";
        // line 20
        if ((isset($context["client"]) ? $context["client"] : null)) {
            // line 21
            echo "                <a class=\"btn btn-primary\" href=\"";
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("example/protected");
            echo "\">";
            echo gettext("See protected content");
            echo "</a>
                ";
        } else {
            // line 23
            echo "                <a class=\"btn btn-primary\" href=\"";
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("login");
            echo "\">";
            echo gettext("Login to client area");
            echo "</a>
                ";
        }
        // line 25
        echo "            </section>
        </div>
    </article>
</div>




";
        // line 33
        if ((isset($context["show_protected"]) ? $context["show_protected"] : null)) {
            // line 34
            echo "<div class=\"row\">
    <article class=\"span12 data-block\">
        <div class=\"data-container\">
            <header>
                <h2>";
            // line 38
            echo gettext("This content can be seen only if client is logged in");
            echo "</h2>
            </header>
            <section>
                <p>";
            // line 41
            echo gettext("Your protected content");
            echo "</p>
                <p><a class=\"btn btn-inverse\" href=\"";
            // line 42
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("client/me");
            echo "\">";
            echo gettext("Manage profile");
            echo "</a></p>
            </section>
        </div>
    </article>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "mod_example_index.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 42,  121 => 41,  115 => 38,  109 => 34,  107 => 33,  97 => 25,  89 => 23,  81 => 21,  79 => 20,  75 => 19,  69 => 16,  65 => 15,  59 => 11,  56 => 10,  49 => 7,  46 => 6,  43 => 5,  35 => 4,  29 => 3,);
    }
}
