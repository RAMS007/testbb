<?php

/* mod_servicehosting_order_form.phtml */
class __TwigTemplate_b42a27e42a884f05a4a45e1d47a7be45ecc3110344d6d65c22f8d4ca68ff743a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["periods"] = $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "system_periods");
        // line 2
        $context["pricing"] = $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "pricing");
        // line 3
        echo "
<section>

";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["product_details"]) ? $context["product_details"] : null), "html", null, true);
        echo "

<div class=\"well\">

    <strong>";
        // line 10
        echo gettext("Domain configuration");
        echo "</strong>
    <div class=\"control-group\">
        <label class=\"radio\">";
        // line 12
        echo gettext("I will use my existing domain and update nameservers");
        // line 13
        echo "            <input type=\"radio\" name=\"domain[action]\" value=\"owndomain\" onclick=\"selectDomainAction(this);\"/>
        </label>

        <label class=\"radio\">";
        // line 16
        echo gettext("I want to register a new domain");
        // line 17
        echo "            <input type=\"radio\" name=\"domain[action]\" value=\"register\" onclick=\"selectDomainAction(this);\"/>
        </label>


    <div id=\"owndomain\" class=\"domain_action\" style=\"display: none;\">
        <fieldset>
            <div class=\"row-fluid\">
            <div class=\"controls\">
                <input type=\"text\" name=\"domain[owndomain_sld]\" value=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "owndomain_sld"), "html", null, true);
        echo "\" placeholder=\"";
        echo gettext("mydomain");
        echo "\" class=\"span4\">
                <input type=\"text\" name=\"domain[owndomain_tld]\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "owndomain_tld", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "owndomain_tld"), ".com")) : (".com")), "html", null, true);
        echo "\">
            </div>
            </div>
        </fieldset>
    </div>

    <div id=\"register\" class=\"domain_action\" style=\"display: none;\">
        <fieldset>
            <div class=\"row-fluid\">
            <div class=\"controls\">
                <input type=\"text\" name=\"domain[register_sld]\" value=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "register_sld"), "html", null, true);
        echo "\" placeholder=\"";
        echo gettext("mydomain");
        echo "\" class=\"span4\">
                ";
        // line 37
        if (($this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "free_domain") == 1)) {
            // line 38
            echo "                    ";
            $context["tlds"] = $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "serviceHosting_free_tlds", array(0 => array("product_id" => $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id"))), "method");
            // line 39
            echo "                ";
        } else {
            // line 40
            echo "                    ";
            $context["tlds"] = $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "serviceDomain_tlds", array(0 => array("allow_register" => 1)), "method");
            // line 41
            echo "                ";
        }
        // line 42
        echo "                <select name=\"domain[register_tld]\">
                    ";
        // line 43
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tlds"]) ? $context["tlds"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["tld"]) {
            // line 44
            echo "                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "tld"), "html", null, true);
            echo "\" label=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "tld"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tld"]) ? $context["tld"] : null), "tld"), "html", null, true);
            echo "</option>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tld'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "                </select>
                <button class=\"btn btn-inverse\" type=\"button\" id=\"domain-check\">";
        // line 47
        echo gettext("Check");
        echo "</button>
            </div>
            </div>

            <div id=\"domain-config\" style=\"display:none;\">
                <div class=\"control-group\">
                    <label>";
        // line 53
        echo gettext("Period");
        echo "</label>
                    <div class=\"controls\">
                        <select name=\"domain[register_years]\"></select>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>
</div>
</section>

";
        // line 65
        $context["currency"] = $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "cart_get_currency");
        // line 66
        echo "<script type=\"text/javascript\">
    function selectDomainAction(el)
    {
        \$('.domain_action').hide();
        \$('#'+\$(el).val()).show();
    }

\$(function() {

    \$('#domain-check').bind('click',function(event){
        var sld = \$('input[name=\"domain[register_sld]\"]').val();
        var tld = \$('select[name=\"domain[register_tld]\"]').val();
        var domain = sld + tld;
        bb.post(
            'guest/servicedomain/check',
            { sld: sld, tld: tld },
            function(result) {
                setRegistrationPricing(tld);
                \$('#domain-config').fadeIn('slow');
            }
        );
        return false;
    });

    if(\$(\".addons\").length && \$(\".addons\").is(':hidden')) {
        \$('#order-button').one('click', function(){
            \$(this).slideUp('fast');
            \$('.addons').slideDown('fast');
            return false;
        });
    }

    \$('#period-selector').change(function(){
        \$('.period').hide();
        \$('.period.' + \$(this).val()).show();
    }).trigger('change');

    \$('.addon-period-selector').change(function(){
        var r = \$(this).attr('rel');
        \$('#' + r + ' span').hide();
        \$('#' + r + ' span.' + \$(this).val()).fadeIn();
    }).trigger('change');

    function setRegistrationPricing(tld)
    {
        bb.post(
            'guest/servicedomain/pricing',
            { tld: tld },
            function(result) {
                var s = \$(\"select[name='domain[register_years]']\");
                s.find('option').remove();
                for (i=1;i<6;i++) {
                    var price = bb.currency(result.price_registration, ";
        // line 118
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["currency"]) ? $context["currency"] : null), "conversion_rate"), "html", null, true);
        echo ", \"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["currency"]) ? $context["currency"] : null), "code"), "html", null, true);
        echo "\", i);
                    s.append(new Option(i + \"";
        // line 119
        echo gettext(" Year/s @ ");
        echo "\" + price, i));
                }
            }
        );
    }

});
</script>";
    }

    public function getTemplateName()
    {
        return "mod_servicehosting_order_form.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  207 => 119,  201 => 118,  147 => 66,  145 => 65,  130 => 53,  121 => 47,  118 => 46,  105 => 44,  101 => 43,  98 => 42,  95 => 41,  92 => 40,  89 => 39,  86 => 38,  84 => 37,  78 => 36,  65 => 26,  59 => 25,  49 => 17,  47 => 16,  42 => 13,  40 => 12,  35 => 10,  28 => 6,  23 => 3,  21 => 2,  19 => 1,);
    }
}
