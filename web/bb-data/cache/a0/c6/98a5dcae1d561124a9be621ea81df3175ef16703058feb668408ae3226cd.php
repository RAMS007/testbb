<?php

/* mod_serviceopenvpn_config.phtml */
class __TwigTemplate_a0c698a5dcae1d561124a9be621ea81df3175ef16703058feb668408ae3226cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"help\">
    <h5>";
        // line 2
        echo gettext("OpenVPN configuration");
        echo "</h5>

    <p><b> bb-modules/Serviceopenvpn/html_admin/mod_serviceopenvpn_config.phtml:3</b></p>
</div>

<form method=\"post\" action=\"";
        // line 7
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/product/update_config");
        echo "\" class=\"mainForm api-form save\"
      data-api-reload=\"1\">
    <fieldset>
        <legend>";
        // line 10
        echo gettext("Plan parameters");
        echo "</legend>

        <div class=\"rowElem\">
            <label>";
        // line 13
        echo gettext("Upload speed");
        echo ":</label>

            <div class=\"formRight\">
                <input type=\"text\" name=\"config[up_speed]\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "up_speed"), "html", null, true);
        echo "\"
                       placeholder=\"Enter your upload speed\"/>
            </div>
            <div class=\"fix\"></div>
        </div>
        <div class=\"rowElem\">
            <label>";
        // line 22
        echo gettext("Download speed");
        echo ":</label>

            <div class=\"formRight\">
                <input type=\"text\" name=\"config[down_speed]\" value=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "down_speed"), "html", null, true);
        echo "\"
                       placeholder=\"Enter your download speed\"/>
            </div>
            <div class=\"fix\"></div>
        </div>
        <div class=\"rowElem\">
            <label>";
        // line 31
        echo gettext("Sessions");
        echo ":</label>

            <div class=\"formRight\">
                <input type=\"text\" name=\"config[sessions]\" value=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "sessions"), "html", null, true);
        echo "\"
                       placeholder=\"Number sessions at same time\"/>
            </div>
            <div class=\"fix\"></div>
        </div>
        <div class=\"rowElem\">
            <label>";
        // line 40
        echo gettext("Traffic per month");
        echo ":</label>
            <div class=\"formRight\">
                <input type=\"text\" name=\"config[trafic]\" value=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "trafic"), "html", null, true);
        echo "\"
                       placeholder=\"Count trafic per month in \" style=\"width: 60%;  float: left;\" />
       <!--        <select name=\"config[traficunit]\">
                    <option value=\"kb\" >kb</option>
                    <option value=\"Mb\">Mb</option>
                    <option value=\"Gb\">Gb</option>
                    <option value=\"Tb\">Tb</option>
                </select> -->
                ";
        // line 50
        $context["units"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "serviceopenvpn_get_units", array(), "method");
        // line 51
        echo "                ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mf"]) ? $context["mf"] : null), "selectbox", array(0 => "config[traficunit]", 1 => (isset($context["units"]) ? $context["units"] : null), 2 => $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "config"), "traficunit"), 3 => "kb", 4 => "kb"), "method"), "html", null, true);
        echo "

            </div>
            <div class=\"fix\"></div>
        </div>

        <input type=\"submit\" value=\"";
        // line 57
        echo gettext("Update");
        echo "\" class=\"greyishBtn submitForm\"/>
    </fieldset>

    <input type=\"hidden\" name=\"config[cluster_id]\" value=\"1\"/>
    <input type=\"hidden\" name=\"id\" value=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id"), "html", null, true);
        echo "\"/>
</form>

";
    }

    public function getTemplateName()
    {
        return "mod_serviceopenvpn_config.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 61,  115 => 57,  105 => 51,  103 => 50,  92 => 42,  87 => 40,  78 => 34,  72 => 31,  63 => 25,  57 => 22,  48 => 16,  42 => 13,  36 => 10,  30 => 7,  22 => 2,  19 => 1,);
    }
}
