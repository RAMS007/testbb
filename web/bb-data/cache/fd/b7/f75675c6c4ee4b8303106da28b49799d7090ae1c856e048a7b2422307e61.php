<?php

/* mod_example_settings.phtml */
class __TwigTemplate_fdb7f75675c6c4ee4b8303106da28b49799d7090ae1c856e048a7b2422307e61 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'breadcrumbs' => array($this, 'block_breadcrumbs'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ajax")) ? ("layout_blank.phtml") : ("layout_default.phtml")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["mf"] = $this->env->loadTemplate("macro_functions.phtml");
        // line 4
        $context["active_menu"] = "system";
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_meta_title($context, array $blocks = array())
    {
        echo "Example extension settings";
    }

    // line 6
    public function block_breadcrumbs($context, array $blocks = array())
    {
        // line 7
        echo "<ul>
    <li class=\"firstB\"><a href=\"";
        // line 8
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("/");
        echo "\">";
        echo gettext("Home");
        echo "</a></li>
    <li><a href=\"";
        // line 9
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("system");
        echo "\">";
        echo gettext("Settings");
        echo "</a></li>
    <li class=\"lastB\">";
        // line 10
        echo gettext("Example module settings");
        echo "</li>
</ul>
";
    }

    // line 14
    public function block_content($context, array $blocks = array())
    {
        // line 15
        echo "
<div class=\"widget\">
    <div class=\"head\">
        <h5 class=\"iCog\">";
        // line 18
        echo gettext("Example extension settings");
        echo "</h5>
    </div>

";
        // line 21
        $context["params"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "extension_config_get", array(0 => array("ext" => "mod_example")), "method");
        // line 22
        echo "
<form method=\"post\" action=\"";
        // line 23
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/extension/config_save");
        echo "\" class=\"mainForm api-form\" data-api-msg=\"";
        echo gettext("Configuration updated");
        echo "\">
    
    <div class=\"help\">
        <h3>";
        // line 26
        echo gettext("Example extension settings");
        echo "</h3>
        <p>";
        // line 27
        echo gettext("Every extension can have settings page. Only requirement is to have mod_example_settings.phtml page in html_admin folder");
        echo "</p>
    </div>
    
    <fieldset>
        <div class=\"rowElem noborder\">
            <label>";
        // line 32
        echo gettext("Parameter title");
        echo "</label>
            <div class=\"formRight\">
                <input type=\"text\" name=\"custom_param\" value=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["params"]) ? $context["params"] : null), "custom_param"), "html", null, true);
        echo "\" placeholder=\"";
        echo gettext("Example custom value");
        echo "\"/>
            </div>
            <div class=\"fix\"></div>
        </div>

        <div class=\"rowElem\">
            <label>";
        // line 40
        echo gettext("Public Parameter title");
        echo "</label>
            <div class=\"formRight\">
                <input type=\"text\" name=\"public[param]\" value=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["params"]) ? $context["params"] : null), "public"), "param"), "html", null, true);
        echo "\" placeholder=\"";
        echo gettext("This setting will be accessible by Guest API");
        echo "\"/>
            </div>
            <div class=\"fix\"></div>
        </div>

        <input type=\"submit\" value=\"";
        // line 47
        echo gettext("Update");
        echo "\" class=\"greyishBtn submitForm\" />
        <input type=\"hidden\" name=\"ext\" value=\"mod_example\" />
    </fieldset>
</form>
</div>

";
    }

    public function getTemplateName()
    {
        return "mod_example_settings.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 47,  123 => 42,  118 => 40,  107 => 34,  102 => 32,  94 => 27,  90 => 26,  82 => 23,  79 => 22,  77 => 21,  71 => 18,  66 => 15,  63 => 14,  56 => 10,  50 => 9,  44 => 8,  41 => 7,  38 => 6,  32 => 3,  27 => 4,  25 => 2,);
    }
}
