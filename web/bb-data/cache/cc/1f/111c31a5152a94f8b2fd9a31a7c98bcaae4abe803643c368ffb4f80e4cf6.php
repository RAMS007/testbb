<?php

/* mod_serviceopenvpn_manage.phtml */
class __TwigTemplate_cc1f111c31a5152a94f8b2fd9a31a7c98bcaae4abe803643c368ffb4f80e4cf6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "status") == "active")) {
            // line 2
            echo "
<div class=\"row\">
    <article class=\"span12 data-block\">
        <div class=\"data-container\">
            <header>
                <h2>";
            // line 7
            echo gettext("VPS management");
            echo "</h2>
                <ul class=\"data-header-actions\">
                    <li class=\"domain-tabs active\"><a href=\"#tab-details\" class=\"btn btn-inverse btn-alt\">";
            // line 9
            echo gettext("Details");
            echo "</a></li>
                    <li class=\"domain-tabs\"><a href=\"#tab-stats\" class=\"btn btn-inverse btn-alt\">";
            // line 10
            echo gettext("Statistics");
            echo "</a></li>
                </ul>
            </header>
            <section class=\"tab-content\">
                <div class=\"tab-pane active\" id=\"tab-details\">
                    ";
            // line 15
            $context["info"] = $this->getAttribute((isset($context["client"]) ? $context["client"] : null), "serviceopenvpn_info", array(0 => array("order_id" => $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "id"))), "method");
            // line 16
            echo "                    <table class=\"table table-striped table-bordered table-condensed\">
                        <tbody>
                        <tr>
                            <td>";
            // line 19
            echo gettext("Count connections");
            echo ":</td>
                            <td>
                                <span id=\"vps-status\" class=\"label label- label-success\">";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "sessions"), "html", null, true);
            echo "</span>
                            </td>
                        </tr>
                        <tr>
                            <td>";
            // line 25
            echo gettext("Root password");
            echo ":</td>
                            <td><b>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "password"), "html", null, true);
            echo "</b></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class=\"tab-pane\" id=\"tab-stats\">
                    <div class=\"body\" style=\"width: 100%; \">
                        ";
            // line 33
            $context["bw"] = twig_split_filter($this->getAttribute((isset($context["info"]) ? $context["info"] : null), "bandwidth"), ",");
            // line 34
            echo "                        <table class=\"table table-striped table-bordered table-condensed\">
                            <caption>";
            // line 35
            echo gettext("Bandwidth");
            echo "</caption>
                            <tbody>
                            <tr>
                                <td>";
            // line 38
            echo gettext("Total");
            echo ":</td>
                                <td>";
            // line 39
            echo twig_escape_filter($this->env, twig_size_filter($this->getAttribute((isset($context["bw"]) ? $context["bw"] : null), 0, array(), "array")), "html", null, true);
            echo "</td>
                            </tr>
                            <tr>
                                <td>";
            // line 42
            echo gettext("Downloaded");
            echo ":</td>
                                <td>";
            // line 43
            echo twig_escape_filter($this->env, twig_size_filter($this->getAttribute((isset($context["bw"]) ? $context["bw"] : null), 1, array(), "array")), "html", null, true);
            echo "</td>
                            </tr>
                            <tr>
                                <td>";
            // line 46
            echo gettext("Uploaded");
            echo ":</td>
                                <td>";
            // line 47
            echo twig_escape_filter($this->env, twig_size_filter($this->getAttribute((isset($context["bw"]) ? $context["bw"] : null), 2, array(), "array")), "html", null, true);
            echo "</td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan=\"2\">
                                    <div class=\"progress progress-";
            // line 53
            if (($this->getAttribute((isset($context["ram"]) ? $context["ram"] : null), 3, array(), "array") < 50)) {
                echo "success";
            } elseif (($this->getAttribute((isset($context["ram"]) ? $context["ram"] : null), 3, array(), "array") < 75)) {
                echo "warning";
            } else {
                echo "danger";
            }
            echo "\" >
                                        <div class=\"bar\" style=\"width: ";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bw"]) ? $context["bw"] : null), 3, array(), "array"), "html", null, true);
            echo "%;\"></div>
                                    </div>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class=\"clear\"></div>
                    <div class=\"block\" style=\"text-align: center;\">
                        <img src=\"";
            // line 64
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "master_url"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "trafficgraph"), "html", null, true);
            echo "\" alt=\"\" style=\"margin-bottom: 2em;\">
                        <img src=\"";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "master_url"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "loadgraph"), "html", null, true);
            echo "\" alt=\"\" style=\"margin-bottom: 2em;\">
                        <img src=\"";
            // line 66
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["service"]) ? $context["service"] : null), "master_url"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "memorygraph"), "html", null, true);
            echo "\" alt=\"\">
                    </div>
                </div>
            </section>
        </div>
    </article>
</div>


<script type=\"text/javascript\">
    \$(function() {
        \$('.domain-tabs a').bind('click',function(e){
            e.preventDefault();
            \$(this).tab('show');
        });
        function onAfterStatusCheck(result)
        {
            \$('#vps-status').text(result);
        }
    });
</script>
";
        }
    }

    public function getTemplateName()
    {
        return "mod_serviceopenvpn_manage.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 66,  150 => 65,  145 => 64,  132 => 54,  122 => 53,  113 => 47,  109 => 46,  103 => 43,  99 => 42,  93 => 39,  89 => 38,  83 => 35,  80 => 34,  78 => 33,  68 => 26,  64 => 25,  57 => 21,  52 => 19,  47 => 16,  45 => 15,  37 => 10,  33 => 9,  28 => 7,  21 => 2,  19 => 1,);
    }
}
