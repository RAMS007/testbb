<?php

/* mod_support_canned_responses.phtml */
class __TwigTemplate_8b21b43b57ddc4b168c0adef9a91da5f4798d092fbdcf7948999832f5fc02e67 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'breadcrumbs' => array($this, 'block_breadcrumbs'),
            'meta_title' => array($this, 'block_meta_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ajax")) ? ("layout_blank.phtml") : ("layout_default.phtml")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["mf"] = $this->env->loadTemplate("macro_functions.phtml");
        // line 12
        $context["active_menu"] = "support";
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_breadcrumbs($context, array $blocks = array())
    {
        // line 5
        echo "<ul>
    <li class=\"firstB\"><a href=\"";
        // line 6
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("/");
        echo "\">";
        echo gettext("Home");
        echo "</a></li>
    <li><a href=\"";
        // line 7
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("support");
        echo "\">";
        echo gettext("Support");
        echo "</a></li>
    <li class=\"lastB\">";
        // line 8
        echo gettext("Canned responses");
        echo "</li>
</ul>
";
    }

    // line 13
    public function block_meta_title($context, array $blocks = array())
    {
        echo "Canned responses";
    }

    // line 15
    public function block_content($context, array $blocks = array())
    {
        // line 16
        echo "<div class=\"widget simpleTabs\">

    <ul class=\"tabs\">
        <li><a href=\"#tab-index\">";
        // line 19
        echo gettext("Canned responses");
        echo "</a></li>
        <li><a href=\"#tab-new\">";
        // line 20
        echo gettext("New response");
        echo "</a></li>
        <li><a href=\"#tab-new-category\">";
        // line 21
        echo gettext("New category");
        echo "</a></li>
        <li><a href=\"#tab-categories\">";
        // line 22
        echo gettext("Categories");
        echo "</a></li>
    </ul>

    <div class=\"tabs_container\">

        <div class=\"fix\"></div>
        <div class=\"tab_content nopadding\" id=\"tab-index\">

        ";
        // line 30
        echo $context["mf"]->gettable_search();
        echo "
        <table class=\"tableStatic wide\">
            <thead>
                <tr>
                    <td>";
        // line 34
        echo gettext("Title");
        echo "</td>
                    <td>";
        // line 35
        echo gettext("Category");
        echo "</td>
                    <td style=\"width: 13%\">&nbsp;</td>
                </tr>
            </thead>

            <tbody>
            ";
        // line 41
        $context["responses"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "support_canned_get_list", array(0 => twig_array_merge(array("per_page" => 90, "page" => $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "page")), (isset($context["request"]) ? $context["request"] : null))), "method");
        // line 42
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["responses"]) ? $context["responses"] : null), "list"));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["i"] => $context["response"]) {
            // line 43
            echo "            <tr>
                <td>";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["response"]) ? $context["response"] : null), "title"), "html", null, true);
            echo "</td>
                <td><a href=\"";
            // line 45
            echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("/support/canned-category");
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["response"]) ? $context["response"] : null), "category"), "id"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["response"]) ? $context["response"] : null), "category"), "title"), "html", null, true);
            echo "</a></td>
                <td class=\"actions\">
                    <a class=\"bb-button btn14\" href=\"";
            // line 47
            echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("/support/canned");
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["response"]) ? $context["response"] : null), "id"), "html", null, true);
            echo "\"><img src=\"images/icons/dark/pencil.png\" alt=\"\"></a>
                    <a class=\"bb-button btn14 bb-rm-tr api-link\" data-api-confirm=\"Are you sure?\" href=\"";
            // line 48
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/support/canned_delete", array("id" => $this->getAttribute((isset($context["response"]) ? $context["response"] : null), "id")));
            echo "\" data-api-redirect=\"";
            echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("support/canned-responses");
            echo "\"><img src=\"images/icons/dark/trash.png\" alt=\"\"></a>
                </td>
            </tr>
            ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 52
            echo "            <tr>
                <td colspan=\"3\">
                    ";
            // line 54
            echo gettext("The list is empty");
            // line 55
            echo "                </td>
            </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['response'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "            </tbody>
        </table>
        ";
        // line 60
        $this->env->loadTemplate("partial_pagination.phtml")->display(array_merge($context, array("list" => (isset($context["responses"]) ? $context["responses"] : null), "url" => "support/canned-responses")));
        // line 61
        echo "        </div>

        <div class=\"tab_content nopadding\" id=\"tab-new\">

            <form method=\"post\" action=\"";
        // line 65
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/support/canned_create");
        echo "\" class=\"mainForm save api-form\" data-api-redirect=\"";
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("support/canned-responses");
        echo "\">
                <fieldset>
                    <div class=\"rowElem noborder\">
                        <label>";
        // line 68
        echo gettext("Category");
        echo ":</label>
                        <div class=\"formRight\">
                            ";
        // line 70
        echo $context["mf"]->getselectbox("category_id", $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "support_canned_category_pairs"), "", 1);
        echo "
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    <div class=\"rowElem\">
                        <label>";
        // line 75
        echo gettext("Title");
        echo ":</label>
                        <div class=\"formRight\">
                            <input type=\"text\" name=\"title\" value=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "title"), "html", null, true);
        echo "\" required=\"required\" placeholder=\"\"/>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    
                    <div class=\"rowElem\">
                        <label>";
        // line 83
        echo gettext("Content");
        echo "</label>
                        <div class=\"formRight\">
                            <textarea name=\"content\" cols=\"5\" rows=\"10\">";
        // line 85
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "content"), "html", null, true);
        echo "</textarea>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    <input type=\"submit\" value=\"";
        // line 89
        echo gettext("Create");
        echo "\" class=\"greyishBtn submitForm\" />
                </fieldset>
            </form>
        </div>

        <div class=\"tab_content nopadding\" id=\"tab-new-category\">

            <form method=\"post\" action=\"";
        // line 96
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/support/canned_category_create");
        echo "\" class=\"mainForm save api-form\" data-api-redirect=\"";
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("support/canned-responses");
        echo "\">
                <fieldset>
                    <div class=\"rowElem noborder\">
                        <label>";
        // line 99
        echo gettext("Title");
        echo ":</label>
                        <div class=\"formRight\">
                            <input type=\"text\" name=\"title\" value=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "title"), "html", null, true);
        echo "\" required=\"required\" placeholder=\"\"/>
                        </div>
                        <div class=\"fix\"></div>
                    </div>
                    <input type=\"submit\" value=\"";
        // line 105
        echo gettext("Create");
        echo "\" class=\"greyishBtn submitForm\" />
                </fieldset>
            </form>
        </div>
        
        <div class=\"tab_content nopadding\" id=\"tab-categories\">
            <table class=\"tableStatic wide\">
                <tbody>
                    ";
        // line 113
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "support_canned_category_pairs"));
        $context['_iterated'] = false;
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["cat_id"] => $context["cat_title"]) {
            // line 114
            echo "                    <tr ";
            if ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) {
                echo "class=\"noborder\"";
            }
            echo ">
                        <td>";
            // line 115
            echo twig_escape_filter($this->env, (isset($context["cat_title"]) ? $context["cat_title"] : null), "html", null, true);
            echo "</td>
                        <td style=\"width:13%\">
                            <a class=\"bb-button btn14\" href=\"";
            // line 117
            echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("/support/canned-category");
            echo "/";
            echo twig_escape_filter($this->env, (isset($context["cat_id"]) ? $context["cat_id"] : null), "html", null, true);
            echo "\"><img src=\"images/icons/dark/pencil.png\" alt=\"\"></a>
                            <a class=\"bb-button btn14 api-link\" data-api-confirm=\"Are you sure?\" href=\"";
            // line 118
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/support/canned_category_delete", array("id" => (isset($context["cat_id"]) ? $context["cat_id"] : null)));
            echo "\" data-api-redirect=\"";
            echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("support/canned-responses");
            echo "\"><img src=\"images/icons/dark/trash.png\" alt=\"\"></a>
                        </td>
                    </tr>
                    ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 122
            echo "                    <tr>
                        <td colspan=\"3\">";
            // line 123
            echo gettext("The list is empty");
            echo "</td>
                    </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['cat_id'], $context['cat_title'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 126
        echo "                </tbody>
            </table>
        </div>

    </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "mod_support_canned_responses.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  336 => 126,  327 => 123,  324 => 122,  305 => 118,  299 => 117,  294 => 115,  287 => 114,  269 => 113,  258 => 105,  251 => 101,  246 => 99,  238 => 96,  228 => 89,  221 => 85,  216 => 83,  207 => 77,  202 => 75,  194 => 70,  189 => 68,  181 => 65,  175 => 61,  173 => 60,  169 => 58,  161 => 55,  159 => 54,  155 => 52,  144 => 48,  138 => 47,  129 => 45,  125 => 44,  122 => 43,  116 => 42,  114 => 41,  105 => 35,  101 => 34,  94 => 30,  83 => 22,  79 => 21,  75 => 20,  71 => 19,  66 => 16,  63 => 15,  57 => 13,  50 => 8,  44 => 7,  38 => 6,  35 => 5,  32 => 4,  27 => 12,  25 => 2,);
    }
}
