<?php

/* mod_product_addon_manage.phtml */
class __TwigTemplate_41baa135248b81dd275915caf43e49fcb165ec35c7ae52b7daa41a025cf0a049 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout_default.phtml");

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'breadcrumbs' => array($this, 'block_breadcrumbs'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout_default.phtml";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["active_menu"] = "products";
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_meta_title($context, array $blocks = array())
    {
        echo "Product addon management";
    }

    // line 5
    public function block_breadcrumbs($context, array $blocks = array())
    {
        // line 6
        echo "<ul>
    <li class=\"firstB\"><a href=\"";
        // line 7
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("/");
        echo "\">";
        echo gettext("Home");
        echo "</a></li>
    <li><a href=\"";
        // line 8
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("product");
        echo "\">";
        echo gettext("Products");
        echo "</a></li>
    <li><a href=\"";
        // line 9
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("product/addons");
        echo "\">";
        echo gettext("Addons");
        echo "</a></li>
    <li class=\"lastB\">";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "title"), "html", null, true);
        echo "</li>
</ul>
";
    }

    // line 14
    public function block_content($context, array $blocks = array())
    {
        // line 15
        echo "<div class=\"widget\">

    <div class=\"head\">
        <h5>";
        // line 18
        echo gettext("Edit product addon");
        echo "</h5>
    </div>

<form method=\"post\" action=\"";
        // line 21
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/product/addon_update");
        echo "\" class=\"mainForm api-form save\" data-api-msg=\"Addon updated\">
    <fieldset>
        <div class=\"rowElem\">
            <label>";
        // line 24
        echo gettext("Status");
        echo ":</label>
            <div class=\"formRight\">
                <input type=\"radio\" name=\"status\" value=\"enabled\"";
        // line 26
        if (($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "status") == "enabled")) {
            echo " checked=\"checked\"";
        }
        echo "/><label>Enabled</label>
                <input type=\"radio\" name=\"status\" value=\"disabled\"";
        // line 27
        if (($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "status") == "disabled")) {
            echo " checked=\"checked\"";
        }
        echo " /><label>Disabled</label>
            </div>
            <div class=\"fix\"></div>
        </div>
        <div class=\"rowElem\">
            <label>";
        // line 32
        echo gettext("Activation");
        echo ":</label>
            <div class=\"formRight\">
                <input type=\"radio\" name=\"setup\" value=\"after_order\"";
        // line 34
        if (($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "setup") == "after_order")) {
            echo " checked=\"checked\"";
        }
        echo "/><label>After order is placed</label>
                <input type=\"radio\" name=\"setup\" value=\"after_payment\"";
        // line 35
        if (($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "setup") == "after_payment")) {
            echo " checked=\"checked\"";
        }
        echo " /><label>After payment is received</label>
                <input type=\"radio\" name=\"setup\" value=\"manual\"";
        // line 36
        if (($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "setup") == "manual")) {
            echo " checked=\"checked\"";
        }
        echo " /><label>Manual activation</label>
            </div>
            <div class=\"fix\"></div>
        </div>

        <div class=\"rowElem\">
            <label>";
        // line 42
        echo gettext("Icon");
        echo ":</label>
            <div class=\"formRight\">
                <input type=\"text\" name=\"icon_url\" value=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "icon_url"), "html", null, true);
        echo "\" placeholder=\"\"/>
            </div>
            <div class=\"fix\"></div>
        </div>
        <div class=\"rowElem\">
            <label>";
        // line 49
        echo gettext("Title");
        echo ":</label>
            <div class=\"formRight\">
                <input type=\"text\" name=\"title\" value=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "title"), "html", null, true);
        echo "\" required=\"required\" placeholder=\"\"/>
            </div>
            <div class=\"fix\"></div>
        </div>
        <div class=\"rowElem\">
            <label>";
        // line 56
        echo gettext("Description");
        echo ":</label>
            <div class=\"formRight\">
                <textarea name=\"description\" cols=\"5\" rows=\"5\">";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "description"), "html", null, true);
        echo "</textarea>
            </div>
            <div class=\"fix\"></div>
        </div>

        ";
        // line 63
        $this->env->loadTemplate("partial_pricing.phtml")->display(array_merge($context, array("product" => (isset($context["product"]) ? $context["product"] : null))));
        // line 64
        echo "
        <input type=\"submit\" value=\"";
        // line 65
        echo gettext("Update");
        echo "\" class=\"greyishBtn submitForm\" />
    </fieldset>

    <input type=\"hidden\" name=\"id\" value=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id"), "html", null, true);
        echo "\" />
</form>
</div>



";
    }

    public function getTemplateName()
    {
        return "mod_product_addon_manage.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 68,  182 => 65,  179 => 64,  177 => 63,  169 => 58,  164 => 56,  156 => 51,  151 => 49,  143 => 44,  138 => 42,  127 => 36,  121 => 35,  115 => 34,  110 => 32,  100 => 27,  94 => 26,  89 => 24,  83 => 21,  77 => 18,  72 => 15,  69 => 14,  62 => 10,  56 => 9,  50 => 8,  44 => 7,  41 => 6,  38 => 5,  32 => 3,  27 => 2,);
    }
}
