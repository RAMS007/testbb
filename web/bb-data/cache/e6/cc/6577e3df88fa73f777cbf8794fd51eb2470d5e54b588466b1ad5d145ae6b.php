<?php

/* mod_activity_index.phtml */
class __TwigTemplate_e6cc6577e3df88fa73f777cbf8794fd51eb2470d5e54b588466b1ad5d145ae6b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ajax")) ? ("layout_blank.phtml") : ("layout_default.phtml")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["mf"] = $this->env->loadTemplate("macro_functions.phtml");
        // line 4
        $context["active_menu"] = "activity";
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_meta_title($context, array $blocks = array())
    {
        echo "System activity";
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "<div class=\"widget\">
    <div class=\"head\"><h5 class=\"iFrames\">";
        // line 8
        echo gettext("System activity");
        echo "</h5></div>

";
        // line 10
        echo $context["mf"]->gettable_search();
        echo "
<table class=\"tableStatic wide\">
    <thead>
        <tr>
            <td style=\"width: 2%\"><input type=\"checkbox\" class=\"batch-delete-master-checkbox\"/></td>
            <td colspan=\"2\">";
        // line 15
        echo gettext("Message");
        echo "</td>
            <td>";
        // line 16
        echo gettext("Ip");
        echo "</td>
            <td>";
        // line 17
        echo gettext("Country");
        echo "</td>
            <td>";
        // line 18
        echo gettext("Date");
        echo "</td>
            <td style=\"width: 5%\">&nbsp;</td>
        </tr>
    </thead>

    <tbody>
    ";
        // line 24
        $context["events"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "activity_log_get_list", array(0 => twig_array_merge(array("per_page" => 30, "page" => $this->getAttribute((isset($context["request"]) ? $context["request"] : null), "page")), (isset($context["request"]) ? $context["request"] : null))), "method");
        // line 25
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["events"]) ? $context["events"] : null), "list"));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["i"] => $context["event"]) {
            // line 26
            echo "    <tr>
        <td><input type=\"checkbox\" class=\"batch-delete-checkbox\" data-item-id=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "id"), "html", null, true);
            echo "\"/></td>
        <td>
            ";
            // line 29
            if ($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "client")) {
                // line 30
                echo "            <a href=\"";
                echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("client/manage");
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "client"), "id"), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "client"), "name"), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, twig_gravatar_filter($this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "client"), "email")), "html", null, true);
                echo "?size=20\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "client"), "name"), "html", null, true);
                echo "\" /> ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "client"), "name"), "html", null, true);
                echo " </a>
            ";
            } elseif ($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "staff")) {
                // line 32
                echo "            <a href=\"";
                echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("staff/manage");
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "staff"), "id"), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "staff"), "name"), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, twig_gravatar_filter($this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "staff"), "email")), "html", null, true);
                echo "?size=20\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "staff"), "name"), "html", null, true);
                echo "\" /> ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "staff"), "name"), "html", null, true);
                echo "</a>
            ";
            } else {
                // line 34
                echo "            Guest
            ";
            }
            // line 36
            echo "        </td>
        <td><div style=\"overflow: auto; width: 250px;\">";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "message"), "html", null, true);
            echo "</div></td>
        <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "ip"), "html", null, true);
            echo "</td>
        <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->env->getExtension('bb')->twig_ipcountryname_filter($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "ip")), "html", null, true);
            echo "</td>
        <td>";
            // line 40
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "created_at"), "Y-m-d H:i"), "html", null, true);
            echo "</td>
        <td class=\"actions\">
            <a class=\"bb-button btn14 bb-rm-tr api-link\" data-api-confirm=\"";
            // line 42
            echo gettext("Are you sure?");
            echo "\" data-api-redirect=\"";
            echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("activity");
            echo "\" href=\"";
            echo $this->env->getExtension('bb')->twig_bb_client_link_filter("api/admin/activity/log_delete", array("id" => $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "id")));
            echo "\"><img src=\"images/icons/dark/trash.png\" alt=\"\"></a>
        </td>
    </tr>
    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 46
            echo "    <tr>
        <td colspan=\"6\">
            ";
            // line 48
            echo gettext("The list is empty");
            // line 49
            echo "        </td>
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['event'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "    </tbody>
</table>

</div>
";
        // line 56
        $this->env->loadTemplate("partial_batch_delete.phtml")->display(array_merge($context, array("action" => "admin/activity/batch_delete")));
        // line 57
        $this->env->loadTemplate("partial_pagination.phtml")->display(array_merge($context, array("list" => (isset($context["events"]) ? $context["events"] : null), "url" => "activity")));
        // line 58
        echo "
";
    }

    public function getTemplateName()
    {
        return "mod_activity_index.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 58,  186 => 57,  184 => 56,  178 => 52,  170 => 49,  168 => 48,  164 => 46,  151 => 42,  146 => 40,  142 => 39,  138 => 38,  134 => 37,  131 => 36,  127 => 34,  111 => 32,  95 => 30,  93 => 29,  88 => 27,  85 => 26,  79 => 25,  77 => 24,  68 => 18,  64 => 17,  60 => 16,  56 => 15,  48 => 10,  43 => 8,  40 => 7,  37 => 6,  31 => 3,  26 => 4,  24 => 2,);
    }
}
