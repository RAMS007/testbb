<?php

/* mod_formbuilder_field.phtml */
class __TwigTemplate_ca10751ad493ad11b1dd6fa5f4cd42d8638f2f0170e8afd7143f83dd4808ff88 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form method=\"post\" action=\"\" name=\"field_form";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "id"), "html", null, true);
        echo "\" class=\"field-form update-field\">
<div class=\"head\"><h5><span class=\"awe-edit\"></span> ";
        // line 2
        echo gettext("Edit field");
        echo "</h5><a href=\"#\" class=\"floatright close-field-form\"><span class=\"ui-icon ui-icon-closethick ui-icon\" ></span></a></div>
<div class=\"element\">

<div class=\"manage highlight\">

<div class=\"rowElem\">

    <label for=\"name\">";
        // line 9
        echo gettext("Name");
        echo "</label>

    <div class=\"formRight moreFields\">
        <ul>
            <li style=\"width: 300px\">
                <input type=\"text\" name=\"name\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name"), "html", null, true);
        echo "\" id=\"name\"/>
            </li>
            <li class=\"sep\">
                ";
        // line 17
        echo gettext("Please keep in mind that field name can not start with number. Special characters will be replaced with underscrore ( _ )");
        // line 18
        echo "            </li>
        </ul>
    </div>

</div>
<div class=\"rowElem\">
<label for=\"label\">";
        // line 24
        echo gettext("Label");
        echo "</label>

    <div class=\"formRight moreFields\">

        <ul>
            <li style=\"width: 300px\">
                <input type=\"text\" name=\"label\" value=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "label"), "html", null, true);
        echo "\" id=\"label\"/>
            </li>

        </ul>

    </div>
</div>
    <div class=\"fix\"></div>



";
        // line 41
        if (((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "checkbox") || ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "radio")) || ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "select"))) {
            // line 42
            echo "<div class=\"rowElem\">
    <label>";
            // line 43
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type")), "html", null, true);
            echo " ";
            echo gettext("options");
            echo "</label>

    <div class=\"formRight moreFields\">
        <div class=\"rowElem noborder\">
            <ul>
                <li style=\"width: 200px\">
                    <label>";
            // line 49
            echo gettext("Label");
            echo "</label>
                </li>
                <li class=\"sep\"></li>
                <li style=\"width: 200px\">
                    <label>";
            // line 53
            echo gettext("Value");
            echo "</label>
                </li>
            </ul>
        </div>
        ";
            // line 57
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"));
            foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                // line 58
                echo "        <div class=\"rowElem noborder\">
            <ul>
                <li style=\"width: 200px\">
                    <input type=\"text\" name=\"labels[]\" value=\"";
                // line 61
                echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                echo "\"/>
                </li>
                <li class=\"sep\">=</li>
                <li style=\"width: 200px\">
                    <input type=\"text\" name=\"values[]\" value=\"";
                // line 65
                echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                echo "\"/>
                </li>
            </ul>
        </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo "        <div class=\"rowElem noborder copyfields\">
            <ul>
                <li style=\"width: 200px\">
                    <input type=\"text\" name=\"labels[]\"/>
                </li>
                <li class=\"sep\">=</li>
                <li style=\"width: 200px\">
                    <input type=\"text\" name=\"values[]\"/>
                </li>
            </ul>
        </div>
        <div class=\"rowElem noborder\">
            <ul>
                <li style=\"width: 200px\">
                    <label>
                        <a href=\"#\" class=\"new-field button greyishBtn\" rel=\"";
            // line 85
            echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
            echo "\">";
            echo gettext("+ Add new option");
            echo "</a>
                    </label>
                </li>
            </ul>
        </div>
        <div class=\"fix\"></div>
    </div>
    <div class=\"fix\"></div>

</div>
";
        }
        // line 96
        echo "
<div class=\"rowElem\">
    <label>";
        // line 98
        echo gettext("Options");
        echo "</label>

    <div class=\"formRight moreFields\">

                <input type=\"checkbox\" name=\"required\" value=\"1\" id=\"required\" value=\"1\" ";
        // line 102
        echo ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "required") == 1)) ? ("checked=\"checked\"") : (""));
        echo "/>
                <label for=\"required\">";
        // line 103
        echo gettext("Required");
        echo "</label>

                <input type=\"checkbox\" name=\"hide_label\" value=\"1\" id=\"hide_label\" value=\"1\" ";
        // line 105
        echo ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "hide_label") == 1)) ? ("checked=\"checked\"") : (""));
        echo "/>
                <label for=\"hide_label\">";
        // line 106
        echo gettext("Hide label");
        echo "</label>

                <input type=\"checkbox\" name=\"readonly\" value=\"1\" id=\"readonly\" value=\"1\" ";
        // line 108
        echo ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "readonly") == 1)) ? ("checked=\"checked\"") : (""));
        echo "/>
                <label for=\"readonly\">";
        // line 109
        echo gettext("Read only");
        echo "</label>

    </div>
    <div class=\"fix\"></div>

</div>
";
        // line 115
        if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "text")) {
            // line 116
            echo "<div class=\"rowElem\">
    <label>";
            // line 117
            echo gettext("Additional settings");
            echo "</label>


    <div class=\"formRight moreFields\">
        <ul>
            <label for=\"prefix_text\">";
            // line 122
            echo gettext("Prepend text");
            echo "</label>
            <li style=\"width: 100px\">
                <input type=\"text\" name=\"prefix\" value=\"";
            // line 124
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "prefix"), "html", null, true);
            echo "\" id=\"prefix_text\"/>
            </li>
            <li class=\"sep\"></li>
            <label for=\"suffix_text\">";
            // line 127
            echo gettext("Append text");
            echo "</label>
            <li style=\"width: 100px\">
                <input type=\"text\" name=\"suffix\" value=\"";
            // line 129
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "suffix"), "html", null, true);
            echo "\" id=\"suffix_text\"/>
            </li>

        </ul>

    </div>
    <div class=\"fix\"></div>

</div>
";
        }
        // line 139
        if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "textarea")) {
            // line 140
            echo "<div class=\"rowElem\">
    <label>";
            // line 141
            echo gettext("Textarea size");
            echo "</label>


    <div class=\"formRight moreFields\">
        <ul>
            <label for=\"textarea-height\">";
            // line 146
            echo gettext("Width");
            echo "</label>
            <li style=\"width: 100px\">
                <input type=\"text\" name=\"textarea_size[]\" value=\"";
            // line 148
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"), "width"), "html", null, true);
            echo "\" id=\"textarea-width\"/>
                <input type=\"hidden\" name=\"textarea_option[]\" value=\"width\"/>
            </li>
            <li class=\"sep\">";
            // line 151
            echo gettext("px");
            echo "</li>
            <label for=\"textarea-width\">";
            // line 152
            echo gettext("Height");
            echo "</label>
            <li style=\"width: 100px\">
                <input type=\"text\" name=\"textarea_size[]\" value=\"";
            // line 154
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"), "height"), "html", null, true);
            echo "\" id=\"textarea-height\"/>
                <input type=\"hidden\" name=\"textarea_option[]\" value=\"height\"/>
            </li>
            <li class=\"sep\">";
            // line 157
            echo gettext("px");
            echo "</li>
        </ul>
    </div>
    <div class=\"fix\"></div>

</div>
";
        }
        // line 164
        echo "
<div class=\"rowElem\">
    <label for=\"default_value\">";
        // line 166
        echo gettext("Default value/option");
        echo "</label>

    <div class=\"formRight moreFields\">
        <ul>
            ";
        // line 170
        if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "text")) {
            // line 171
            echo "                    <li class=\"sep\" id=\"prepended_text\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "prefix"), "html", null, true);
            echo "</li>
                    <li style=\"width: 50%\"><input type=\"text\" name=\"default_value\" value=\"";
            // line 172
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "default_value"), "html", null, true);
            echo "\"  id=\"default_value\"/></li>
                    <li class=\"sep\" id=\"appended_text\">";
            // line 173
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "suffix"), "html", null, true);
            echo "</li>
            ";
        } elseif (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "checkbox")) {
            // line 175
            echo "            ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"));
            foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                // line 176
                echo "                <input type=\"checkbox\" name=\"default_value[]\" value=\"";
                echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                echo "\" id=\"";
                echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                echo "_";
                echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                echo "\" ";
                if (twig_in_filter((isset($context["v"]) ? $context["v"] : null), $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "default_value"))) {
                    echo "checked=\"checked\"";
                }
                echo ">
                <label for=\"";
                // line 177
                echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                echo "_";
                echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                echo "</label>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 179
            echo "            ";
        } elseif (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "radio")) {
            // line 180
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"));
            foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                // line 181
                echo "                <input type=\"radio\" name=\"default_value\" value=\"";
                echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                echo "\" id=\"";
                echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                echo "_";
                echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                echo "\" ";
                if (((isset($context["v"]) ? $context["v"] : null) == $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "default_value"))) {
                    echo "checked=\"checked\"";
                }
                echo ">
                <label for=\"";
                // line 182
                echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                echo "_";
                echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                echo "</label>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 184
            echo "            ";
        } elseif (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "select")) {
            // line 185
            echo "                <select name=\"default_value\">
                    <option value=\"\">---</option>
                    ";
            // line 187
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"));
            foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                // line 188
                echo "                    <option value=\"";
                echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                echo "\"
                    ";
                // line 189
                echo ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "default_value") == (isset($context["v"]) ? $context["v"] : null))) ? ("selected") : (""));
                echo ">";
                echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                echo "</option>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 191
            echo "                </select>
            ";
        } elseif (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "textarea")) {
            // line 193
            echo "            <li style=\"width: auto; max-width:500px; overflow: scroll;\">
                <textarea name=\"default_value\" style=\"width: ";
            // line 194
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"), "width"), "html", null, true);
            echo "px; height: ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "options"), "height"), "html", null, true);
            echo "px; \">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "default_value"), "html", null, true);
            echo "</textarea>
            </li>
            ";
        }
        // line 197
        echo "        </ul>
    </div>

    <div class=\"fix\"></div>
</div>
<div class=\"rowElem\">
    <label for=\"description\">";
        // line 203
        echo gettext("Description");
        echo "</label>

    <div class=\"formRight moreFields\">
        <ul>
            <li style=\"width: 300px\">
                <input type=\"text\" name=\"description\" value=\"";
        // line 208
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "description"), "html", null, true);
        echo "\" id=\"description\"/>
            </li>
        </ul>
    </div>
    <div class=\"fix\"></div>
</div>

<div class=\"rowElem pull-right\">
    <input type=\"submit\" class=\"button blueBtn save-field-form\" value=\"";
        // line 216
        echo gettext("Save");
        echo "\">
    <div class=\"fix\"></div>
</div>
</div>
</div>

<input type=\"hidden\" name=\"id\" value=\"";
        // line 222
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "id"), "html", null, true);
        echo "\">
<input type=\"hidden\" name=\"form_id\" value=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "id"), "html", null, true);
        echo "\">
<input type=\"hidden\" name=\"type\" value=\"";
        // line 224
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type"), "html", null, true);
        echo "\"/>
</form>";
    }

    public function getTemplateName()
    {
        return "mod_formbuilder_field.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  481 => 224,  477 => 223,  473 => 222,  464 => 216,  453 => 208,  445 => 203,  437 => 197,  427 => 194,  424 => 193,  420 => 191,  410 => 189,  405 => 188,  401 => 187,  397 => 185,  394 => 184,  382 => 182,  369 => 181,  364 => 180,  361 => 179,  349 => 177,  336 => 176,  331 => 175,  326 => 173,  322 => 172,  317 => 171,  315 => 170,  308 => 166,  304 => 164,  294 => 157,  288 => 154,  283 => 152,  279 => 151,  273 => 148,  268 => 146,  260 => 141,  257 => 140,  255 => 139,  242 => 129,  237 => 127,  231 => 124,  226 => 122,  218 => 117,  215 => 116,  213 => 115,  204 => 109,  200 => 108,  195 => 106,  191 => 105,  186 => 103,  182 => 102,  175 => 98,  171 => 96,  155 => 85,  138 => 70,  127 => 65,  120 => 61,  115 => 58,  111 => 57,  104 => 53,  97 => 49,  86 => 43,  83 => 42,  81 => 41,  67 => 30,  58 => 24,  50 => 18,  48 => 17,  42 => 14,  34 => 9,  24 => 2,  19 => 1,);
    }
}
