<?php

/* mod_example_index.phtml */
class __TwigTemplate_6cca5f8a3205fa9809dabc83324bbe804996a3d5db7602d899f432dde4c6bb80 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((($this->getAttribute((isset($context["request"]) ? $context["request"] : null), "ajax")) ? ("layout_blank.phtml") : ("layout_default.phtml")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 5
        $context["active_menu"] = "example";
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_meta_title($context, array $blocks = array())
    {
        echo gettext("Example module");
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "
<div class=\"widget\">
    <div class=\"head\">
        <h5>";
        // line 11
        echo gettext("Example module title");
        echo "</h5>
    </div>

    <div class=\"help\">
        <h3>";
        // line 15
        echo gettext("This is an example extension content");
        echo "</h3>
    </div>
    <div class=\"body\">
        <a href=\"";
        // line 18
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("example");
        echo "\">Example module index</a> <br/>
        <a href=\"";
        // line 19
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("extension/settings/example");
        echo "\">Example settings page</a> <br/>
        <a href=\"";
        // line 20
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("example");
        echo "/test\">Test link with static URL</a> <br/>
        <a href=\"";
        // line 21
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("example");
        echo "/user/1\">Test link location with parameter 1</a> <br/>
        <a href=\"";
        // line 22
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("example");
        echo "/user/2\">Test link location with parameter 2</a> <br/>
        <a href=\"";
        // line 23
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("example");
        echo "/user/3\">Test link location with parameter 3</a> <br/>
        <a href=\"";
        // line 24
        echo $this->env->getExtension('bb')->twig_bb_admin_link_filter("example");
        echo "/api\">API example</a>
        <a href=\"";
        // line 25
        echo $this->env->getExtension('bb')->twig_bb_client_link_filter("example");
        echo "\" target=\"_blank\">Client area link example</a>
    </div>

    ";
        // line 29
        echo "    ";
        if ((isset($context["youparamname"]) ? $context["youparamname"] : null)) {
            // line 30
            echo "    <div class=\"help\">
        <h3>";
            // line 31
            echo gettext("Parameters from Controller");
            echo "</h3>
    </div>
    <div class=\"body\">
        <p>You have passed parameter youparamname: <strong>";
            // line 34
            echo twig_escape_filter($this->env, (isset($context["youparamname"]) ? $context["youparamname"] : null), "html", null, true);
            echo "</strong></p>
    </div>
    ";
        }
        // line 37
        echo "


    ";
        // line 41
        echo "    ";
        if ((isset($context["userid"]) ? $context["userid"] : null)) {
            // line 42
            echo "    <div class=\"help\">
        <h3>";
            // line 43
            echo gettext("Parameters from URL");
            echo "</h3>
    </div>
    <div class=\"body\">
        <p>You have passed parameter userid: <strong>";
            // line 46
            echo twig_escape_filter($this->env, (isset($context["userid"]) ? $context["userid"] : null), "html", null, true);
            echo "</strong></p>
    </div>
    ";
        }
        // line 49
        echo "
    ";
        // line 50
        if ((isset($context["api_example"]) ? $context["api_example"] : null)) {
            // line 51
            echo "        ";
            // line 52
            echo "        <div class=\"help\">
            <h3>";
            // line 53
            echo gettext("API example");
            echo "</h3>
        </div>
        <div class=\"body\">
        <h5 class=\"pt12\">Data from API and passed to template from controller</h5>
        <pre>";
            // line 57
            if ($this->env->isDebug()) {
                var_dump((isset($context["list_from_controller"]) ? $context["list_from_controller"] : null));
            }
            echo "</pre>

        <h5 class=\"pt12\">Data from API accessed directly from template file</h5>
        ";
            // line 60
            $context["list"] = $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "example_get_something", array(0 => array("microsoft" => 1)), "method");
            // line 61
            echo "        <pre>";
            if ($this->env->isDebug()) {
                var_dump((isset($context["list"]) ? $context["list"] : null));
            }
            echo "</pre>
        </div>
    ";
        }
        // line 64
        echo "</div>

<div class=\"widget\">
    <div class=\"head\">
        <h5>";
        // line 68
        echo gettext("README");
        echo "</h5>
    </div>
    
    <div class=\"body list arrowBlue\">
        ";
        // line 72
        echo twig_markdown_filter($this->env, $this->getAttribute((isset($context["guest"]) ? $context["guest"] : null), "example_readme"));
        echo "
    </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "mod_example_index.phtml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 72,  173 => 68,  167 => 64,  158 => 61,  156 => 60,  148 => 57,  141 => 53,  138 => 52,  136 => 51,  134 => 50,  131 => 49,  125 => 46,  119 => 43,  116 => 42,  113 => 41,  108 => 37,  102 => 34,  96 => 31,  93 => 30,  90 => 29,  84 => 25,  80 => 24,  76 => 23,  72 => 22,  68 => 21,  64 => 20,  60 => 19,  56 => 18,  50 => 15,  43 => 11,  38 => 8,  35 => 7,  29 => 3,  24 => 5,);
    }
}
