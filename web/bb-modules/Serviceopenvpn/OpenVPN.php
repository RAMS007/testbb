<?php

namespace Box\Mod\Serviceopenvpn;

class OpenVPN implements \Box\InjectionAwareInterface{

    protected $api_host = null; //
    protected $api_ID = ''; // API ID
    protected $api_key = ''; // API KEY

    protected $_parameters = array();

    /**
     * @var \Box_Di
     */
    protected $di;

    /**
     * @param \Box_Di $di
     */
    public function setDi($di)
    {
        $this->di = $di;
    }

    /**
     * @return \Box_Di
     */
    public function getDi()
    {
        return $this->di;
    }


    /**
     *  Controlpanel URL
     * @return string
     */
    public function getApiHost()
    {
        return $this->api_host;
    }

    /**
     * @param string $api_host
     */
    public function setApiHost($api_host)
    {
        $this->api_host = $api_host;
    }

    /**
     * @return string
     */
    public function getApiID()
    {
        return $this->api_ID;
    }

    /**
     * @param string $api_ID
     */
    public function setApiID($api_ID)
    {
        $this->api_ID = $api_ID;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->api_key;
    }

    /**
     * @param string $api_key
     */
    public function setApiKey($api_key)
    {
        $this->api_key = $api_key;
    }

    public function buildUrl(array $config)
    {
        return $config['protocol'] ."://". $config['ipaddress'] . ":" . $config['port'] . "/api/" . $config['usertype'] . "/command.php";
    }

    public function getSecureUrl(array $config)
    {
        $config['port'] = $this->di['array_get']($config, 'port', 5656);
        $config['protocol'] = 'https';
        return $this->buildUrl($config);
    }

    public function getUrl(array $config)
    {
        $config['port'] = $this->di['array_get']($config, 'port', 5353);
        $config['protocol'] = 'http';
        return $this->buildUrl($config);
    }

    public function setConfig(array $c)
    {
        $required = array(
            'id'        => 'API ID is missing',
            'key'       => 'API key is missing',
            'ipaddress' => 'API ip address is missing',
        );
        $this->di['validator']->checkRequiredParamsForArray($required, $c);

        $c['usertype'] = $this->di['array_get']($c, 'usertype', 'admin');
        $c['secure']   = $this->di['array_get']($c, 'secure', false);
        $c['port']     = $this->di['array_get']($c, 'port', null);

        $url = $this->getUrl($c);

        if ($c['secure']) {
            $url = $this->getSecureUrl($c);
        }

        $this->setApiHost($url);
        $this->setApiID($c['id']);
        $this->setApiKey($c['key']);
    }


}