<?php
/**
 * BoxBilling
 *
 * @copyright BoxBilling, Inc (http://www.boxbilling.com)
 * @license   Apache-2.0
 *
 * Copyright BoxBilling, Inc
 * This source file is subject to the Apache-2.0 License that is bundled
 * with this source code in the file LICENSE
 */

namespace Box\Mod\Serviceopenvpn;

use Box\InjectionAwareInterface;

class Service implements InjectionAwareInterface
{
    protected $di;

    public function setDi($di)
    {
        $this->di = $di;
    }

    public function getDi()
    {
        return $this->di;
    }

    public function install()
    {
        $sql = "

CREATE TABLE IF NOT EXISTS `service_openvpn` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `username` varchar(255) NOT NULL DEFAULT '0',
  `password` varchar(255) NOT NULL DEFAULT '0',
  `upload` int(10) unsigned NOT NULL DEFAULT '0',
  `download` int(10) unsigned NOT NULL DEFAULT '0',
  `sessions` int(10) unsigned NOT NULL DEFAULT '0',
  `trafic` bigint(20) unsigned NOT NULL DEFAULT '0',
  `lock` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


    ";
        $this->di['db']->exec($sql);
    }

    public function uninstall()
    {
        $this->di['db']->exec("DROP TABLE IF EXISTS `service_openvpn`");
    }

    public function getCartProductTitle($product, array $data)
    {
        return __('Virtual private server :title',
            array(
                ':title' => $product->title
                //          ':template' => $data['template'],
                //               ':hostname' => $data['hostname']
            ));
    }

    /**
     * Check what user entered in for fields when placing order
     *
     * @param array $data
     * @throws \Box_Exception
     */
    public function validateOrderData(array $data)
    {
        /*   if (!isset($data['hostname']) || empty($data['hostname'])) {
               throw new \Box_Exception('Please enter VPS hostname.', null, 7101);
           }

           $ValidHostnameRegex = "/^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$/";
           if (!preg_match($ValidHostnameRegex, $data['hostname'])) {
               throw new \Box_Exception('Hostname :hostname is not valid.', array(':hostname' => $data['hostname']), 7102);
           }*/

        /*      if (!isset($data['template']) || empty($data['template'])) {
                  throw new \Box_Exception('Please select VPS template.', null, 7103);
              }*/
    }

    /**
     * Save config values from admin panel (section API configuration)
     *
     * @param $cluster_id
     * @param $data
     * @throws \Box_Exception
     */
    public function updateMasterConfig($cluster_id, $data)
    {
        if (!isset($data['id'])) {
            throw new \Box_Exception(' API ID is missing.', null, 7201);
        }

        if (!isset($data['key'])) {
            throw new \Box_Exception(' API KEY is missing.', null, 7202);
        }

        if (!isset($data['ipaddress'])) {
            throw new \Box_Exception(' API ipaddress is missing.', null, 7203);
        }

        if (!isset($data['secure'])) {
            throw new \Box_Exception(' API secure is missing.', null, 7204);
        }

        if (!isset($data['port'])) {
            throw new \Box_Exception(' API port is missing.', null, 7204);
        }

        $sql = "
            UPDATE extension_meta 
            SET meta_value = :config
            WHERE rel_type = 'cluster'
            AND extension = 'mod_serviceopenvpn'
            AND rel_id = :cluster_id
            AND meta_key = 'config'
        ";

        $c = array(
            'id'        => $data['id'],
            'key'       => $data['key'],
            'ipaddress' => $data['ipaddress'],
            'secure'    => $data['secure'],
            'port'      => $data['port'],
            'usertype'  => 'admin',
        );
        $params = array(
            'config'     => json_encode($c),
            'cluster_id' => $cluster_id,
        );
        $this->di['db']->exec($sql, $params);
    }

    /**
     * Get config values from DB and paste them to admin panel (section API configuration)
     *
     * @param $cluster_id
     * @return array|mixed
     */
    public function getMasterConfig($cluster_id)
    {
        $sql = "
            SELECT meta_value 
            FROM extension_meta 
            WHERE extension = 'mod_serviceopenvpn'
            AND rel_type = 'cluster'
            AND rel_id = :cluster_id
            AND meta_key = 'config'
        ";
        $config = $this->di['db']->getCell($sql, array('cluster_id' => $cluster_id));
        if (!$config) {
            $config = array();
            $meta = $this->di['db']->dispense('extension_meta');
            $meta->extension = 'mod_serviceopenvpn';
            $meta->rel_type = 'cluster';
            $meta->rel_id = $cluster_id;
            $meta->meta_key = 'config';
            $meta->meta_value = json_encode($config);
            $meta->created_at = date('Y-m-d H:i:s');
            $meta->updated_at = date('Y-m-d H:i:s');
            $this->di['db']->store($meta);
        } else {
            $config = json_decode($config, 1);
        }
        if (!isset($config['secure'])) {
            $config['secure'] = null;
        }
        if (!isset($config['port'])) {
            $config['port'] = null;
        }

        return $config;
    }


    public function setSolusUserPassword($client, $username, $password)
    {
        $meta = $this->di['db']->dispense('extension_meta');
        $meta->extension = 'mod_serviceopenvpn';
        $meta->client_id = $client->id;
        $meta->meta_key = 'openvpn_username';
        $meta->meta_value = $username;
        $meta->created_at = date('Y-m-d H:i:s');
        $meta->updated_at = date('Y-m-d H:i:s');
        $this->di['db']->store($meta);

        $meta = $this->di['db']->dispense('extension_meta');
        $meta->extension = 'mod_serviceopenvpn';
        $meta->client_id = $client->id;
        $meta->meta_key = 'openvpn_password';
        $meta->meta_value = $password;
        $meta->created_at = date('Y-m-d H:i:s');
        $meta->updated_at = date('Y-m-d H:i:s');
        $this->di['db']->store($meta);
    }

    public function getSolusUserPassword($client)
    {
        $sql = "
            SELECT meta_value 
            FROM extension_meta 
            WHERE client_id = :cid 
            AND extension = 'mod_serviceopenvpn'
            AND meta_key = :key
        ";
        $username = $this->di['db']->getCell($sql, array('cid' => $client->id, 'key' => 'openvpn_username'));
        $password = $this->di['db']->getCell($sql, array('cid' => $client->id, 'key' => 'openvpn_password'));

        if (!$username) {
            $username = $client->email;
            $password = $this->di['tools']->generatePassword(14, 4);
            $this->setSolusUserPassword($client, $username, $password);
        }

        return array($username, $password);
    }

    /**
     * Check if user have username and pass to server
     * @param $data
     * @return mixed
     */
    public function checkServer($data)
    {
        $sql = "
            SELECT id, download,upload
            FROM service_openvpn
            WHERE username = :username
            AND password = :pass
        ";
        $row = $this->di['db']->getAll($sql, array('username' => $data['email'], 'pass' => $data['password2']));
        return ($row);
    }

    /**
     * Process connect action
     * @param $data
     * @return mixed
     */
    public function workConnect($data)
    {

        $sql = "
                SELECT  service_openvpn.upload AS uploadDB,
                        service_openvpn.download AS downloadDB,
                        service_openvpn.sessions,service_openvpn.trafic ,
                        service_openvpn.client_id
                FROM service_openvpn
                INNER JOIN client_order ON
                        service_openvpn.client_id = client_order.client_id
                WHERE service_openvpn.username= :username AND
                        client_order.status='active'
        ";
        $row = $this->di['db']->getAll($sql, array('username' => $data['email']));
        return ($row);
    }

    /**
     * Process Update action
     * @param $data
     * @return mixed
     */
    public function workUpdate($data)
    {

        $sql = "
                SELECT  id
                FROM    connections
                WHERE user_id=:user_id
                      AND ip =INET_ATON(:ip)
                      AND port =:port
                      AND server =:server
                      AND is_closed=0
        ";
        $id = $this->di['db']->getCell($sql, array('user_id' => $data['client_id'], 'ip' => $data['ip'], 'port' => $data['port'], 'server' => $data['server']));

        if ($id) {
            $query = "
                UPDATE connections
                SET recieve = :receive ,
                    send = :send ,
                    updated_at = NOW()
                WHERE id = :id
                ";
            $values = array(
                'receive' => $data['receive'],
                'send'    => $data['send'],
                'id'      => $id
            );
            $this->di['db']->exec($query, $values);
        } else {
            $sql = "
           INSERT INTO `connections` (`user_id`, `ip`, `port`, `server`, `recieve`, `send`, `opened_at`) VALUES
           (:user_id, INET_ATON (:ip), :port, :server, :receive, :send, NOW());

        ";
            $values = array(
                'user_id' => $data['client_id'],
                'ip'      => $data['ip'],
                'port'    => $data['port'],
                'server'  => $data['server'],
                'receive' => $data['receive'],
                'send'    => $data['send']
            );
            $this->di['db']->exec($sql, $values);
        }
        $trafic = $this->getTrafic($data['client_id']);
        return ($trafic);
    }

    /**
     * Process Disconect Action
     * @param $data
     */
    public function workDisconnect($data)
    {
        $sql = "
                SELECT  connections.id
                FROM    service_openvpn
                INNER JOIN connections
                ON service_openvpn.client_id = connections.user_id
                WHERE service_openvpn.username=:user_name
                      AND ip =INET_ATON(:ip)
                      AND port =:port
                      AND server =:server
                      AND is_closed=0
        ";
        $id = $this->di['db']->getCell($sql, array('user_name' => $data['email'], 'ip' => $data['ip'], 'port' => $data['port'], 'server' => $data['server']));

        if ($id) {
            $query = "
                UPDATE connections
                SET recieve = :receive ,
                    send = :send ,
                    updated_at = NOW(),
                    is_closed=1
                WHERE id = :id
                ";
            $values = array(
                'receive' => $data['receive'],
                'send'    => $data['send'],
                'id'      => $id
            );
            $this->di['db']->exec($query, $values);
        }
    }

    /**
     * Get traficfor current user
     *
     * @param $userId
     */
    public function getTrafic($userId,$separatly=false)
    {

        $sql = "
                SELECT SUM(recieve) as download , SUM(send) as upload
                FROM `connections`
                WHERE MONTH(`opened_at`) = MONTH(NOW())
                      AND user_id = :user_id ";
        $res = $this->di['db']->getAll($sql, array('user_id' => $userId));
        if($separatly){
            return $res[0];
        }else {
            $trafic = $res[0]['download'] + $res[0]['upload'];
            return $trafic;
        }
    }

    /**
     * Count current sessions
     * @param $userId
     * @return mixed
     */
    public function countSessions($userId)
    {
        $sql = "
                SELECT COUNT(id) AS COUNT
                FROM `connections`
                WHERE is_closed=0
                      AND user_id = :user_id ";
        $id = $this->di['db']->getCell($sql, array('user_id' => $userId));
        return $id;
    }

    /**
     * Create order
     *
     * @param $order
     * @return void
     */
    public function create($order)
    {
        $c = json_decode($order->config, 1);
        $this->validateOrderData($c);
        $model = $this->di['db']->dispense('service_openvpn');
        $model->client_id = $order->client_id;
        $model->created_at = date('Y-m-d H:i:s');
        $model->updated_at = date('Y-m-d H:i:s');
        $this->di['db']->store($model);
        return $model;
    }

    /**
     * @param $order
     * @return void
     */
    public function activate($order, $model)
    {
        if (!is_object($model)) {
            throw new \Box_Exception('Could not activate order. Service was not created', null, 7456);
        }

        $client = $this->di['db']->load('client', $order->client_id);
        $product = $this->di['db']->load('product', $order->product_id);
        if (!$product) {
            throw new \Box_Exception('Could not activate order because ordered product does not exists', null, 7457);
        }
        $pconfig = json_decode($product->config, 1);
        list($username, $password) = $this->getSolusUserPassword($client);
        /**
         * fill model table service_openvpn
         */
        $model->username = $username;
        $model->password = $password;
        $model->upload = $pconfig['up_speed'];
        $model->download = $pconfig['down_speed'];
        $model->sessions = $pconfig['sessions'];
        switch ($pconfig['traficunit']) {
            case 'kb':
                $multiple = 1;
                break;
            case 'Mb':
                $multiple = 1024;
                break;
            case 'Gb':
                $multiple = 1024 * 1024;
                break;
            case 'Tb':
                $multiple = 1024 * 1024 * 1024;
                break;
        }
        $model->trafic = $pconfig['trafic'] * $multiple; //stored in Kb
        $model->updated_at = date('Y-m-d H:i:s');
        $this->di['db']->store($model);
        return;
    }

    private function _getApi()
    {
        if (APPLICATION_ENV == 'testing') {
            return new \Vps_openvpnMock($this->getMasterConfig(1));
        }
        return $this->di['openvpn']($this->getMasterConfig(1));
    }

    public function info($client_id)
    {
        $client = $this->di['db']->load('client', $client_id);
        $order = $this->di['db']->load('client_order', $client_id);
/*        $product = $this->di['db']->load('product', $order->product_id);
        $pconfig = json_decode($product->config, 1);
        switch ($pconfig['traficunit']) {
            case 'kb':
                $multiple = 1;
                break;
            case 'Mb':
                $multiple = 1024;
                break;
            case 'Gb':
                $multiple = 1024 * 1024;
                break;
            case 'Tb':
                $multiple = 1024 * 1024 * 1024;
                break;
        }
        $config_trafic = $pconfig['trafic'] * $multiple;*/



        list($username, $password) = $this->getSolusUserPassword($client);
        $result['sessions']=$this->countSessions($client_id);
        $result['password']=$password;



        $trafic = $this->getTrafic($client_id, true);
        $total_trafic =$trafic['download'] + $trafic['upload']; ;
        $result['bandwidth']=$total_trafic.','.implode(',',$trafic);
  //      $result = array('state' => 'online', 'plan' => 'plan', 'type' => 'test');
        return $result;
    }


}

