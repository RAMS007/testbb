<?php
namespace Box\Mod\Serviceopenvpn\Api;

/**
 * openvpn service management
 */
class Guest extends \Api_Abstract
{

    /**
     * Return is user logined or not (for billing)
     *
     * @param $data
     * @return string
     */
    public function login($data)
    {
        $response_none = "0, Auth failed";
        $data = array('email' => 'sramsiks@gmail.com', 'password' => 'wemorion007');
        if (empty($_REQUEST["email"]) OR empty($_REQUEST["password"])) {
            die("0,No Data");
        }
        $model = $this->di['db']->findOne('Client', 'email = ? AND status = ?', array($data['email'], \Model_Client::ACTIVE));
        if ($model == null) {
            die("0,No such user");
        }
        list($username, $password) = $this->getService()->getSolusUserPassword($model);
        $config = $this->di['mod_config']('client');
        if (isset($config['require_email_confirmation']) && (int)$config['require_email_confirmation']) {
            if (!$model->email_approved) {
                die($response_none);
            }
        }
        $t = $this->di['auth']->authorizeUser($model, $data['password']);
        if (($t)) {
            die('1,' . $password);
        } else {
            die($response_none);
        }
    }

    /**
     * check login on server
     *
     * @param $data
     * @return string
     */
    public function check_server($data)
    {
        //       $data = array('email' => 'sramsiks@gmail.com', 'password2' => 's3ed8l3m');
        if (empty($_REQUEST["email"]) OR empty($_REQUEST["password2"])) {
            die("0,No Data");
        }
        $server = $this->getService()->checkServer($data);
        if (!empty($server)) {
            //          die('1,' . $server[0]['upload'] . ',' . $server[0]['download']);
            die('1,Ok');
        } else {
            die('0,Error');
        }
    }

    /**
     * Method connect check if user can coonect, if order is active, check count sessions
     *
     * @param $data
     *
     * return 0 at error. 1 at succes connect
     */
    public function connect($data)
    {
        //       $data = array('email' => 'sramsiks@gmail.com');
        if (empty($_REQUEST["email"])) {
            die("0,No Data");
        }
        $parametrs = $this->getService()->workConnect($data);
        $sessions = $this->getService()->countSessions($parametrs[0]['client_id']);
        if (!empty($parametrs)) {
            if ($sessions >= $parametrs[0]['sessions']) {
                die("0,Session limit");
            } else {
                $this->getService()->workUpdate($data);
            }
            die("1,Ok");
        } else {
            die("0,Error");
        }
    }

    /**
     * Method update check if user still  active and can connect, update connection statistics,
     * count used traffic and compare with total allowed
     *
     * @param $data
     */
    public function update($data)
    {
        //     $data = array('email' => 'sramsiks@gmail.com', 'ip' => '192.168.0.1', 'port' => '8080', 'server' => 'test server', 'receive' => '1234', 'send' => '321', 'download' => '10M', 'upload' => '100M');
        if (empty($_REQUEST["email"])) {
            die("0,No Data"); //If not, die.
        }
        $parametrs = $this->getService()->workConnect($data);
        $data = array_merge($data, $parametrs[0]);
        $trafic = $this->getService()->workUpdate($data);
//dont check trafic rifght now
        if (preg_match('/([0-9]+)M/', $_REQUEST["upload"], $speed)) {
            $_REQUEST["upload"] = $speed[1];
        }
        if (preg_match('/([0-9]+)M/', $_REQUEST["download"], $speed)) {
            $_REQUEST["download"] = $speed[1];
        }

        if (!empty($parametrs)) {
            if (($_REQUEST["upload"] <> $parametrs[0]['upload']) OR ($_REQUEST["download"] <> $parametrs[0]['download'])) {
                die('0,' . $parametrs[0]['download'] . 'M,' . $parametrs[0]['upload'] . 'M');

            } else {
                die("1,Ok");
            }
        } else {
            die("0,0M,0M");
        }

    }

    /**
     * Method disconnect close active connection, write used trafic to db
     *
     * @param $data
     */
    public function disconnect($data)
    {
        //      $data = array('email' => 'sramsiks@gmail.com', 'ip' => '192.168.0.1', 'port' => '8080', 'server' => 'test server', 'receive' => '1234', 'send' => '321');
        if (empty($_REQUEST["email"])) {
            die("0,No Data"); //If not, die.
        }
        $this->getService()->workDisconnect($data);
        die("1,Ok");
    }
}