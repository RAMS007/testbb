<?php
/**
 * BoxBilling
 *
 * @copyright BoxBilling, Inc (http://www.boxbilling.com)
 * @license   Apache-2.0
 *
 * Copyright BoxBilling, Inc
 * This source file is subject to the Apache-2.0 License that is bundled
 * with this source code in the file LICENSE
 */

namespace Box\Mod\Serviceopenvpn\Controller;

class Admin implements \Box\InjectionAwareInterface
{
    protected $di;

    /**
     * @param mixed $di
     */
    public function setDi($di)
    {
        $this->di = $di;
    }

    /**
     * @return mixed
     */
    public function getDi()
    {
        return $this->di;
    }

    public function register(\Box_App &$app)
    {
        $app->get('/serviceopenvpn', 'get_index', array(), get_class($this));
        $app->get('/serviceopenvpn/import/clients', 'get_import_clients', array(), get_class($this));
        $app->get('/serviceopenvpn/import/servers', 'get_import_servers', array(), get_class($this));
    }
    
    public function get_index(\Box_App $app)
    {
        $this->di['is_admin_logged'];
        $app->redirect('/extension/settings/serviceopenvpn');
        //  return $app->render('mod_servicesolusvm_import_clients');
    }

}