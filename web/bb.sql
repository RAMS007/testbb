-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.7.13-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win64
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица boxbilling.activity_admin_history
CREATE TABLE IF NOT EXISTS `activity_admin_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `admin_id` bigint(20) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_id_idx` (`admin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.activity_admin_history: 0 rows
/*!40000 ALTER TABLE `activity_admin_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity_admin_history` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.activity_client_email
CREATE TABLE IF NOT EXISTS `activity_client_email` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) DEFAULT NULL,
  `sender` varchar(255) DEFAULT NULL,
  `recipients` text,
  `subject` varchar(255) DEFAULT NULL,
  `content_html` text,
  `content_text` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id_idx` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.activity_client_email: 0 rows
/*!40000 ALTER TABLE `activity_client_email` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity_client_email` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.activity_client_history
CREATE TABLE IF NOT EXISTS `activity_client_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id_idx` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.activity_client_history: 0 rows
/*!40000 ALTER TABLE `activity_client_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity_client_history` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.activity_system
CREATE TABLE IF NOT EXISTS `activity_system` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `priority` tinyint(4) DEFAULT NULL,
  `admin_id` bigint(20) DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  `message` text,
  `ip` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_id_idx` (`admin_id`),
  KEY `client_id_idx` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=394 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.activity_system: 393 rows
/*!40000 ALTER TABLE `activity_system` DISABLE KEYS */;
INSERT INTO `activity_system` (`id`, `priority`, `admin_id`, `client_id`, `message`, `ip`, `created_at`, `updated_at`) VALUES
	(1, 6, NULL, NULL, 'Added new  email template #1', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(2, 6, NULL, NULL, 'Added new  email template #2', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(3, 6, NULL, NULL, 'Added new  email template #3', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(4, 6, NULL, NULL, 'Added new  email template #4', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(5, 6, NULL, NULL, 'Added new  email template #5', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(6, 6, NULL, NULL, 'Added new  email template #6', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(7, 6, NULL, NULL, 'Added new  email template #7', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(8, 6, NULL, NULL, 'Added new  email template #8', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(9, 6, NULL, NULL, 'Added new  email template #9', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(10, 6, NULL, NULL, 'Added new  email template #10', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(11, 6, NULL, NULL, 'Added new  email template #11', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(12, 6, NULL, NULL, 'Added new  email template #12', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(13, 6, NULL, NULL, 'Added new  email template #13', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(14, 6, NULL, NULL, 'Added new  email template #14', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(15, 6, NULL, NULL, 'Added new  email template #15', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(16, 6, NULL, NULL, 'Added new  email template #16', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(17, 6, NULL, NULL, 'Added new  email template #17', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(18, 6, NULL, NULL, 'Added new  email template #18', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(19, 6, NULL, NULL, 'Added new  email template #19', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(20, 6, NULL, NULL, 'Added new  email template #20', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(21, 6, NULL, NULL, 'Added new  email template #21', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(22, 6, NULL, NULL, 'Added new  email template #22', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(23, 6, NULL, NULL, 'Added new  email template #23', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(24, 6, NULL, NULL, 'Added new  email template #24', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(25, 6, NULL, NULL, 'Added new  email template #25', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(26, 6, NULL, NULL, 'Added new  email template #26', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(27, 6, NULL, NULL, 'Added new  email template #27', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(28, 6, NULL, NULL, 'Added new  email template #28', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(29, 6, NULL, NULL, 'Added new  email template #29', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(30, 6, NULL, NULL, 'Added new  email template #30', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(31, 6, NULL, NULL, 'Added new  email template #31', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(32, 6, NULL, NULL, 'Added new  email template #32', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(33, 6, NULL, NULL, 'Added new  email template #33', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(34, 6, NULL, NULL, 'Added new  email template #34', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(35, 6, NULL, NULL, 'Added new  email template #35', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(36, 6, NULL, NULL, 'Added new  email template #36', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(37, 6, NULL, NULL, 'Added new  email template #37', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(38, 6, NULL, NULL, 'Added new  email template #38', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(39, 6, NULL, NULL, 'Added new  email template #39', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(40, 6, NULL, NULL, 'Added new  email template #40', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(41, 6, NULL, NULL, 'Added new  email template #41', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(42, 6, NULL, NULL, 'Added new  email template #42', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(43, 6, NULL, NULL, 'Added new  email template #43', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(44, 6, NULL, NULL, 'Added new  email template #44', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(45, 6, NULL, NULL, 'Added new  email template #45', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(46, 6, NULL, NULL, 'Added new  email template #46', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(47, 6, NULL, NULL, 'Added new  email template #47', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(48, 6, NULL, NULL, 'Generated email templates for installed extensions', '127.0.0.1', '2016-08-05 16:54:44', '2016-08-05 16:54:44'),
	(49, 6, 1, NULL, 'Staff member 1 logged in', '127.0.0.1', '2016-08-05 16:56:23', '2016-08-05 16:56:23'),
	(50, 6, 1, NULL, 'Updated extension "mod_client" configuration', '127.0.0.1', '2016-08-05 16:58:20', '2016-08-05 16:58:20'),
	(51, 6, 1, NULL, 'Created new product #2', '127.0.0.1', '2016-08-05 18:38:55', '2016-08-05 18:38:55'),
	(52, 6, 1, NULL, 'Created new form My new form with id 1', '127.0.0.1', '2016-08-05 18:39:08', '2016-08-05 18:39:08'),
	(53, 6, 1, NULL, 'Added new field 1 to form 1', '127.0.0.1', '2016-08-05 18:39:18', '2016-08-05 18:39:18'),
	(54, 6, 1, NULL, 'Updated custom form 1', '127.0.0.1', '2016-08-05 18:39:35', '2016-08-05 18:39:35'),
	(55, 6, 1, NULL, 'Updated product #2 configuration', '127.0.0.1', '2016-08-05 18:40:43', '2016-08-05 18:40:43'),
	(56, 6, 1, NULL, 'Created new addon #3', '127.0.0.1', '2016-08-05 18:41:32', '2016-08-05 18:41:32'),
	(57, 6, 1, NULL, 'Updated addon #3', '127.0.0.1', '2016-08-05 18:41:38', '2016-08-05 18:41:38'),
	(58, 6, 1, NULL, 'Updated product #3 configuration', '127.0.0.1', '2016-08-05 18:41:38', '2016-08-05 18:41:38'),
	(59, 6, 1, NULL, 'Updated extension "mod_client" configuration', '127.0.0.1', '2016-08-05 18:43:59', '2016-08-05 18:43:59'),
	(60, 6, 1, NULL, 'Added new currency UAH', '127.0.0.1', '2016-08-05 18:45:17', '2016-08-05 18:45:17'),
	(61, 6, 1, NULL, 'Updated currency rates', '127.0.0.1', '2016-08-05 18:45:22', '2016-08-05 18:45:22'),
	(62, 6, 1, NULL, 'Updated extension "mod_order" configuration', '127.0.0.1', '2016-08-05 18:46:38', '2016-08-05 18:46:38'),
	(63, 6, 1, NULL, 'Updated extension "mod_orderbutton" configuration', '127.0.0.1', '2016-08-05 18:49:01', '2016-08-05 18:49:01'),
	(64, 6, 1, NULL, 'Activated extension "example"', '127.0.0.1', '2016-08-05 18:51:47', '2016-08-05 18:51:47'),
	(65, 6, 1, NULL, 'Activated extension "servicesolusvm"', '127.0.0.1', '2016-08-05 18:53:32', '2016-08-05 18:53:32'),
	(66, 6, 1, NULL, 'Activated extension "servicemembership"', '127.0.0.1', '2016-08-05 18:54:36', '2016-08-05 18:54:36'),
	(67, 6, 1, NULL, 'Created new product #4', '127.0.0.1', '2016-08-05 18:56:22', '2016-08-05 18:56:22'),
	(68, 6, 1, NULL, 'Deactivated extension "mod forum"', '127.0.0.1', '2016-08-05 19:22:45', '2016-08-05 19:22:45'),
	(69, 6, 1, NULL, 'Deactivated extension "mod news"', '127.0.0.1', '2016-08-05 19:22:51', '2016-08-05 19:22:51'),
	(70, 6, 1, NULL, 'Client #1 signed up', '127.0.0.1', '2016-08-05 19:33:49', '2016-08-05 19:33:49'),
	(71, 6, 1, NULL, 'Client #1 logged in', '127.0.0.1', '2016-08-05 19:33:49', '2016-08-05 19:33:49'),
	(72, 6, 1, NULL, 'Added "vpn small" to shopping cart', '127.0.0.1', '2016-08-05 19:34:14', '2016-08-05 19:34:14'),
	(73, 6, 1, NULL, 'Prepared new invoice "1"', '127.0.0.1', '2016-08-05 19:34:24', '2016-08-05 19:34:24'),
	(74, 6, 1, NULL, 'Approved invoice "1"', '127.0.0.1', '2016-08-05 19:34:24', '2016-08-05 19:34:24'),
	(75, 6, 1, NULL, 'Checked out shopping cart', '127.0.0.1', '2016-08-05 19:34:24', '2016-08-05 19:34:24'),
	(76, 6, 1, NULL, 'Deactivated extension "mod example"', '127.0.0.1', '2016-08-08 06:26:16', '2016-08-08 06:26:16'),
	(77, 7, 1, NULL, 'onBeforeAdminActivateExtension: array (\n  \'id\' => \'9\',\n)', '127.0.0.1', '2016-08-08 06:52:00', '2016-08-08 06:52:00'),
	(78, 7, 1, NULL, 'onBeforeAdminActivateExtension: array (\n  \'id\' => \'9\',\n)', '127.0.0.1', '2016-08-08 06:56:31', '2016-08-08 06:56:31'),
	(79, 7, 1, NULL, 'onAfterAdminActivateExtension: array (\n  \'id\' => \'9\',\n)', '127.0.0.1', '2016-08-08 06:56:31', '2016-08-08 06:56:31'),
	(80, 6, 1, NULL, 'Activated extension "serviceopenvpn"', '127.0.0.1', '2016-08-08 06:56:31', '2016-08-08 06:56:31'),
	(81, 7, 1, NULL, 'onBeforeAdminDeactivateExtension: array (\n  \'id\' => \'7\',\n)', '127.0.0.1', '2016-08-08 06:57:53', '2016-08-08 06:57:53'),
	(82, 7, 1, NULL, 'onAfterAdminDeactivateExtension: array (\n  \'id\' => \'servicesolusvm\',\n  \'type\' => \'mod\',\n)', '127.0.0.1', '2016-08-08 06:57:53', '2016-08-08 06:57:53'),
	(83, 6, 1, NULL, 'Deactivated extension "mod servicesolusvm"', '127.0.0.1', '2016-08-08 06:57:53', '2016-08-08 06:57:53'),
	(84, 7, 1, NULL, 'onBeforeAdminDeactivateExtension: array (\n  \'id\' => \'9\',\n)', '127.0.0.1', '2016-08-08 06:58:03', '2016-08-08 06:58:03'),
	(85, 7, 1, NULL, 'onAfterAdminDeactivateExtension: array (\n  \'id\' => \'serviceopenvpn\',\n  \'type\' => \'mod\',\n)', '127.0.0.1', '2016-08-08 06:58:03', '2016-08-08 06:58:03'),
	(86, 6, 1, NULL, 'Deactivated extension "mod serviceopenvpn"', '127.0.0.1', '2016-08-08 06:58:03', '2016-08-08 06:58:03'),
	(87, 7, 1, NULL, 'onBeforeAdminActivateExtension: array (\n  \'id\' => 10,\n)', '127.0.0.1', '2016-08-08 06:58:09', '2016-08-08 06:58:09'),
	(88, 7, 1, NULL, 'onAfterAdminActivateExtension: array (\n  \'id\' => 10,\n)', '127.0.0.1', '2016-08-08 06:58:09', '2016-08-08 06:58:09'),
	(89, 6, 1, NULL, 'Activated extension "servicesolusvm"', '127.0.0.1', '2016-08-08 06:58:09', '2016-08-08 06:58:09'),
	(90, 7, 1, NULL, 'onBeforeAdminDeactivateExtension: array (\n  \'id\' => \'10\',\n)', '127.0.0.1', '2016-08-08 06:58:20', '2016-08-08 06:58:20'),
	(91, 7, 1, NULL, 'onAfterAdminDeactivateExtension: array (\n  \'id\' => \'servicesolusvm\',\n  \'type\' => \'mod\',\n)', '127.0.0.1', '2016-08-08 06:58:20', '2016-08-08 06:58:20'),
	(92, 6, 1, NULL, 'Deactivated extension "mod servicesolusvm"', '127.0.0.1', '2016-08-08 06:58:20', '2016-08-08 06:58:20'),
	(93, 7, 1, NULL, 'onBeforeAdminActivateExtension: array (\n  \'id\' => 11,\n)', '127.0.0.1', '2016-08-08 06:58:26', '2016-08-08 06:58:26'),
	(94, 7, 1, NULL, 'onAfterAdminActivateExtension: array (\n  \'id\' => 11,\n)', '127.0.0.1', '2016-08-08 06:58:26', '2016-08-08 06:58:26'),
	(95, 6, 1, NULL, 'Activated extension "serviceopenvpn"', '127.0.0.1', '2016-08-08 06:58:26', '2016-08-08 06:58:26'),
	(96, 6, 1, NULL, 'Created new product category #2', '127.0.0.1', '2016-08-08 07:23:59', '2016-08-08 07:23:59'),
	(97, 6, 1, NULL, 'Deleted product #1', '127.0.0.1', '2016-08-08 07:24:14', '2016-08-08 07:24:14'),
	(98, 6, 1, NULL, 'Deleted product #4', '127.0.0.1', '2016-08-08 07:24:20', '2016-08-08 07:24:20'),
	(99, 7, 1, NULL, 'onBeforeAdminActivateExtension: array (\n  \'id\' => 12,\n)', '127.0.0.1', '2016-08-08 07:25:40', '2016-08-08 07:25:40'),
	(100, 7, 1, NULL, 'onAfterAdminActivateExtension: array (\n  \'id\' => 12,\n)', '127.0.0.1', '2016-08-08 07:25:40', '2016-08-08 07:25:40'),
	(101, 6, 1, NULL, 'Activated extension "servicesolusvm"', '127.0.0.1', '2016-08-08 07:25:40', '2016-08-08 07:25:40'),
	(102, 7, 1, NULL, 'onBeforeAdminDeactivateExtension: array (\n  \'id\' => \'12\',\n)', '127.0.0.1', '2016-08-08 07:25:58', '2016-08-08 07:25:58'),
	(103, 7, 1, NULL, 'onAfterAdminDeactivateExtension: array (\n  \'id\' => \'servicesolusvm\',\n  \'type\' => \'mod\',\n)', '127.0.0.1', '2016-08-08 07:25:58', '2016-08-08 07:25:58'),
	(104, 6, 1, NULL, 'Deactivated extension "mod servicesolusvm"', '127.0.0.1', '2016-08-08 07:25:58', '2016-08-08 07:25:58'),
	(105, 6, 1, NULL, 'Created new product #5', '127.0.0.1', '2016-08-08 07:26:38', '2016-08-08 07:26:38'),
	(106, 7, 1, NULL, 'onBeforeAdminActivateExtension: array (\n  \'id\' => 13,\n)', '127.0.0.1', '2016-08-08 07:31:51', '2016-08-08 07:31:51'),
	(107, 7, 1, NULL, 'onAfterAdminActivateExtension: array (\n  \'id\' => 13,\n)', '127.0.0.1', '2016-08-08 07:31:51', '2016-08-08 07:31:51'),
	(108, 6, 1, NULL, 'Activated extension "example"', '127.0.0.1', '2016-08-08 07:31:51', '2016-08-08 07:31:51'),
	(109, 7, 1, NULL, 'onBeforeAdminDeactivateExtension: array (\n  \'id\' => \'13\',\n)', '127.0.0.1', '2016-08-08 07:32:28', '2016-08-08 07:32:28'),
	(110, 7, 1, NULL, 'onAfterAdminDeactivateExtension: array (\n  \'id\' => \'example\',\n  \'type\' => \'mod\',\n)', '127.0.0.1', '2016-08-08 07:32:28', '2016-08-08 07:32:28'),
	(111, 6, 1, NULL, 'Deactivated extension "mod example"', '127.0.0.1', '2016-08-08 07:32:28', '2016-08-08 07:32:28'),
	(112, 6, 1, NULL, 'Deleted product #3', '127.0.0.1', '2016-08-08 07:36:26', '2016-08-08 07:36:26'),
	(113, 6, 1, NULL, 'Updated product #5 configuration', '127.0.0.1', '2016-08-08 07:37:02', '2016-08-08 07:37:02'),
	(114, 6, 1, NULL, 'Updated payment gateway PayPalEmail', '127.0.0.1', '2016-08-08 07:42:17', '2016-08-08 07:42:17'),
	(115, 6, 1, NULL, 'Updated payment gateway Custom', '127.0.0.1', '2016-08-08 07:42:48', '2016-08-08 07:42:48'),
	(116, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'hostname\' => \'\',\n  \'single\' => \'1\',\n  \'id\' => \'5\',\n  \'multiple\' => \'1\',\n  \'cart_id\' => \'2\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 07:44:43', '2016-08-08 07:44:43'),
	(117, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'hostname\' => \'1111\',\n  \'single\' => \'1\',\n  \'id\' => \'5\',\n  \'multiple\' => \'1\',\n  \'cart_id\' => \'2\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 07:44:48', '2016-08-08 07:44:48'),
	(118, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'hostname\' => \'www.gmail.com\',\n  \'single\' => \'1\',\n  \'id\' => \'5\',\n  \'multiple\' => \'1\',\n  \'cart_id\' => \'2\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 07:44:57', '2016-08-08 07:44:57'),
	(119, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'hostname\' => \'www.gmail.com\',\n  \'single\' => \'1\',\n  \'id\' => \'5\',\n  \'multiple\' => \'1\',\n  \'cart_id\' => \'2\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 07:45:02', '2016-08-08 07:45:02'),
	(120, 7, 1, NULL, 'onBeforeAdminActivateExtension: array (\n  \'id\' => 14,\n)', '127.0.0.1', '2016-08-08 07:46:59', '2016-08-08 07:46:59'),
	(121, 7, 1, NULL, 'onAfterAdminActivateExtension: array (\n  \'id\' => 14,\n)', '127.0.0.1', '2016-08-08 07:46:59', '2016-08-08 07:46:59'),
	(122, 6, 1, NULL, 'Activated extension "servicesolusvm"', '127.0.0.1', '2016-08-08 07:46:59', '2016-08-08 07:46:59'),
	(123, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'hostname\' => \'\',\n  \'single\' => \'1\',\n  \'id\' => \'5\',\n  \'multiple\' => \'1\',\n  \'cart_id\' => \'2\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 07:47:48', '2016-08-08 07:47:48'),
	(124, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'hostname\' => \'google.com\',\n  \'single\' => \'1\',\n  \'id\' => \'5\',\n  \'multiple\' => \'1\',\n  \'cart_id\' => \'2\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 07:52:38', '2016-08-08 07:52:38'),
	(125, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'hostname\' => \'google.com\',\n  \'single\' => \'1\',\n  \'id\' => \'5\',\n  \'multiple\' => \'1\',\n  \'cart_id\' => \'2\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 07:53:30', '2016-08-08 07:53:30'),
	(126, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'hostname\' => \'google.com\',\n  \'single\' => \'1\',\n  \'id\' => \'5\',\n  \'multiple\' => \'1\',\n  \'cart_id\' => \'2\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 07:55:55', '2016-08-08 07:55:55'),
	(127, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 07:55:55', '2016-08-08 07:55:55'),
	(128, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'hostname\' => \'google.com\',\n  \'single\' => \'1\',\n  \'id\' => \'5\',\n  \'multiple\' => \'1\',\n  \'cart_id\' => \'2\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 07:55:55', '2016-08-08 07:55:55'),
	(129, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'hostname\' => \'google.com\',\n  \'single\' => \'1\',\n  \'id\' => \'5\',\n  \'multiple\' => \'1\',\n  \'cart_id\' => \'2\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 07:56:33', '2016-08-08 07:56:33'),
	(130, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 07:56:33', '2016-08-08 07:56:33'),
	(131, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'hostname\' => \'google.com\',\n  \'single\' => \'1\',\n  \'id\' => \'5\',\n  \'multiple\' => \'1\',\n  \'cart_id\' => \'2\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 07:56:33', '2016-08-08 07:56:33'),
	(132, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'2\',\n)', '127.0.0.1', '2016-08-08 07:56:44', '2016-08-08 07:56:44'),
	(133, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'2\',\n)', '127.0.0.1', '2016-08-08 07:56:44', '2016-08-08 07:56:44'),
	(134, 6, 1, NULL, 'Added order status history message to order #4', '127.0.0.1', '2016-08-08 07:56:44', '2016-08-08 07:56:44'),
	(135, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'3\',\n)', '127.0.0.1', '2016-08-08 07:56:44', '2016-08-08 07:56:44'),
	(136, 6, 1, NULL, 'Added order status history message to order #5', '127.0.0.1', '2016-08-08 07:56:44', '2016-08-08 07:56:44'),
	(137, 6, 1, NULL, 'Checked out shopping cart', '127.0.0.1', '2016-08-08 07:56:44', '2016-08-08 07:56:44'),
	(138, 7, 1, NULL, 'onAfterClientOrderCreate: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'id\' => \'2\',\n)', '127.0.0.1', '2016-08-08 07:56:44', '2016-08-08 07:56:44'),
	(139, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'3\',\n)', '127.0.0.1', '2016-08-08 07:57:18', '2016-08-08 07:57:18'),
	(140, 7, 1, NULL, 'onBeforeAdminInvoiceDelete: array (\n  \'id\' => \'1\',\n)', '127.0.0.1', '2016-08-08 07:58:30', '2016-08-08 07:58:30'),
	(141, 7, 1, NULL, 'onAfterAdminInvoiceDelete: array (\n  \'id\' => \'1\',\n)', '127.0.0.1', '2016-08-08 07:58:30', '2016-08-08 07:58:30'),
	(142, 6, 1, NULL, 'Removed invoice #1', '127.0.0.1', '2016-08-08 07:58:30', '2016-08-08 07:58:30'),
	(143, 7, 1, NULL, 'onBeforeAdminOrderDelete: array (\n  \'id\' => \'1\',\n)', '127.0.0.1', '2016-08-08 07:58:58', '2016-08-08 07:58:58'),
	(144, 7, 1, NULL, 'onAfterAdminOrderDelete: array (\n  \'id\' => \'1\',\n)', '127.0.0.1', '2016-08-08 07:58:58', '2016-08-08 07:58:58'),
	(145, 6, 1, NULL, 'Deleted order #1', '127.0.0.1', '2016-08-08 07:58:58', '2016-08-08 07:58:58'),
	(146, 7, 1, NULL, 'onBeforeAdminDeactivateExtension: array (\n  \'id\' => \'11\',\n)', '127.0.0.1', '2016-08-08 08:03:04', '2016-08-08 08:03:04'),
	(147, 7, 1, NULL, 'onAfterAdminDeactivateExtension: array (\n  \'id\' => \'serviceopenvpn\',\n  \'type\' => \'mod\',\n)', '127.0.0.1', '2016-08-08 08:03:04', '2016-08-08 08:03:04'),
	(148, 6, 1, NULL, 'Deactivated extension "mod serviceopenvpn"', '127.0.0.1', '2016-08-08 08:03:04', '2016-08-08 08:03:04'),
	(149, 7, 1, NULL, 'onBeforeAdminActivateExtension: array (\n  \'id\' => 15,\n)', '127.0.0.1', '2016-08-08 08:03:21', '2016-08-08 08:03:21'),
	(150, 7, 1, NULL, 'onAfterAdminActivateExtension: array (\n  \'id\' => 15,\n)', '127.0.0.1', '2016-08-08 08:03:22', '2016-08-08 08:03:22'),
	(151, 6, 1, NULL, 'Activated extension "serviceopenvpn"', '127.0.0.1', '2016-08-08 08:03:22', '2016-08-08 08:03:22'),
	(152, 6, 1, NULL, 'Updated openvpn API configuration', '127.0.0.1', '2016-08-08 08:05:09', '2016-08-08 08:05:09'),
	(153, 6, 1, NULL, 'Updated product #5 configuration', '127.0.0.1', '2016-08-08 09:22:35', '2016-08-08 09:22:35'),
	(154, 6, 1, NULL, 'Updated product #5 configuration', '127.0.0.1', '2016-08-08 09:22:53', '2016-08-08 09:22:53'),
	(155, 6, 1, NULL, 'Updated product #5 configuration', '127.0.0.1', '2016-08-08 09:27:04', '2016-08-08 09:27:04'),
	(156, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'3\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 09:27:14', '2016-08-08 09:27:14'),
	(157, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'3\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 09:28:07', '2016-08-08 09:28:07'),
	(158, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'3\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 09:28:40', '2016-08-08 09:28:40'),
	(159, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 09:28:40', '2016-08-08 09:28:40'),
	(160, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'3\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 09:28:40', '2016-08-08 09:28:40'),
	(161, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'3\',\n)', '127.0.0.1', '2016-08-08 09:28:48', '2016-08-08 09:28:48'),
	(162, 6, 1, NULL, 'Prepared new invoice "2"', '127.0.0.1', '2016-08-08 09:28:48', '2016-08-08 09:28:48'),
	(163, 7, 1, NULL, 'onBeforeAdminInvoiceApprove: array (\n  \'id\' => \'2\',\n)', '127.0.0.1', '2016-08-08 09:28:48', '2016-08-08 09:28:48'),
	(164, 7, 1, NULL, 'onAfterAdminInvoiceApprove: array (\n  \'id\' => \'2\',\n)', '127.0.0.1', '2016-08-08 09:28:48', '2016-08-08 09:28:48'),
	(165, 6, 1, NULL, 'Approved invoice "2"', '127.0.0.1', '2016-08-08 09:28:48', '2016-08-08 09:28:48'),
	(166, 6, 1, NULL, 'Checked out shopping cart', '127.0.0.1', '2016-08-08 09:28:48', '2016-08-08 09:28:48'),
	(167, 7, 1, NULL, 'onAfterClientOrderCreate: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'id\' => \'4\',\n)', '127.0.0.1', '2016-08-08 09:28:48', '2016-08-08 09:28:48'),
	(168, 6, 1, NULL, 'Changed shopping cart #4 currency to тугрики', '127.0.0.1', '2016-08-08 09:28:53', '2016-08-08 09:28:53'),
	(169, 6, 1, NULL, 'Changed shopping cart #4 currency to US Dollar', '127.0.0.1', '2016-08-08 09:28:56', '2016-08-08 09:28:56'),
	(170, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'4\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 09:29:07', '2016-08-08 09:29:07'),
	(171, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 09:29:07', '2016-08-08 09:29:07'),
	(172, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'4\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 09:29:07', '2016-08-08 09:29:07'),
	(173, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'4\',\n)', '127.0.0.1', '2016-08-08 09:29:11', '2016-08-08 09:29:11'),
	(174, 6, 1, NULL, 'Prepared new invoice "3"', '127.0.0.1', '2016-08-08 09:29:11', '2016-08-08 09:29:11'),
	(175, 7, 1, NULL, 'onBeforeAdminInvoiceApprove: array (\n  \'id\' => \'3\',\n)', '127.0.0.1', '2016-08-08 09:29:11', '2016-08-08 09:29:11'),
	(176, 7, 1, NULL, 'onAfterAdminInvoiceApprove: array (\n  \'id\' => \'3\',\n)', '127.0.0.1', '2016-08-08 09:29:11', '2016-08-08 09:29:11'),
	(177, 6, 1, NULL, 'Approved invoice "3"', '127.0.0.1', '2016-08-08 09:29:11', '2016-08-08 09:29:11'),
	(178, 6, 1, NULL, 'Checked out shopping cart', '127.0.0.1', '2016-08-08 09:29:11', '2016-08-08 09:29:11'),
	(179, 7, 1, NULL, 'onAfterClientOrderCreate: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 09:29:11', '2016-08-08 09:29:11'),
	(180, 7, 1, NULL, 'onAfterAdminInvoicePaymentReceived: array (\n  \'id\' => \'3\',\n)', '127.0.0.1', '2016-08-08 09:41:03', '2016-08-08 09:41:03'),
	(181, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 09:41:03', '2016-08-08 09:41:03'),
	(182, 6, 1, NULL, 'Marked invoice "3" as paid', '127.0.0.1', '2016-08-08 09:41:03', '2016-08-08 09:41:03'),
	(183, 6, 1, NULL, 'Created new product #6', '127.0.0.1', '2016-08-08 09:43:04', '2016-08-08 09:43:04'),
	(184, 6, 1, NULL, 'Updated product #6 configuration', '127.0.0.1', '2016-08-08 09:43:16', '2016-08-08 09:43:16'),
	(185, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'domain\' => \n  array (\n    \'action\' => \'owndomain\',\n    \'owndomain_sld\' => \'rams\',\n    \'owndomain_tld\' => \'.com\',\n    \'register_sld\' => \'\',\n    \'register_tld\' => \'.com\',\n  ),\n  \'multiple\' => \'1\',\n  \'id\' => \'6\',\n  \'cart_id\' => \'5\',\n  \'product_id\' => \'6\',\n)', '127.0.0.1', '2016-08-08 09:43:36', '2016-08-08 09:43:36'),
	(186, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'domain\' => \n  array (\n    \'action\' => \'owndomain\',\n    \'owndomain_sld\' => \'rams.testddns\',\n    \'owndomain_tld\' => \'.com\',\n    \'register_sld\' => \'\',\n    \'register_tld\' => \'.com\',\n  ),\n  \'multiple\' => \'1\',\n  \'id\' => \'6\',\n  \'cart_id\' => \'5\',\n  \'product_id\' => \'6\',\n)', '127.0.0.1', '2016-08-08 09:43:58', '2016-08-08 09:43:58'),
	(187, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'domain\' => \n  array (\n    \'action\' => \'owndomain\',\n    \'owndomain_sld\' => \'google\',\n    \'owndomain_tld\' => \'.com\',\n    \'register_sld\' => \'\',\n    \'register_tld\' => \'.com\',\n  ),\n  \'multiple\' => \'1\',\n  \'id\' => \'6\',\n  \'cart_id\' => \'5\',\n  \'product_id\' => \'6\',\n)', '127.0.0.1', '2016-08-08 09:44:05', '2016-08-08 09:44:05'),
	(188, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'domain\' => \n  array (\n    \'action\' => \'register\',\n    \'owndomain_sld\' => \'google\',\n    \'owndomain_tld\' => \'.com\',\n    \'register_sld\' => \'google\',\n    \'register_tld\' => \'.com\',\n    \'register_years\' => \'1\',\n  ),\n  \'multiple\' => \'1\',\n  \'id\' => \'6\',\n  \'cart_id\' => \'5\',\n  \'product_id\' => \'6\',\n)', '127.0.0.1', '2016-08-08 09:44:18', '2016-08-08 09:44:18'),
	(189, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'domain\' => \n  array (\n    \'action\' => \'owndomain\',\n    \'owndomain_sld\' => \'google\',\n    \'owndomain_tld\' => \'.com\',\n    \'register_sld\' => \'google\',\n    \'register_tld\' => \'.com\',\n    \'register_years\' => \'1\',\n  ),\n  \'multiple\' => \'1\',\n  \'id\' => \'6\',\n  \'cart_id\' => \'5\',\n  \'product_id\' => \'6\',\n)', '127.0.0.1', '2016-08-08 09:44:25', '2016-08-08 09:44:25'),
	(190, 6, 1, NULL, 'Added new hosting plan 1', '127.0.0.1', '2016-08-08 09:45:37', '2016-08-08 09:45:37'),
	(191, 6, 1, NULL, 'Updated payment gateway PayPalEmail', '127.0.0.1', '2016-08-08 09:47:15', '2016-08-08 09:47:15'),
	(192, 6, 1, NULL, 'Updated product #5 configuration', '127.0.0.1', '2016-08-08 09:52:03', '2016-08-08 09:52:03'),
	(193, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'5\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 09:52:58', '2016-08-08 09:52:58'),
	(194, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 09:52:58', '2016-08-08 09:52:58'),
	(195, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'5\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 09:52:58', '2016-08-08 09:52:58'),
	(196, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 09:53:02', '2016-08-08 09:53:02'),
	(197, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'6\',\n)', '127.0.0.1', '2016-08-08 09:53:02', '2016-08-08 09:53:02'),
	(198, 6, 1, NULL, 'Added order status history message to order #10', '127.0.0.1', '2016-08-08 09:54:27', '2016-08-08 09:54:27'),
	(199, 6, 1, NULL, 'Checked out shopping cart', '127.0.0.1', '2016-08-08 09:54:27', '2016-08-08 09:54:27'),
	(200, 7, 1, NULL, 'onAfterClientOrderCreate: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'id\' => \'6\',\n)', '127.0.0.1', '2016-08-08 09:54:27', '2016-08-08 09:54:27'),
	(201, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'6\',\n)', '127.0.0.1', '2016-08-08 09:54:31', '2016-08-08 09:54:31'),
	(202, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'6\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 09:54:46', '2016-08-08 09:54:46'),
	(203, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 09:54:46', '2016-08-08 09:54:46'),
	(204, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'6\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 09:54:46', '2016-08-08 09:54:46'),
	(205, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'6\',\n)', '127.0.0.1', '2016-08-08 09:54:49', '2016-08-08 09:54:49'),
	(206, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'7\',\n)', '127.0.0.1', '2016-08-08 09:54:49', '2016-08-08 09:54:49'),
	(207, 6, 1, NULL, 'Added order status history message to order #13', '127.0.0.1', '2016-08-08 10:00:58', '2016-08-08 10:00:58'),
	(208, 6, 1, NULL, 'Checked out shopping cart', '127.0.0.1', '2016-08-08 10:00:58', '2016-08-08 10:00:58'),
	(209, 7, 1, NULL, 'onAfterClientOrderCreate: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'id\' => \'7\',\n)', '127.0.0.1', '2016-08-08 10:00:58', '2016-08-08 10:00:58'),
	(210, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'7\',\n)', '127.0.0.1', '2016-08-08 10:06:48', '2016-08-08 10:06:48'),
	(211, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'7\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 10:07:04', '2016-08-08 10:07:04'),
	(212, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 10:07:04', '2016-08-08 10:07:04'),
	(213, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'7\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 10:07:04', '2016-08-08 10:07:04'),
	(214, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'7\',\n)', '127.0.0.1', '2016-08-08 10:07:06', '2016-08-08 10:07:06'),
	(215, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'8\',\n)', '127.0.0.1', '2016-08-08 10:07:06', '2016-08-08 10:07:06'),
	(216, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'7\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 10:08:49', '2016-08-08 10:08:49'),
	(217, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 10:08:49', '2016-08-08 10:08:49'),
	(218, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'7\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 10:08:49', '2016-08-08 10:08:49'),
	(219, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'7\',\n)', '127.0.0.1', '2016-08-08 10:09:15', '2016-08-08 10:09:15'),
	(220, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'9\',\n)', '127.0.0.1', '2016-08-08 10:09:15', '2016-08-08 10:09:15'),
	(221, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'7\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 10:12:36', '2016-08-08 10:12:36'),
	(222, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 10:12:36', '2016-08-08 10:12:36'),
	(223, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'7\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 10:12:36', '2016-08-08 10:12:36'),
	(224, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'7\',\n)', '127.0.0.1', '2016-08-08 10:12:42', '2016-08-08 10:12:42'),
	(225, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'11\',\n)', '127.0.0.1', '2016-08-08 10:12:42', '2016-08-08 10:12:42'),
	(226, 6, 1, NULL, 'Added order status history message to order #21', '127.0.0.1', '2016-08-08 10:19:48', '2016-08-08 10:19:48'),
	(227, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'12\',\n)', '127.0.0.1', '2016-08-08 10:19:48', '2016-08-08 10:19:48'),
	(228, 6, 1, NULL, 'Added order status history message to order #23', '127.0.0.1', '2016-08-08 10:19:56', '2016-08-08 10:19:56'),
	(229, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'13\',\n)', '127.0.0.1', '2016-08-08 10:19:56', '2016-08-08 10:19:56'),
	(230, 6, 1, NULL, 'Added order status history message to order #25', '127.0.0.1', '2016-08-08 10:20:03', '2016-08-08 10:20:03'),
	(231, 6, 1, NULL, 'Checked out shopping cart', '127.0.0.1', '2016-08-08 10:20:03', '2016-08-08 10:20:03'),
	(232, 7, 1, NULL, 'onAfterClientOrderCreate: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'id\' => \'11\',\n)', '127.0.0.1', '2016-08-08 10:20:03', '2016-08-08 10:20:03'),
	(233, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'8\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 10:20:36', '2016-08-08 10:20:36'),
	(234, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 10:20:36', '2016-08-08 10:20:36'),
	(235, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'8\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 10:20:36', '2016-08-08 10:20:36'),
	(236, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'8\',\n)', '127.0.0.1', '2016-08-08 10:20:39', '2016-08-08 10:20:39'),
	(237, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'14\',\n)', '127.0.0.1', '2016-08-08 10:20:39', '2016-08-08 10:20:39'),
	(238, 6, 1, NULL, 'Added order status history message to order #28', '127.0.0.1', '2016-08-08 10:21:05', '2016-08-08 10:21:05'),
	(239, 6, 1, NULL, 'Checked out shopping cart', '127.0.0.1', '2016-08-08 10:21:05', '2016-08-08 10:21:05'),
	(240, 7, 1, NULL, 'onAfterClientOrderCreate: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'id\' => \'14\',\n)', '127.0.0.1', '2016-08-08 10:21:05', '2016-08-08 10:21:05'),
	(241, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'9\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 10:21:30', '2016-08-08 10:21:30'),
	(242, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 10:21:30', '2016-08-08 10:21:30'),
	(243, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'9\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 10:21:30', '2016-08-08 10:21:30'),
	(244, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'9\',\n)', '127.0.0.1', '2016-08-08 10:21:32', '2016-08-08 10:21:32'),
	(245, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'15\',\n)', '127.0.0.1', '2016-08-08 10:21:32', '2016-08-08 10:21:32'),
	(246, 7, 1, NULL, 'onAfterAdminOrderActivate: array (\n  \'id\' => \'15\',\n  \'rootpassword\' => 123,\n  \'consolepassword\' => 456,\n)', '127.0.0.1', '2016-08-08 10:21:51', '2016-08-08 10:21:51'),
	(247, 6, 1, NULL, 'Activated order #15', '127.0.0.1', '2016-08-08 10:21:51', '2016-08-08 10:21:51'),
	(248, 6, 1, NULL, 'Checked out shopping cart', '127.0.0.1', '2016-08-08 10:21:51', '2016-08-08 10:21:51'),
	(249, 7, 1, NULL, 'onAfterClientOrderCreate: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'id\' => \'15\',\n)', '127.0.0.1', '2016-08-08 10:21:51', '2016-08-08 10:21:51'),
	(250, 6, 1, NULL, 'Updated product #5 configuration', '127.0.0.1', '2016-08-08 11:03:55', '2016-08-08 11:03:55'),
	(251, 6, 1, NULL, 'Updated product #5 configuration', '127.0.0.1', '2016-08-08 11:04:18', '2016-08-08 11:04:18'),
	(252, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'10\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 11:04:32', '2016-08-08 11:04:32'),
	(253, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 11:04:32', '2016-08-08 11:04:32'),
	(254, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'10\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 11:04:32', '2016-08-08 11:04:32'),
	(255, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'10\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 11:05:18', '2016-08-08 11:05:18'),
	(256, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 11:05:18', '2016-08-08 11:05:18'),
	(257, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'10\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 11:05:18', '2016-08-08 11:05:18'),
	(258, 6, 1, NULL, 'Removed product from shopping cart', '127.0.0.1', '2016-08-08 11:05:22', '2016-08-08 11:05:22'),
	(259, 7, 1, NULL, 'onBeforeAdminOrderDelete: array (\n  \'id\' => \'14\',\n)', '127.0.0.1', '2016-08-08 11:09:48', '2016-08-08 11:09:48'),
	(260, 7, 1, NULL, 'onAfterAdminOrderDelete: array (\n  \'id\' => \'14\',\n)', '127.0.0.1', '2016-08-08 11:09:48', '2016-08-08 11:09:48'),
	(261, 6, 1, NULL, 'Deleted order #14', '127.0.0.1', '2016-08-08 11:09:48', '2016-08-08 11:09:48'),
	(262, 7, 1, NULL, 'onBeforeAdminOrderDelete: array (\n  \'id\' => \'11\',\n)', '127.0.0.1', '2016-08-08 11:09:50', '2016-08-08 11:09:50'),
	(263, 7, 1, NULL, 'onAfterAdminOrderDelete: array (\n  \'id\' => \'11\',\n)', '127.0.0.1', '2016-08-08 11:09:50', '2016-08-08 11:09:50'),
	(264, 6, 1, NULL, 'Deleted order #11', '127.0.0.1', '2016-08-08 11:09:50', '2016-08-08 11:09:50'),
	(265, 7, 1, NULL, 'onBeforeAdminOrderDelete: array (\n  \'id\' => \'7\',\n)', '127.0.0.1', '2016-08-08 11:09:53', '2016-08-08 11:09:53'),
	(266, 7, 1, NULL, 'onAfterAdminOrderDelete: array (\n  \'id\' => \'7\',\n)', '127.0.0.1', '2016-08-08 11:09:53', '2016-08-08 11:09:53'),
	(267, 6, 1, NULL, 'Deleted order #7', '127.0.0.1', '2016-08-08 11:09:53', '2016-08-08 11:09:53'),
	(268, 6, 1, NULL, 'Installed new payment gateway TwoCheckout', '127.0.0.1', '2016-08-08 11:14:01', '2016-08-08 11:14:01'),
	(269, 6, 1, NULL, 'Removed payment gateway 1', '127.0.0.1', '2016-08-08 11:14:09', '2016-08-08 11:14:09'),
	(270, 6, 1, NULL, 'Removed payment gateway 3', '127.0.0.1', '2016-08-08 11:14:13', '2016-08-08 11:14:13'),
	(271, 6, 1, NULL, 'Updated payment gateway TwoCheckout', '127.0.0.1', '2016-08-08 11:14:48', '2016-08-08 11:14:48'),
	(272, 6, 1, NULL, 'Deleted product #6', '127.0.0.1', '2016-08-08 11:16:21', '2016-08-08 11:16:21'),
	(273, 6, 1, NULL, 'Deleted product #2', '127.0.0.1', '2016-08-08 11:16:24', '2016-08-08 11:16:24'),
	(274, 6, 1, NULL, 'Deleted product category #1', '127.0.0.1', '2016-08-08 11:16:46', '2016-08-08 11:16:46'),
	(275, 7, 1, NULL, 'onBeforeAdminOrderDelete: array (\n  \'id\' => \'10\',\n)', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(276, 7, 1, NULL, 'onAfterAdminOrderDelete: array (\n  \'id\' => \'10\',\n)', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(277, 6, 1, NULL, 'Deleted order #10', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(278, 7, 1, NULL, 'onBeforeAdminOrderDelete: array (\n  \'id\' => \'9\',\n)', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(279, 7, 1, NULL, 'onAfterAdminOrderDelete: array (\n  \'id\' => \'9\',\n)', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(280, 6, 1, NULL, 'Deleted order #9', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(281, 7, 1, NULL, 'onBeforeAdminOrderDelete: array (\n  \'id\' => \'8\',\n)', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(282, 7, 1, NULL, 'onAfterAdminOrderDelete: array (\n  \'id\' => \'8\',\n)', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(283, 6, 1, NULL, 'Deleted order #8', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(284, 7, 1, NULL, 'onBeforeAdminOrderDelete: array (\n  \'id\' => \'6\',\n)', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(285, 7, 1, NULL, 'onAfterAdminOrderDelete: array (\n  \'id\' => \'6\',\n)', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(286, 6, 1, NULL, 'Deleted order #6', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(287, 7, 1, NULL, 'onBeforeAdminOrderDelete: array (\n  \'id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(288, 7, 1, NULL, 'onAfterAdminOrderDelete: array (\n  \'id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(289, 6, 1, NULL, 'Deleted order #5', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(290, 7, 1, NULL, 'onBeforeAdminOrderDelete: array (\n  \'id\' => \'4\',\n)', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(291, 7, 1, NULL, 'onAfterAdminOrderDelete: array (\n  \'id\' => \'4\',\n)', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(292, 6, 1, NULL, 'Deleted order #4', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(293, 7, 1, NULL, 'onBeforeAdminOrderDelete: array (\n  \'id\' => \'2\',\n)', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(294, 7, 1, NULL, 'onAfterAdminOrderDelete: array (\n  \'id\' => \'2\',\n)', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(295, 6, 1, NULL, 'Deleted order #2', '127.0.0.1', '2016-08-08 11:16:58', '2016-08-08 11:16:58'),
	(296, 7, 1, NULL, 'onBeforeAdminOrderDelete: array (\n  \'id\' => \'13\',\n)', '127.0.0.1', '2016-08-08 11:17:13', '2016-08-08 11:17:13'),
	(297, 7, 1, NULL, 'onAfterAdminOrderDelete: array (\n  \'id\' => \'13\',\n)', '127.0.0.1', '2016-08-08 11:17:13', '2016-08-08 11:17:13'),
	(298, 6, 1, NULL, 'Deleted order #13', '127.0.0.1', '2016-08-08 11:17:13', '2016-08-08 11:17:13'),
	(299, 7, 1, NULL, 'onBeforeAdminOrderDelete: array (\n  \'id\' => \'12\',\n)', '127.0.0.1', '2016-08-08 11:17:13', '2016-08-08 11:17:13'),
	(300, 7, 1, NULL, 'onAfterAdminOrderDelete: array (\n  \'id\' => \'12\',\n)', '127.0.0.1', '2016-08-08 11:17:13', '2016-08-08 11:17:13'),
	(301, 6, 1, NULL, 'Deleted order #12', '127.0.0.1', '2016-08-08 11:17:13', '2016-08-08 11:17:13'),
	(302, 7, 1, NULL, 'onBeforeAdminOrderDelete: array (\n  \'id\' => \'3\',\n)', '127.0.0.1', '2016-08-08 11:17:13', '2016-08-08 11:17:13'),
	(303, 7, 1, NULL, 'onAfterAdminOrderDelete: array (\n  \'id\' => \'3\',\n)', '127.0.0.1', '2016-08-08 11:17:13', '2016-08-08 11:17:13'),
	(304, 6, 1, NULL, 'Deleted order #3', '127.0.0.1', '2016-08-08 11:17:13', '2016-08-08 11:17:13'),
	(305, 7, 1, NULL, 'onBeforeAdminInvoiceDelete: array (\n  \'id\' => \'2\',\n)', '127.0.0.1', '2016-08-08 11:17:23', '2016-08-08 11:17:23'),
	(306, 7, 1, NULL, 'onAfterAdminInvoiceDelete: array (\n  \'id\' => \'2\',\n)', '127.0.0.1', '2016-08-08 11:17:23', '2016-08-08 11:17:23'),
	(307, 6, 1, NULL, 'Removed invoice #2', '127.0.0.1', '2016-08-08 11:17:23', '2016-08-08 11:17:23'),
	(308, 6, 1, NULL, 'Updated product #5 configuration', '127.0.0.1', '2016-08-08 13:22:58', '2016-08-08 13:22:58'),
	(309, 6, 1, NULL, 'Updated product #5 configuration', '127.0.0.1', '2016-08-08 14:27:28', '2016-08-08 14:27:28'),
	(310, 6, 1, NULL, 'Updated product #5 configuration', '127.0.0.1', '2016-08-08 14:42:30', '2016-08-08 14:42:30'),
	(311, 7, 1, NULL, 'onBeforeAdminOrderDelete: array (\n  \'id\' => \'15\',\n)', '127.0.0.1', '2016-08-08 14:43:05', '2016-08-08 14:43:05'),
	(312, 7, 1, NULL, 'onAfterAdminOrderDelete: array (\n  \'id\' => \'15\',\n)', '127.0.0.1', '2016-08-08 14:43:05', '2016-08-08 14:43:05'),
	(313, 6, 1, NULL, 'Deleted order #15', '127.0.0.1', '2016-08-08 14:43:05', '2016-08-08 14:43:05'),
	(314, 7, 1, NULL, 'onBeforeAdminInvoiceDelete: array (\n  \'id\' => \'3\',\n)', '127.0.0.1', '2016-08-08 14:43:14', '2016-08-08 14:43:14'),
	(315, 7, 1, NULL, 'onAfterAdminInvoiceDelete: array (\n  \'id\' => \'3\',\n)', '127.0.0.1', '2016-08-08 14:43:14', '2016-08-08 14:43:14'),
	(316, 6, 1, NULL, 'Removed invoice #3', '127.0.0.1', '2016-08-08 14:43:14', '2016-08-08 14:43:14'),
	(317, 6, 1, NULL, 'Updated product #5 configuration', '127.0.0.1', '2016-08-08 14:43:54', '2016-08-08 14:43:54'),
	(318, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'10\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 14:44:06', '2016-08-08 14:44:06'),
	(319, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 14:44:06', '2016-08-08 14:44:06'),
	(320, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'10\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 14:44:06', '2016-08-08 14:44:06'),
	(321, 6, 1, NULL, 'Removed product from shopping cart', '127.0.0.1', '2016-08-08 14:44:10', '2016-08-08 14:44:10'),
	(322, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'10\',\n)', '127.0.0.1', '2016-08-08 14:44:14', '2016-08-08 14:44:14'),
	(323, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'16\',\n)', '127.0.0.1', '2016-08-08 14:44:14', '2016-08-08 14:44:14'),
	(324, 6, 1, NULL, 'Added order status history message to order #33', '127.0.0.1', '2016-08-08 14:44:14', '2016-08-08 14:44:14'),
	(325, 6, 1, NULL, 'Checked out shopping cart', '127.0.0.1', '2016-08-08 14:44:14', '2016-08-08 14:44:14'),
	(326, 7, 1, NULL, 'onAfterClientOrderCreate: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'id\' => \'16\',\n)', '127.0.0.1', '2016-08-08 14:44:14', '2016-08-08 14:44:14'),
	(327, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'11\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 14:49:00', '2016-08-08 14:49:00'),
	(328, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 14:49:00', '2016-08-08 14:49:00'),
	(329, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'11\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 14:49:00', '2016-08-08 14:49:00'),
	(330, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'11\',\n)', '127.0.0.1', '2016-08-08 14:49:01', '2016-08-08 14:49:01'),
	(331, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'17\',\n)', '127.0.0.1', '2016-08-08 14:49:01', '2016-08-08 14:49:01'),
	(332, 6, 1, NULL, 'Added order status history message to order #36', '127.0.0.1', '2016-08-08 14:59:35', '2016-08-08 14:59:35'),
	(333, 6, 1, NULL, 'Checked out shopping cart', '127.0.0.1', '2016-08-08 14:59:35', '2016-08-08 14:59:35'),
	(334, 7, 1, NULL, 'onAfterClientOrderCreate: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'id\' => \'17\',\n)', '127.0.0.1', '2016-08-08 14:59:35', '2016-08-08 14:59:35'),
	(335, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'12\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 14:59:47', '2016-08-08 14:59:47'),
	(336, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 14:59:47', '2016-08-08 14:59:47'),
	(337, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'12\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 14:59:47', '2016-08-08 14:59:47'),
	(338, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'12\',\n)', '127.0.0.1', '2016-08-08 14:59:49', '2016-08-08 14:59:49'),
	(339, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'18\',\n)', '127.0.0.1', '2016-08-08 14:59:49', '2016-08-08 14:59:49'),
	(340, 6, 1, NULL, 'Added order status history message to order #39', '127.0.0.1', '2016-08-08 14:59:51', '2016-08-08 14:59:51'),
	(341, 6, 1, NULL, 'Checked out shopping cart', '127.0.0.1', '2016-08-08 14:59:51', '2016-08-08 14:59:51'),
	(342, 7, 1, NULL, 'onAfterClientOrderCreate: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'id\' => \'18\',\n)', '127.0.0.1', '2016-08-08 14:59:51', '2016-08-08 14:59:51'),
	(343, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'13\',\n)', '127.0.0.1', '2016-08-08 15:00:17', '2016-08-08 15:00:17'),
	(344, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'13\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 15:00:33', '2016-08-08 15:00:33'),
	(345, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 15:00:33', '2016-08-08 15:00:33'),
	(346, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'13\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 15:00:33', '2016-08-08 15:00:33'),
	(347, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'13\',\n)', '127.0.0.1', '2016-08-08 15:00:34', '2016-08-08 15:00:34'),
	(348, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'19\',\n)', '127.0.0.1', '2016-08-08 15:00:34', '2016-08-08 15:00:34'),
	(349, 6, 1, NULL, 'Added order status history message to order #42', '127.0.0.1', '2016-08-08 15:02:19', '2016-08-08 15:02:19'),
	(350, 6, 1, NULL, 'Checked out shopping cart', '127.0.0.1', '2016-08-08 15:02:19', '2016-08-08 15:02:19'),
	(351, 7, 1, NULL, 'onAfterClientOrderCreate: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'id\' => \'19\',\n)', '127.0.0.1', '2016-08-08 15:02:19', '2016-08-08 15:02:19'),
	(352, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'14\',\n)', '127.0.0.1', '2016-08-08 15:02:26', '2016-08-08 15:02:26'),
	(353, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'14\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 15:02:31', '2016-08-08 15:02:31'),
	(354, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 15:02:31', '2016-08-08 15:02:31'),
	(355, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'14\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 15:02:31', '2016-08-08 15:02:31'),
	(356, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'14\',\n)', '127.0.0.1', '2016-08-08 15:02:33', '2016-08-08 15:02:33'),
	(357, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'20\',\n)', '127.0.0.1', '2016-08-08 15:02:33', '2016-08-08 15:02:33'),
	(358, 7, 1, NULL, 'onAfterAdminOrderActivate: array (\n  \'id\' => \'20\',\n)', '127.0.0.1', '2016-08-08 15:02:55', '2016-08-08 15:02:55'),
	(359, 6, 1, NULL, 'Activated order #20', '127.0.0.1', '2016-08-08 15:02:55', '2016-08-08 15:02:55'),
	(360, 6, 1, NULL, 'Checked out shopping cart', '127.0.0.1', '2016-08-08 15:02:55', '2016-08-08 15:02:55'),
	(361, 7, 1, NULL, 'onAfterClientOrderCreate: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'id\' => \'20\',\n)', '127.0.0.1', '2016-08-08 15:02:55', '2016-08-08 15:02:55'),
	(362, 7, 1, NULL, 'onBeforeProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'15\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 15:09:57', '2016-08-08 15:09:57'),
	(363, 6, 1, NULL, 'Added "VPS-1" to shopping cart', '127.0.0.1', '2016-08-08 15:09:57', '2016-08-08 15:09:57'),
	(364, 7, 1, NULL, 'onAfterProductAddedToCart: array (\n  \'multiple\' => \'1\',\n  \'id\' => \'5\',\n  \'cart_id\' => \'15\',\n  \'product_id\' => \'5\',\n)', '127.0.0.1', '2016-08-08 15:09:57', '2016-08-08 15:09:57'),
	(365, 7, 1, NULL, 'onBeforeClientCheckout: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'cart_id\' => \'15\',\n)', '127.0.0.1', '2016-08-08 15:09:59', '2016-08-08 15:09:59'),
	(366, 7, 1, NULL, 'onBeforeAdminOrderActivate: array (\n  \'id\' => \'1\',\n)', '127.0.0.1', '2016-08-08 15:09:59', '2016-08-08 15:09:59'),
	(367, 7, 1, NULL, 'onAfterAdminOrderActivate: array (\n  \'id\' => \'1\',\n)', '127.0.0.1', '2016-08-08 15:10:17', '2016-08-08 15:10:17'),
	(368, 6, 1, NULL, 'Activated order #1', '127.0.0.1', '2016-08-08 15:10:17', '2016-08-08 15:10:17'),
	(369, 6, 1, NULL, 'Checked out shopping cart', '127.0.0.1', '2016-08-08 15:10:17', '2016-08-08 15:10:17'),
	(370, 7, 1, NULL, 'onAfterClientOrderCreate: array (\n  \'ip\' => \'127.0.0.1\',\n  \'client_id\' => \'1\',\n  \'id\' => \'1\',\n)', '127.0.0.1', '2016-08-08 15:10:17', '2016-08-08 15:10:17'),
	(371, 7, 1, NULL, 'onBeforeAdminExtensionConfigSave: array (\n  \'allow_signup\' => \'1\',\n  \'require_email_confirmation\' => \'0\',\n  \'allow_change_email\' => \'0\',\n  \'required\' => \n  array (\n    0 => \'last_name\',\n    1 => \'country\',\n    2 => \'postcode\',\n  ),\n  \'custom_fields\' => \n  array (\n    \'custom_1\' => \n    array (\n      \'active\' => \'1\',\n      \'required\' => \'1\',\n      \'title\' => \'Username\',\n    ),\n    \'custom_2\' => \n    array (\n      \'title\' => \'\',\n    ),\n    \'custom_3\' => \n    array (\n      \'title\' => \'\',\n    ),\n    \'custom_4\' => \n    array (\n      \'title\' => \'\',\n    ),\n    \'custom_5\' => \n    array (\n      \'title\' => \'\',\n    ),\n    \'custom_6\' => \n    array (\n      \'title\' => \'\',\n    ),\n    \'custom_7\' => \n    array (\n      \'title\' => \'\',\n    ),\n    \'custom_8\' => \n    array (\n      \'title\' => \'\',\n    ),\n    \'custom_9\' => \n    array (\n      \'title\' => \'\',\n    ),\n    \'custom_10\' => \n    array (\n      \'title\' => \'\',\n    ),\n  ),\n  \'ext\' => \'mod_client\',\n)', '127.0.0.1', '2016-08-08 15:17:46', '2016-08-08 15:17:46'),
	(372, 7, 1, NULL, 'onAfterAdminExtensionConfigSave: array (\n  \'allow_signup\' => \'1\',\n  \'require_email_confirmation\' => \'0\',\n  \'allow_change_email\' => \'0\',\n  \'required\' => \n  array (\n    0 => \'last_name\',\n    1 => \'country\',\n    2 => \'postcode\',\n  ),\n  \'custom_fields\' => \n  array (\n    \'custom_1\' => \n    array (\n      \'active\' => \'1\',\n      \'required\' => \'1\',\n      \'title\' => \'Username\',\n    ),\n    \'custom_2\' => \n    array (\n      \'title\' => \'\',\n    ),\n    \'custom_3\' => \n    array (\n      \'title\' => \'\',\n    ),\n    \'custom_4\' => \n    array (\n      \'title\' => \'\',\n    ),\n    \'custom_5\' => \n    array (\n      \'title\' => \'\',\n    ),\n    \'custom_6\' => \n    array (\n      \'title\' => \'\',\n    ),\n    \'custom_7\' => \n    array (\n      \'title\' => \'\',\n    ),\n    \'custom_8\' => \n    array (\n      \'title\' => \'\',\n    ),\n    \'custom_9\' => \n    array (\n      \'title\' => \'\',\n    ),\n    \'custom_10\' => \n    array (\n      \'title\' => \'\',\n    ),\n  ),\n  \'ext\' => \'mod_client\',\n)', '127.0.0.1', '2016-08-08 15:17:46', '2016-08-08 15:17:46'),
	(373, 6, 1, NULL, 'Updated extension "mod_client" configuration', '127.0.0.1', '2016-08-08 15:17:46', '2016-08-08 15:17:46'),
	(374, 7, 1, NULL, 'onBeforeAdminDeactivateExtension: array (\n  \'id\' => \'14\',\n)', '127.0.0.1', '2016-08-08 15:34:47', '2016-08-08 15:34:47'),
	(375, 7, 1, NULL, 'onAfterAdminDeactivateExtension: array (\n  \'id\' => \'servicesolusvm\',\n  \'type\' => \'mod\',\n)', '127.0.0.1', '2016-08-08 15:34:47', '2016-08-08 15:34:47'),
	(376, 6, 1, NULL, 'Deactivated extension "mod servicesolusvm"', '127.0.0.1', '2016-08-08 15:34:47', '2016-08-08 15:34:47'),
	(377, 7, 1, NULL, 'onBeforeAdminActivateExtension: array (\n  \'id\' => 16,\n)', '127.0.0.1', '2016-08-08 15:35:02', '2016-08-08 15:35:02'),
	(378, 7, 1, NULL, 'onAfterAdminActivateExtension: array (\n  \'id\' => 16,\n)', '127.0.0.1', '2016-08-08 15:35:02', '2016-08-08 15:35:02'),
	(379, 6, 1, NULL, 'Activated extension "servicesolusvm"', '127.0.0.1', '2016-08-08 15:35:02', '2016-08-08 15:35:02'),
	(380, 7, 1, NULL, 'onBeforeAdminActivateExtension: array (\n  \'id\' => \'16\',\n)', '127.0.0.1', '2016-08-08 15:35:18', '2016-08-08 15:35:18'),
	(381, 7, 1, NULL, 'onAfterAdminActivateExtension: array (\n  \'id\' => \'16\',\n)', '127.0.0.1', '2016-08-08 15:35:18', '2016-08-08 15:35:18'),
	(382, 6, 1, NULL, 'Activated extension "servicesolusvm"', '127.0.0.1', '2016-08-08 15:35:18', '2016-08-08 15:35:18'),
	(383, 7, NULL, NULL, 'onBeforeClientLogin: array (\n  \'email\' => \'sramsiks@gmail.com\',\n  \'password\' => \'123456\',\n  \'ip\' => \'127.0.0.1\',\n)', '127.0.0.1', '2016-08-08 15:48:56', '2016-08-08 15:48:56'),
	(384, 7, NULL, NULL, 'onBeforeClientLogin: array (\n  \'email\' => \'sramsiks@gmail.com\',\n  \'password\' => \'123456\',\n  \'ip\' => \'127.0.0.1\',\n)', '127.0.0.1', '2016-08-08 15:50:50', '2016-08-08 15:50:50'),
	(385, 7, 1, NULL, 'onBeforeClientLogin: array (\n  \'email\' => \'sramsiks@gmail.com\',\n  \'password\' => \'123456\',\n  \'ip\' => \'127.0.0.1\',\n)', '127.0.0.1', '2016-08-08 15:51:40', '2016-08-08 15:51:40'),
	(386, 7, 1, NULL, 'onBeforeClientLogin: array (\n  \'email\' => \'sramsiks@gmail.com\',\n  \'password\' => \'123456\',\n  \'ip\' => \'127.0.0.1\',\n)', '127.0.0.1', '2016-08-08 15:52:09', '2016-08-08 15:52:09'),
	(387, 7, 1, NULL, 'onBeforeClientProfilePasswordChange: array (\n  \'password\' => \'123456\',\n  \'password_confirm\' => \'123456\',\n  \'id\' => \'1\',\n)', '127.0.0.1', '2016-08-08 16:00:05', '2016-08-08 16:00:05'),
	(388, 7, 1, NULL, 'onBeforeClientProfilePasswordChange: array (\n  \'password\' => \'123456\',\n  \'password_confirm\' => \'123456\',\n  \'id\' => \'1\',\n)', '127.0.0.1', '2016-08-08 16:00:14', '2016-08-08 16:00:14'),
	(389, 7, 1, NULL, 'onBeforeClientProfilePasswordChange: array (\n  \'password\' => \'wemorion007\',\n  \'password_confirm\' => \'wemorion007\',\n  \'id\' => \'1\',\n)', '127.0.0.1', '2016-08-08 16:00:27', '2016-08-08 16:00:27'),
	(390, 7, 1, NULL, 'onAfterClientProfilePasswordChange: array (\n  \'id\' => \'1\',\n)', '127.0.0.1', '2016-08-08 16:00:27', '2016-08-08 16:00:27'),
	(391, 6, 1, NULL, 'Changed profile password', '127.0.0.1', '2016-08-08 16:00:27', '2016-08-08 16:00:27'),
	(392, 6, 1, NULL, 'Created new redirect #22', '127.0.0.1', '2016-08-08 16:04:27', '2016-08-08 16:04:27'),
	(393, 6, 1, NULL, 'Removed redirect #22', '127.0.0.1', '2016-08-08 16:04:44', '2016-08-08 16:04:44');
/*!40000 ALTER TABLE `activity_system` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.admin
CREATE TABLE IF NOT EXISTS `admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role` varchar(30) DEFAULT 'staff' COMMENT 'admin, staff',
  `admin_group_id` bigint(20) DEFAULT '1',
  `email` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `signature` varchar(255) DEFAULT NULL,
  `protected` tinyint(1) DEFAULT '0',
  `status` varchar(30) DEFAULT 'active' COMMENT 'active, inactive',
  `api_token` varchar(128) DEFAULT NULL,
  `permissions` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `admin_group_id_idx` (`admin_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.admin: 2 rows
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` (`id`, `role`, `admin_group_id`, `email`, `pass`, `salt`, `name`, `signature`, `protected`, `status`, `api_token`, `permissions`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 1, 'sramsiks@gmail.com', '$2y$10$0t5EwWcvabBLkTb55E9VO.P1auwjVEImIA8qG9aEDytp4YQTQFNZK', NULL, 'rams', NULL, 1, 'active', NULL, NULL, '2016-08-05 19:54:38', '2016-08-05 19:54:38'),
	(2, 'cron', 1, 'A7R0pqUM@u3JV20Wf.com', '$2y$10$96oofweYteIdV2qUD5/gauU68xfgEAF/6/3QlhvkydLG7YY1xSP4a', NULL, 'System Cron Job', '', 1, 'active', NULL, NULL, '2016-08-05 19:34:25', '2016-08-05 19:34:25');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.admin_group
CREATE TABLE IF NOT EXISTS `admin_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.admin_group: 2 rows
/*!40000 ALTER TABLE `admin_group` DISABLE KEYS */;
INSERT INTO `admin_group` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Administrators', '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(2, 'Support', '2012-01-01 12:00:00', '2012-01-01 12:00:00');
/*!40000 ALTER TABLE `admin_group` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.api_request
CREATE TABLE IF NOT EXISTS `api_request` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip` varchar(45) DEFAULT NULL,
  `request` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=323 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.api_request: 322 rows
/*!40000 ALTER TABLE `api_request` DISABLE KEYS */;
INSERT INTO `api_request` (`id`, `ip`, `request`, `created_at`) VALUES
	(1, '127.0.0.1', '/index.php?_url=/api/guest/client/login', '2016-08-05 19:55:58'),
	(2, '127.0.0.1', '/index.php?_url=/api/guest/client/login', '2016-08-05 19:56:04'),
	(3, '127.0.0.1', '/index.php?_url=/api/guest/staff/login', '2016-08-05 19:56:23'),
	(4, '127.0.0.1', '/index.php?_url=/api/admin/theme/preset_select', '2016-08-05 19:57:21'),
	(5, '127.0.0.1', '/index.php?_url=/api/admin/theme/preset_select', '2016-08-05 19:57:23'),
	(6, '127.0.0.1', '/index.php?_url=/api/admin/theme/preset_select', '2016-08-05 19:57:25'),
	(7, '127.0.0.1', '/index.php?_url=/api/admin/extension/config_save', '2016-08-05 19:58:20'),
	(8, '127.0.0.1', '/index.php?_url=/api/admin/product/prepare', '2016-08-05 21:38:55'),
	(9, '127.0.0.1', '/index.php?_url=/api/admin/formbuilder/create_form', '2016-08-05 21:39:08'),
	(10, '127.0.0.1', '/index.php?_url=/api/admin/formbuilder/add_field&form_id=1&type=text', '2016-08-05 21:39:18'),
	(11, '127.0.0.1', '/index.php?_url=/api/admin/formbuilder/update_field', '2016-08-05 21:39:34'),
	(12, '127.0.0.1', '/index.php?_url=/api/admin/product/update', '2016-08-05 21:40:43'),
	(13, '127.0.0.1', '/index.php?_url=/api/admin/product/addon_create', '2016-08-05 21:41:32'),
	(14, '127.0.0.1', '/index.php?_url=/api/admin/product/addon_update', '2016-08-05 21:41:38'),
	(15, '127.0.0.1', '/index.php?_url=/api/admin/extension/config_save', '2016-08-05 21:43:59'),
	(16, '127.0.0.1', '/index.php?_url=/api/admin/currency/create', '2016-08-05 21:45:17'),
	(17, '127.0.0.1', '/index.php?_url=/api/admin/currency/update_rates', '2016-08-05 21:45:21'),
	(18, '127.0.0.1', '/index.php?_url=/api/admin/extension/config_save', '2016-08-05 21:46:38'),
	(19, '127.0.0.1', '/index.php?_url=/api/admin/extension/config_save', '2016-08-05 21:49:00'),
	(20, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=example', '2016-08-05 21:51:47'),
	(21, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=servicesolusvm', '2016-08-05 21:53:32'),
	(22, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=servicemembership', '2016-08-05 21:54:36'),
	(23, '127.0.0.1', '/index.php?_url=/api/admin/product/prepare', '2016-08-05 21:56:22'),
	(24, '127.0.0.1', '/index.php?_url=/api/admin/extension/deactivate&type=mod&id=forum', '2016-08-05 22:22:45'),
	(25, '127.0.0.1', '/index.php?_url=/api/admin/extension/deactivate&type=mod&id=news', '2016-08-05 22:22:51'),
	(26, '127.0.0.1', '/index.php?_url=/api/admin/extension/deactivate&type=mod&id=branding', '2016-08-05 22:22:59'),
	(27, '127.0.0.1', '/index.php?_url=/api/guest/client/create', '2016-08-05 22:33:09'),
	(28, '127.0.0.1', '/index.php?_url=/api/guest/client/create', '2016-08-05 22:33:17'),
	(29, '127.0.0.1', '/index.php?_url=/api/guest/client/create', '2016-08-05 22:33:49'),
	(30, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-05 22:34:14'),
	(31, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-05 22:34:24'),
	(32, '127.0.0.1', '/index.php?_url=/api/guest/invoice/payment', '2016-08-05 22:34:25'),
	(33, '127.0.0.1', '/index.php?_url=/api/admin/extension/deactivate&type=mod&id=example', '2016-08-08 09:26:16'),
	(34, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=serviceopenvpn', '2016-08-08 09:47:40'),
	(35, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=serviceopenvpn', '2016-08-08 09:48:02'),
	(36, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=serviceopenvpn', '2016-08-08 09:49:04'),
	(37, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=serviceopenvpn', '2016-08-08 09:49:39'),
	(38, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=serviceopenvpn', '2016-08-08 09:49:58'),
	(39, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=serviceopenvpn', '2016-08-08 09:51:32'),
	(40, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=serviceopenvpn', '2016-08-08 09:51:35'),
	(41, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=serviceopenvpn', '2016-08-08 09:51:38'),
	(42, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=serviceopenvpn', '2016-08-08 09:52:00'),
	(43, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=serviceopenvpn', '2016-08-08 09:56:31'),
	(44, '127.0.0.1', '/index.php?_url=/api/admin/extension/deactivate&type=mod&id=servicesolusvm', '2016-08-08 09:57:53'),
	(45, '127.0.0.1', '/index.php?_url=/api/admin/extension/deactivate&type=mod&id=serviceopenvpn', '2016-08-08 09:58:03'),
	(46, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=servicesolusvm', '2016-08-08 09:58:09'),
	(47, '127.0.0.1', '/index.php?_url=/api/admin/extension/deactivate&type=mod&id=servicesolusvm', '2016-08-08 09:58:20'),
	(48, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=serviceopenvpn', '2016-08-08 09:58:26'),
	(49, '127.0.0.1', '/index.php?_url=/api/admin/serviceopenvpn/test_connection', '2016-08-08 10:23:13'),
	(50, '127.0.0.1', '/index.php?_url=/api/admin/serviceopenvpn/test_connection', '2016-08-08 10:23:18'),
	(51, '127.0.0.1', '/index.php?_url=/api/admin/product/category_create', '2016-08-08 10:23:59'),
	(52, '127.0.0.1', '/index.php?_url=/api/admin/product/category_delete&id=1', '2016-08-08 10:24:07'),
	(53, '127.0.0.1', '/index.php?_url=/api/admin/product/delete&id=1', '2016-08-08 10:24:14'),
	(54, '127.0.0.1', '/index.php?_url=/api/admin/product/delete&id=2', '2016-08-08 10:24:16'),
	(55, '127.0.0.1', '/index.php?_url=/api/admin/product/delete&id=4', '2016-08-08 10:24:20'),
	(56, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=servicesolusvm', '2016-08-08 10:25:40'),
	(57, '127.0.0.1', '/index.php?_url=/api/admin/extension/deactivate&type=mod&id=servicesolusvm', '2016-08-08 10:25:58'),
	(58, '127.0.0.1', '/index.php?_url=/api/admin/product/prepare', '2016-08-08 10:26:38'),
	(59, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=example', '2016-08-08 10:31:51'),
	(60, '127.0.0.1', '/index.php?_url=/api/admin/extension/deactivate&type=mod&id=example', '2016-08-08 10:32:28'),
	(61, '127.0.0.1', '/index.php?_url=/api/admin/product/delete&id=3', '2016-08-08 10:36:26'),
	(62, '127.0.0.1', '/index.php?_url=/api/admin/product/update', '2016-08-08 10:37:02'),
	(63, '127.0.0.1', '/index.php?_url=/api/admin/invoice/gateway_update', '2016-08-08 10:42:17'),
	(64, '127.0.0.1', '/index.php?_url=/api/admin/invoice/gateway_update', '2016-08-08 10:42:48'),
	(65, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 10:44:43'),
	(66, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 10:44:48'),
	(67, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 10:44:57'),
	(68, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 10:45:02'),
	(69, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=servicesolusvm', '2016-08-08 10:46:59'),
	(70, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 10:47:48'),
	(71, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 10:52:38'),
	(72, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 10:53:30'),
	(73, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 10:55:55'),
	(74, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 10:56:33'),
	(75, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 10:56:44'),
	(76, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 10:57:18'),
	(77, '127.0.0.1', '/index.php?_url=/api/guest/cart/remove_item', '2016-08-08 10:57:29'),
	(78, '127.0.0.1', '/index.php?_url=/api/admin/invoice/delete&id=1', '2016-08-08 10:58:30'),
	(79, '127.0.0.1', '/index.php?_url=/api/admin/order/delete&id=1', '2016-08-08 10:58:58'),
	(80, '127.0.0.1', '/index.php?_url=/api/admin/extension/deactivate&type=mod&id=serviceopenvpn', '2016-08-08 11:03:04'),
	(81, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=serviceopenvpn', '2016-08-08 11:03:21'),
	(82, '127.0.0.1', '/index.php?_url=/api/admin/serviceopenvpn/cluster_config_update', '2016-08-08 11:05:09'),
	(83, '127.0.0.1', '/index.php?_url=/api/admin/product/update', '2016-08-08 12:22:35'),
	(84, '127.0.0.1', '/index.php?_url=/api/admin/product/update_config', '2016-08-08 12:22:52'),
	(85, '127.0.0.1', '/index.php?_url=/api/admin/product/update', '2016-08-08 12:27:04'),
	(86, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 12:27:14'),
	(87, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 12:28:07'),
	(88, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 12:28:40'),
	(89, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 12:28:48'),
	(90, '127.0.0.1', '/index.php?_url=/api/guest/invoice/payment', '2016-08-08 12:28:48'),
	(91, '127.0.0.1', '/index.php?_url=/api/guest/cart/set_currency', '2016-08-08 12:28:53'),
	(92, '127.0.0.1', '/index.php?_url=/api/guest/cart/set_currency', '2016-08-08 12:28:56'),
	(93, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 12:29:07'),
	(94, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 12:29:11'),
	(95, '127.0.0.1', '/index.php?_url=/api/guest/invoice/payment', '2016-08-08 12:29:11'),
	(96, '127.0.0.1', '/index.php?_url=/api/admin/invoice/mark_as_paid&id=3&execute=1', '2016-08-08 12:41:03'),
	(97, '127.0.0.1', '/index.php?_url=/api/admin/invoice/mark_as_paid&id=3&execute=1', '2016-08-08 12:41:13'),
	(98, '127.0.0.1', '/index.php?_url=/api/admin/product/prepare', '2016-08-08 12:43:04'),
	(99, '127.0.0.1', '/index.php?_url=/api/admin/product/update', '2016-08-08 12:43:16'),
	(100, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 12:43:36'),
	(101, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 12:43:58'),
	(102, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 12:44:05'),
	(103, '127.0.0.1', '/index.php?_url=/api/guest/servicedomain/check', '2016-08-08 12:44:15'),
	(104, '127.0.0.1', '/index.php?_url=/api/guest/servicedomain/pricing', '2016-08-08 12:44:15'),
	(105, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 12:44:18'),
	(106, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 12:44:25'),
	(107, '127.0.0.1', '/index.php?_url=/api/admin/servicehosting/hp_create', '2016-08-08 12:45:37'),
	(108, '127.0.0.1', '/index.php?_url=/api/admin/invoice/gateway_update', '2016-08-08 12:47:15'),
	(109, '127.0.0.1', '/index.php?_url=/api/admin/product/update', '2016-08-08 12:52:03'),
	(110, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 12:52:58'),
	(111, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 12:53:02'),
	(112, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 12:54:31'),
	(113, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 12:54:46'),
	(114, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 12:54:49'),
	(115, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 13:06:48'),
	(116, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 13:07:04'),
	(117, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 13:07:06'),
	(118, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 13:08:49'),
	(119, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 13:09:15'),
	(120, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 13:12:36'),
	(121, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 13:12:42'),
	(122, '127.0.0.1', '/index.php?_url=/api/guest/cart/remove_item', '2016-08-08 13:20:12'),
	(123, '127.0.0.1', '/index.php?_url=/api/guest/cart/remove_item', '2016-08-08 13:20:17'),
	(124, '127.0.0.1', '/index.php?_url=/api/guest/cart/remove_item', '2016-08-08 13:20:19'),
	(125, '127.0.0.1', '/index.php?_url=/api/guest/cart/remove_item', '2016-08-08 13:20:21'),
	(126, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 13:20:36'),
	(127, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 13:20:39'),
	(128, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 13:21:30'),
	(129, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 13:21:32'),
	(130, '127.0.0.1', '/index.php?_url=/api/client/serviceopenvpn/status&order_id=15', '2016-08-08 13:34:48'),
	(131, '127.0.0.1', '/index.php?_url=/api/client/serviceopenvpn/status&order_id=15', '2016-08-08 13:34:56'),
	(132, '127.0.0.1', '/index.php?_url=/api/admin/product/update_config', '2016-08-08 14:03:55'),
	(133, '127.0.0.1', '/index.php?_url=/api/admin/product/update_config', '2016-08-08 14:04:18'),
	(134, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 14:04:32'),
	(135, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 14:05:18'),
	(136, '127.0.0.1', '/index.php?_url=/api/guest/cart/remove_item', '2016-08-08 14:05:22'),
	(137, '127.0.0.1', '/index.php?_url=/api/admin/order/delete&id=14', '2016-08-08 14:09:48'),
	(138, '127.0.0.1', '/index.php?_url=/api/admin/order/delete&id=11', '2016-08-08 14:09:50'),
	(139, '127.0.0.1', '/index.php?_url=/api/admin/order/delete&id=7', '2016-08-08 14:09:53'),
	(140, '127.0.0.1', '/index.php?_url=/api/admin/invoice/gateway_install&code=TwoCheckout', '2016-08-08 14:14:01'),
	(141, '127.0.0.1', '/index.php?_url=/api/admin/invoice/gateway_delete&id=1', '2016-08-08 14:14:09'),
	(142, '127.0.0.1', '/index.php?_url=/api/admin/invoice/gateway_delete&id=3', '2016-08-08 14:14:13'),
	(143, '127.0.0.1', '/index.php?_url=/api/admin/invoice/gateway_update', '2016-08-08 14:14:48'),
	(144, '127.0.0.1', '/index.php?_url=/api/admin/product/delete&id=6', '2016-08-08 14:16:21'),
	(145, '127.0.0.1', '/index.php?_url=/api/admin/product/delete&id=2', '2016-08-08 14:16:24'),
	(146, '127.0.0.1', '/index.php?_url=/api/admin/product/category_delete&id=1', '2016-08-08 14:16:46'),
	(147, '127.0.0.1', '/index.php?_url=/api/admin/order/batch_delete', '2016-08-08 14:16:58'),
	(148, '127.0.0.1', '/index.php?_url=/api/admin/order/batch_delete', '2016-08-08 14:17:13'),
	(149, '127.0.0.1', '/index.php?_url=/api/admin/invoice/batch_delete', '2016-08-08 14:17:23'),
	(150, '127.0.0.1', '/index.php?_url=/api/admin/product/update_config', '2016-08-08 16:22:58'),
	(151, '127.0.0.1', '/index.php?_url=/api/admin/product/update_config', '2016-08-08 17:27:28'),
	(152, '127.0.0.1', '/index.php?_url=/api/admin/product/update_config', '2016-08-08 17:42:30'),
	(153, '127.0.0.1', '/index.php?_url=/api/admin/order/batch_delete', '2016-08-08 17:43:04'),
	(154, '127.0.0.1', '/index.php?_url=/api/admin/invoice/batch_delete', '2016-08-08 17:43:14'),
	(155, '127.0.0.1', '/index.php?_url=/api/admin/product/update', '2016-08-08 17:43:54'),
	(156, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 17:44:06'),
	(157, '127.0.0.1', '/index.php?_url=/api/guest/cart/remove_item', '2016-08-08 17:44:10'),
	(158, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 17:44:14'),
	(159, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 17:49:00'),
	(160, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 17:49:01'),
	(161, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 17:59:47'),
	(162, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 17:59:49'),
	(163, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 18:00:17'),
	(164, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 18:00:33'),
	(165, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 18:00:34'),
	(166, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 18:02:26'),
	(167, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 18:02:31'),
	(168, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 18:02:33'),
	(169, '127.0.0.1', '/index.php?_url=/api/guest/cart/add_item', '2016-08-08 18:09:57'),
	(170, '127.0.0.1', '/index.php?_url=/api/client/cart/checkout', '2016-08-08 18:09:59'),
	(171, '127.0.0.1', '/index.php?_url=/api/admin/extension/config_save', '2016-08-08 18:17:46'),
	(172, '127.0.0.1', '/index.php?_url=/api/admin/extension/deactivate&type=mod&id=servicesolusvm', '2016-08-08 18:34:47'),
	(173, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=servicesolusvm', '2016-08-08 18:35:02'),
	(174, '127.0.0.1', '/index.php?_url=/api/admin/extension/activate&type=mod&id=servicesolusvm', '2016-08-08 18:35:18'),
	(175, '127.0.0.1', '/index.php?_url=/api/admin/servicesolusvm/test_connection', '2016-08-08 18:37:27'),
	(176, '127.0.0.1', '/index.php?_url=/api/admin/servicesolusvm/test_connection', '2016-08-08 18:37:34'),
	(177, '127.0.0.1', '/index.php?_url=/api/guest/servicesolusvm/test_connection', '2016-08-08 18:37:44'),
	(178, '127.0.0.1', '/index.php?_url=/api/guest/servicesolusvm/templates', '2016-08-08 18:37:51'),
	(179, '127.0.0.1', '/index.php?_url=/api/admin/servicesolusvm/test_connection', '2016-08-08 18:38:05'),
	(180, '127.0.0.1', '/index.php?_url=/api/admin/servicesolusvm/update', '2016-08-08 18:38:27'),
	(181, '127.0.0.1', '/index.php?_url=/api/admin/serviceopenvpn/update', '2016-08-08 18:38:36'),
	(182, '127.0.0.1', '/index.php?_url=/api/admin/guests/update', '2016-08-08 18:38:53'),
	(183, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-08 18:39:16'),
	(184, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/templates', '2016-08-08 18:39:20'),
	(185, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/get_templates', '2016-08-08 18:39:25'),
	(186, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/get_templates', '2016-08-08 18:39:47'),
	(187, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/get_templates', '2016-08-08 18:41:23'),
	(188, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 18:41:30'),
	(189, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 18:45:32'),
	(190, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 18:45:45'),
	(191, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 18:46:01'),
	(192, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 18:48:56'),
	(193, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 18:50:50'),
	(194, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 18:51:40'),
	(195, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 18:51:50'),
	(196, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 18:53:15'),
	(197, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 18:55:35'),
	(198, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 18:57:14'),
	(199, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 18:58:45'),
	(200, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 18:59:01'),
	(201, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 18:59:16'),
	(202, '127.0.0.1', '/index.php?_url=/api/client/client/change_password', '2016-08-08 19:00:05'),
	(203, '127.0.0.1', '/index.php?_url=/api/client/client/change_password', '2016-08-08 19:00:14'),
	(204, '127.0.0.1', '/index.php?_url=/api/client/client/change_password', '2016-08-08 19:00:27'),
	(205, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 19:00:51'),
	(206, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 19:01:50'),
	(207, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 19:02:02'),
	(208, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 19:02:09'),
	(209, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 19:02:11'),
	(210, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 19:02:12'),
	(211, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 19:02:33'),
	(212, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 19:02:47'),
	(213, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 19:02:54'),
	(214, '127.0.0.1', '/index.php?_url=/api/admin/redirect/create', '2016-08-08 19:04:27'),
	(215, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 19:04:31'),
	(216, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 19:04:33'),
	(217, '127.0.0.1', '/index.php?_url=/api/admin/redirect/delete&id=22', '2016-08-08 19:04:44'),
	(218, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 19:05:05'),
	(219, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 19:05:15'),
	(220, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_server', '2016-08-08 19:16:36'),
	(221, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_server', '2016-08-08 19:17:28'),
	(222, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_server', '2016-08-08 19:17:53'),
	(223, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_server', '2016-08-08 19:18:23'),
	(224, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_server', '2016-08-08 19:18:31'),
	(225, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 19:18:34'),
	(226, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 19:18:46'),
	(227, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 19:18:55'),
	(228, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_server', '2016-08-08 19:19:10'),
	(229, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_server', '2016-08-08 19:20:00'),
	(230, '127.0.0.1', '/api/guest/serviceopenvpn/check_server', '2016-08-08 19:22:32'),
	(231, '127.0.0.1', '/api/guest/serviceopenvpn/check_server', '2016-08-08 19:22:34'),
	(232, '127.0.0.1', '/api/guest/serviceopenvpn/check_users', '2016-08-08 19:25:23'),
	(233, '127.0.0.1', '/api/guest/serviceopenvpn/check_user', '2016-08-08 19:25:31'),
	(234, '127.0.0.1', '/api/guest/serviceopenvpn/check_user', '2016-08-08 19:26:44'),
	(235, '127.0.0.1', '/api/guest/serviceopenvpn/check_user', '2016-08-08 19:29:07'),
	(236, '127.0.0.1', '/api/guest/serviceopenvpn/check_user', '2016-08-08 19:29:31'),
	(237, '127.0.0.1', '/api/guest/serviceopenvpn/check_user', '2016-08-08 19:29:39'),
	(238, '127.0.0.1', '/api/guest/serviceopenvpn/check_user', '2016-08-08 19:31:06'),
	(239, '127.0.0.1', '/api/guest/serviceopenvpn/check_user', '2016-08-08 19:31:34'),
	(240, '127.0.0.1', '/api/guest/serviceopenvpn/check_user', '2016-08-08 19:32:08'),
	(241, '127.0.0.1', '/api/guest/serviceopenvpn/check_user', '2016-08-08 19:32:39'),
	(242, '127.0.0.1', '/api/guest/serviceopenvpn/check_user', '2016-08-08 19:33:31'),
	(243, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_server', '2016-08-08 20:48:04'),
	(244, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_server', '2016-08-08 20:48:39'),
	(245, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/login', '2016-08-08 20:48:44'),
	(246, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-08 20:48:57'),
	(247, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-08 20:49:12'),
	(248, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-08 20:49:47'),
	(249, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-08 20:50:00'),
	(250, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-08 20:52:46'),
	(251, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-08 20:53:36'),
	(252, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-08 20:54:05'),
	(253, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-08 20:54:39'),
	(254, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-08 20:54:50'),
	(255, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 20:59:49'),
	(256, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_server', '2016-08-08 20:59:57'),
	(257, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_server', '2016-08-08 21:00:08'),
	(258, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_server', '2016-08-08 21:04:52'),
	(259, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 21:04:56'),
	(260, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 21:05:46'),
	(261, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_user', '2016-08-08 21:06:02'),
	(262, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/check_server', '2016-08-08 21:08:16'),
	(263, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/login', '2016-08-08 21:09:50'),
	(264, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/login', '2016-08-08 21:09:53'),
	(265, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/login', '2016-08-08 21:10:19'),
	(266, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-08 21:11:03'),
	(267, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-08 21:11:18'),
	(268, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-08 21:11:37'),
	(269, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-08 21:17:45'),
	(270, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-08 21:42:54'),
	(271, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-08 21:43:09'),
	(272, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-08 21:56:48'),
	(273, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-08 22:00:15'),
	(274, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 08:42:22'),
	(275, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 08:44:10'),
	(276, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 08:44:39'),
	(277, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 08:47:44'),
	(278, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 08:48:30'),
	(279, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 08:49:48'),
	(280, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 08:55:12'),
	(281, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 08:56:14'),
	(282, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 08:57:02'),
	(283, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 08:57:32'),
	(284, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 08:58:58'),
	(285, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 08:59:25'),
	(286, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 09:00:14'),
	(287, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 09:01:09'),
	(288, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 09:06:02'),
	(289, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 09:07:28'),
	(290, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 09:09:57'),
	(291, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 09:11:31'),
	(292, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 09:11:58'),
	(293, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 09:26:40'),
	(294, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 09:26:45'),
	(295, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 09:27:39'),
	(296, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-09 09:33:42'),
	(297, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-09 09:33:55'),
	(298, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-09 09:34:07'),
	(299, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-09 09:34:56'),
	(300, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-09 09:39:26'),
	(301, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/connect', '2016-08-09 11:51:06'),
	(302, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/disconnect', '2016-08-09 11:56:09'),
	(303, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/disconnect', '2016-08-09 11:56:33'),
	(304, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 12:11:54'),
	(305, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 12:12:12'),
	(306, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 12:13:21'),
	(307, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 12:13:40'),
	(308, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 12:14:00'),
	(309, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 12:14:39'),
	(310, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 12:15:09'),
	(311, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/disconnect', '2016-08-09 12:30:39'),
	(312, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/disconnect', '2016-08-09 12:31:24'),
	(313, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 14:53:33'),
	(314, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 14:53:33'),
	(315, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 14:53:41'),
	(316, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 14:55:05'),
	(317, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 14:57:13'),
	(318, '127.0.0.1', '/index.php?_url=/api/client/serviceopenvpn/status&order_id=1', '2016-08-09 15:34:13'),
	(319, '127.0.0.1', '/index.php?_url=/api/client/serviceopenvpn/update&order_id=1', '2016-08-09 15:42:39'),
	(320, '127.0.0.1', '/index.php?_url=/api/client/serviceopenvpn/update&order_id=1', '2016-08-09 15:43:27'),
	(321, '127.0.0.1', '/index.php?_url=/api/client/serviceopenvpn/update&order_id=1', '2016-08-09 15:43:38'),
	(322, '127.0.0.1', '/index.php?_url=/api/guest/serviceopenvpn/update', '2016-08-09 15:57:45');
/*!40000 ALTER TABLE `api_request` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.cart
CREATE TABLE IF NOT EXISTS `cart` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(32) DEFAULT NULL,
  `currency_id` bigint(20) DEFAULT NULL,
  `promo_id` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `session_id_idx` (`session_id`),
  KEY `currency_id_idx` (`currency_id`),
  KEY `promo_id_idx` (`promo_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.cart: 0 rows
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.cart_product
CREATE TABLE IF NOT EXISTS `cart_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cart_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `config` text,
  PRIMARY KEY (`id`),
  KEY `cart_id_idx` (`cart_id`),
  KEY `product_id_idx` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.cart_product: 0 rows
/*!40000 ALTER TABLE `cart_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart_product` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.client
CREATE TABLE IF NOT EXISTS `client` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `aid` varchar(255) DEFAULT NULL COMMENT 'Alternative id for foreign systems',
  `client_group_id` bigint(20) DEFAULT NULL,
  `role` varchar(30) NOT NULL DEFAULT 'client' COMMENT 'client',
  `auth_type` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `status` varchar(30) DEFAULT 'active' COMMENT 'active, suspended, canceled',
  `email_approved` tinyint(1) DEFAULT NULL,
  `tax_exempt` tinyint(1) DEFAULT '0',
  `type` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `phone_cc` varchar(10) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `company_vat` varchar(100) DEFAULT NULL,
  `company_number` varchar(255) DEFAULT NULL,
  `address_1` varchar(100) DEFAULT NULL,
  `address_2` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `document_type` varchar(100) DEFAULT NULL,
  `document_nr` varchar(20) DEFAULT NULL,
  `notes` text,
  `currency` varchar(10) DEFAULT NULL,
  `lang` varchar(10) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `api_token` varchar(128) DEFAULT NULL,
  `referred_by` varchar(255) DEFAULT NULL,
  `custom_1` text,
  `custom_2` text,
  `custom_3` text,
  `custom_4` text,
  `custom_5` text,
  `custom_6` text,
  `custom_7` text,
  `custom_8` text,
  `custom_9` text,
  `custom_10` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `alternative_id_idx` (`aid`),
  KEY `client_group_id_idx` (`client_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.client: 1 rows
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` (`id`, `aid`, `client_group_id`, `role`, `auth_type`, `email`, `pass`, `salt`, `status`, `email_approved`, `tax_exempt`, `type`, `first_name`, `last_name`, `gender`, `birthday`, `phone_cc`, `phone`, `company`, `company_vat`, `company_number`, `address_1`, `address_2`, `city`, `state`, `postcode`, `country`, `document_type`, `document_nr`, `notes`, `currency`, `lang`, `ip`, `api_token`, `referred_by`, `custom_1`, `custom_2`, `custom_3`, `custom_4`, `custom_5`, `custom_6`, `custom_7`, `custom_8`, `custom_9`, `custom_10`, `created_at`, `updated_at`) VALUES
	(1, NULL, NULL, 'client', NULL, 'sramsiks@gmail.com', '$2y$10$qonp4oLXSgW2hBgRr8FhT.NhAEC8rVxW7dm17ro9xTZB4bDWdC5TS', NULL, 'active', NULL, 0, NULL, 'Roman', 'govtvian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'UA', NULL, NULL, NULL, 'USD', NULL, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-08-05 19:33:49', '2016-08-05 19:33:49');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.client_balance
CREATE TABLE IF NOT EXISTS `client_balance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `rel_id` varchar(20) DEFAULT NULL,
  `amount` decimal(18,2) DEFAULT '0.00',
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id_idx` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.client_balance: 0 rows
/*!40000 ALTER TABLE `client_balance` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_balance` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.client_group
CREATE TABLE IF NOT EXISTS `client_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.client_group: 1 rows
/*!40000 ALTER TABLE `client_group` DISABLE KEYS */;
INSERT INTO `client_group` (`id`, `title`, `created_at`, `updated_at`) VALUES
	(1, 'Default', '2012-01-01 12:00:00', '2012-01-01 12:00:00');
/*!40000 ALTER TABLE `client_group` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.client_order
CREATE TABLE IF NOT EXISTS `client_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `form_id` bigint(20) DEFAULT NULL,
  `promo_id` bigint(20) DEFAULT NULL,
  `promo_recurring` tinyint(1) DEFAULT NULL,
  `promo_used` bigint(20) DEFAULT NULL,
  `group_id` varchar(255) DEFAULT NULL,
  `group_master` tinyint(1) DEFAULT '0',
  `invoice_option` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `currency` varchar(20) DEFAULT NULL,
  `unpaid_invoice_id` bigint(20) DEFAULT NULL,
  `service_id` bigint(20) DEFAULT NULL,
  `service_type` varchar(100) DEFAULT NULL,
  `period` varchar(20) DEFAULT NULL,
  `quantity` bigint(20) DEFAULT '1',
  `unit` varchar(100) DEFAULT NULL,
  `price` double(18,2) DEFAULT NULL,
  `discount` double(18,2) DEFAULT NULL COMMENT 'first invoice discount',
  `status` varchar(50) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL COMMENT 'suspend/cancel reason',
  `notes` text,
  `config` text,
  `referred_by` varchar(255) DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  `activated_at` datetime DEFAULT NULL,
  `suspended_at` datetime DEFAULT NULL,
  `unsuspended_at` datetime DEFAULT NULL,
  `canceled_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id_idx` (`client_id`),
  KEY `product_id_idx` (`product_id`),
  KEY `form_id_idx` (`form_id`),
  KEY `promo_id_idx` (`promo_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.client_order: 1 rows
/*!40000 ALTER TABLE `client_order` DISABLE KEYS */;
INSERT INTO `client_order` (`id`, `client_id`, `product_id`, `form_id`, `promo_id`, `promo_recurring`, `promo_used`, `group_id`, `group_master`, `invoice_option`, `title`, `currency`, `unpaid_invoice_id`, `service_id`, `service_type`, `period`, `quantity`, `unit`, `price`, `discount`, `status`, `reason`, `notes`, `config`, `referred_by`, `expires_at`, `activated_at`, `suspended_at`, `unsuspended_at`, `canceled_at`, `created_at`, `updated_at`) VALUES
	(1, 1, 5, NULL, NULL, NULL, NULL, '15', 1, 'issue-invoice', 'Virtual private server VPS-1', 'USD', NULL, 1, 'openvpn', NULL, 1, 'product', 0.00, 0.00, 'active', NULL, NULL, '{"multiple":"1","id":"20","product_id":"5","form_id":null,"title":"Virtual private server VPS-1","type":"openvpn","quantity":1,"unit":"product","price":0,"setup_price":0,"discount":0,"discount_price":0,"discount_setup":0,"total":0}', NULL, NULL, '2016-08-08 15:10:17', NULL, NULL, NULL, '2016-08-08 15:09:59', '2016-08-08 15:10:17');
/*!40000 ALTER TABLE `client_order` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.client_order_meta
CREATE TABLE IF NOT EXISTS `client_order_meta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_order_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_order_id_idx` (`client_order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.client_order_meta: 0 rows
/*!40000 ALTER TABLE `client_order_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_order_meta` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.client_order_status
CREATE TABLE IF NOT EXISTS `client_order_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_order_id` bigint(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `notes` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_order_id_idx` (`client_order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.client_order_status: 16 rows
/*!40000 ALTER TABLE `client_order_status` DISABLE KEYS */;
INSERT INTO `client_order_status` (`id`, `client_order_id`, `status`, `notes`, `created_at`, `updated_at`) VALUES
	(32, 16, 'failed_setup', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'up\' in \'field list\'', '2016-08-08 14:44:14', '2016-08-08 14:44:14'),
	(33, 16, 'error', 'Order could not be activated after checkout due to error: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'up\' in \'field list\'', '2016-08-08 14:44:14', '2016-08-08 14:44:14'),
	(34, 17, 'pending_setup', 'Order created', '2016-08-08 14:49:01', '2016-08-08 14:49:01'),
	(35, 17, 'failed_setup', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'up\' in \'field list\'', '2016-08-08 14:59:35', '2016-08-08 14:59:35'),
	(36, 17, 'error', 'Order could not be activated after checkout due to error: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'up\' in \'field list\'', '2016-08-08 14:59:35', '2016-08-08 14:59:35'),
	(37, 18, 'pending_setup', 'Order created', '2016-08-08 14:59:49', '2016-08-08 14:59:49'),
	(38, 18, 'failed_setup', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'trafic\' in \'field list\'', '2016-08-08 14:59:51', '2016-08-08 14:59:51'),
	(39, 18, 'error', 'Order could not be activated after checkout due to error: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'trafic\' in \'field list\'', '2016-08-08 14:59:51', '2016-08-08 14:59:51'),
	(40, 19, 'pending_setup', 'Order created', '2016-08-08 15:00:34', '2016-08-08 15:00:34'),
	(41, 19, 'failed_setup', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'trafic\' in \'field list\'', '2016-08-08 15:02:19', '2016-08-08 15:02:19'),
	(42, 19, 'error', 'Order could not be activated after checkout due to error: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'trafic\' in \'field list\'', '2016-08-08 15:02:19', '2016-08-08 15:02:19'),
	(43, 20, 'pending_setup', 'Order created', '2016-08-08 15:02:33', '2016-08-08 15:02:33'),
	(44, 20, 'active', 'Order activated', '2016-08-08 15:02:51', '2016-08-08 15:02:51'),
	(45, 1, 'pending_setup', 'Order created', '2016-08-08 15:09:59', '2016-08-08 15:09:59'),
	(46, 1, 'active', 'Order activated', '2016-08-08 15:10:17', '2016-08-08 15:10:17'),
	(31, 16, 'pending_setup', 'Order created', '2016-08-08 14:44:14', '2016-08-08 14:44:14');
/*!40000 ALTER TABLE `client_order_status` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.client_password_reset
CREATE TABLE IF NOT EXISTS `client_password_reset` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) DEFAULT NULL,
  `hash` varchar(100) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id_idx` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.client_password_reset: 0 rows
/*!40000 ALTER TABLE `client_password_reset` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_password_reset` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.connections
CREATE TABLE IF NOT EXISTS `connections` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` int(10) unsigned NOT NULL DEFAULT '0',
  `port` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `server` varchar(50) NOT NULL DEFAULT '0',
  `recieve` bigint(20) unsigned NOT NULL DEFAULT '0',
  `send` bigint(20) unsigned NOT NULL DEFAULT '0',
  `is_closed` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `opened_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `closet_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Индекс 2` (`user_id`,`ip`,`port`,`server`,`opened_at`),
  KEY `Индекс 3` (`is_closed`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.connections: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `connections` DISABLE KEYS */;
INSERT INTO `connections` (`id`, `user_id`, `ip`, `port`, `server`, `recieve`, `send`, `is_closed`, `opened_at`, `updated_at`, `closet_at`) VALUES
	(6, 1, 3232235521, 8080, 'test server', 1234, 321, 1, '2016-08-09 09:11:36', '2016-08-09 12:31:31', NULL),
	(7, 1, 1, 1, '1', 444, 44444, 0, '2016-08-09 09:20:47', '2016-08-09 09:31:24', NULL),
	(10, 1, 3232235521, 8080, 'test server1', 1234, 321, 5, '2016-08-09 12:13:26', '2016-08-09 12:16:42', NULL),
	(12, 1, 3232235521, 8080, 'test server', 1234, 321, 0, '2016-08-09 14:53:33', '2016-08-09 15:23:20', NULL);
/*!40000 ALTER TABLE `connections` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.currency
CREATE TABLE IF NOT EXISTS `currency` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `code` varchar(3) DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  `conversion_rate` decimal(13,6) DEFAULT '1.000000',
  `format` varchar(30) DEFAULT NULL,
  `price_format` varchar(50) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.currency: 2 rows
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` (`id`, `title`, `code`, `is_default`, `conversion_rate`, `format`, `price_format`, `created_at`, `updated_at`) VALUES
	(1, 'US Dollar', 'USD', 1, 1.000000, '${{price}}', '1', '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(2, 'тугрики', 'UAH', 0, 24.810000, '{{price}} UAH', '1', '2016-08-05 18:45:17', '2016-08-05 18:45:17');
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.email_template
CREATE TABLE IF NOT EXISTS `email_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action_code` varchar(255) DEFAULT NULL,
  `category` varchar(30) DEFAULT NULL COMMENT 'general, domain, invoice, hosting, support, download, custom, license',
  `enabled` tinyint(1) DEFAULT '1',
  `subject` varchar(255) DEFAULT NULL,
  `content` text,
  `description` text,
  `vars` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `action_code` (`action_code`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.email_template: 47 rows
/*!40000 ALTER TABLE `email_template` DISABLE KEYS */;
INSERT INTO `email_template` (`id`, `action_code`, `category`, `enabled`, `subject`, `content`, `description`, `vars`) VALUES
	(1, 'mod_client_confirm', 'client', 1, '[{{ guest.system_company.name }}] Please confirm your email address ', '\n{% filter markdown %}\nHello {{ c.first_name }} {{ c.last_name }},\n\nPlease verify your email by clicking on the link below:\n\n{{email_confirmation_link}}\n\nTo login, visit {{\'login\'|link({\'email\' : c.email }) }}\nEdit your profile at {{\'client/me\'|link}}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(2, 'mod_client_password_reset_approve', 'client', 1, '[{{ guest.system_company.name }}] Password Changed', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nAs you requested, your password for our client area has now been reset. \nYour new login details are as follows:\n\nLogin at: {{\'login\'|link({\'email\' : c.email }) }}\nEmail: {{ c.email }}\nPassword: {{ password }}\n\nTo change your password to something more memorable, after logging in go to \nProfile &gt; Change Password.\n\nEdit your profile at {{ \'client/me\'|link }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(3, 'mod_client_password_reset_request', 'client', 1, '[{{ guest.system_company.name }}] Confirm Password Reset', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nRecently a request was submitted to reset your password for the client area.\nIf you did not request this, please ignore this email. It will expire and will not work in 2 hours time.\n\nTo reset your password, please visit the url below:    \n{{\'client/reset-password-confirm\'|link}}/{{ hash }}\n\n\nWhen you visit the link above, your password will be reset and a new \npassword will be emailed to you.\n\nTo login, visit {{\'login\'|link({\'email\' : c.email }) }}\nEdit your profile at {{\'client/me\'|link}}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(4, 'mod_client_signup', 'client', 1, '[{{ guest.system_company.name }}] Welcome {{ c.first_name }}', '\n{% filter markdown %}\nHello {{ c.first_name }} {{ c.last_name }},\n\nThank you for signing up with us. Your new account has been setup and you can now login to our client area using the details below.\n\n\nEmail: {{c.email}}    \nPassword: {{password}}\n\n{% if require_email_confirmation %}\n\nApprove your email by clicking on the link below:\n\n{{email_confirmation_link}}\n\n{% endif %}\n\nTo login, visit {{\'login\'|link({\'email\' : c.email }) }}\nEdit your profile at {{\'client/me\'|link}}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(5, 'mod_email_test', 'email', 1, '[{{ guest.system_company.name }}] BoxBilling Email Test', '\n{% filter markdown %}\nHi {{ staff.name }},\n\nIf you are reading this email, BoxBilling is **configured properly** \nand is **able to send emails**.\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(6, 'mod_forum_new_post', 'forum', 1, '[{{ guest.system_company.name }}] New Forum Message', '\n{% filter markdown %}\nHello {{ c.first_name }} {{ c.last_name }},\n\nNew message was posted on Forum **{{ topic.forum.title }} > {{ topic.title }}**\n\n***\n\n{{ message.message }}\n\n***\n\nTo reply to this message or disable notifications, visit the link below:\n\n{{ \'forum\'|link }}/{{ topic.forum.slug }}/{{ topic.slug }}   \n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(7, 'mod_invoice_created', 'invoice', 1, '[{{ guest.system_company.name }}] Invoice Created', '\n{% filter markdown %}\nHello {{ c.first_name }} {{ c.last_name }},\n\nThis is to notify that proforma invoice {{ invoice.id }} was generated on {{ invoice.created_at|bb_date }}.\nAmount Due: {{ invoice.total | money(invoice.currency) }}\nDue Date: {{invoice.due_at|bb_date}}\n\nYou can view and pay the invoice at: {{\'invoice\'|link}}/{{invoice.hash}}\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(8, 'mod_invoice_due_after', 'invoice', 1, '[{{ guest.system_company.name }}] Invoice Due', '\n{% filter markdown %}\nHello {{ c.first_name }} {{ c.last_name }},\n\nThis is a payment reminder that your proforma invoice **{{ invoice.serie_nr }}** is already\ndue for {{ days_passed }} days.   \n\nAmount due: {{ invoice.total | money(invoice.currency) }}\nDue Date: {{ invoice.due_at|bb_date }}\n\nYou can view and pay the invoice at: {{\'invoice\'|link}}/{{invoice.hash}}\n\nYou may review your invoice history at any time by logging in to your client area.\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nView and manage invoices: {{\'invoice\'|link}}  \n\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(9, 'mod_invoice_paid', 'invoice', 1, '[{{ guest.system_company.name }}] Payment Received', '\n{% filter markdown %}\nHello {{ c.first_name }} {{ c.last_name }},\n\nThis is a payment receipt for Invoice **{{ invoice.serie_nr }}** issued on\n{{invoice.created_at|date(\'Y-m-d\')}}\n\nTotal Paid: {{ invoice.total | money(invoice.currency) }}\n\nYou may review your invoice history at any time by logging in to your client area.\nNote: This email serves as an official receipt for this payment.\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nView invoice: {{\'invoice\'|link}}/{{invoice.hash}}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(10, 'mod_invoice_payment_reminder', 'invoice', 1, '[{{ guest.system_company.name }}] Payment Reminder', '\n{% filter markdown %}\nHello {{ c.first_name }} {{ c.last_name }},\n\nThis is to remind that your proforma invoice **{{ invoice.serie_nr }}** is due\nin {{ invoice.due_at|daysleft }} days.   \n\nAmount due: {{ invoice.total | money(invoice.currency) }}\nDue Date: {{ invoice.due_at|bb_date }}\n\nYou can view and pay the invoice at: {{\'invoice\'|link}}/{{invoice.hash}}\n\nYou may review your invoice history at any time by logging in to your client area.\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nView and manage invoices: {{\'invoice\'|link}}  \n\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(11, 'mod_servicecustom_activated', 'servicecustom', 1, '[{{ guest.system_company.name }}] {{ order.title }} Activated', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour **{{ order.title }}** is now active.\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nManage order: {{ \'order/service/manage\'|link }}/{{ order.id }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(12, 'mod_servicecustom_canceled', 'servicecustom', 1, '[{{ guest.system_company.name }}] {{ order.title }} Canceled', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour *{{ order.title }}* that was activated on *{{ order.activated_at|bb_date }}* is now canceled\n{% if order.reason %} Reason:     \n\n**{{ order.reason }}** {% endif %}   \n\nIf you have any questions regarding this message please login to the members area and submit a support ticket.\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nSubmit support ticket: {{ \'support\'|link({\'ticket\' : 1}) }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(13, 'mod_servicecustom_renewed', 'servicecustom', 1, '[{{ guest.system_company.name }}] {{ order.title }} Renewed', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour **{{ order.title }}** has been renewed.\n\n{% if order.expires_at %}\n\nNext renewal date: {{ order.expires_at|bb_date }}\n\n{% endif %}\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nManage order: {{ \'order/service/manage\'|link }}/{{ order.id }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(14, 'mod_servicecustom_suspended', 'servicecustom', 1, '[{{ guest.system_company.name }}] {{ order.title }} Suspended', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour *{{ order.title }}* that was activated at *{{ order.activated_at|bb_date }}* is now suspended\n{% if order.reason %} Reason:     \n\n**{{ order.reason }}** {% endif %}   \n\nIf you have any questions regarding this message please login to the members area and submit a support ticket.\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nSubmit support ticket: {{ \'support\'|link({\'ticket\' : 1}) }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(15, 'mod_servicecustom_unsuspended', 'servicecustom', 1, '[{{ guest.system_company.name }}] {{ order.title }} Reactivated ', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour *{{ order.title }}* has been reactivated. \n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nManage order: {{ \'order/service/manage\'|link }}/{{ order.id }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(16, 'mod_servicedomain_activated', 'servicedomain', 1, '[{{ guest.system_company.name }}] {{ order.title }}', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nThis message is to confirm that your **{{ order.title }}** has been successful. \n\nPlease keep in mind that your domain name will not be visible  on the internet \ninstantly. This process is called propagation and can take up to 48 hours. \nYour website and  email will not function until the domain has propagated.\n\n\n**Domain details:**\n\n\nDomain: {{ service.domain }}     \nRegistration date: {{order.created_at|bb_date}}\nRegistration period: {{service.period}} Year(s)   \n{% if order.expires_at %}Expires on: {{ order.expires_at|bb_date }} {% endif %}\n{% if order.period %}\nBilling period:\n\n{{ order.total | money(order.currency) }}\n{{ order.period | period_title }}\n\n{% endif %}       \n\n\n**Domain nameservers**\n\nNameserver 1: {{ service.ns1 }}   \nNameserver 2: {{ service.ns2 }}   \n{% if  service.server.ns3 %}Nameserver 3: {{ service.ns3 }}   {% endif %}\n{% if  service.server.ns4 %}Nameserver 4: {{ service.ns4 }}   {% endif %}\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nManage order: {{ \'order/service/manage\'|link }}/{{ order.id }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(17, 'mod_servicedomain_renewed', 'servicedomain', 1, '[{{ guest.system_company.name }}] {{ order.title }} Renewed', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour **{{ order.title }}** has been renewed.\n\n{% if order.expires_at %}\n\nNext renewal date: {{ order.expires_at|bb_date }}\n\n{% endif %}\n\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nManage order: {{ \'order/service/manage\'|link }}/{{ order.id }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(18, 'mod_servicedomain_suspended', 'servicedomain', 1, '[{{ guest.system_company.name }}] {{ order.title }} Suspended', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour *{{ order.title }}* that was activated on *{{ order.activated_at|bb_date }}* is now suspended.\n{% if order.reason %} Reason:     \n\n**{{ order.reason }}** {% endif %}   \n\nIf you have any questions regarding this message please login to the members area and submit a support ticket.\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nSubmit support ticket: {{ \'support\'|link({\'ticket\' : 1}) }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(19, 'mod_servicedomain_unsuspended', 'servicedomain', 1, '[{{ guest.system_company.name }}] {{ order.title }} Reactivated', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour *{{ order.title }}* has been reactivated. \n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nManage order: {{ \'order/service/manage\'|link }}/{{ order.id }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(20, 'mod_servicedownloadable_activated', 'servicedownloadable', 1, '[{{ guest.system_company.name }}] {{ order.title }} Ready to Download', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour **{{ order.title }}** is now active and ready for download.\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nDownload URL: {{ \'servicedownloadable/get-file\'|link }}/{{ order.id }}     \n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(21, 'mod_servicehosting_activated', 'servicehosting', 1, '[{{ guest.system_company.name }}] {{ order.title }} Activated', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nThank you for ordering with us! Your hosting account has now been set up. This email contains all the information you will need in order to begin using your service.\n\nIf you have requested a domain name during the signup, please keep in mind that \nyour domain name will not be visible  on the internet instantly. \nThis process is called propagation and can take up to 48 hours. \nYour website and email will not function until your domain has propagated.\n\n**{{ order.title }}**\n\nActivated: {{ order.activated_at|bb_date }}\n{% if order.expires_at %}Expires: {{ order.expires_at|bb_date }} {% endif %}\n{% if order.period %}\nBilling period:\n\n{{ order.total | money(order.currency) }}\n{{ order.period | period_title }}\n\n{% endif %}       \n\n**New Account Information**\n\n\nHosting Package: {{ service.hosting_plan.name }}    \nDomain: {{ service.domain }}    \nIP Address: {{ service.server.ip }}\n\n\n**Control Panel Login Details**\n\nUsername: {{ service.username }}    \nPassword: {{ password }}     \nControl Panel URL: {{ service.server.cpanel_url }}\n\n\n**Server Information**\n\nServer Name: {{ service.server.name }}     \nServer IP: {{ service.server.ip }}\n\nIf you are using an existing domain with your new hosting account, you  will \nneed to update the domain settings to point it to the nameservers listed below.\n\nNameserver 1: {{ service.server.ns1 }}   \nNameserver 2: {{ service.server.ns2 }}   \n{% if  service.server.ns3 %}Nameserver 3: {{ service.server.ns3 }}   {% endif %}\n{% if  service.server.ns4 %}Nameserver 4: {{ service.server.ns4 }}   {% endif %}\n\n**Uploading Your Website**\n\n\nYou may use one of the addresses given below to manage your web site:\n\n\nTemporary FTP Hostname: {{ service.server.ip }}    \nFull FTP Hostname: {{ service.domain }}    \nFTP Username: {{ service.username }}    \nFTP Password: {{ password }}    \n\nYou must upload files to the **public_html** folder!\nThank you for choosing us.\n\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nManage order: {{ \'order/service/manage\'|link }}/{{ order.id }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(22, 'mod_servicehosting_canceled', 'servicehosting', 1, '[{{ guest.system_company.name }}] {{ order.title }} Canceled', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour *{{ order.title }}* that was issued on *{{ order.activated_at|bb_date }}* is now canceled.\n{% if order.reason %} Reason:     \n\n**{{ order.reason }}** {% endif %}   \n\nIf you have any questions regarding this message please login to the members area and submit a support ticket.\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nSubmit support ticket: {{ \'support\'|link({\'ticket\' : 1}) }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(23, 'mod_servicehosting_renewed', 'servicehosting', 1, '[{{ guest.system_company.name }}] {{ order.title }} Renewed', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour **{{ order.title }}** has been renewed.\n\n{% if order.expires_at %}\n\nNext renewal date: {{ order.expires_at|bb_date }}\n\n{% endif %}\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nManage order: {{ \'order/service/manage\'|link }}/{{ order.id }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(24, 'mod_servicehosting_suspended', 'servicehosting', 1, '[{{ guest.system_company.name }}] {{ order.title }} Suspended', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour *{{ order.title }}* that was issued on *{{ order.activated_at|bb_date }}* is now suspended.\n{% if order.reason %} Reason:     \n\n**{{ order.reason }}** {% endif %}   \n\nIf you have any questions regarding this message please login to the members area and submit a support ticket.\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nSubmit support ticket: {{ \'support\'|link({\'ticket\' : 1}) }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(25, 'mod_servicehosting_unsuspended', 'servicehosting', 1, '[{{ guest.system_company.name }}] {{ order.title }} Reactivated', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour *{{ order.title }}* has been reactivated.\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nManage order: {{ \'order/service/manage\'|link }}/{{ order.id }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(26, 'mod_servicelicense_activated', 'servicelicense', 1, '[{{ guest.system_company.name }}] {{ order.title }} Activated', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour **{{ order.title }}** is now active.\n\nLicense key: **{{ service.license_key }}**\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nManage order: {{ \'order/service/manage\'|link }}/{{ order.id }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(27, 'mod_servicelicense_canceled', 'servicelicense', 1, '[{{ guest.system_company.name }}] {{ order.title }} Canceled', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour *{{ order.title }}* that was issued on *{{ order.activated_at|bb_date }}* is now canceled.\n{% if order.reason %} due to reason:     \n\n**{{ order.reason }}** {% endif %}   \n\nIf you have any questions regarding this message please login to the members area and submit a support ticket.\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nSubmit support ticket: {{ \'support\'|link({\'ticket\' : 1}) }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(28, 'mod_servicelicense_renewed', 'servicelicense', 1, '[{{ guest.system_company.name }}] {{ order.title }} Renewed', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour **{{ order.title }}** has been renewed.\n\n{% if order.expires_at %}\n\nNext renewal date: {{ order.expires_at|bb_date }}\n\n{% endif %}\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nManage order: {{ \'order/service/manage\'|link }}/{{ order.id }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(29, 'mod_servicelicense_suspended', 'servicelicense', 1, '[{{ guest.system_company.name }}] {{ order.title }} Suspended', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour *{{ order.title }}* that was issued on *{{ order.activated_at|bb_date }}* is now suspended.\n{% if order.reason %} due to reason:     \n\n**{{ order.reason }}** {% endif %}   \n\nIf you have any questions regarding this message please login to the members area and submit a support ticket.\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nSubmit support ticket: {{ \'support\'|link({\'ticket\' : 1}) }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(30, 'mod_servicelicense_unsuspended', 'servicelicense', 1, '[{{ guest.system_company.name }}] {{ order.title }} Reactivated', '\n{% filter markdown %}\n\nHello {{ c.first_name }} {{ c.last_name }},\n\nYour *{{ order.title }}* has been reactivated. \n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nManage order: {{ \'order/service/manage\'|link }}/{{ order.id }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(31, 'mod_staff_client_order', 'staff', 1, '[{{ guest.system_company.name }}] New Order Placed', '\n{% filter markdown %}\nHi {{ staff.name }},\n\nClient **{{ order.client.first_name }} {{ order.client.last_name }}** placed a new order for **{{ order.title }}** on {{ order.created_at|bb_date }}\n\nManage order {{\'order/manage\'|alink}}/{{order.id}}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(32, 'mod_staff_client_signup', 'staff', 1, '[{{ guest.system_company.name }}] New Client Signed Up', '\n{% filter markdown %}\nHi {{ staff.name }},\n\n *{{ c.first_name }} {{ c.last_name }}* has just signed up with {{ guest.system_company.name }}\n\nManage client at {{\'client/manage\'|alink}}/{{c.id}}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(33, 'mod_staff_pticket_close', 'staff', 1, '[{{ guest.system_company.name }}] {{ticket.subject}} [closed]', '\n{% filter markdown %}\n\nHi {{ staff.name }},\n\nPublic ticket {{ \'support/public-ticket/\'|alink }}/{{ticket.id}} was closed by client.\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(34, 'mod_staff_pticket_open', 'staff', 1, '[{{ guest.system_company.name }}] {{ ticket.subject }}', '\n{% filter markdown %}\nHi {{ staff.name }},\n\nNew public ticket received. \n\nReply at {{\'support/public-ticket\'|alink }}/{{ ticket.id }}\n\nTrack conversation at:  {{\'support/contact-us/conversation\'|link }}/{{ ticket.hash }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(35, 'mod_staff_pticket_reply', 'staff', 1, '[{{ guest.system_company.name }}] {{ticket.subject}}', '\n{% filter markdown %}\n\nHi {{ staff.name }},\n\nNew reply posted on ticket {{ \'support/public-ticket/\'|alink }}/{{ticket.id}}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(36, 'mod_staff_ticket_close', 'staff', 1, '[{{ guest.system_company.name }}] {{ ticket.subject }}', '\n{% filter markdown %}\nHi {{ staff.name }},\n\n **{{ ticket.client.first_name }} {{ ticket.client.last_name }}** closed support ticket #{{ ticket.id }}\n\nReview the ticket at {{\'support/ticket\'|alink }}/{{ ticket.id }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(37, 'mod_staff_ticket_open', 'staff', 1, '[{{ guest.system_company.name }}] {{ ticket.subject }}', '\n{% filter markdown %}\nHi {{ staff.name }},\n\n **{{ ticket.client.first_name }} {{ ticket.client.last_name }}** opened a new support ticket #{{ ticket.id }}\n\n***\n\n{{ ticket.messages[0].content }}\n\n***\n\nTicket ID: #{{ticket.id}}   \nDepartment: {{ticket.helpdesk.name}}   \nStatus: {{ticket.status|title}}\n\nReply at {{\'support/ticket\'|alink }}/{{ ticket.id }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(38, 'mod_staff_ticket_reply', 'staff', 1, '[{{ guest.system_company.name }}] {{ ticket.subject }}', '\n{% filter markdown %}\nHi {{ staff.name }},\n\n **{{ ticket.client.first_name }} {{ ticket.client.last_name }}** replied to support ticket #{{ ticket.id }}\n\nTicket ID: #{{ticket.id}}   \nDepartment: {{ticket.helpdesk.name}}   \nStatus: {{ticket.status|title}}\n\nReply at {{\'support/ticket\'|alink }}/{{ ticket.id }}\n\n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(39, 'mod_support_helpdesk_ticket_open', 'support', 1, '[{{ guest.system_company.name }}] {{ticket.subject}}', '\n{% filter markdown %}\n\n{{ticket.messages[0].content}}\n\n***\n\nTicket ID: #{{ticket.id}}   \nDepartment: {{ticket.helpdesk.name}}   \nStatus: {{ticket.status|title}}   \n\nReply Ticket at: {{\'support/ticket\'|link}}/{{ ticket.id }}\n\n\nPlease do not reply to this email directly. Use the link provided above.\n      \n{{ guest.system_company.signature }}\n\n{% endfilter %}\n\n', NULL, NULL),
	(40, 'mod_support_pticket_open', 'support', 1, '[{{ guest.system_company.name }}] {{ ticket.subject }}', '\n{% filter markdown %}\nHi {{ ticket.author_name }},\n\nThank You for Your request. We will reply in 24 hours.\nYou can track and respond to this conversation at {{\'support/contact-us/conversation\'|link }}/{{ ticket.hash }}\n\nPlease do not reply to this email directly. Use the link provided above.\n      \n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(41, 'mod_support_pticket_staff_close', 'support', 1, '[{{ guest.system_company.name }}] {{ticket.subject}} [closed]', '\n{% filter markdown %}\n\nHi {{ ticket.author_name }},\n\nThis is a confirmation email to inform you that your ticket was closed.\n\nYou can track conversation at   \n\n{{\'support/contact-us/conversation\'|link }}/{{ ticket.hash }}\n      \nPlease do not reply to this email directly. Use the link provided above.\n      \n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(42, 'mod_support_pticket_staff_open', 'support', 1, '[{{ guest.system_company.name }}] {{ticket.subject}}', '\n{% filter markdown %}\n\nHi {{ ticket.author_name }},\n\n{{ticket.messages[0].content}}\n\n***\n\nYou can track and respond to this conversation at:\n\n{{\'support/contact-us/conversation\'|link }}/{{ ticket.hash }}\n      \nPlease do not reply to this email directly. Use the link provided above.\n      \n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(43, 'mod_support_pticket_staff_reply', 'support', 1, '[{{ guest.system_company.name }}] {{ticket.subject}}', '\n{% filter markdown %}\n\nHi {{ ticket.author_name }},\n\nNew reply was posted to request **{{ticket.subject}}**\n\nYou can track and respond to this conversation at:\n\n{{\'support/contact-us/conversation\'|link }}/{{ ticket.hash }}\n      \nPlease do not reply to this email directly. Use the link provided above.\n      \n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(44, 'mod_support_ticket_open', 'support', 1, '[{{ guest.system_company.name }}] {{ ticket.subject }}', '\n{% filter markdown %}\nHi {{ c.first_name }} {{ c.last_name }},\n\nThank you for contacting our support team. \nA support ticket has now been opened for your request. You will be notified when \na response is made by email. The details of your ticket are shown below.\n\nTicket ID: #{{ticket.id}}   \nDepartment: {{ticket.helpdesk.name}}   \nStatus: {{ticket.status|title}}\n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nReply Ticket at: {{\'support/ticket\'|link}}/{{ ticket.id }}\n\nPlease do not reply to this email directly.\n      \n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(45, 'mod_support_ticket_staff_close', 'support', 1, '[{{ guest.system_company.name }}] {{ticket.subject}}', '\n{% filter markdown %}\n\nHi {{ c.first_name }} {{ c.last_name }},\n\nThis a notification message to inform you that ticket **{{ticket.subject}}**\nis now closed.\n\n\nTicket ID: #{{ticket.id}}   \nDepartment: {{ticket.helpdesk.name}}   \nStatus: {{ticket.status|title}}   \n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nReply Ticket at: {{\'support/ticket\'|link}}/{{ ticket.id }}\n\nPlease do not reply to this email directly. Use the link provided above.\n      \n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL),
	(46, 'mod_support_ticket_staff_open', 'support', 1, '[{{ guest.system_company.name }}] {{ticket.subject}}', '\n{% filter markdown %}\n\nHi {{ c.first_name }} {{ c.last_name }},\n\n{{ticket.messages[0].content}}\n\n***\n\nTicket ID: #{{ticket.id}}   \nDepartment: {{ticket.helpdesk.name}}   \nStatus: {{ticket.status|title}}   \n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nReply Ticket at: {{\'support/ticket\'|link}}/{{ ticket.id }}\n\n\nPlease do not reply to this email directly. Use the link provided above.\n      \n{{ guest.system_company.signature }}\n\n{% endfilter %}\n\n', NULL, NULL),
	(47, 'mod_support_ticket_staff_reply', 'support', 1, '[{{ guest.system_company.name }}] {{ticket.subject}}', '\n{% filter markdown %}\n\nHi {{ c.first_name }} {{ c.last_name }},\n\nNew reply was posted to request **{{ticket.subject}}**\n\nTicket ID: #{{ticket.id}}   \nDepartment: {{ticket.helpdesk.name}}   \nStatus: {{ticket.status|title}}  \n\nLogin to members area: {{\'login\'|link({\'email\' : c.email }) }}\nReply Ticket at: {{\'support/ticket\'|link}}/{{ ticket.id }}\n\n\nPlease do not reply to this email directly. Use the link provided above.\n      \n{{ guest.system_company.signature }}\n\n{% endfilter %}\n', NULL, NULL);
/*!40000 ALTER TABLE `email_template` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.extension
CREATE TABLE IF NOT EXISTS `extension` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `version` varchar(100) DEFAULT NULL,
  `manifest` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.extension: 6 rows
/*!40000 ALTER TABLE `extension` DISABLE KEYS */;
INSERT INTO `extension` (`id`, `type`, `name`, `status`, `version`, `manifest`) VALUES
	(2, 'mod', 'kb', 'installed', '1.0.0', '{"id":"kb","type":"mod","name":"Knowledge base","description":"Knowledge base module for BoxBilling","homepage_url":"http:\\/\\/github.com\\/boxbilling\\/","author":"BoxBilling","author_url":"http:\\/\\/extensions.boxbilling.com\\/","license":"http:\\/\\/www.boxbilling.com\\/license.txt","version":"1.0.0","icon_url":null,"download_url":null,"project_url":"https:\\/\\/extensions.boxbilling.com\\/","minimum_boxbilling_version":null,"maximum_boxbilling_version":null}\n'),
	(4, 'mod', 'branding', 'installed', '1.0.0', '{"id":"branding","type":"mod","name":"Branding","description":"BoxBilling branding module. Can be deactivated by PRO license owners only.","homepage_url":"http:\\/\\/github.com\\/boxbilling\\/","author":"BoxBilling","author_url":"http:\\/\\/extensions.boxbilling.com\\/","license":"http:\\/\\/www.boxbilling.com\\/license.txt","version":"1.0.0","icon_url":null,"download_url":null,"project_url":"https:\\/\\/extensions.boxbilling.com\\/","minimum_boxbilling_version":null,"maximum_boxbilling_version":null}\n'),
	(5, 'mod', 'redirect', 'installed', '1.0.0', '{"id":"redirect","type":"mod","name":"Redirect","description":"Manage redirects","homepage_url":"https:\\/\\/github.com\\/boxbilling\\/","author":"BoxBilling","author_url":"http:\\/\\/www.boxbilling.com","license":"GPL version 2 or later - http:\\/\\/www.gnu.org\\/licenses\\/old-licenses\\/gpl-2.0.html","version":"1.0.0","icon_url":null,"download_url":null,"project_url":"https:\\/\\/extensions.boxbilling.com\\/","minimum_boxbilling_version":null,"maximum_boxbilling_version":null}'),
	(15, 'mod', 'serviceopenvpn', 'installed', '1.0.0', '{"id":"serviceopenvpn","type":"mod","name":"Open VPN","description":"Manage open vpn users with extension for BoxBilling","homepage_url":"http:\\/\\/github.com\\/boxbilling\\/","author":"R@MS","author_url":"","license":"","version":"1.0.0","icon_url":"\\/bb-modules\\/Serviceopenvpn\\/icon.png","download_url":null,"project_url":"https:\\/\\/extensions.boxbilling.com\\/","minimum_boxbilling_version":null,"maximum_boxbilling_version":null}'),
	(16, 'mod', 'servicesolusvm', 'installed', '1.0.0', '{"id":"servicesolusvm","type":"mod","name":"SolusVM product","description":"Sell virtual servers using SolusVM extension for BoxBilling","homepage_url":"http:\\/\\/github.com\\/boxbilling\\/","author":"BoxBilling","author_url":"http:\\/\\/extensions.boxbilling.com\\/","license":"http:\\/\\/www.boxbilling.com\\/license.txt","version":"1.0.0","icon_url":"\\/bb-modules\\/Servicesolusvm\\/icon.png","download_url":null,"project_url":"https:\\/\\/extensions.boxbilling.com\\/","minimum_boxbilling_version":null,"maximum_boxbilling_version":null}'),
	(8, 'mod', 'servicemembership', 'installed', '1.0.0', '{"id":"servicemembership","type":"mod","name":"Membership product","description":"Sell membership subscriptions to clients","homepage_url":"http:\\/\\/github.com\\/boxbilling\\/","author":"BoxBilling","author_url":"http:\\/\\/extensions.boxbilling.com\\/","license":"http:\\/\\/www.boxbilling.com\\/license.txt","version":"1.0.0","icon_url":null,"download_url":null,"project_url":"https:\\/\\/extensions.boxbilling.com\\/","minimum_boxbilling_version":null,"maximum_boxbilling_version":null}');
/*!40000 ALTER TABLE `extension` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.extension_meta
CREATE TABLE IF NOT EXISTS `extension_meta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) DEFAULT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `rel_type` varchar(255) DEFAULT NULL,
  `rel_id` varchar(255) DEFAULT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id_idx` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.extension_meta: 21 rows
/*!40000 ALTER TABLE `extension_meta` DISABLE KEYS */;
INSERT INTO `extension_meta` (`id`, `client_id`, `extension`, `rel_type`, `rel_id`, `meta_key`, `meta_value`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'mod_theme', 'preset', 'current', 'huraga', 'Test', '2016-08-05 19:55:39', '2016-08-05 19:55:39'),
	(2, NULL, 'mod_system', NULL, NULL, 'config', NULL, '2016-08-05 16:56:43', '2016-08-05 16:56:43'),
	(3, NULL, 'mod_theme', 'settings', 'huraga', 'default', '{"color_scheme":"blue","show_company_logo":"0","show_company_name":"1","show_client_details":"1","sidebar_balance_enabled":"1","top_menu_dashboard":"1","top_menu_order":"1","top_menu_signout":"1","login_page_show_logo":"1","show_password_reset_link":"1","side_menu_dashboard":"1","side_menu_order":"1","side_menu_support":"1","side_menu_services":"1","side_menu_invoices":"1","side_menu_emails":"1","side_menu_payments":"1","sidebar_note_enabled":"0","sidebar_note_title":"","sidebar_note_content":"","meta_title":"","meta_description":"Members area","meta_keywords":"members area","meta_robots":"index, follow","meta_author":"BoxBilling"}', '2016-08-05 16:57:08', '2016-08-05 16:57:08'),
	(4, NULL, 'mod_theme', 'settings', 'huraga', 'juoda', '{"color_scheme":"dark","show_company_logo":"0","show_company_name":"1","show_client_details":"0","sidebar_balance_enabled":"0","top_menu_dashboard":"1","top_menu_order":"1","top_menu_signout":"1","login_page_show_logo":"1","login_page_logo_url":"\\/","show_password_reset_link":"1","show_signup_link":"1","login_page_show_remember_me":"1","side_menu_dashboard":"1","side_menu_order":"1","side_menu_support":"1","side_menu_services":"1","side_menu_invoices":"1","side_menu_emails":"1","side_menu_payments":"1","sidebar_note_enabled":"0","sidebar_note_title":"","sidebar_note_content":"","meta_title":"","meta_description":"Members area","meta_keywords":"members area","meta_robots":"index, follow","meta_author":"BoxBilling","inject_javascript":""}', '2016-08-05 16:57:08', '2016-08-05 16:57:08'),
	(5, NULL, 'mod_theme', 'settings', 'huraga', 'Test', '{"color_scheme":"green","show_page_header":"1","show_company_logo":"1","show_company_name":"1","show_client_details":"1","sidebar_balance_enabled":"1","top_menu_dashboard":"1","top_menu_order":"1","top_menu_profile":"1","top_menu_signout":"1","login_page_show_logo":"1","login_page_logo_url":"\\/","show_password_reset_link":"1","show_signup_link":"1","login_page_show_remember_me":"1","show_breadcrumb":"1","hide_dashboard_breadcrumb":"1","showcase_enabled":"0","showcase_heading":"Showcase heading","showcase_text":"Showcase text","showcase_button_title":"Showcase button title","showcase_button_url":"Showcase link URL","side_menu_dashboard":"1","side_menu_order":"1","side_menu_support":"1","side_menu_services":"1","side_menu_invoices":"1","side_menu_emails":"1","side_menu_payments":"1","sidebar_note_enabled":"0","sidebar_note_title":"Note title","sidebar_note_content":"Note content","meta_title":"","meta_description":"Members area","meta_keywords":"members area","meta_robots":"index, follow","meta_author":"BoxBilling","footer_enabled":"1","footer_to_top_enabled":"1","footer_signature":"","footer_link_1_enabled":"on","footer_link_1_title":"About us","footer_link_1_page":"about-us","footer_link_2_enabled":"on","footer_link_2_title":"Terms and conditions","footer_link_2_page":"tos","footer_link_3_enabled":"on","footer_link_3_title":"Privacy policy","footer_link_3_page":"privacy-policy","footer_link_4_title":"","footer_link_4_page":"","footer_link_5_title":"","footer_link_5_page":"","inject_javascript":""}', '2016-08-05 16:57:08', '2016-08-05 16:57:08'),
	(6, NULL, 'mod_client', NULL, NULL, 'config', 'IX9PgtTQHeENpc4tQHnPrUI9cVTbz1flJcocmYBTZXf5AsQoWoiziAC7gisOEyafjD8aL/DkXzjGx+jejwuX1AxgSy7NmBwzOD6jGn5fXjC5DtQ99vDODZsxkBTz67nfnRZLoyPtX2lPYCk7k+Dte1dk/1XHeks/sIECDBMIwbFyZIdENoB+pvSbDsltBVf9UNi07W3nSqA/bvIe/dXnAPS64wte4y5/MWMMwRKIrML/62h1cV1f+lL9i9uQTWWue0Jg72y9ugvdcpNm+H6Esx/VqlQIjFTl+PU16x22kQaSwUEt4IQd/yugU6ybDK85yEcPs8KVCxohASssegVlAmzo+TJP5KvnZ8tJRo1Do4sYIZ3X2gV1AL6+gAuQfptEzxfAdbvYw6/+qzaNmSaLEXJWa1dipajvQdgk/45MROOAscIaBIvJRj2Ry8f5wOBhAfxoUDQGqGf4GmrLnP5F9Q3Z4vKtUZMmvcz6Epqf9liuvCTXl2y1eXRv0Wu/2Z++so6WOckjYy6oi7HAztC6zD4/Cw666daCWzm80qfECuUM2wxkJkgG+6+Y4P8vhQV/8KHEzq+WMPfCuf8ZnUBwX7XF9D3qSJdiUpNWjfXNxo4silkl8nNxCeW3UTNEpJzr', '2016-08-05 16:58:04', '2016-08-05 16:58:04'),
	(7, NULL, 'mod_theme', 'preset', 'current', 'boxbilling', 'default', '2016-08-05 19:58:53', '2016-08-05 19:58:53'),
	(8, NULL, 'mod_theme', 'settings', 'boxbilling', 'default', '{"text_input":"foo","header_font":"\'Helvetica Neue\', Helvetica, Arial, sans-serif","show_visa":true,"table_color":"#F9F9F9","product_bg":"#1f2527","link_color":"#fe6f61","twitter_follow_hover":"#fe6f61","slide_3_title":"","slide_1_link":"\\/collections\\/all","slide_4_link":"\\/collections\\/all","bg_position":"0 0","slide_2_price":"$599","share_products":"enabled","footer_text_color":"#b0c1c7","marquee_color":"#b0c1c7","notes_text":"Tell us about any special instructions:","rotate_freq":"5000","slide_5_title":"","home_twitter":"enabled","slide_2_display":"enabled","side_blog_excerpt":"no_excerpt","slide_3_price":"$599","accent_hover":"#637479","twitter_follow":"enabled","container_bg_color":"#15191b","slideshow":"enabled","mailchimp_subscribe_hover":"#fe6f61","footer_links":"","bg_attachment":"scroll","slide_4_display":"disabled","logo":"disabled","slide_5_display":"disabled","nav_bg_color":"#15191b","show_discover":true,"blog_tags":"enabled","footer_blog_icon":"news","slide_2_content":"disabled","slide_3_link":"\\/collections\\/all","input_border":"#384347","marquee_text":"Restored vintage framesets, hand-finished on site.","marquee":"enabled","nav_top_color":"#333f43","featured_blog":"news","shop_bybrand":"enabled","slide_1_title":"Slide 1","nav_link_hover":"#FFF","about_footer_title":"About Us","twitter_bubble":"#333f43","slide_3_desc":"","side_blog":"enabled","marquee_title":"Hand-finished Framesets","share_articles":"enabled","mailing_list_text":"Subscribe to our mailing list to stay up-to-date with promotions, new products and our community.","enable_mailing_list":true,"slide_2_title":"Slide 2","product_brand":"enabled","nav_dropdown_bg":"#1f2527","footer_title_color":"#eeeeee","bg_color":"#212527","logo_font":"\'BebasNeueRegular\', Helvetica, Arial, sans-serif","slide_1_price":"$599","slide_1_content":"enabled","product_display_type":"fixed","marquee_price":"$599","input_background":"#b0c1c7","text_color":"#b0c1c7","slide_5_link":"\\/collections\\/all","slide_3_content":"enabled","marquee_content":"enabled","home_blog_excerpt":"body","slide_2_link":"\\/collections\\/all","blog_tag_text":"#FFFFFF","show_paypal":true,"slide_5_desc":"","about_footer":"Jitensha is a Shopify theme by the chaps over at Pixel Union. This is just a demo theme and all of the products are fake.","secondary_featured_products":"","bg_image":"img-dark-body_bg.gif","twitter_side":"enabled","logo_text_color":"#e7ecee","shop_bycat":"enabled","mailing_list_form_action":"","enable_creditcards":true,"slide_5_content":"enabled","fixed_height":"300","slide_2_desc":"Curabitur blandit tempus porttitor.","rectangle_font":"\'OstrichSansBlack\', Helvetica, Arial, sans-serif","nav_active_link_bg_color":"#333f43","base_color":"dark","featured_products_hidden":false,"footer_icon_hover":"#fe6f61","search_bg_color":"#b0c1c7","slide_5_price":"$599","facebook_url":"","display_recently_viewed":"enabled","secondary_text_color":"#637479","customer_layout":"theme","twitter_handle":"pixelunion","twitter_follow_color":"#333f43","featured_products":"frontpage","sidebar_title_color":"#eeeeee","marquee_url":"http:\\/\\/pixelunion.net","footer_link_color":"#fe6f61","slide_4_content":"enabled","slide_4_desc":"","footer_bg":"#1f2527","show_mastercard":true,"input_text":"#1f2527","sidebar_bg_color":"#1f2527","slide_1_desc":"Donec ullamcorper nulla non metus auctor fringilla.","mailing_footer_title":"Mailing List","products_per_page":"9","link_hover":"#fe9898","mailchimp_subscribe_color":"#637479","notes":"enabled","home_blog":"enabled","show_amex":true,"text_font":"\'Helvetica Neue\', Helvetica, Arial, sans-serif","text_accent_color":"#fe6f61","payment_footer_title":"Payment & Shipping","title_color":"#eeeeee","nav_link_hover_bg_color":"#fe6f61","sidebar_text_color":"#b0c1c7","footer_icon":"#333f43","secondary_featured_products_hidden":false,"table_alt_color":"#F4F4F4","search_border_color":"#b0c1c7","search_text_color":"#1f2527","intro":"frontpage","bg_repeat":"repeat","blog_tag_background":"#fe6f61","we_ship_to":"Shipping regions include Canada, US, Europe, Australia, Japan. Contact us to get a quote for other territories.","slide_1_display":"enabled","marquee_title_color":"#e7ecee","secondary_text_color_two":"#637479","slide_4_price":"$599","secondary_header_font":"\'BebasNeueRegular\', Helvetica, Arial, sans-serif","slide_3_display":"disabled","cart_row_border_color":"#333f43","nav_link_color":"#b0c1c7","show_pu_link":true,"twitter_footer":"enabled","text_over_accent":"#FFFFFF","nav_active_link_color":"#e7ecee","twitter_count":"3"}', '2016-08-05 16:58:53', '2016-08-05 16:58:53'),
	(9, NULL, 'mod_theme', 'settings', 'boxbilling', 'White', '{"blog_tag_background":"#3bb5ed","marquee_content":"enabled","mailing_list_form_action":"","nav_active_link_bg_color":"#dddddd","secondary_text_color_two":"#999999","accent_hover":"#333333","sidebar_text_color":"#777777","display_recently_viewed":"enabled","share_products":"enabled","shop_bybrand":"enabled","nav_bg_color":"#eeeeee","rectangle_font":"\'OstrichSansBlack\', Helvetica, Arial, sans-serif","footer_bg":"#f1f1f1","shop_bycat":"enabled","twitter_footer":"enabled","secondary_text_color":"#AAAAAA","nav_link_color":"#555555","twitter_count":"3","show_amex":true,"featured_products_hidden":false,"twitter_handle":"pixelunion","text_accent_color":"#44b3e3","side_blog_excerpt":"no_excerpt","we_ship_to":"Shipping regions include Canada, US, Europe, Australia, Japan. Contact us to get a quote for other territories.","intro":"frontpage","customer_layout":"theme","fixed_height":"300","marquee_product":"","enable_mailing_list":true,"home_blog":"enabled","nav_active_link_color":"#555555","link_color":"#44b3e3","secondary_header_font":"\'BebasNeueRegular\', Helvetica, Arial, sans-serif","footer_link_color":"#3bb5ed","notes":"enabled","facebook_url":"","show_pu_link":true,"logo_text_color":"#333333","blog_tags":"enabled","marquee_url":"http:\\/\\/pixelunion.net","notes_text":"Tell us about any special instructions:","show_paypal":true,"product_bg":"#F1F1F1","home_twitter":"enabled","shop_byprice":"enabled","nav_link_hover":"#FFF","blog_tag_text":"#FFFFFF","twitter_side":"enabled","logo":"disabled","sidebar_title_color":"#555555","nav_link_hover_bg_color":"#3bb5ed","share_articles":"enabled","footer_text_color":"#777777","text_color":"#555555","home_blog_excerpt":"body","footer_title_color":"#555555","show_visa":true,"link_hover":"#63cbfb","secondary_featured_products":"","show_mastercard":true,"twitter_follow":"enabled","product_display_type":"fixed","text_font":"\'Helvetica Neue\', Helvetica, Arial, sans-serif","side_blog":"enabled","products_per_page":"9","show_discover":true,"featured_products":"frontpage","cart_row_border_color":"#dddddd","secondary_featured_products_hidden":false,"enable_creditcards":true,"marquee":"enabled","title_color":"#333333","logo_font":"\'BebasNeueRegular\', Helvetica, Arial, sans-serif","text_over_accent":"#FFFFFF"}', '2016-08-05 16:58:53', '2016-08-05 16:58:53'),
	(10, NULL, 'mod_theme', 'settings', 'boxbilling', 'phpUnit', '[]', '2016-08-05 16:58:53', '2016-08-05 16:58:53'),
	(11, NULL, 'mod_support', NULL, NULL, 'config', NULL, '2016-08-05 16:59:11', '2016-08-05 16:59:11'),
	(12, NULL, 'mod_email', NULL, NULL, 'config', NULL, '2016-08-05 18:45:37', '2016-08-05 18:45:37'),
	(13, NULL, 'mod_order', NULL, NULL, 'config', 'M7TI6MGMMIu9WuhVRuOXSZssExPn4CIx2ERjfaemmj5cFjlakhCN/lruuCQ4gzyawCAEmFsCZ5vMZN3cTipLUM0Fuu22mastzh317luWduFvAr1A5O/Cco8c2x0mRhSS0HsIB8uufJ+6prNpigI5xymzZnCYhkTHvnzTk3M5BtrMC4WDXl8O1HzB3iZyRHEreDB9zOYYfFkgP6aaKxAZf0dH2as8cU2APqK0Y3xSnvAlYN2/q3tmpuC0ero4/+DGgLyu22n3YK313cXGd5Fgvzc8tgtuTIeeU+81KpC6AVpQTBsL7WyHdhLaTQDAj7A7BFEt0EwD7sc/9CFDnFaQ38qysu44w248u4pF2kXTarderURFDrFDwsuw4dwYCa6u', '2016-08-05 18:46:16', '2016-08-05 18:46:16'),
	(14, NULL, 'mod_staff', NULL, NULL, 'config', NULL, '2016-08-05 18:46:45', '2016-08-05 18:46:45'),
	(15, NULL, 'mod_orderbutton', NULL, NULL, 'config', 'UWgvx/Zk+VFJQaDX7doQ658mwZlhNI5owGcakCc4rF7SHQSjElsl8zxQC5FWoeciGO3FFzbDJ9aqFhXzas7L76cnvJQFPj9Lw8Hsfk3xeVAx46YiiUeSXOXTuPXRLMsngAyYIud+5CA4VpA006M6RPZ/zMTihP0yN7XXYtksJ1BiwoI6K03xaI2A7ld4wWUM3jlbdvizahj8jW+4P994jNLz01FlvmUQYGb4f/XaVDcZmchsXoE79sqLw1NawHAg', '2016-08-05 18:48:25', '2016-08-05 18:48:25'),
	(16, NULL, 'mod_forum', NULL, NULL, 'config', NULL, '2016-08-05 18:50:01', '2016-08-05 18:50:01'),
	(17, NULL, 'mod_example', NULL, NULL, 'config', NULL, '2016-08-05 18:51:55', '2016-08-05 18:51:55'),
	(18, NULL, 'mod_servicesolusvm', 'cluster', '1', 'config', '[]', '2016-08-05 18:53:33', '2016-08-05 18:53:33'),
	(19, NULL, 'mod_serviceopenvpn', 'cluster', '1', 'config', '{"id":"1","key":"2","ipaddress":"3","secure":"1","port":"5","usertype":"admin"}', '2016-08-08 07:15:08', '2016-08-08 07:15:08'),
	(23, 1, 'mod_serviceopenvpn', NULL, NULL, 'openvpn_username', 'sramsiks@gmail.com', '2016-08-08 16:31:07', '2016-08-08 16:31:07'),
	(24, 1, 'mod_serviceopenvpn', NULL, NULL, 'openvpn_password', '(6%$&7u?9&?)U2', '2016-08-08 16:31:07', '2016-08-08 16:31:07');
/*!40000 ALTER TABLE `extension_meta` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.form
CREATE TABLE IF NOT EXISTS `form` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `style` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.form: 1 rows
/*!40000 ALTER TABLE `form` DISABLE KEYS */;
INSERT INTO `form` (`id`, `name`, `style`, `created_at`, `updated_at`) VALUES
	(1, 'My new form', '{"type":"horizontal","show_title":"0"}', '2016-08-05 18:39:08', '2016-08-05 18:39:08');
/*!40000 ALTER TABLE `form` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.form_field
CREATE TABLE IF NOT EXISTS `form_field` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `form_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `hide_label` tinyint(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `default_value` varchar(255) DEFAULT NULL,
  `required` tinyint(1) DEFAULT NULL,
  `hidden` tinyint(1) DEFAULT NULL,
  `readonly` tinyint(1) DEFAULT NULL,
  `is_unique` tinyint(1) DEFAULT NULL,
  `prefix` varchar(255) DEFAULT NULL,
  `suffix` varchar(255) DEFAULT NULL,
  `options` text,
  `show_initial` varchar(255) DEFAULT NULL,
  `show_middle` varchar(255) DEFAULT NULL,
  `show_prefix` varchar(255) DEFAULT NULL,
  `show_suffix` varchar(255) DEFAULT NULL,
  `text_size` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `form_id_idx` (`form_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.form_field: 1 rows
/*!40000 ALTER TABLE `form_field` DISABLE KEYS */;
INSERT INTO `form_field` (`id`, `form_id`, `name`, `label`, `hide_label`, `description`, `type`, `default_value`, `required`, `hidden`, `readonly`, `is_unique`, `prefix`, `suffix`, `options`, `show_initial`, `show_middle`, `show_prefix`, `show_suffix`, `text_size`, `created_at`, `updated_at`) VALUES
	(1, 1, 'new_text_1', 'Text input 1', NULL, '', 'text', '', 1, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, '2016-08-05 18:39:18', '2016-08-05 18:39:34');
/*!40000 ALTER TABLE `form_field` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.forum
CREATE TABLE IF NOT EXISTS `forum` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `title` text,
  `description` text,
  `slug` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.forum: 1 rows
/*!40000 ALTER TABLE `forum` DISABLE KEYS */;
INSERT INTO `forum` (`id`, `category`, `title`, `description`, `slug`, `status`, `priority`, `created_at`, `updated_at`) VALUES
	(1, 'General', 'Discussions Rules', 'Please read our forum rules before posting to our forums', 'forum-rules', 'active', 1, '2012-01-01 12:00:00', '2012-01-01 12:00:00');
/*!40000 ALTER TABLE `forum` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.forum_topic
CREATE TABLE IF NOT EXISTS `forum_topic` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `forum_id` bigint(20) DEFAULT NULL,
  `title` text,
  `slug` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `sticky` tinyint(1) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `forum_id_idx` (`forum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.forum_topic: 0 rows
/*!40000 ALTER TABLE `forum_topic` DISABLE KEYS */;
/*!40000 ALTER TABLE `forum_topic` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.forum_topic_message
CREATE TABLE IF NOT EXISTS `forum_topic_message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `forum_topic_id` bigint(20) DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  `admin_id` bigint(20) DEFAULT NULL,
  `message` text,
  `ip` varchar(45) DEFAULT NULL,
  `points` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `forum_topic_id_idx` (`forum_topic_id`),
  KEY `client_id_idx` (`client_id`),
  KEY `admin_id_idx` (`admin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.forum_topic_message: 0 rows
/*!40000 ALTER TABLE `forum_topic_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `forum_topic_message` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.invoice
CREATE TABLE IF NOT EXISTS `invoice` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) DEFAULT NULL,
  `serie` varchar(50) DEFAULT NULL,
  `nr` varchar(255) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL COMMENT 'To access via public link',
  `currency` varchar(25) DEFAULT NULL,
  `currency_rate` decimal(13,6) DEFAULT NULL,
  `credit` double(18,2) DEFAULT NULL,
  `base_income` double(18,2) DEFAULT NULL COMMENT 'Income in default currency',
  `base_refund` double(18,2) DEFAULT NULL COMMENT 'Refund in default currency',
  `refund` double(18,2) DEFAULT NULL,
  `notes` text,
  `text_1` text,
  `text_2` text,
  `status` varchar(50) DEFAULT 'unpaid' COMMENT 'paid, unpaid',
  `seller_company` varchar(255) DEFAULT NULL,
  `seller_company_vat` varchar(255) DEFAULT NULL,
  `seller_company_number` varchar(255) DEFAULT NULL,
  `seller_address` varchar(255) DEFAULT NULL,
  `seller_phone` varchar(255) DEFAULT NULL,
  `seller_email` varchar(255) DEFAULT NULL,
  `buyer_first_name` varchar(255) DEFAULT NULL,
  `buyer_last_name` varchar(255) DEFAULT NULL,
  `buyer_company` varchar(255) DEFAULT NULL,
  `buyer_company_vat` varchar(255) DEFAULT NULL,
  `buyer_company_number` varchar(255) DEFAULT NULL,
  `buyer_address` varchar(255) DEFAULT NULL,
  `buyer_city` varchar(255) DEFAULT NULL,
  `buyer_state` varchar(255) DEFAULT NULL,
  `buyer_country` varchar(255) DEFAULT NULL,
  `buyer_zip` varchar(255) DEFAULT NULL,
  `buyer_phone` varchar(255) DEFAULT NULL,
  `buyer_phone_cc` varchar(255) DEFAULT NULL,
  `buyer_email` varchar(255) DEFAULT NULL,
  `gateway_id` int(11) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT '0',
  `taxname` varchar(255) DEFAULT NULL,
  `taxrate` varchar(35) DEFAULT NULL,
  `due_at` datetime DEFAULT NULL,
  `reminded_at` datetime DEFAULT NULL,
  `paid_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hash` (`hash`),
  KEY `client_id_idx` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.invoice: 0 rows
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.invoice_item
CREATE TABLE IF NOT EXISTS `invoice_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice_id` bigint(20) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `rel_id` text,
  `task` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `period` varchar(10) DEFAULT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `price` double(18,2) DEFAULT NULL,
  `charged` tinyint(1) DEFAULT '0',
  `taxed` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_id_idx` (`invoice_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.invoice_item: 0 rows
/*!40000 ALTER TABLE `invoice_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_item` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.kb_article
CREATE TABLE IF NOT EXISTS `kb_article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kb_article_category_id` bigint(20) DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `slug` varchar(255) DEFAULT NULL,
  `status` varchar(30) DEFAULT 'active' COMMENT 'active, draft',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `kb_article_category_id_idx` (`kb_article_category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.kb_article: 3 rows
/*!40000 ALTER TABLE `kb_article` DISABLE KEYS */;
INSERT INTO `kb_article` (`id`, `kb_article_category_id`, `views`, `title`, `content`, `slug`, `status`, `created_at`, `updated_at`) VALUES
	(1, 2, 0, 'How to contact support', 'Registered clients can contact our support team:\n------------------------------------------------------------\n\n* Login to clients area\n* Select **Support** menu item\n* Click **Submit new ticket**\n* Fill the form and press *Submit*\n\nGuests can contact our support team:\n------------------------------------------------------------\n\n* Use our contact form.\n* Fill the form and click *Submit*\n', 'how-to-contact-support', 'active', '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(2, 2, 0, 'How to place new order', 'To place new order, follow these steps:\n------------------------------------------------------------\n\n* Select our services at *Order* page.\n* Depending on selected product, you might need to provide additional information to complete order request.\n* Click "Continue" and your product/service is now in shopping cart.\n* If you have promo code, you can apply it and get discount.\n* Click on "Checkout" button to proceed with checkout process\n    * If you are already logged in, uou will be automaticaly redirected to new invoice\n    * If you are registerd client, you can provide your login details\n    * If you have never purchased any service from us, fill up client sign up form, and continue checkout\n* Choose payment method to pay for invoice. List of all avalable payment methods will be listed below invoice details.\n* Choose payment method\n* You will be redirected to payment gateway page\n* After successfull payment, You will be redirected back to invoice page.\n* Depending on selected services your order will be reviewed and activated by our staff members.\n* After you receive confirmation email about order activation you are able to manage your services.\n', 'how-to-place-new-order', 'active', '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(3, 2, 0, 'Example article', 'Example article heading\n------------------------------------------------------------\n\nCursus, parturient porta dis sit? Habitasse non, sociis porttitor, sagittis dapibus scelerisque? Pid, porttitor integer, montes. Hac, in? Arcu nunc integer nascetur dis nisi. In, sed a amet? Adipiscing odio mauris mauris, porta, integer, adipiscing habitasse, elementum phasellus, turpis in? Quis magna placerat eu, cursus urna mattis egestas, a ac massa turpis mus et odio pid in, urna dapibus ridiculus in turpis cursus ac a urna magna purus etiam ac nisi porttitor! Auctor est? In adipiscing, hac platea augue vut, hac est cum sagittis! Montes nascetur pulvinar tristique porta platea? Magnis vel etiam nisi augue auctor sit pulvinar! Aliquet rhoncus, elit porta? Magnis pulvinar eu turpis purus sociis a augue? Sit, nascetur! Mattis nisi, penatibus ac ac natoque auctor turpis.\n\nExample article heading\n------------------------------------------------------------\n\nUt diam cursus, elit pulvinar, habitasse purus? Enim. Urna? Velit arcu, rhoncus sociis sed, et, ultrices nascetur lacus vut purus tempor a. Vel? Sagittis integer scelerisque, dapibus lectus mid, magnis, augue duis velit etiam tortor! Eros, a et phasellus est ultricies integer elementum in, tempor sed parturient. Dictumst rhoncus, ut sed sagittis facilisis? In, proin? Urna augue in sociis enim dignissim! Velit magna tincidunt ac. Nunc, vel auctor porta enim integer. Phasellus amet eu. Tristique lundium arcu! In? Massa penatibus arcu, rhoncus augue ut pid pulvinar, porttitor, porta, et! A sit odio, proin natoque ultrices cras cras magna porttitor! Ultrices sed magna in! Porttitor nunc, tincidunt nec, amet integer aenean. Tincidunt, placerat nec dolor parturient et ac pulvinar a.\n', 'example-article-slug', 'active', '2012-01-01 12:00:00', '2012-01-01 12:00:00');
/*!40000 ALTER TABLE `kb_article` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.kb_article_category
CREATE TABLE IF NOT EXISTS `kb_article_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `slug` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.kb_article_category: 2 rows
/*!40000 ALTER TABLE `kb_article_category` DISABLE KEYS */;
INSERT INTO `kb_article_category` (`id`, `title`, `description`, `slug`, `created_at`, `updated_at`) VALUES
	(1, 'Frequently asked questions', 'Section for common issues', 'faq', '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(2, 'How to\'s', 'Section dedicated for tutorials', 'how-to', '2012-01-01 12:00:00', '2012-01-01 12:00:00');
/*!40000 ALTER TABLE `kb_article_category` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.mod_email_queue
CREATE TABLE IF NOT EXISTS `mod_email_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipient` varchar(255) NOT NULL,
  `sender` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `to_name` varchar(255) DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `tries` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.mod_email_queue: 0 rows
/*!40000 ALTER TABLE `mod_email_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `mod_email_queue` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.mod_massmailer
CREATE TABLE IF NOT EXISTS `mod_massmailer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `from_email` varchar(255) DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `content` text,
  `filter` text,
  `status` varchar(255) DEFAULT NULL,
  `sent_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.mod_massmailer: 0 rows
/*!40000 ALTER TABLE `mod_massmailer` DISABLE KEYS */;
/*!40000 ALTER TABLE `mod_massmailer` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.pay_gateway
CREATE TABLE IF NOT EXISTS `pay_gateway` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `gateway` varchar(255) DEFAULT NULL,
  `accepted_currencies` text,
  `enabled` tinyint(1) DEFAULT '1',
  `allow_single` tinyint(1) DEFAULT '1',
  `allow_recurrent` tinyint(1) DEFAULT '1',
  `test_mode` tinyint(1) DEFAULT '0',
  `config` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.pay_gateway: 2 rows
/*!40000 ALTER TABLE `pay_gateway` DISABLE KEYS */;
INSERT INTO `pay_gateway` (`id`, `name`, `gateway`, `accepted_currencies`, `enabled`, `allow_single`, `allow_recurrent`, `test_mode`, `config`) VALUES
	(2, 'PayPal', 'PayPalEmail', '["USD","UAH"]', 1, 1, 1, 1, '{"email":"sramsiks-facilitator@gmail.com"}'),
	(4, 'TwoCheckout', 'TwoCheckout', '["USD","UAH"]', 1, 1, 1, 1, '{"vendor_nr":"sramsiks","secret":"save","single_page":"1"}');
/*!40000 ALTER TABLE `pay_gateway` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.post
CREATE TABLE IF NOT EXISTS `post` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `admin_id` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `slug` varchar(255) DEFAULT NULL,
  `status` varchar(30) DEFAULT 'draft' COMMENT 'active, draft',
  `image` varchar(255) DEFAULT NULL,
  `section` varchar(255) DEFAULT NULL,
  `publish_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `admin_id_idx` (`admin_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.post: 3 rows
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` (`id`, `admin_id`, `title`, `content`, `slug`, `status`, `image`, `section`, `publish_at`, `published_at`, `expires_at`, `created_at`, `updated_at`) VALUES
	(1, 1, 'BoxBilling is the most affordable Billing Application Ever!', 'Just in case you weren\'t already aware, BoxBilling is the most affordable client management application ever!\n\nTo learn more about it You can always visit [www.boxbilling.com](http://www.boxbilling.com/)\n', 'boxbilling-is-affordable-billing-system', 'active', NULL, NULL, NULL, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(2, 1, 'Check out great features of BoxBilling', '* Supports automated billing, invoicing, product provisioning\n* Automatically create accounts as soon as the payment is received, suspend when account becomes overdue, terminate when a specified amount of time passes.\n* Boxbilling is perfectly created to sell shared and reseller hosting accounts, software licenses and downloadable products.\n* Integrated helpdesk, knowledgebase, news and announcements system.\n', 'great-features-of-boxbilling', 'active', NULL, NULL, NULL, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(3, 1, 'BoxBilling is customizable', '* You can create your own simple or advanced hooks on BoxBilling events. For example, send notification via sms when new client signs up.\n* Create custom theme for your client interface\n', 'boxbilling-is-customizable', 'active', NULL, NULL, NULL, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00');
/*!40000 ALTER TABLE `post` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_category_id` bigint(20) DEFAULT NULL,
  `product_payment_id` bigint(20) DEFAULT NULL,
  `form_id` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `unit` varchar(50) DEFAULT 'product',
  `active` tinyint(1) DEFAULT '1',
  `status` varchar(50) DEFAULT 'enabled' COMMENT 'enabled, disabled',
  `hidden` tinyint(1) DEFAULT '0',
  `is_addon` tinyint(1) DEFAULT '0',
  `setup` varchar(50) DEFAULT 'after_payment',
  `addons` text,
  `icon_url` varchar(255) DEFAULT NULL,
  `allow_quantity_select` tinyint(1) DEFAULT '0',
  `stock_control` tinyint(1) DEFAULT '0',
  `quantity_in_stock` int(11) DEFAULT '0',
  `plugin` varchar(255) DEFAULT NULL,
  `plugin_config` text,
  `upgrades` text,
  `priority` bigint(20) DEFAULT NULL,
  `config` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `product_type_idx` (`type`),
  KEY `product_category_id_idx` (`product_category_id`),
  KEY `product_payment_id_idx` (`product_payment_id`),
  KEY `form_id_idx` (`form_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.product: 1 rows
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `product_category_id`, `product_payment_id`, `form_id`, `title`, `slug`, `description`, `unit`, `active`, `status`, `hidden`, `is_addon`, `setup`, `addons`, `icon_url`, `allow_quantity_select`, `stock_control`, `quantity_in_stock`, `plugin`, `plugin_config`, `upgrades`, `priority`, `config`, `created_at`, `updated_at`, `type`) VALUES
	(5, 2, 4, NULL, 'VPS-1', 'vps-1', '1 dollar test', 'product', 1, 'enabled', 0, 0, 'after_order', NULL, '', 0, 0, 0, NULL, NULL, NULL, 21, '{"vtype":"openvz","up_speed":"1","down_speed":"10","sessions":"2","trafic":"30","cluster_id":"1","slider_cpu":"4","slider_ram":"5","slider_hdd":"6","slider_bandwidth":"7","slider_desc":"descr","traficunit":"Mb"}', '2016-08-08 07:26:38', '2016-08-08 14:43:54', 'openvpn');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.product_category
CREATE TABLE IF NOT EXISTS `product_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `icon_url` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.product_category: 1 rows
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
INSERT INTO `product_category` (`id`, `title`, `description`, `icon_url`, `created_at`, `updated_at`) VALUES
	(2, 'VPS', 'Virtual private servers', '', '2016-08-08 07:23:59', '2016-08-08 07:23:59');
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.product_payment
CREATE TABLE IF NOT EXISTS `product_payment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) DEFAULT NULL COMMENT 'free, once, recurrent',
  `once_price` decimal(18,2) DEFAULT '0.00',
  `once_setup_price` decimal(18,2) DEFAULT '0.00',
  `w_price` decimal(18,2) DEFAULT '0.00',
  `m_price` decimal(18,2) DEFAULT '0.00',
  `q_price` decimal(18,2) DEFAULT '0.00',
  `b_price` decimal(18,2) DEFAULT '0.00',
  `a_price` decimal(18,2) DEFAULT '0.00',
  `bia_price` decimal(18,2) DEFAULT '0.00',
  `tria_price` decimal(18,2) DEFAULT '0.00',
  `w_setup_price` decimal(18,2) DEFAULT '0.00',
  `m_setup_price` decimal(18,2) DEFAULT '0.00',
  `q_setup_price` decimal(18,2) DEFAULT '0.00',
  `b_setup_price` decimal(18,2) DEFAULT '0.00',
  `a_setup_price` decimal(18,2) DEFAULT '0.00',
  `bia_setup_price` decimal(18,2) DEFAULT '0.00',
  `tria_setup_price` decimal(18,2) DEFAULT '0.00',
  `w_enabled` tinyint(1) DEFAULT '1',
  `m_enabled` tinyint(1) DEFAULT '1',
  `q_enabled` tinyint(1) DEFAULT '1',
  `b_enabled` tinyint(1) DEFAULT '1',
  `a_enabled` tinyint(1) DEFAULT '1',
  `bia_enabled` tinyint(1) DEFAULT '1',
  `tria_enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.product_payment: 5 rows
/*!40000 ALTER TABLE `product_payment` DISABLE KEYS */;
INSERT INTO `product_payment` (`id`, `type`, `once_price`, `once_setup_price`, `w_price`, `m_price`, `q_price`, `b_price`, `a_price`, `bia_price`, `tria_price`, `w_setup_price`, `m_setup_price`, `q_setup_price`, `b_setup_price`, `a_setup_price`, `bia_setup_price`, `tria_setup_price`, `w_enabled`, `m_enabled`, `q_enabled`, `b_enabled`, `a_enabled`, `bia_enabled`, `tria_enabled`) VALUES
	(1, 'once', 5.00, 4.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 1, 1, 1),
	(2, 'free', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 1, 1, 1),
	(3, 'free', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 1, 1, 1),
	(4, 'free', 1.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 1, 1, 1),
	(5, 'free', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 1, 1, 1);
/*!40000 ALTER TABLE `product_payment` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.promo
CREATE TABLE IF NOT EXISTS `promo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `description` text,
  `type` varchar(30) NOT NULL DEFAULT 'percentage' COMMENT 'absolute, percentage, trial',
  `value` decimal(18,2) DEFAULT NULL,
  `maxuses` int(11) DEFAULT '0',
  `used` int(11) DEFAULT '0',
  `freesetup` tinyint(1) DEFAULT '0',
  `once_per_client` tinyint(1) DEFAULT '0',
  `recurring` tinyint(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0',
  `products` text,
  `periods` text,
  `client_groups` text,
  `start_at` datetime DEFAULT NULL,
  `end_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `start_index_idx` (`start_at`),
  KEY `end_index_idx` (`end_at`),
  KEY `active_index_idx` (`active`),
  KEY `code_index_idx` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.promo: 0 rows
/*!40000 ALTER TABLE `promo` DISABLE KEYS */;
/*!40000 ALTER TABLE `promo` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.queue
CREATE TABLE IF NOT EXISTS `queue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `module` varchar(255) DEFAULT NULL,
  `timeout` bigint(20) DEFAULT NULL,
  `iteration` int(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.queue: 0 rows
/*!40000 ALTER TABLE `queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `queue` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.queue_message
CREATE TABLE IF NOT EXISTS `queue_message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `queue_id` bigint(20) DEFAULT NULL,
  `handle` char(32) DEFAULT NULL,
  `handler` varchar(255) DEFAULT NULL,
  `body` longblob,
  `hash` char(32) DEFAULT NULL,
  `timeout` double(18,2) DEFAULT NULL,
  `log` text,
  `execute_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `queue_id_idx` (`queue_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.queue_message: 0 rows
/*!40000 ALTER TABLE `queue_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `queue_message` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.service_boxbillinglicense
CREATE TABLE IF NOT EXISTS `service_boxbillinglicense` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) DEFAULT NULL,
  `oid` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oid` (`oid`),
  KEY `client_id_idx` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.service_boxbillinglicense: 0 rows
/*!40000 ALTER TABLE `service_boxbillinglicense` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_boxbillinglicense` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.service_custom
CREATE TABLE IF NOT EXISTS `service_custom` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) DEFAULT NULL,
  `plugin` varchar(255) DEFAULT NULL,
  `plugin_config` text,
  `f1` text,
  `f2` text,
  `f3` text,
  `f4` text,
  `f5` text,
  `f6` text,
  `f7` text,
  `f8` text,
  `f9` text,
  `f10` text,
  `config` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id_idx` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.service_custom: 0 rows
/*!40000 ALTER TABLE `service_custom` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_custom` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.service_domain
CREATE TABLE IF NOT EXISTS `service_domain` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) DEFAULT NULL,
  `tld_registrar_id` bigint(20) DEFAULT NULL,
  `sld` varchar(255) DEFAULT NULL,
  `tld` varchar(100) DEFAULT NULL,
  `ns1` varchar(255) DEFAULT NULL,
  `ns2` varchar(255) DEFAULT NULL,
  `ns3` varchar(255) DEFAULT NULL,
  `ns4` varchar(255) DEFAULT NULL,
  `period` int(11) DEFAULT NULL,
  `privacy` int(11) DEFAULT NULL,
  `locked` tinyint(1) DEFAULT '1',
  `transfer_code` varchar(255) DEFAULT NULL,
  `action` varchar(30) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `contact_company` varchar(255) DEFAULT NULL,
  `contact_first_name` varchar(255) DEFAULT NULL,
  `contact_last_name` varchar(255) DEFAULT NULL,
  `contact_address1` varchar(255) DEFAULT NULL,
  `contact_address2` varchar(255) DEFAULT NULL,
  `contact_city` varchar(255) DEFAULT NULL,
  `contact_state` varchar(255) DEFAULT NULL,
  `contact_postcode` varchar(255) DEFAULT NULL,
  `contact_country` varchar(255) DEFAULT NULL,
  `contact_phone_cc` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(255) DEFAULT NULL,
  `details` text,
  `synced_at` datetime DEFAULT NULL,
  `registered_at` datetime DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id_idx` (`client_id`),
  KEY `tld_registrar_id_idx` (`tld_registrar_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.service_domain: 0 rows
/*!40000 ALTER TABLE `service_domain` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_domain` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.service_downloadable
CREATE TABLE IF NOT EXISTS `service_downloadable` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `downloads` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id_idx` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.service_downloadable: 0 rows
/*!40000 ALTER TABLE `service_downloadable` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_downloadable` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.service_hosting
CREATE TABLE IF NOT EXISTS `service_hosting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) DEFAULT NULL,
  `service_hosting_server_id` bigint(20) DEFAULT NULL,
  `service_hosting_hp_id` bigint(20) DEFAULT NULL,
  `sld` varchar(255) DEFAULT NULL,
  `tld` varchar(255) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `reseller` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id_idx` (`client_id`),
  KEY `service_hosting_server_id_idx` (`service_hosting_server_id`),
  KEY `service_hosting_hp_id_idx` (`service_hosting_hp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.service_hosting: 0 rows
/*!40000 ALTER TABLE `service_hosting` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_hosting` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.service_hosting_hp
CREATE TABLE IF NOT EXISTS `service_hosting_hp` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `quota` varchar(50) DEFAULT NULL,
  `bandwidth` varchar(50) DEFAULT NULL,
  `max_ftp` varchar(50) DEFAULT NULL,
  `max_sql` varchar(50) DEFAULT NULL,
  `max_pop` varchar(50) DEFAULT NULL,
  `max_sub` varchar(50) DEFAULT NULL,
  `max_park` varchar(50) DEFAULT NULL,
  `max_addon` varchar(50) DEFAULT NULL,
  `config` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.service_hosting_hp: 1 rows
/*!40000 ALTER TABLE `service_hosting_hp` DISABLE KEYS */;
INSERT INTO `service_hosting_hp` (`id`, `name`, `quota`, `bandwidth`, `max_ftp`, `max_sql`, `max_pop`, `max_sub`, `max_park`, `max_addon`, `config`, `created_at`, `updated_at`) VALUES
	(1, '1024', '1024', '1024', '1', '1', '1', '1', '1', '1', NULL, '2016-08-08 09:45:37', '2016-08-08 09:45:37');
/*!40000 ALTER TABLE `service_hosting_hp` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.service_hosting_server
CREATE TABLE IF NOT EXISTS `service_hosting_server` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `hostname` varchar(100) DEFAULT NULL,
  `assigned_ips` text,
  `status_url` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `max_accounts` bigint(20) DEFAULT NULL,
  `ns1` varchar(100) DEFAULT NULL,
  `ns2` varchar(100) DEFAULT NULL,
  `ns3` varchar(100) DEFAULT NULL,
  `ns4` varchar(100) DEFAULT NULL,
  `manager` varchar(100) DEFAULT NULL,
  `username` text,
  `password` text,
  `accesshash` text,
  `port` varchar(20) DEFAULT NULL,
  `config` text,
  `secure` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.service_hosting_server: 0 rows
/*!40000 ALTER TABLE `service_hosting_server` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_hosting_server` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.service_license
CREATE TABLE IF NOT EXISTS `service_license` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) DEFAULT NULL,
  `license_key` varchar(255) DEFAULT NULL,
  `validate_ip` tinyint(1) DEFAULT '1',
  `validate_host` tinyint(1) DEFAULT '1',
  `validate_path` tinyint(1) DEFAULT '0',
  `validate_version` tinyint(1) DEFAULT '0',
  `ips` text,
  `hosts` text,
  `paths` text,
  `versions` text,
  `config` text,
  `plugin` varchar(255) DEFAULT NULL,
  `checked_at` datetime DEFAULT NULL,
  `pinged_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `license_key` (`license_key`),
  KEY `client_id_idx` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.service_license: 0 rows
/*!40000 ALTER TABLE `service_license` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_license` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.service_membership
CREATE TABLE IF NOT EXISTS `service_membership` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) DEFAULT NULL,
  `config` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id_idx` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.service_membership: 0 rows
/*!40000 ALTER TABLE `service_membership` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_membership` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.service_openvpn
CREATE TABLE IF NOT EXISTS `service_openvpn` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `username` varchar(255) NOT NULL DEFAULT '0',
  `password` varchar(255) NOT NULL DEFAULT '0',
  `upload` int(10) unsigned NOT NULL DEFAULT '0',
  `download` int(10) unsigned NOT NULL DEFAULT '0',
  `sessions` int(10) unsigned NOT NULL DEFAULT '0',
  `trafic` bigint(20) unsigned NOT NULL DEFAULT '0',
  `lock` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.service_openvpn: 1 rows
/*!40000 ALTER TABLE `service_openvpn` DISABLE KEYS */;
INSERT INTO `service_openvpn` (`id`, `client_id`, `username`, `password`, `upload`, `download`, `sessions`, `trafic`, `lock`, `created_at`, `updated_at`) VALUES
	(1, 1, 'sramsiks@gmail.com', 's3ed8l3m', 1, 10, 2, 30720, 0, '2016-08-08 15:10:04', '2016-08-08 15:10:17');
/*!40000 ALTER TABLE `service_openvpn` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.service_solusvm
CREATE TABLE IF NOT EXISTS `service_solusvm` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cluster_id` bigint(20) DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  `vserverid` varchar(255) DEFAULT NULL,
  `virtid` varchar(255) DEFAULT NULL,
  `nodeid` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `node` varchar(255) DEFAULT NULL,
  `nodegroup` varchar(255) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `rootpassword` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `plan` varchar(255) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL,
  `ips` varchar(255) DEFAULT NULL,
  `hvmt` varchar(255) DEFAULT NULL,
  `custommemory` varchar(255) DEFAULT NULL,
  `customdiskspace` varchar(255) DEFAULT NULL,
  `custombandwidth` varchar(255) DEFAULT NULL,
  `customcpu` varchar(255) DEFAULT NULL,
  `customextraip` varchar(255) DEFAULT NULL,
  `issuelicense` varchar(255) DEFAULT NULL,
  `mainipaddress` varchar(255) DEFAULT NULL,
  `extraipaddress` varchar(255) DEFAULT NULL,
  `consoleuser` varchar(255) DEFAULT NULL,
  `consolepassword` varchar(255) DEFAULT NULL,
  `config` text,
  `created_at` varchar(35) DEFAULT NULL,
  `updated_at` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id_idx` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.service_solusvm: 0 rows
/*!40000 ALTER TABLE `service_solusvm` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_solusvm` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.session
CREATE TABLE IF NOT EXISTS `session` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `modified_at` int(11) DEFAULT NULL,
  `content` text,
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.session: 14 rows
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` (`id`, `modified_at`, `content`) VALUES
	('me81o66217jim9spgqfsta6pq6', 1470748650, 'YWRtaW58YTo0OntzOjI6ImlkIjtzOjE6IjEiO3M6NToiZW1haWwiO3M6MTg6InNyYW1zaWtzQGdtYWlsLmNvbSI7czo0OiJuYW1lIjtzOjQ6InJhbXMiO3M6NDoicm9sZSI7czo1OiJhZG1pbiI7fWNsaWVudF9pZHxzOjE6IjEiOw=='),
	('cpjphff8ago2a66jg6b2qb29h4', 1470671450, ''),
	('6u9v3jmbc9gulo7dvc41t98vv2', 1470671336, ''),
	('armg8r2cfhsms3c1j5t6eaj7r5', 1470672122, ''),
	('rnmcf9nh7m48enpfmt6fn6c6r3', 1470672129, ''),
	('cefm9639oeq7ace2gnnid7vn43', 1470672131, ''),
	('aj5r64hhhgqspvf98d0qt0uju0', 1470672132, ''),
	('jp9pdcltuje7vu6ucjldljnl36', 1470672153, ''),
	('p0bhd453qhegqkefa1h56sj4p3', 1470672167, ''),
	('sr0ph5q6kou5d06dpdcclmh0g3', 1470672315, ''),
	('nudnngldck661npbaol0ve8kl6', 1470673114, ''),
	('dnll64fnhtunp8cn1qms1pu7c4', 1470679189, ''),
	('kkvvkqk214h2p0j2h1vn0i2de4', 1470680236, ''),
	('9ejhtj22rd4196rh0gq1thluc1', 1470748721, '');
/*!40000 ALTER TABLE `session` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.setting
CREATE TABLE IF NOT EXISTS `setting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `param` varchar(255) DEFAULT NULL,
  `value` text,
  `public` tinyint(1) DEFAULT '0',
  `category` varchar(255) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `param` (`param`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.setting: 30 rows
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` (`id`, `param`, `value`, `public`, `category`, `hash`, `created_at`, `updated_at`) VALUES
	(1, 'last_patch', '17', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(2, 'company_name', 'Company Name', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(3, 'company_email', 'company@email.com', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(4, 'company_signature', 'BoxBilling.com - Client Management, Invoice and Support Software', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(5, 'company_logo', 'https://sites.google.com/site/boxbilling/_/rsrc/1308483006796/home/logo_boxbilling.png', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(6, 'company_address_1', 'Demo address line 1', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(7, 'company_address_2', 'Demo address line 2', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(8, 'company_address_3', 'Demo address line 3', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(9, 'company_tel', '+123 456 12345', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(10, 'company_tos', 'Sit ridiculus nascetur porta purus tortor, augue natoque, pulvinar integer nisi mattis dignissim mus, elementum nascetur, augue etiam. Mus mus tortor? A mauris habitasse dictumst, scelerisque, dis nec pulvinar magnis velit, integer, nisi, aliquet, elit phasellus? Parturient odio purus tristique porttitor augue diam pulvinar magna ac lacus in. Augue tincidunt sociis ultrices parturient aliquet dapibus sit. Pulvinar mauris platea in amet penatibus augue ut non ridiculus, nunc lundium. Duis dapibus a mid proin pellentesque lundium vut mauris egestas dolor nec? Diam eu duis sociis. Dapibus porta! Proin, turpis nascetur et. Aenean tristique eu in elit dolor, montes sit nec, magna amet montes, hac diam ac, pellentesque duis sociis, est placerat? Montes ac, nunc aliquet ridiculus nisi? Dignissim. Et aliquet sed.\n\nAuctor mid, mauris placerat? Scelerisque amet a a facilisis porttitor aenean dolor, placerat dapibus, odio parturient scelerisque? In dis arcu nec mid ac in adipiscing ultricies, pulvinar purus dis. Nisi dis massa magnis, porta amet, scelerisque turpis etiam scelerisque porttitor ac dictumst, cras, enim? Placerat enim pulvinar turpis a cum! Aliquam? Urna ut facilisis diam diam lorem mattis ut, ac pid, sed pellentesque. Egestas nunc, lacus, tempor amet? Lacus, nunc dictumst, ac porttitor magna, nisi, montes scelerisque? Cum, rhoncus. Pid adipiscing porta dictumst porta amet dignissim purus, aliquet dolor non sagittis porta urna? Tortor egestas, ultricies elementum, placerat velit magnis lacus? Augue nunc? Ac et cras ut? Ac odio tortor lectus. Mattis adipiscing urna, scelerisque nec aenean adipiscing mid.\n', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(11, 'company_privacy_policy', 'Ac dapibus. Rhoncus integer sit aliquam a! Natoque? Lacus porttitor rhoncus, aliquam porttitor in risus turpis adipiscing! Integer, mus mattis sed enim ac velit proin est et ut, amet eros! Hac augue et vel ac sit duis facilisis purus tincidunt, porttitor eu a penatibus rhoncus platea et mauris rhoncus magnis rhoncus, montes? Et porttitor, urna, dolor, dapibus elementum porttitor aliquam.\n\nCras risus? Turpis, mus tincidunt vel dolor lectus pulvinar aliquam nascetur parturient nunc proin aenean tortor, augue aenean ac penatibus vut arcu. Augue, aenean dapibus in nec. In tempor turpis dictumst cursus, nec eros, elit non, ut integer magna. Augue placerat magnis facilisis platea ridiculus tincidunt et ut porttitor! Cursus odio, aliquet purus tristique vel tempor urna, vut enim.\n\nPorta habitasse scelerisque elementum adipiscing elit pulvinar? Cursus! Turpis! Massa ac elementum a, facilisis eu, sed ac porta massa sociis nascetur rhoncus sed, scelerisque habitasse aliquam? Velit adipiscing turpis, risus ut duis non integer rhoncus, placerat eu adipiscing, hac? Integer cursus porttitor rhoncus turpis lundium nisi, velit? Arcu tincidunt turpis, nunc integer turpis! Ridiculus enim natoque in, eros odio.\n\nScelerisque tempor dolor magnis natoque cras nascetur lorem, augue habitasse ac ut mid rhoncus? Montes tristique arcu, nisi integer? Augue? Adipiscing tempor parturient elementum nunc? Amet mid aliquam penatibus. Aliquam proin, parturient vel parturient dictumst? A porttitor rhoncus, a sit egestas massa tincidunt! Nunc purus. Hac ac! Enim placerat augue cursus augue sociis cum cras, pulvinar placerat nec platea.\n\nPenatibus et duis, urna. Massa cum porttitor elit porta, natoque etiam et turpis placerat lacus etiam scelerisque nunc, egestas, urna non tincidunt cursus odio urna tempor dictumst dignissim habitasse. Mus non et, nisi purus, pulvinar natoque in vel nascetur. Porttitor phasellus sed aenean eu quis? Nec vel, dignissim magna placerat turpis, ridiculus cum est auctor, sagittis, sit scelerisque duis.\n', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(12, 'company_note', 'Amet auctor, sed massa lacus phasellus turpis urna mauris dictumst, dapibus turpis? Sociis amet, mid aliquam, sagittis, risus, eros porta mid placerat eros in? Elementum porta ac pulvinar porttitor adipiscing, tristique porta pid dolor elementum? Eros, pulvinar amet auctor, urna enim amet magnis ultrices etiam? Dictumst ultrices velit eu tortor aliquet, rhoncus! Magnis porttitor. Vel parturient, ac, nascetur magnis tincidunt.\n\nQuis, pid. Lacus lorem scelerisque tortor phasellus, duis adipiscing nec mid mus purus placerat nunc porttitor placerat, risus odio pulvinar penatibus tincidunt, proin. Est tincidunt aliquam vel, ut scelerisque. Enim lorem magna tempor, auctor elit? Magnis lorem ut cursus, nunc nascetur! Est et odio nunc odio adipiscing amet nunc, ridiculus magnis egestas proin, montes nunc tristique tortor, ridiculus magna.\n', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(13, 'invoice_series', 'BOX', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(14, 'invoice_due_days', '5', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(15, 'invoice_auto_approval', '1', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(16, 'invoice_issue_days_before_expire', '14', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(17, 'theme', 'huraga', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(18, 'issue_invoice_days_before_expire', '7', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(19, 'invoice_refund_logic', 'credit_note', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(20, 'invoice_cn_series', 'CN-', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(21, 'invoice_cn_starting_number', '1', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(22, 'nameserver_1', NULL, 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(23, 'nameserver_2', NULL, 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(24, 'nameserver_3', NULL, 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(25, 'nameserver_4', NULL, 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(26, 'funds_min_amount', '10', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(27, 'funds_max_amount', '200', 0, NULL, NULL, '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(28, 'd365dc7093f6fd75615a6ff0bc8d7163', 'G/95KJ/fCF0GnbbVGQadgSgtheUKY+Kwo9zNPDZpiEViH0XZqd0ogvMw7Yea8728w434+21BwxOiMxy1s1R0b9yo3VAUeOWiWe5L/174PqaKv/FxREi0Oc22s0+k/sZTpzQ2T457LBK2WTAy6rbaxiwyThJP4BmcJIao4mRzdKckwHHy6kSEHp7Pk1A3/X+wEppobFRzX+cvYg20Y8Eo5o6HBpzm7Ymu0kcIMqaOgUxBj0vHWeblP53Uvgj/JfC731XpDxoOKiPzQPKDFrcqxOknZj8UpjS9mW/Lb3Xqm1c=', 0, NULL, NULL, '2016-08-05 16:55:58', '2016-08-05 16:55:58'),
	(29, '9041a1bba6c04025a2440ba89a64e644', 'kPfnCmMtpLZ9KtWp37wL6JmC8clI270h4V2xyeto18XmqDaxDKKf3w0YYtRlHfVC1Utl6eWfbekfRXiHLa8T3w==', 0, NULL, NULL, '2016-08-05 16:55:58', '2016-08-05 16:55:58'),
	(30, 'invoice_starting_number', '5', 0, NULL, NULL, '2016-08-08 09:41:03', '2016-08-08 09:41:03');
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.subscription
CREATE TABLE IF NOT EXISTS `subscription` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) DEFAULT NULL,
  `pay_gateway_id` bigint(20) DEFAULT NULL,
  `sid` varchar(255) DEFAULT NULL,
  `rel_type` varchar(100) DEFAULT NULL,
  `rel_id` bigint(20) DEFAULT NULL,
  `period` varchar(255) DEFAULT NULL,
  `amount` double(18,2) DEFAULT NULL,
  `currency` varchar(50) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id_idx` (`client_id`),
  KEY `pay_gateway_id_idx` (`pay_gateway_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.subscription: 0 rows
/*!40000 ALTER TABLE `subscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscription` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.support_helpdesk
CREATE TABLE IF NOT EXISTS `support_helpdesk` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `close_after` smallint(6) DEFAULT '24',
  `can_reopen` tinyint(1) DEFAULT '0',
  `signature` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.support_helpdesk: 1 rows
/*!40000 ALTER TABLE `support_helpdesk` DISABLE KEYS */;
INSERT INTO `support_helpdesk` (`id`, `name`, `email`, `close_after`, `can_reopen`, `signature`, `created_at`, `updated_at`) VALUES
	(1, 'General', 'info@yourdomain.com', 24, 0, 'It is always a pleasure to help.\nHave a Nice Day !', '2012-01-01 12:00:00', '2012-01-01 12:00:00');
/*!40000 ALTER TABLE `support_helpdesk` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.support_pr
CREATE TABLE IF NOT EXISTS `support_pr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `support_pr_category_id` bigint(20) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `support_pr_category_id_idx` (`support_pr_category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.support_pr: 8 rows
/*!40000 ALTER TABLE `support_pr` DISABLE KEYS */;
INSERT INTO `support_pr` (`id`, `support_pr_category_id`, `title`, `content`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Hello #1', 'Hello,\n\n\n\nThank you for using our services.', '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(2, 1, 'Hello #2', 'Greetings,\n\n\n\nThank you.', '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(3, 2, 'It was fixed', '\nIt was fixed for your account. If you have any more questions or requests, please let us to know.', '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(4, 2, 'It was done as requested', '\nIt\'s done as you have requested. Please let us to know if you have any further requests or questions.', '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(5, 2, 'Your website works fine', '\nI have just checked your website and it works fine. Please check it from your end and let us to know if you still experience any problems.', '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(6, 3, 'Do you get any errors?', '\nDo you get any errors and maybe you can copy/paste full error messages?', '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(7, 3, 'Domain is not pointing to our server', '\nYour domain is not pointing to our server. Please set our nameservers for your domain and give 24 hours until changes will apply worldwide.', '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(8, 3, 'What is your domain and username?', '\nWhat is your domain name and username?', '2012-01-01 12:00:00', '2012-01-01 12:00:00');
/*!40000 ALTER TABLE `support_pr` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.support_pr_category
CREATE TABLE IF NOT EXISTS `support_pr_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.support_pr_category: 3 rows
/*!40000 ALTER TABLE `support_pr_category` DISABLE KEYS */;
INSERT INTO `support_pr_category` (`id`, `title`, `created_at`, `updated_at`) VALUES
	(1, 'Greetings', '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(2, 'General', '2012-01-01 12:00:00', '2012-01-01 12:00:00'),
	(3, 'Accounting', '2012-01-01 12:00:00', '2012-01-01 12:00:00');
/*!40000 ALTER TABLE `support_pr_category` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.support_p_ticket
CREATE TABLE IF NOT EXISTS `support_p_ticket` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hash` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `author_email` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `status` varchar(30) DEFAULT 'open',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.support_p_ticket: 0 rows
/*!40000 ALTER TABLE `support_p_ticket` DISABLE KEYS */;
/*!40000 ALTER TABLE `support_p_ticket` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.support_p_ticket_message
CREATE TABLE IF NOT EXISTS `support_p_ticket_message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `support_p_ticket_id` bigint(20) DEFAULT NULL,
  `admin_id` bigint(20) DEFAULT NULL COMMENT 'Filled when message author is admin',
  `content` text,
  `ip` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `support_p_ticket_id_idx` (`support_p_ticket_id`),
  KEY `admin_id_idx` (`admin_id`),
  FULLTEXT KEY `content_idx` (`content`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.support_p_ticket_message: 0 rows
/*!40000 ALTER TABLE `support_p_ticket_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `support_p_ticket_message` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.support_ticket
CREATE TABLE IF NOT EXISTS `support_ticket` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `support_helpdesk_id` bigint(20) DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  `priority` int(11) DEFAULT '100',
  `subject` varchar(255) DEFAULT NULL,
  `status` varchar(30) DEFAULT 'open' COMMENT 'open, closed, on_hold',
  `rel_type` varchar(100) DEFAULT NULL,
  `rel_id` bigint(20) DEFAULT NULL,
  `rel_task` varchar(100) DEFAULT NULL,
  `rel_new_value` text,
  `rel_status` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `support_helpdesk_id_idx` (`support_helpdesk_id`),
  KEY `client_id_idx` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.support_ticket: 0 rows
/*!40000 ALTER TABLE `support_ticket` DISABLE KEYS */;
/*!40000 ALTER TABLE `support_ticket` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.support_ticket_message
CREATE TABLE IF NOT EXISTS `support_ticket_message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `support_ticket_id` bigint(20) DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  `admin_id` bigint(20) DEFAULT NULL,
  `content` text,
  `attachment` varchar(255) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `support_ticket_id_idx` (`support_ticket_id`),
  KEY `client_id_idx` (`client_id`),
  KEY `admin_id_idx` (`admin_id`),
  FULLTEXT KEY `content_idx` (`content`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.support_ticket_message: 0 rows
/*!40000 ALTER TABLE `support_ticket_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `support_ticket_message` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.support_ticket_note
CREATE TABLE IF NOT EXISTS `support_ticket_note` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `support_ticket_id` bigint(20) DEFAULT NULL,
  `admin_id` bigint(20) DEFAULT NULL,
  `note` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `support_ticket_id_idx` (`support_ticket_id`),
  KEY `admin_id_idx` (`admin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.support_ticket_note: 0 rows
/*!40000 ALTER TABLE `support_ticket_note` DISABLE KEYS */;
/*!40000 ALTER TABLE `support_ticket_note` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.tax
CREATE TABLE IF NOT EXISTS `tax` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `level` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `taxrate` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.tax: 0 rows
/*!40000 ALTER TABLE `tax` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.tld
CREATE TABLE IF NOT EXISTS `tld` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tld_registrar_id` bigint(20) DEFAULT NULL,
  `tld` varchar(15) DEFAULT NULL,
  `price_registration` decimal(18,2) DEFAULT '0.00',
  `price_renew` decimal(18,2) DEFAULT '0.00',
  `price_transfer` decimal(18,2) DEFAULT '0.00',
  `allow_register` tinyint(1) DEFAULT NULL,
  `allow_transfer` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `min_years` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tld` (`tld`),
  KEY `tld_registrar_id_idx` (`tld_registrar_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.tld: 1 rows
/*!40000 ALTER TABLE `tld` DISABLE KEYS */;
INSERT INTO `tld` (`id`, `tld_registrar_id`, `tld`, `price_registration`, `price_renew`, `price_transfer`, `allow_register`, `allow_transfer`, `active`, `min_years`, `created_at`, `updated_at`) VALUES
	(1, 1, '.com', 11.99, 11.99, 11.99, 1, 1, 1, 1, '2012-01-01 12:00:00', '2012-01-01 12:00:00');
/*!40000 ALTER TABLE `tld` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.tld_registrar
CREATE TABLE IF NOT EXISTS `tld_registrar` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `registrar` varchar(255) DEFAULT NULL,
  `test_mode` tinyint(4) DEFAULT '0',
  `config` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.tld_registrar: 3 rows
/*!40000 ALTER TABLE `tld_registrar` DISABLE KEYS */;
INSERT INTO `tld_registrar` (`id`, `name`, `registrar`, `test_mode`, `config`) VALUES
	(1, 'Custom', 'Custom', 0, NULL),
	(2, 'Reseller Club', 'Resellerclub', 0, NULL),
	(3, 'Internet.bs', 'Internetbs', 0, NULL);
/*!40000 ALTER TABLE `tld_registrar` ENABLE KEYS */;


-- Дамп структуры для таблица boxbilling.transaction
CREATE TABLE IF NOT EXISTS `transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice_id` bigint(20) DEFAULT NULL,
  `gateway_id` int(11) DEFAULT NULL,
  `txn_id` varchar(255) DEFAULT NULL,
  `txn_status` varchar(255) DEFAULT NULL,
  `s_id` varchar(255) DEFAULT NULL,
  `s_period` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'received',
  `ip` varchar(45) DEFAULT NULL,
  `error` text,
  `error_code` int(11) DEFAULT NULL,
  `validate_ipn` tinyint(1) DEFAULT '1',
  `ipn` text,
  `output` text,
  `note` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_id_idx` (`invoice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы boxbilling.transaction: 0 rows
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
